restart()
skip()

def gaunt5():
    forward(4,3)
    turnRight(4,1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,3)
def gaunt6():
    forward(4,3)
    motors(1,0.5)
    wait(48)
    stop()
    turnLeft(4,1)
    forward(2,2)
def gaunt9():
    turnRight(1,1)
    forward(4,2.5)
    turnLeft(2,1)
    forward(4,2.2)
    turnRight(2,1)
    forward(4,2)
    use()
    forward(4,4)
def lower():
    forward(4,1)
    turnRight(4,1)
    forward(4,2)
    turnLeft(4,1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,2)
    use()

def upper():
    forward(4,3)
    turnRight(4,1)
    forward(4,4)
    turnLeft(4,1)
    forward(4,1)   
    
def right():
    forward(4,1) 
    turnRight(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,1)
    use()
    
def left():
    forward(4,1)
    turnLeft(4,1)
    forward(4,2)
    turnLeft(4,1)
    forward(4,1)
    use()
def chamber8():
    backward(4,1)
    #left,right=getIR()
    left,right=getLine()
    
    if left==0 and right==0:
        use(0)
    elif left==0 and right==1:
        use(1)
    elif left==1 and right==0:
        use(2)
    else:
        use(3)
def chamber2():
    skip()
    for i in range(6):
        forward(4,1)
        sample=pickup()
        forward(4,1)
        if sample==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        else:
            turnLeft(-4,1)
            forward(4,2)
            turnRight(-4,1)
            forward(4,1)
        wait(20)
        print("going again")
        
def chamber3():
    forward(4,1)
    sample=pickup()
    forward(4,1)
    if sample==0:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif sample==1:
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    
    else:
        turnLeft(-4,1)
        forward(4,2)
        turnRight(-4,1)
        forward(4,1)

def chamber4():
    forward(4,1)
    sample1=pickup()
    forward(4,1)
    sample2=pickup()
    
    if sample1==0 and sample2==0:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)

def chamber5():
    forward(4,1)
    sample1=pickup()
    forward(4,1)
    sample2=pickup()
    
    if sample1<sample2:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif sample1>sample2:
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    elif sample1==sample2:
        forward(4,1)    
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)
 
def chamber6():
    forward(4,1)
    sample1=pickup()
    forward(4,1)
    sample2=pickup()
    
    if sample1==0 and sample2==0:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif sample1==0 and sample2==1:
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    elif sample1==1 and sample2==0:
        forward(4,1)    
    elif sample1==1 and sample2==1:
        turnRight(4,1)
        forward(4,1)
        turnLeft(4,1)
        forward(4,1)        
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)       
#wait(1)  
 
#chamber4()                    

def chamber11a():
    backward(4,1)
    use()
def chamber11b():
    backward(4,1)
    left,right=getIR()
    print(left,right)
    if left==0 and right==0:
        forward(4,2)
    elif left==0 and right==1:
        forward(4,3)
    elif left==1 and right==0:
        forward(4,4)
    else:
        forward(4,5)
    use()
        
    