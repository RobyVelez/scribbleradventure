# README #

This is the README for Scribbler Adventure version 0.01.

### What is this repository for? ###

Scribbler Adventure is a learning environment for teaching programming skills to middle and high-school students.


### How do I get set up? ###

Download Calico, load portalWindow.py and click the run button.


### Contribution guidelines ###

We try to stick to the Python style guid [PEP 8](https://www.python.org/dev/peps/pep-0008) with the exception that we use mixed case instead of lower case with underscores for variable names.


### Who do I talk to? ###

Roby Velez