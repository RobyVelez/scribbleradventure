from os.path import realpath, dirname
import sys
import shutil   
import os
import Cairo
import Gdk
import Gtk
import threading
import pickle
import time as timeModule
import filecmp

# Set the path to the portal window directory
PORTAL_DIR = realpath(dirname(realpath(__file__)) + "/../..")
portal_valid = True
portal_valid = portal_valid and os.path.exists(os.path.join(PORTAL_DIR, "CommonFunctions"))
portal_valid = portal_valid and os.path.exists(os.path.join(PORTAL_DIR, "WindowObjects"))
if portal_valid:
    if PORTAL_DIR not in sys.path:
        sys.path.append(PORTAL_DIR)
    if PORTAL_DIR not in calico.persistentPaths:
        calico.persistentPaths.Add(PORTAL_DIR)
        
# Set the path to the platformer root
PORTAL_ROOT_DIR = realpath(dirname(realpath(__file__)) + "/..")
if PORTAL_ROOT_DIR not in sys.path:
    sys.path.append(PORTAL_ROOT_DIR)

# Import general portal window objects
from CommonFunctions.makeShapeFunctions import *
import CommonFunctions.utilityFunctions as util
from WindowObjects.windowObjects import *

# Import platformer objects
from core.platformerUtilFunctions import *
from core.platformerUserData import PlatformerUserData
from core.platformerLevelInfo import LevelInfo
from core.platformerLevelInfo import PLATFORMER_LEVELS

# Constants
PLATFORMER_SUB_DIR_NAME = "Platformer"
PLATFORMER_STUDENT_ROOT_DIR_NAME = "MyPlatformer"
PLATFORMER_SOURCE_ROOT_DIR_NAME = "PlatformerBase"
DEFAULT_STUDENT_DIR = os.path.join(PORTAL_DIR, PLATFORMER_SOURCE_ROOT_DIR_NAME)
SOURCE_ROOT_DIR = os.path.join(PORTAL_DIR, PLATFORMER_SOURCE_ROOT_DIR_NAME)
SOURCE_SUB_DIR = os.path.join(SOURCE_ROOT_DIR, PLATFORMER_SUB_DIR_NAME)

PLATFORMER_FILES = [
    "__init__.py",
    "platformerWindow.py",
]

PLATFORMER_DIRS = [
    "core",
    "images",
    "objects",
    "stages",
]
         

class PlatformerWindow(WindowBase):
    def __init__(self, parent=None, studentDirectory=None):
        WindowBase.__init__(self, "Platformer", parent=parent)
        self.mainWindow.mode = "manual"
        
        # Set persistent variables (experimental)
        self.persistentVars = calico.manager["python"].engine.persistentVariables
        
        # Set student directory
        if studentDirectory is not None:
            self.studentRootDirectory = studentDirectory
        elif "studentRootDir" in self.persistentVars:
            self.studentRootDirectory = self.persistentVars["studentRootDir"]
        else:
            self.studentRootDirectory = DEFAULT_STUDENT_DIR
        self.persistentVars["studentRootDir"] = self.studentRootDirectory
        self.studentSubDirectory = os.path.join(self.studentRootDirectory, PLATFORMER_SUB_DIR_NAME)
        self.persistentVars["studentDir"] = self.studentSubDirectory
        if self.studentRootDirectory not in calico.persistentPaths:
            calico.persistentPaths.Add(self.studentRootDirectory)
        
        # Copy all files if student directory does not exist yet
        if not os.path.exists(self.studentRootDirectory):
            self.copyFiles()
        
        # Initialize user data
        self.userData = PlatformerUserData()
        if not self.userData.good:
            print("ERROR: Unable to load used data.")  
            return
        self.persistentVars["userData"] = self.userData

        
        # Set current file
        self.currentFile = None
        
        # Set background
        self.setBackgroundImage(self.studentSubDirectory+"/images/background/full_background.png", 3000, 1500, -1500, -500, True)
        
        # Add semi-transparant box
        self.box = Rectangle((10,10), (self.preferedWidth-10, self.preferedHeight-10), fill=Color(255,255,255,200))
        self.drawDecor(self.box)
        
        # Add title
        title = createHeader("Platformer")
        title.xJustification = "left"
        title.moveTo(20,25+10)
        self.drawDecor(title)
        
        # Add description
        self.description = createHeader(" ")
        self.description.xJustification = "left"
        self.description.fontSize = 16
        self.description.moveTo(20, 680)
        self.drawDecor(self.description)

        # Set the horizontal layout
        self.mainLayout = ScrolledLayout(515,400)
        self.mainLayout.moveTo(145,195)
        self.mainLayout.pageWidth = 255
        self.mainLayout.nrOfFrames = 10
        self.mainLayout.maxPages = 2
        self.draw(self.mainLayout)
        buttonRow = HorizontalLayout()
        self.mainLayout.add(buttonRow)
        
        # Add buttons
        self.buttonList = list()
        currentGroup = 0
        maxlevel = self.userData.getMaxLevel()
        for i, levelStrct in enumerate(PLATFORMER_LEVELS):
            levelStrct.index = i
            levelStrct.nextLevel = i+1
            if currentGroup != levelStrct.group:
                currentGroup = levelStrct.group
                buttonColumn = VerticalLayout()        
                buttonRow.add(buttonColumn)
                header = createHeader("Stage " + str(currentGroup))
                buttonColumn.add(header)
            button = createRoundedButton(250, 40, levelStrct.name, self.launchActivity, levelStrct.description, self, 16)
            buttonColumn.add(button)
            button.levelInfo=levelStrct
            self.buttonList.append(button)
        
        self.backButton=createRoundedButton(75,40,"Back",self.back,"Go back in stages", self, 16)
        self.backButton.moveTo(75,350)
        self.draw(self.backButton)

        self.forwardButton=createRoundedButton(75,40,"Next",self.next,"Go forward in stages", self, 16)
        self.forwardButton.moveTo(725,350)
        self.draw(self.forwardButton)
            
        # Add back button
        if self.parent != None:
            self.backToPortalButton=createRoundedButton(200,40,"Back To Portal",self.returnToPortalAction,"Return to the portal window", self, 16)
            self.backToPortalButton.moveTo(125,625)
            self.draw(self.backToPortalButton)
            self.backToPortalButton.enable()
            
        # NOTE: buttons need to be added before this line, otherwise the overlay will capture all the button presses
        # Add loading overlay
        self.overlay = Rectangle((0,0), (self.preferedWidth, self.preferedHeight), color=Color(255,255,255,200))
        self.loadingText = makeText("Loading...", size=64, xjust="center")
        self.loadingText.draw(self.overlay)
        self.overlay.visible=False
        self.drawDecor(self.overlay)
        self.mainWindow.onMouseDown(self.onMouseDown)
        self.loadStatus="none"
        
        # Final line will draw everything to the screen
        self.update()
        self.setup()
        self.mainWindow.QueueDraw()


    def update(self):
        """
        Updates the interface, enabling and disabling buttons as appropriate.
        """
        maxlevel = self.userData.getMaxLevel()
        for i, button in enumerate(self.buttonList):
            if i >= maxlevel:
                button.disable()
            else:
                button.enable()
        if self.mainLayout.page <= 0:
            self.backButton.disable()
        else:
            self.backButton.enable()
        if maxlevel < 14 or self.mainLayout.page >= self.mainLayout.maxPages:
            self.forwardButton.disable()
        else:
            self.forwardButton.enable()
        self.mainWindow.QueueDraw()


    def launchActivity(self, button):
        """
        Launches the stage or level associated with the provided button name.
        """
        self.overlay.visible = True
        self.loadStatus="loading"
        try:
            self.mainWindow.QueueDraw()
            levelInfo = button.levelInfo
            self.persistentVars["showBriefing"] = True
            calico.config.SetValue('python-language','reset-shell-on-run',True)
            # TODO: Eventually set forced overwrite back to false
            self.userData.loadLevel(levelInfo, forceOverwrite=False)
        except:
            self.loadingText.text="Load failed"
            self.loadStatus="failed"
            raise


    def back(self, button):
        self.mainLayout.scrollBack()
        self.update()

            
    def next(self, button):
        self.mainLayout.scrollNext()
        self.update()

                
    def copyFiles(self, overwrite=False):
        """
        Copies all files from the platformer directory to the student directory,
        with the exception of the 'user' directory, which should be create locally.
        """
        tryCreateDir(self.studentRootDirectory, verbose=False, dryrun=False)
        tryCreateDir(self.studentSubDirectory, verbose=False, dryrun=False)
        for filename in PLATFORMER_FILES:
            source = os.path.join(SOURCE_SUB_DIR, filename)
            target = os.path.join(self.studentSubDirectory, filename)
            updateFile(source, target, overwrite, verbose=False, dryrun=False)
        for dirname in PLATFORMER_DIRS:
            source =  os.path.join(SOURCE_SUB_DIR, dirname)
            target = os.path.join(self.studentSubDirectory, dirname)
            tryCreateDir(target, verbose=False, dryrun=False)
            updateTree(source, target, overwrite, verbose=False, dryrun=False)


    def onMouseDown(self, obj, event):
        """
        Hides the 'Load failed' message when the screen is clicked.
        """
        if self.loadStatus=="failed":
            self.loadStatus="waitingOnUser"
        elif self.loadStatus=="waitingOnUser":
            self.loadStatus="none"
            self.overlay.visible=False
            self.loadingText.text="Loading..."
            self.mainWindow.QueueDraw()

    def returnToPortalAction(self, button):
        self.quit()
        
    def setDescription(self, text):
        #print("Description:", text)
        self.description.text=text
        self.description.visible = True
        
    def hideDescription(self):
        #print("Description hidden")
        self.description.visible = False
        

platformerWindow=None        
def start(gameData=None, parent=None):    
    global platformerWindow
    studentDirectory = None
    if parent:
        studentDirectory = parent.workspaceLoc + "/" + PLATFORMER_STUDENT_ROOT_DIR_NAME
    platformerWindow=PlatformerWindow(parent, studentDirectory)
    #platformerWindow=PlatformerWindow(parent, "/Users/joost/Desktop/MyPlatformer")
    return platformerWindow
        
if __name__ == "__main__":
    start()
       
