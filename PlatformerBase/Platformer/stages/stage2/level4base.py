# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addCrate = pf.addCrate

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(getStudentDir(), 'r') as f:
        addCrateCount = 0
        for line in f:
            if "addCrate" in line:
                addCrateCount += 1
            if addCrateCount > 1:
                game.addPopup("In this level, only one\naddCrate(x, y)\nstatement is allowed.\n\nPlease remove some and rerun.")
                return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 400, 400, 200)
    pf.addPlatform(0, 600, 1600, 100)
    pf.addPlatform(1000, 400, 600, 200)   
    pf.addGoal(1600, 275, 50, 50)
    pf.addPlayer(100, 350)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add crates to cross the gap!")
    b.bul("You are only allowed one ", Mono("addCrate(x, y)"), " statement.")
    b.bul("Add multiple crates with a for loop!")
    b.bul("Create a for loop with:\n")
    b.add(Mono("for  i  in  range(5):\n"))
    b.add(Mono("        addCrate(250, 100)"))
    b.bul("Here, ", Bold("5"), " determines the number of crates added.")
    b.bul("IMPORTANT: note that ", Mono("addCrate(x, y)"), " needs to be indented!")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
