from Platformer.stages.stage2.level2base import *
initLevel(__file__)
### Change variables here ###
x1=170
y1=200
x2=600
y2=100



### Do not change the code below ###
addPlatform(x1, y1)
addPlatform(x2, y2)

### Start the game! ###
startGame(globals())
