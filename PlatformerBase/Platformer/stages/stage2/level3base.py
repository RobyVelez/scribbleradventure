# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
textObjs = []
x=123

# Constants
X1 = 300
Y1 = 500
X2 = 600
Y2 = 400
X3 = 300
Y3 = 300
X4 = 0
Y4 = 200
W = 200
H = 40

# Make functions available to the player
pass

def getXValue():
    global x
    import random
    x=float(random.randint(1, 100))
    return x 

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 300, 100)    
    pf.addGoal(150, 50, 50, 50)
    pf.addPlayer(150, 550)
    t=pf.addText(X1 + W/2, Y1 + H/2,"a=14+x", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(X2 + W/2, Y2 + H/2,"b=x*9", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(X3 + W/2, Y3 + H/2,"c=23+x*1337", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(X4 + W/2, Y4 + H/2,"d=42-x/2", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    
# This is where the game object should be created and launched
def startGame(userVariables):
    if "a" in userVariables and userVariables["a"] == 14 + x:
            p = pf.addPlatform(X1, Y1, W, H)
            p.z = -1
            textObjs[0].shape.color = Color("gold")
    else:
        pf.addRectangle(X1, Y1, W, H, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "b" in userVariables and userVariables["b"] == x*9:
            p = pf.addPlatform(X2, Y2, W, H)
            p.z = -1
            textObjs[1].shape.color = Color("gold")
    else:
        pf.addRectangle(X2, Y2, W, H, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "c" in userVariables and userVariables["c"] == 23 + x*1337:
        p = pf.addPlatform(X3, Y3, W, H)
        p.z = -1
        textObjs[2].shape.color = Color("gold")
    else:
        pf.addRectangle(X3, Y3, W, H, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "d" in userVariables and userVariables["d"] == 42 - x/2:
        p = pf.addPlatform(X4, Y4, W, H)
        p.z = -1
        textObjs[3].shape.color = Color("gold")
    else:
        pf.addRectangle(X4, Y4, W, H, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)

    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Compute sums to make platforms appear!")
    b.bul("You can sum with: ",  Mono("name = value1 + value2"))
    b.bul("You can subtract with: ",  Mono("name = value1 - value2"))
    b.bul("You can multiply with: ",  Mono("name = value1 * value2"))
    b.bul("You can divide with: ",  Mono("name = value1 / value2"))
    b.bul("You can combine these operators in a single sum. For example: ",  Mono("name = 7 + x*52"))
    b.bul("Replace, ", Bold("name"), " with the letter inside the platform you want to create.")
    b.bul("Replace, ", Bold("value1"), " and ", Bold("value2"), " by either a number, or by the name of a variable.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)

