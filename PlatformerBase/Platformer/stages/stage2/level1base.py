# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
textObjs = []

# Make functions available to the player
pass

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)    
    pf.addGoal(400, 50, 50, 50)
    pf.addPlayer(450, 550)
    t=pf.addText(50,520,"a=1", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(170,420,"b=2", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(290,320,"c=3", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    t=pf.addText(410,220,"d=4", color=Color(100, 0, 0))
    t.shape.fontFace = "Helvetica Neue"
    textObjs.append(t)
    
# This is where the game object should be created and launched
def startGame(userVariables):
    if "a" in userVariables:
        p = pf.addPlatform(0, 500, 100, 40)
        p.z = -1
        textObjs[0].shape.color = Color("gold")
    else:
        pf.addRectangle(0, 500, 100, 40, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "b" in userVariables:
        p = pf.addPlatform(120, 400, 100, 40)
        p.z = -1
        textObjs[1].shape.color = Color("gold")
    else:
        pf.addRectangle(120, 400, 100, 40, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "c" in userVariables:
        p = pf.addPlatform(240, 300, 100, 40)
        p.z = -1
        textObjs[2].shape.color = Color("gold")
    else:
        pf.addRectangle(240, 300, 100, 40, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)
    if "d" in userVariables:
        p = pf.addPlatform(360, 200, 100, 40)
        p.z = -1
        textObjs[3].shape.color = Color("gold")
    else:
        pf.addRectangle(360, 200, 100, 40, fill=Color(200,200,200,200), outline=Color(100,100,100), back=True)

    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Define variables to make platforms appear.")
    b.bul("Define a variable with: ",  Mono("name = value"))
    b.bul("Replace, ", Bold("name"), " with the letter inside the platform you want to create.")
    b.bul("Replace, ", Bold("value"), " by a number.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
