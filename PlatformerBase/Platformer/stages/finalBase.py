# Boilerplate
from Platformer.core import *
from Platformer.objects import *
from Platformer.objects.platformerUserFunctions import *

# Global variables
level = None
game = None


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    global game
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    game = PlatformerGame()
    
# This is where the game object should be created and launched
def startGame():
    b = PlatformerBriefing()
    b.bul("Congratualtions, you have completed all levels in platformer!")
    b.bul("The current level is an example of the things you can do with the things you have learned so far.")
    b.bul("All remaining levels are set to sandbox mode, so you can use them to create any levels you want.")
    b.bul("Stay tuned for updates featuring more levels and more options!")
    b.bul("Have fun!")
    game.addBriefing(b.write())
    game.allowUnlock()
    game.run(level)

