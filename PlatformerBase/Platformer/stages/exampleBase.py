# Boilerplate
from Platformer.core import *
from Platformer.objects import *
from Platformer.objects.platformerUserFunctions import *

# Global variables
level = None
game = None


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    global game
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    game = PlatformerGame()
    
# This is where the game object should be created and launched
def startGame():
    b = PlatformerBriefing()
    b.bul("This is an example level.")
    b.bul("Feel free to complete it in its current form, or use it as a basis to create your own level.")
    game.addBriefing(b.write())
    game.allowUnlock()
    game.run(level)

