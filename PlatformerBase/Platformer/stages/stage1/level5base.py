# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addPlatform = pf.addPlatform

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    customPlatforms = 0
    for platform in game.objs:
        if not isinstance(platform, Platform): continue
        if platform.tag == "default": continue
        if platform.shape.width == 100 and platform.shape.height == 20:
            platform.shape.fill = Color(255,0,0)
            platform.shape.outline = Color(0,0,0)
        else:
            platform.shape.fill = Color(100,255,100)
            platform.shape.outline = Color(0,0,0)
            customPlatforms += 1
    if customPlatforms < 4:
        game.addPopup("Your level does not contain\n4 custom size platforms\n\nAdd 4 custom size platforms\nbefore finishing the level!\n\nAdjust your code,\nthen press run to continue.", h=300)
    return customPlatforms >= 4


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100).tag="default"
    pf.addGoal(400, 75, 50, 50)
    pf.addPlayer(350, 550)
    
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add 4 platforms of different sizes to your world!")
    b.bul("You can add platforms of varying sizes with the ",  Mono("addPlatform(x, y, width, height)"), " command")
    b.bul("As usual, replace ", Bold("width"), " and ", Bold("height"), " by numbers.")
    b.bul('As you may have guessed, ', Bold("width"), ' determines the width of the platform, while ')
    b.add(Bold("height"), " determines the height of the platform.")
    b.bul("Add at least 4 custom-size platforms to your level.")
    b.bul("Once you are done adding platforms, beat the level to unlock the next.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
