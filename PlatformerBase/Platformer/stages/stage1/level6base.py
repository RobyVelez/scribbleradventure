# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
setPlayer = pf.setPlayer

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)
    pf.addPlatform(200,500, 100, 100)
    pf.addPlatform(400,500, 100, 100)
    pf.addPlatform(200,400, 300, 100)
    pf.addGoal(400, 200, 50, 50)
    pf.addPlayer(350, 550)
    
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Set the position of the player to escape the cave.")
    b.bul("You can set the position of the player with the",  Mono("setPlayer(x, y)"), " command")
    b.bul("As usual, replace ", Bold("x"), " and ", Bold("y"), " by numbers.")
    b.bul('The first number "', Bold("x"), '" determines the horizontal')
    b.add(' position of the player, the second number "', Bold("y"))
    b.add('" determines the vertical position of the player.')
    b.bul("Move the player outside of the cave, so you can beat the level.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
