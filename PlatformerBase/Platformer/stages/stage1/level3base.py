# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addEnemy = pf.addEnemy

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)
    pf.addPlatform(500, 550, 20, 150)
    pf.addPlatform(0, 550, 20, 150)
    pf.addPlatform(0, 400, 300)
    pf.addPlatform(0, 350, 20, 70)
    pf.addPlatform(300, 350, 20, 70)
    pf.addPlatform(0, 200, 200)
    pf.addPlatform(0, 150, 20, 70)
    pf.addPlatform(200, 150, 20, 70)
    pf.addGoal(400, 75, 50, 50)
    pf.addPlayer(550, 550)
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Reach the goal by adding enemies to the world!")
    b.bul("Create enemies with the ",  Mono("addEnemy(x, y)"), " command")
    b.bul("Once again, when writing ", Mono("addEnemy(x, y)"))
    b.add(", replace ", Bold("x"), " and ", Bold("y")," with numbers.")
    b.bul('The first number "', Bold("x"), '" determines the horizontal')
    b.add(' position of the enemy, the second number "', Bold("y"))
    b.add('" determines the vertical position of the enemy.')
    b.bul("You can jump on enemies to gain extra height!")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
