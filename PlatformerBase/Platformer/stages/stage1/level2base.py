# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
zones = []

# Make functions available to the player
addHazard = pf.addHazard

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    marked = [False] * len(zones)
    for hazard in game.objs:
        if not isinstance(hazard, Hazard): continue
        for i, zone in enumerate(zones):
            if pf.hit(hazard, zone):
                marked[i] = True
                zone.shape.color = Color(100,255,100, 200)
            elif not marked[i]:
                zone.shape.color = Color(255,0,0, 255)
    if not all(marked):
        game.addPopup("Your level is too easy!\n\nPut a hazard in each marked zone\nbefore finishing the level!\n\nAdjust your code,\nthen press run to continue.", h=300)
    return all(marked)


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    zones.append(pf.addDecor(Rectangle((100, 100), (300, 300), color=Color(255,100,100, 200))))
    zones.append(pf.addDecor(Rectangle((350, 300), (550, 500), color=Color(255,100,100, 200))))
    zones.append(pf.addDecor(Rectangle((100, 400), (300, 600), color=Color(255,100,100, 200))))
    pf.addPlatform(0, 600, 700, 100)
    pf.addPlatform(100, 500)
    pf.addPlatform(350, 400)
    pf.addPlatform(100, 300)
    pf.addPlatform(350, 200)
    pf.addGoal(400, 75, 50, 50)
    pf.addPlayer(350, 550)
    
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add hazards to make the game more difficult!")
    b.bul("Create hazards with the ",  Mono("addHazard(x, y)"), " command")
    b.bul("Once again, when writing ", Mono("addHazard(x, y)"))
    b.add(", replace ", Bold("x"), " and ", Bold("y")," with numbers.")
    b.bul('The first number "', Bold("x"), '" determines the horizontal')
    b.add(' position of the hazard, the second number "', Bold("y"))
    b.add('" determines the vertical position of the hazard.')
    b.bul("Make sure there is at least one hazard in each marked area.")
    b.bul("Once you are done adding hazards, beat the level to unlock the next.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
