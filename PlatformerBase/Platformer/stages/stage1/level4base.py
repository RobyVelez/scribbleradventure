# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addGoal = pf.addGoal

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)
    pf.addPlatform(0, 400, 50, 300)
    pf.addPlatform(0, 200, 200)
    pf.addPlatform(300, 200, 200, 100)
    pf.addPlatform(600, 500, 200, 100)
    pf.addPlatform(700, 400, 200, 100)
    pf.addPlatform(800, 300, 200, 100)
    pf.addPlatform(600, 200, 200)
    pf.addPlayer(350, 550)
    
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add a goal to finish the level!")
    b.bul("Create a goal with the ",  Mono("addGoal(x, y)"), " command")
    b.bul("Once again, when writing ", Mono("addGoal(x, y)"))
    b.add(", replace ", Bold("x"), " and ", Bold("y")," with numbers.")
    b.bul('The first number "', Bold("x"), '" determines the horizontal')
    b.add(' position of the goal, the second number "', Bold("y"))
    b.add('" determines the vertical position of the goal.')
    b.bul("You can place the goal anywhere you want!")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
