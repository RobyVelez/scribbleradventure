# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addPlatform = pf.addPlatform

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True


# Create the level
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)
    pf.addGoal(400, 75, 50, 50)
    pf.addPlayer(350, 550)
    
    
# Start the game
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Create platforms to reach the goal (green square)")
    b.bul("Create platforms by typing ",  Mono("addPlatform(x, y)"))
    b.add(" commands in the level1 script (in the other window)")
    b.bul("When writing the ", Mono("addPlatform(x, y)"))
    b.add(" command, replace ", Bold("x"), " and ", Bold("y")," by numbers.")
    b.bul('The first number "', Bold("x"), '" determines the horizontal')
    b.add(' position of the platform, the second number "', Bold("y"))
    b.add('" determines the vertical position of the platform.')
    b.bul("Platforms will be placed in the world once you press the run button")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.allowUnlock()
    game.run(level)
