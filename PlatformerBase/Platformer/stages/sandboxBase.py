# Boilerplate
from Platformer.core import *
from Platformer.objects import *
from Platformer.objects.platformerUserFunctions import *

# Global variables
level = None
game = None


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    global game
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    game = PlatformerGame()
    
# This is where the game object should be created and launched
def startGame():
    b = PlatformerBriefing()
    b.bul("Welcome to sandbox mode!")
    b.bul("Create any level you want!")
    b.bul("All objects and commands are available to you!")
    b.bul("See previous levels for examples on how to use the different commands.")
    game.addBriefing(b.write())
    game.allowUnlock()
    game.run(level)

