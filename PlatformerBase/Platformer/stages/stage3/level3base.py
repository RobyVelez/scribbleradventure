# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None


# Make functions available to the player
addMovingPlatform = pf.addMovingPlatform


# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    count = 0
    for hazard in game.objs:
        if not isinstance(hazard, MovingPlatform): continue
        count += 1
    if count > 1:
        game.addPopup("You are only allowed one platform.\n\nPlease remove all but one platform\n and press run.", h=300)
        return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 300, 100)    
    pf.addGoal(600, 50, 50, 50)
    pf.addPlayer(150, 550)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Use variables to move a platform to the goal!")
    b.bul("Create a suitable platform, and assign it to a variable, with:\n", Mono("platform = addMovingPlatform(x, y)"))
    b.bul("Make the platform move with:\n", Mono("platform.addWaypoint(x, y)"))
    b.bul("You are only allowed to add one platform to the game.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)

