# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addHazard = pf.addHazard

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(getStudentDir(), 'r') as f:
        req1 = False
        req2 = False
        req3 = False
        illegalPlatform = False
        for line in f:
            if line.strip() == "blade1=addHazard(200,550)" and not req1:
                req1 = True
            elif line.strip() == "blade2=addHazard(400,550)" and not req2:
                req2 = True
            elif line.strip() == "blade3=addHazard(600,550)" and not req3:
                req3 = True
        if not req1:
            game.addPopup("The line\nblade1=addHazard(200,550)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
        elif not req2:
            game.addPopup("The line\nblade2=addHazard(400,550)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
        elif not req3:
            game.addPopup("The line\nblade3=addHazard(600,550)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    #wallColor = Color("slategrey")
    wallColor = Color(10,10,20)
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 800, 100, fill=wallColor, outline=wallColor) 
    pf.addPlatform(-800, -200, 800, 1000, fill=wallColor, outline=wallColor)
    pf.addPlatform(800, -200, 800, 1000, fill=wallColor, outline=wallColor)
    pf.addPlatform(0, -200, 800, 600, fill=wallColor, outline=wallColor)
    pf.addPlatform(150, 400, 100, 100, fill=wallColor, outline=wallColor)
    pf.addPlatform(350, 400, 100, 100, fill=wallColor, outline=wallColor)
    pf.addPlatform(550, 400, 100, 100, fill=wallColor, outline=wallColor)
    pf.addGoal(700, 500, 50, 50)
    pf.addPlayer(50, 550)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    #game.backgroundColor = Color("lightgrey")
    #game.window.setBackground(Color("lightgrey"))
    game.window.setBackground(Color("slategrey"))
    b = PlatformerBriefing()
    b.bul("Use variables to move hazards out of the way!")
    b.bul("Three spinning blades have been assigned to three different variables: ",  Mono("blade1, blade2, blade3"))
    b.bul("Move a blade to a different position with: ", Mono("blade1.addWaypoint(x, y)"))
    b.bul('The first number "', Bold("x"), '" will determine the new horizontal')
    b.add(' position of the blade, the second number "', Bold("y"))
    b.add('" will determine the new vertical position of the platform.')
    b.bul("Carefull: these blades will now move back and forth!")
    b.bul("IMPORTANT: You are not allowed to change the code that adds the blades.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
