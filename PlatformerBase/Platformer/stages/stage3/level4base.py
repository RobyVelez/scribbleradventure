# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addEnemy = pf.addEnemy

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(getStudentDir(), 'r') as f:
        req1 = False
        req2 = False
        for line in f:
            if line.strip() == "enemy1=addEnemy(300, 550)" and not req1:
                req1 = True
            elif line.strip() == "enemy2=addEnemy(900, 550)" and not req2:
                req2 = True
        if not req1:
            game.addPopup("The line\nenemy1=addEnemy(300, 550)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
        elif not req2:
            game.addPopup("The line\nenemy2=addEnemy(900, 550)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
    return True


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 200, 100)
    pf.addPlatform(300, 600, 200, 100)
    pf.addPlatform(900, 600, 200, 100)  
    pf.addGoal(1200, 275, 50, 50)
    pf.addPlayer(100, 350)

    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Use variables to make enemies patrol!")
    b.bul("You can use enemies to jump from platform to platform, but the enemies are falling off their platforms.")
    b.bul("Make enemies patrol on their platform.")
    b.bul("Make an enemy patrol with:\n")
    b.add(Mono("enemy.setPatrol(x1, x2)"))
    b.bul("Here, ", Bold("enemy"), " is the name of the variable that the enemy is assigned to, ")
    b.add(Bold("x1"), " is the left patrol boundary, and ", Bold("x2"), " is the right patrol boundary.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
