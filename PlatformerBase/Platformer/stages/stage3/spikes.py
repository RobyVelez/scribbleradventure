from Platformer.stages.finalBase import *
initLevel(__file__)
### Build your level here ###
setPlayer(150, 550)             # The position of the player
addPlatform(0, 600, 800, 100)   # The ground platform
addPlatform(0, 0, 100, 800)    
addPlatform(0, -100, 800, 100)  
addPlatform(800, -100, 100, 800)  
addHazard(250, 550)
addHazard(150, 400)
addHazard(250, 350)
addHazard(350, 350)
addPlatform(300, 500, 50, 100)  
addPlatform(725, 500, 75, 100)  
addHazard(400, 550)
addHazard(488, 510)
addHazard(582, 510)
addHazard(675, 550)
addPlatform(725, 380, 75, 20)  
addPlatform(400, 300, 50, 20)  
addPlatform(100, 300, 50, 20)  
addPlatform(100, 180, 50, 20)  
addHazard(150, 50)
addPlatform(400, 100, 50, 20)  
addPlatform(550, 100, 50, 20)  
addHazard(500, 125)
addPlatform(750, 100, 50, 20)  
addHazard(675, 115)
addHazard(750, 250)
addGoal(750, 50)






### Start the game! ###
startGame()
