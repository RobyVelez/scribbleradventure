from Platformer.stages.stage2.level7base import *
initLevel(__file__)
### Build your level here ###
setPlayer(350, 550)             # The position of the player
addPlatform(0, 600, 2550, 100)  # The ground platform

addPlatform(0, 500, 100, 100)
addPlatform(400, 480, 300, 25)  
addPlatform(500, 360, 250, 25)
addEnemy(500, 500)
addEnemy(500, 400)
addEnemy(500, 300)
addPlatform(800, 500, 100, 100)
addPlatform(950, 360, 400, 25)
addPlatform(950, 340, 20, 20)
addPlatform(1330, 340, 20, 20)
addHazard(1000, 430)
addHazard(1200, 430)
addHazard(1400, 430)
addEnemy(1100, 300)
addPlatform(1500, 500, 100, 100)
addPlatform(1600, 400, 100, 200)
addPlatform(1700, 300, 100, 300)
addHazard(1850, 350)
addHazard(1950, 350)
addPlatform(2000, 300, 100, 300)
addPlatform(2100, 300, 200, 25)
addPlatform(2200, 450, 200, 25)
addPlatform(2400, 0, 100, 475)
addEnemy(2250, 250)
addJumppad(2500, 580)
addPlatform(2700, 350, 300, 25)
addHazard(2750, 400)
addHazard(2850, 400)
addHazard(2950, 400)
addPlatform(3200, 600, 600, 100)


addGoal(3500,500)















































### Start the game! ###
startGame()
