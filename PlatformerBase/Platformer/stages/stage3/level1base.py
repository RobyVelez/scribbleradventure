# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
textObjs = []

# Make functions available to the player
addPlatform = pf.addPlatform

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    defaultPlatform = Platform(0,0)
    coloredPlatforms = 0
    for platform in game.objs:
        if not isinstance(platform, Platform): continue
        if not platform.shape.color.Equals(defaultPlatform.shape.color):
            coloredPlatforms+=1
    print(coloredPlatforms)
    if coloredPlatforms >= 3:
        return True
    else:
        game.addPopup("You need to add at least 3\ncolored platforms to your game\n\nAdjust your code,\nthen press run to continue.", h=300)
        return False

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    x = 0
    y = 0
    for i, keyValue in enumerate(colors):
        name = keyValue.Key
        color = keyValue.Value
        x1=x*200 - 2000
        y1=100 + y*50
        if (color.blue + color.red + color.green) >= ((255*3)/3):
            pf.addDecor(Rectangle((x1-100, y1-25), (x1+100, y1+25), color=Color("black")))
        pf.addText(x1, y1, name, color=color)
        y+=1
        if y > 6:
            x+=1
            y=0
    pf.addPlatform(-3000, 600, 6000, 100)    
    pf.addGoal(600, 500, 50, 50)
    pf.addPlayer(450, 550)
    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Assign objects to variables to change their color.")
    b.bul("The ", Mono("addPlatform"), " returns an object.")
    b.bul("By assigning this object to a variable we can change the color of the object.")
    b.bul("To assign a platform to a variable, write: ", Mono("platform = addPlatform(0,0)"))
    b.bul("To then change the color of the platform, write: ", Mono('platform.setColor("gold")'))
    b.bul("Add at least three platforms with a different color.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
