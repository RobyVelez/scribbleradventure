# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
getPlayer = pf.getPlayer

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 250, 100)
    pf.addGoal(600, 300, 50, 50)
    pf.addPlayer(200, 550)

    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Modify the player to reach the goal!")
    b.bul("To retrieve the player and store it in a variable, type:\n")
    b.add(Mono("player = getPlayer()"))
    b.bul("Once the player is assigned to a variable, you can change its jump height with:\n")
    b.add(Mono("player.jumpHeight = 20\n"))
    b.add("Where ", Bold("20"), " determines the height the player can jump.")
    b.bul("You can also change the player speed with:\n")
    b.add(Mono("player.speed = 20\n"))
    b.add("Where ", Bold("20"), " determines the speed with which the player can move left and right.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
