# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
game = None

# Make functions available to the player
getGame = pf.getGame

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    global game
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 100, 100)
    pf.addPlatform(0, 0, 700, 100)
    pf.addGoal(600, 100, 50, 50)
    pf.addPlayer(50, 350)
    game = PlatformerGame()
    pf.setGame(game)
    b = PlatformerBriefing()
    b.bul("Change the gravity to reach the goal!")
    b.bul("First, retrieve the game and assign it to a variable with:\n")
    b.add(Mono("game = getGame()"))
    b.bul("Then, change gravity with:\n")
    b.add(Mono("game.setGravity(x, y)\n"))
    b.add("Where ", Bold("x"), " and ", Bold("y"), " need to be replaced by numbers.")
    b.bul(Bold("x"), " determines the horizontal gravity, and ", Bold("y"), " determines the vertical gravity.")
    b.pln("Good luck!")
    game.addBriefing(b.write())

    
# This is where the game object should be created and launched
def startGame(userVariables=None):
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
