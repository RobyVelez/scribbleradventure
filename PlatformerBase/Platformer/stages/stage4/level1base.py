# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None
game = None

# Make functions available to the player
addObject = pf.addObject

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    global game
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    x = 500
    y = 400
    arrow = Polygon((x,y+50), (x+100,y+50), (x+100,y+100), (x+200,y), (x+100,y-100), (x+100,y-50), (x,y-50))
    arrow.fill = Color(100, 255, 100)
    pf.addDecor(arrow)
    x = 1200
    y = 400
    arrow = Polygon((x,y+50), (x+100,y+50), (x+100,y+100), (x+200,y), (x+100,y-100), (x+100,y-50), (x,y-50))
    arrow.fill = Color(100, 255, 100)
    pf.addDecor(arrow)
    pf.addPlatform(0, 600, 400, 100)
    pf.addPlatform(900, 600, 250, 100)
    pf.addPlatform(1500, 600, 250, 100)    
    pf.addGoal(1600, 500, 50, 50)
    pf.addPlayer(200, 550)
    game = PlatformerGame()
    pf.setGame(game)

    
# This is where the game object should be created and launched
def startGame(userVariables=None):
    b = PlatformerBriefing()
    b.bul("Add a custom pickup to the game to cross the gap!")
    b.bul("The custom pickup is defined at the top of the file. Note that it is called: ", Bold("MyPickup."))
    b.bul("To add the pickup to your game, type:\n")
    b.add(Mono("pickup=MyPickup(x, y)\n"))
    b.add(Mono("addObject(pickup)"))
    b.bul("Replace ", Bold("x"), " and ", Bold("y"), " by numbers.")
    b.bul(Bold("x"), " is the horizontal position of the pickup, while ", Bold("y"), " is the vertical position of the pickup.")
    b.bul("Make sure to add your code below:\n")
    b.add(Mono("### Add custom pickup here ###"))
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
