# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addPlatform = pf.addPlatform

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(getStudentDir(), 'r') as f:
        count = 0
        for line in f:
            if "addPlatform" in line:
                count += 1
            if count > 1:
                game.addPopup("In this level, only one\naddPlatform(x, y)\nstatement is allowed.\n\nPlease remove all but one\naddPlatform statement and rerun.")
                return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 100, 100)
    pf.addGoal(600, 50, 50, 50)
    pf.addPlayer(50, 350)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add platforms to reach the goal!")
    b.bul("You are only allowed one ", Mono("addPlatform(x, y)"), " statement.")
    b.bul("Add multiple platforms with a for loop!")
    b.bul("Create a for loop with:\n")
    b.add(Mono("for  i  in  range(5):\n"))
    b.add(Mono("        addPlatform(i, i)"))
    b.bul("Add to ", Bold("i"), " to move your platforms into position.")
    b.bul("Multiply ", Bold("i"), " to place the platforms further apart.")
    b.bul("IMPORTANT: note that ", Mono("addPlatform(x, y)"), " needs to be indented!")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
