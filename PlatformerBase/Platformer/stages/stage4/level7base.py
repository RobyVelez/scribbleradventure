# Boiler plate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addPlatform = pf.addPlatform
addHazard = pf.addHazard
addEnemy = pf.addEnemy
addGoal = pf.addGoal
addJumppad = pf.addJumppad
addCrate = pf.addCrate
setPlayer = pf.setPlayer

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    return True


# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlayer(350, 550)
    
    
# This is where the game object should be created and launched
def startGame():
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Create any level you want with the following commands:")
    b.bul(Mono("addPlatform(x, y, width, height)"))
    b.bul(Mono("addHazard(x, y)"))
    b.bul(Mono("addEnemy(x, y)"))
    b.bul(Mono("addJumppad(x, y)"))
    b.bul(Mono("addCrate(x, y)"))
    b.bul(Mono("addGoal(x, y)"))
    b.bul(Mono("setPlayer(x, y)"))
    b.pln("Have fun!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.allowUnlock()
    game.run(level)

