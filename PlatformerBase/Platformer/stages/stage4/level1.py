from Platformer.stages.stage4.level1base import *
initLevel(__file__)

### The custom pickup is defined here ###
class MyPickup(Pickup):
    def onPickup(self, player):
        player.speed = 20                           # Set the speed of the player when it is picked-up
        aura=createPicture("adventurer/aura.png")   # Loads the image of an aura
        aura.draw(player.shape)                     # Draws the aura on top of the player
        self.hide()                                 # Hide the pickup when it is picked-up

### Add custom pickup here ###






### Start the game! ###
startGame()
