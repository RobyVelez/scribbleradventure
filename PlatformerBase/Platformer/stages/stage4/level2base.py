# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addPlatform = pf.addPlatform

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(game.studentDir + "/" + game.currentLevel, 'r') as f:
        req1 = False
        req2 = False
        illegalPlatform = False
        for line in f:
            if "addPlatform" in line:
                if line.strip() == "addPlatform(x1, y1)" and not req1:
                    req1 = True
                elif line.strip() == "addPlatform(x2, y2)" and not req2:
                    req2 = True
                else:
                    illegalPlatform=True
        if not req1:
            game.addPopup("The line\naddPlatform(x1, y1)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
        elif not req2:
            game.addPopup("The line\naddPlatform(x2, y2)\nwas altered or removed.\n\nPlease restore this line and rerun.")
            return False
        elif illegalPlatform:
            print("here")
            game.addPopup("You are not allowed to create\nadditional platforms in this level.\n\nPlease remove all extra\naddPlatform\nstatements and rerun.")
            return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    pf.addPlatform(0, 600, 700, 100)    
    pf.addGoal(400, 275, 50, 50)
    pf.addPlayer(450, 550)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Change variables to move platforms into position!")
    b.bul("You can change a variable in the same way you create a variable, with: ",  Mono("name = value"))
    b.bul("Replace, ", Bold("name"), " by the variable you want to change.")
    b.bul("Replace, ", Bold("value"), " by a number.")
    b.bul("This level has four variables: ", Mono("x1"), ", ", Mono("y1"), ", ", Mono("x2"), ", and ", Mono("y2"), ".")
    b.bul("These variables are used by two ",  Mono("addPlatform(x1, y1)"))
    b.add(" functions, so if you change the value of ", Mono("x1"), " that will change the horizontal position of that platform.")
    b.bul("IMPORTANT: You are not allowed to add additional platforms.")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
