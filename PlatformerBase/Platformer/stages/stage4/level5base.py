# Boilerplate
from Platformer.core import *
from Platformer.objects import *

# Global variables
level = None

# Make functions available to the player
addJumppad = pf.addJumppad

# Here you can specify which conditions need to met before the game is finished
def victoryCondition(game):
    with file(getStudentDir(), 'r') as f:
        count = 0
        for line in f:
            if "addJumppad" in line:
                count += 1
            if count > 1:
                game.addPopup("In this level, only one\naddJumppad(x, y)\nstatement is allowed.\n\nPlease remove all but one\naddJumppad statement and rerun.")
                return False
    count = 0
    for obj in game.objs:
        if isinstance(obj, Jumppad):
            count += 1
        if count > 20:
            game.addPopup("A maximum of 20 jumppads can\n be placed in this level.\n\nPlease add fewer\njumppads and rerun.")
            return False
    return True

# Initialize the level with everything you don't want the player to change
def initLevel(studentFile):
    global level
    setCurrentLevel(studentFile)
    level = PlatformerLevel()
    pf.setLevel(level)
    x = 200
    y = 400
    arrow = Polygon((x,y+50), (x+100,y+50), (x+100,y+100), (x+200,y), (x+100,y-100), (x+100,y-50), (x,y-50))
    arrow.fill = Color(100, 255, 100)
    pf.addDecor(arrow)
    x = 600
    y = 400
    arrow = Polygon((x,y+50), (x+100,y+50), (x+100,y+100), (x+200,y), (x+100,y-100), (x+100,y-50), (x,y-50))
    arrow.fill = Color(100, 255, 100)
    pf.addDecor(arrow)
    pf.addPlatform(0, 600, 100, 100)
    pf.addGoal(900, 375, 50, 50)
    pf.addPlayer(50, 350)

    
# This is where the game object should be created and launched
def startGame(userVariables):
    game = PlatformerGame()
    b = PlatformerBriefing()
    b.bul("Add jumppads to reach the goal!")
    b.bul("You are only allowed one ", Mono("addJumppad(x, y)"), " statement.")
    b.bul("You are only allowed 20 jumppads in total.")
    b.bul("Add multiple jumppads with a for loop!")
    b.bul("Create a for loop with:\n")
    b.add(Mono("for  i  in  range(5):\n"))
    b.add(Mono("        addJumppad(i, 580)"))
    b.bul("Add to ", Bold("i"), " to move your platforms into position.")
    b.bul("Multiply ", Bold("i"), " to place the platforms further apart.")
    b.bul("IMPORTANT: note that ", Mono("addJumppad(x, y)"), " needs to be indented!")
    b.pln("Good luck!")
    game.addBriefing(b.write())
    game.victoryCondition = victoryCondition
    game.startCondition = victoryCondition
    game.allowUnlock()
    game.run(level)
