#Perform imports
from Graphics import *
import traceback
import Cairo

class LRC_Frame(Shape):
    def __init__(self):
        self.frame = None
        self.flipped = False

    def render(self, context):
        try:
            context.Save()
            if self.flipped:
                context.Scale(-1,1)
            else:
                context.Scale(1,1)
            if self.frame: self.frame.render(context)
            context.Restore()
        except:
            print(traceback.format_exc())