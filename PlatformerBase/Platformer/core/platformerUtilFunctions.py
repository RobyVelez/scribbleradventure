import os
from Graphics import *
from platformerConstants import *
from platformerLevelInfo import PLATFORMER_LEVELS
import shutil

# def makeRectangle(x, y, w=100, h=10, fill=Color("brown"), outline=Color("brown")):
#     """
#     Convenience function for making rectangles.
    
#     Rather than taking two points, this function takes one point,
#     and the width and height of the rectangle.
#     """
#     return Rectangle((x, y), (x+w, y+h), fill=fill, outline=outline)
    
    
def hit(obj1, obj2):
    if obj1.shape.getX() < obj2.shape.getP1().x: return False
    if obj1.shape.getX() > obj2.shape.getP2().x: return False
    if obj1.shape.getY() < obj2.shape.getP1().y: return False
    if obj1.shape.getY() > obj2.shape.getP2().y: return False
    return True
    
    
def testCollDir(contact, shape):
    if not contact.IsTouching():
        return NO_HIT
    if contact.FixtureA.UserData == shape:
        invert = 1
    else:
        invert = -1
    norm = contact.Manifold.LocalNormal
    if abs(norm.X) > abs(norm.Y):
        if (invert*norm.X) > 0:
            return RIGHT
        else:
            return LEFT
    elif (invert*norm.Y) > 0:
        return BOTTOM
    else:
        return TOP
    
    
def testOnGround(player):
    player.onGround=False
    contactList = player.shape.body.ContactList
    while contactList:
        if testCollDir(contactList.Contact, player.shape) == BOTTOM:
            player.onGround=True
        contactList = contactList.Next


def getImagePath(path):
    if os.path.exists(path):
        return path
    perVars = calico.manager["python"].engine.persistentVariables
    if "studentDir" in perVars:
        studentDir = perVars["studentDir"]
        result =  os.path.join(studentDir, "images", path)
        if os.path.exists(result):
            return result
        result =  os.path.join(studentDir, "user/images", path)
        if os.path.exists(result):
            return result
    return None

def createPicture(path):
    pic = Picture(getImagePath(path))
    pic.border = 0
    pic.moveTo(0,0)
    return pic


def tryCreateDir(directory, verbose= False, dryrun = False):
        if not os.path.exists(directory):
            if verbose:
                print("Creating directory:", directory)
            if not dryrun:
                os.makedirs(directory)
            
            
def updateFile(source, target, overwrite= False, verbose = False, dryrun = False):
    if os.path.exists(target):
        # Poormans version of os.path.samefile, because that implementation
        # seems completely broken in Calico (it returns true for any pair of files).
        if os.stat(source) == os.stat(target):
            if verbose:
                print("Paths point to the same file")
                print("  Source:", source)
                print("  Target:", target)
            return
        if filecmp.cmp(source, target):
            if verbose:
                print("Files are the same")
                print("  Source:", source)
                print("  Target:", target)
            return
        else:
            if overwrite:
                if verbose:
                    print("Removing file:", target)
                if not dryrun:
                    os.remove(target)
    if not os.path.exists(target):
        if verbose:
            print("Copying file:", source, "to:", target)
        if not dryrun:
            shutil.copyfile(source, target)


def updateTree(source, target, overwrite= False, verbose = False, dryrun = False):
    for dirName, subdirList, fileList in os.walk(source):
        if source not in dirName:
            raise Exception("Could not update tree.")
        reldir = dirName.replace(source, "", 1)
        while len(reldir) > 0 and (reldir[0] == '/' or reldir[0] == '\\'): reldir = reldir[1:]
        targetSubDir = os.path.join(target, reldir)
        for filename in fileList:
            # Ignore hidden unix files
            if filename[0] == ".": continue 
            targetFile = os.path.join(targetSubDir, filename)
            sourceFile = os.path.join(dirName, filename)
            updateFile(sourceFile, targetFile, overwrite, verbose, dryrun)
        for directory in subdirList:
            targetDir = os.path.join(targetSubDir, directory)
            tryCreateDir(targetDir, verbose, dryrun)


def setCurrentLevel(filename):
    persistentVars = calico.manager["python"].engine.persistentVariables
    if "studentDir" not in persistentVars:
        # TODO: Resolve potential student dir based on filename
        print("WARNING: Student directory not set!")
        pass
    for levelInfo in PLATFORMER_LEVELS:
        targetpath = levelInfo.getTargetPath()
        if targetpath is None: continue 
        if os.path.normpath(targetpath) == os.path.normpath(filename):
            persistentVars["currentLevel"] = levelInfo
            return levelInfo
    print("WARNING: Setting current level failed!")
    plainLevelInfo = LevelInfo()
    plainLevelInfo.fullpath = filename
    persistentVars["currentLevel"] = plainLevelInfo
    return plainLevelInfo


def getStudentDir():
    persistentVars = calico.manager["python"].engine.persistentVariables
    if "currentLevel" not in persistentVars:
        print("Something went wrong while loading the level.\n\nPlease restart the game.")
        return None
    return persistentVars["currentLevel"].getTargetPath()
