import copy

class PlatformerLevel:
    def __init__(self):
        self.objects = []
        self.player = None
        self.playerStart = (0,0)
        self.background = None
        
    def load(self, game):
        self.objects.sort(key=lambda obj: obj.z)
        for obj in self.objects:
            #game.addObject(copy.copy(obj))
            game.addObject(obj)
        if self.player:
            game.player = self.player
            game.addObject(self.player)
        if self.background is not None:
            game.setBackground(self.background)
            #game.addObject(copy.copy(self.player))
        # game.player.moveTo(self.playerStart[0], self.playerStart[1])
        # game.addObject(game.player)
##     
##         for obstacle in self.obstacles:
##             #game.obstacles.append(obstacle)
##             game.addShape(obstacle)
##         for hazard in self.hazards:
##             #game.hazards.append(hazard)
##             game.addObject(hazard)
##         for enemy in self.enemies:
##             #game.enemies.append(enemy)
##             game.addObject(enemy)
##         for goal in self.goals:
##             #game.goals.append(goal)
##             game.addObject(goal)
##         for sprite in self.sprites:
##             #game.sprites.append(sprite)
##             game.addObject(sprite)
##         for decor in self.decor:
##             game.addDecor(decor)

