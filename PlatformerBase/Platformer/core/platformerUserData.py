# Standard library imports
import os       # For opening and creating directories
import pickle   # For writing user data to a file
import shutil   # For copying files

# Constants
from platformerConstants import *

# Classes
class PlatformerUserData(object):
    def __init__(self):
        # Initialize all attributes to something empty
        self.userData = {"maxLevel" : 1}
        self.studentDir = None
        self.good = False
        
        self.persistentVars = calico.manager["python"].engine.persistentVariables
        if "studentDir" not in self.persistentVars:
            print("WARNING: Student directory not set. We will be unable to load or save files.")
        else:
            self.studentDir = self.persistentVars["studentDir"]
            self.userDataDir = os.path.join(self.studentDir, USER_DATA_DIR)
            self.userDataFile = os.path.join(self.userDataDir, USER_DATA_FILE)
            self.userStagesDir = os.path.join(self.userDataDir, STAGES_DIR)
            self.good = True
            if not os.path.exists(self.userDataFile):
                self.create()
            else:
                self.load()
                
        #self.userData["maxLevel"] = 30
            
            
    #######################
    #### IO Functions #####
    #######################
    
    def create(self):
        if not self.good: return
        if not os.path.exists(self.userDataDir):
            #print("Creating directory:", self.userDataDir)
            os.makedirs(self.userDataDir)
        if not os.path.exists(self.userDataFile):
            with open(self.userDataFile, "w") as picleFile:
                #print("Writing file:", self.userDataFile)
                pickle.dump(self.userData, picleFile)
                
                
    def load(self):
        if not self.good: return
        if os.path.exists(self.userDataFile):
            with open(self.userDataFile, "r") as picleFile:
                #print("Loading file:", self.userDataFile)
                self.userData = pickle.load(picleFile)
                
                
    def save(self):
        if not self.good: return
        with open(self.userDataFile, "w") as picleFile:
            #print("Writing file:", self.userDataFile)
            pickle.dump(self.userData, picleFile)
            

    #######################
    # Max level functions #
    #######################

    def getMaxLevel(self):
        return self.userData["maxLevel"]
        
        
    def setMaxLevel(self, level):
        self.userData["maxLevel"] = level
        
        
    def updateMaxLevel(self, level):
        if level > self.getMaxLevel():
            self.setMaxLevel(level)
            self.save()
            

    def unlockNextLevel(self):
        if "currentLevel" in self.persistentVars:
            levelInfo = self.persistentVars["currentLevel"]
            self.updateMaxLevel(levelInfo.getNextLevel())
        else:
            print("WARNING: currentLevel not set, unable to unlock next level.")
            
    #######################
    ### Level functions ###
    #######################
    
    def copyLevel(self, levelInfo):
        source = levelInfo.getSourcePath()
        target = levelInfo.getTargetPath()
        if not source or not target:
            print("WARNING: Source or target not defined, cannot copy level")
            return
        targetDir = os.path.dirname(target)
        if not os.path.exists(targetDir):
            #print("Creating directory:", targetDir)
            os.makedirs(targetDir)
        if os.path.exists(target):
            #print("Removing file:", target)
            os.remove(target)
        #print("Copying file:", source, "to:", target)
        shutil.copy(source, target)
        
        
    def loadLevel(self, levelInfo, forceOverwrite=False):
        target = levelInfo.getTargetPath()
        if not target:
            print("WARNING: Target path not defined, cannot load level")
            return
        if not os.path.exists(target) or forceOverwrite:
            self.copyLevel(levelInfo)
        calico.Open(target)
        if not calico.CurrentDocument:
            print("WARNING: No current document set, maybe load failed?")
        else:
            calico.CurrentDocument.Save()
        calico.ExecuteFileInBackground(target)
        

    def reloadLevel(self):
        if "currentLevel" in self.persistentVars:
            levelInfo = self.persistentVars["currentLevel"]
            self.loadLevel(levelInfo)
        else:
            print("WARNING: currentLevel not set, unable to reload level.")
            
            
    def resetLevel(self):
        if "currentLevel" in self.persistentVars:
            levelInfo = self.persistentVars["currentLevel"]
            path = levelInfo.getTargetPath()
            if not path:
                print("WARNING: Target path not defined, cannot reset level")
                return
            calico.Open(path)
            calico.CurrentDocument.Save()
            calico.TryToClose(calico.CurrentDocument)
            self.copyLevel(levelInfo)
            calico.Open(path)
        else:
            print("WARNING: currentLevel not set, unable to reset level.")
