# Standard imports
import time
import threading
import shutil
import pickle

# Calico imports
from Graphics import *

# Portal window imports
from WindowObjects.windowObjects import *
from CommonFunctions.makeShapeFunctions import *

# Platformer imports
import Platformer.platformerWindow
from Platformer.core.platformerUtilFunctions import *

###################################
############ PLATFORMER ###########
###################################
class PlatformerGame:
    def __init__(self, standalone = False):
        # The prefered width and height will be the size of the window
        # if not specified before. Certain centering operations are based
        # on these values.
        self.preferedWidth = 700
        self.preferedHeight = 700
        
        # Create or commandeer a window
        try:
            self.window = getWindow()
        except Exception:
            self.window = Window("My platformer", self.preferedWidth, self.preferedHeight)
        if self.window.IsRealized:
            self.window.resetBlocking(False)
        else:
            self.window = Window("My platformer", self.preferedWidth, self.preferedHeight)
        self.window.mode = "physicsmanual"
        #self.window.Fullscreen()
        
        # Retrieve information from the persistent variables
        self.persistentVars = calico.manager["python"].engine.persistentVariables
        if "userData" in self.persistentVars:
            self.userData = self.persistentVars["userData"]
        else:
            self.userData = None
        if "showBriefing" not in self.persistentVars:
            self.persistentVars["showBriefing"] = True
        
        # Add frames, one for the world, and one for the interface
        self.frame = Frame((0,0))
        self.interfaceFrame = Frame((0,0))
        self.frame.draw(self.window)
        self.interfaceFrame.draw(self.window)
        
        # Track current state
        self.running = True
        self.won = False
        self.quit = False
        self.closed = False
        self.backToPlatformerWindow = True
        self.unlockAllowed = False
        
        # Track and maintain framerate
        self.t = time.time()
        self.frameRate = 0.03

        # Track local objects
        self.objsToUpdate = []
        self.objs = []
        self.previousButtons = []
        self.popup = None
        self.victoryCondition = None
        self.startCondition = None
        self.briefing = None
        self.backgroundColor = Color("skyblue")
        self.backgroundImageWrapper = BackgroundWrapper(self.window)

        # Initialize game specific and interface specific elements
        self.initInterface()
        self.initGame()
        
        # Start registering mouseclicks
        self.window.onMouseDown(self.onMouseClick)
        #self.window.onConfigure(self.onResize)

    #########################################
    ## Initialization and cleanup functions #
    #########################################

    def initGame(self):
        self.levelHeight = 700
        self.window.gravity = Vector(0,50)
        self.window.setBackground(self.backgroundColor)
        self.backgroundImageWrapper._paintBackground()
        self.player = None
        
        
    def load(self, level):
        self.clean()
        level.load(self)
        if self.startCondition is not None:
            if not self.startCondition(self):
                self.running = False
                self.quit = True
                self.closeOnExit = False
        self.update()
        
        
    def clean(self):
        self.removePopup()
        for obj in self.objs:
            obj.undraw()
        del self.objs[:]
        del self.objsToUpdate[:]
        self.frame.moveTo(0,0)
        self.interfaceFrame.moveTo(0,0)
        
        
    def run(self, level):
        self.won = False
        self.quit = False
        self.closeOnExit = True
        #while not self.won and not self.quit and self.window.IsRealized:
        self.running = True
        self.load(level)
        self.runLevel()
        if not(self.won or self.quit or not self.window.IsRealized):
            self.softReset()
        elif self.closeOnExit:
            self.close()
            
            
    def runLevel(self):
        while self.running and self.window.IsRealized:
            self.updateGame()



    #########################################
    ######### Add stuff functions ###########
    #########################################
        
    def addObject(self, obj):
        """
        This function is specifically for adding PlatformerObject object to the platformer.
        """
        # When the level is reset, the platformer will attempt to undraw all added objects
        assert(hasattr(obj, "undraw"))
        
        # If the object has an 'init' function, it is assumed that it knows how to add itself to the game
        if hasattr(obj, "init"):
            obj.init(self)
            
        # If the object has an 'update' function, that function will be called every time step
        if hasattr(obj, "update"):
            self.objsToUpdate.append(obj)
        self.objs.append(obj)
            
            
    def addShape(self, shape):
        """
        This function is for adding shapes to the platformer as physics objects.
        """
        shape.draw(self.frame)
        shape.addToPhysics()
        self.objs.append(shape)
        
        
    def addDecor(self, shape):
        """
        This function is for adding shapes to the platformer that do not have physics.
        """
        shape.draw(self.frame)
        self.objs.append(shape)
        
        
    def draw(self, shape):
        """
        This function is for drawing shapes to the game world.
        """
        shape.draw(self.frame)
        
        
    def drawInterfaceItem(self, shape):
        """
        This function is for drawing shapes to the interface.
        
        Interface shapes do not move with the world as the player moves.
        """
        shape.draw(self.interfaceFrame)
        

    def addBriefing(self, briefing):
        """
        This function adds a briefing to the game.
        """
        self.briefing = briefing
        self.briefing.moveTo(700/2,700/2)
        self.briefing.visible = self.persistentVars["showBriefing"]
        self.drawInterfaceItem(self.briefing)


    #def setBackground(self, image, width=3000, height=1500, xOffset=0, yOffset=0):
    def setBackground(self, image, width=3000, height=1500, xOffset=0, yOffset=0):
        if isinstance(image, Color):
            self.backgroundColor = image
        elif isinstance(image, str):
            path = getImagePath(image)
            self.backgroundImageWrapper.setBackgroundImage(path, width, height, xOffset, yOffset)
    
        
    def setGravity(self, x, y):
        self.window.gravity = Vector(x, y)

    #########################################
    ########### Update functions ############
    #########################################
    
    def updateGame(self):
        self.updateObjs()
        self.updateFrame()
        self.update()
        
    
    def update(self):
        #while (time.time() - self.t) < self.frameRate: 
        #    pass
        self.fps.text = "%4f" % (time.time() - self.t)
        self.t = time.time()
        self.window.step(0.03)
        if self.window.height > self.levelHeight:
            self.frame.setY(self.window.height - self.levelHeight)
            self.interfaceFrame.setY(self.window.height - self.levelHeight)
        if self.window.getKeyPressed('q'):
            self.quitGame()
            #self.running = False
            #self.quit = True
            #self.close()

        
    def updateFrame(self):
        if not self.player: return
        if self.player.getX() + self.frame.getX() > 500:
            self.frame.setX(500 - self.player.getX())
        if self.player.getX() + self.frame.getX() < 200:
            self.frame.setX(200 - self.player.getX())
            
            
    def updateObjs(self):
        for obj in self.objsToUpdate:
            obj.update(self)
            
            
    #########################################
    ######## Game control functions #########
    #########################################
                
    def win(self):
        self.briefing.visible = False
        trueVictory = True
        if self.victoryCondition is not None:
            if not self.victoryCondition(self):
                trueVictory = False
        if trueVictory:
            self.saveProgress()
            self.addPopup("You win!\n\nPress space to continue...")
            self.spin()
        else:
            self.closeOnExit = False
        self.won = True
        self.running = False
            
        
    def lose(self):
        self.addPopup("Game over!\n\nPress space to continue...")
        self.running = False
        self.spin()
        
        
    def spin(self):
        while not self.window.getKeyPressed('space') and not self.window.getKeyPressed('q'):
            self.window.update()
        

    def quitGame(self):
        if self.quit or self.won or not self.window.IsRealized:
            self.close()
        self.quit = True
        self.running = False
        self.closeOnExit = True
             
                   
    def close(self):
        self.clean()
        if self.backToPlatformerWindow:
            Platformer.platformerWindow.start()
        self.closed = True
        #self.window.close()
        

    def allowUnlock(self):
        self.unlockAllowed = True
        
    def saveProgress(self):
        if self.userData and self.unlockAllowed: 
            self.userData.unlockNextLevel()
        #pass
        # maxLevel = self.userData["maxLevel"]
        # if maxLevel < (self.currentLevelIndex+1):
        #     return
        # self.userData["maxLevel"] = self.currentLevelIndex+2
        # with open(self.studentDir + "/" + self.userDataFile, "w") as picleFile:
        #     pickle.dump(self.userData, picleFile)
        
        
    def runOnceQuit(self):
        i=0
        while calico.ProgramRunning and i < 20:
            time.sleep(0.2)
            i+=1
        if i < 20:
            if self.userData: self.userData.reloadLevel()
            #calico.Open(self.studentFile)
            #calico.CurrentDocument.Save()
            #calico.ExecuteFileInBackground(self.studentFile)
        else:
            print("Platformer failed to quit")
        
    def hardReset(self):
        calico.config.SetValue('python-language','reset-shell-on-run', True)
        self.runStudentFile()

    def softReset(self):
        calico.config.SetValue('python-language','reset-shell-on-run', False)
        self.runStudentFile()
        
    def runStudentFile(self):
        self.backToPlatformerWindow = False
        self.quitGame()
        waitForQuitThread = threading.Thread(target=self.runOnceQuit)
        waitForQuitThread.start()
                

    #########################################
    ########## Interface functions ##########
    #########################################
            
    def onMouseClick(self, obj, event):
        for button in self.buttons:
            if button.hit(event.x, event.y):
                if button.function is not None:
                    button.function()

         
    def cancelReset(self):
        self.buttons = self.previousButtons
        self.popup.undraw()

        
    def performReset(self):
        if self.userData:
            self.userData.resetLevel()
        calico.config.SetValue('python-language','reset-shell-on-run', True)
        self.buttons = self.previousButtons
        self.popup.undraw()

    def addPopup(self, text, w=400, h=200):
        self.removePopup()
        x = self.preferedWidth/2 - w/2
        y = self.preferedHeight/2 - h/2
        self.popup = RoundedRectangle((x,y),(x+w,y+h), 10, fill=Color(255,255,255,200), outline=Color(0,0,0))
        text = makeText(text, size=24)
        text.xJustification = "center"
        text.yJustification = "center"
        text.draw(self.popup)
        self.drawInterfaceItem(self.popup)
        
    def removePopup(self):
        if self.popup is not None:
            self.popup.undraw()
        self.popup=None

    def reset(self):
        self.previousButtons = self.buttons
        self.buttons = []
        buttons = [
            ("No", Color(255, 200, 200), self.cancelReset), 
            ("Yes", Color(200, 255, 200), self.performReset),
        ]
        self.popup = Rectangle((0,0), (10000, 10000), color=Color(200,200,200,100))
        dialogBox=RoundedRectangle((150 - 5000,250- 5000),(550- 5000,450- 5000), 10, fill=Color(255,255,255), outline=Color(0,0,0))
        text = makeText("This action will delete all code\nyou have written for this level.\nAre you sure you wish to reset?", size=24)
        text.xJustification = "center"
        text.yJustification = "bottom"
        text.draw(dialogBox)
        x = 0
        xStart = -120
        y = 25
        width = 100
        height = 50
        for button in buttons:
            title, color, function = button
            shadow=RoundedRectangle((x + xStart+ 2,y + 2),(x + xStart + width+2,y + height + 2), 5, color=Color(0,0,0,100), border=0)
            shadow.draw(dialogBox)
            t=ButtonP((x + xStart,y),(x + xStart + width,y + height), 5)
            t.xMargin=0
            t.function=function
            t.init(True)
            t.fill=color
            t.text.text = title
            t.draw(dialogBox)
            x+= width + (-width-xStart)*2
            self.buttons.append(t)
        dialogBox.draw(self.popup)
        self.drawInterfaceItem(self.popup)
           
    def toggleBriefing(self):
        if self.briefing:
            self.briefing.visible = not self.briefing.visible
            self.persistentVars["showBriefing"] = self.briefing.visible
            self.window.update()
            
    def initInterface(self):
        buttons = [
            ("Back", Color(255, 200, 200), self.quitGame), 
            ("Run", Color(200, 255, 200), self.hardReset),
            ("Briefing", Color(200, 200, 255), self.toggleBriefing), 
            ("Reset", Color(200, 200, 200), self.reset)]
        x = 0
        self.buttons = []
        for button in buttons:
            title, color, function = button
            shadow=RoundedRectangle((x + 120+2,640+2),(x+220+2,670+2), 5, color=Color(0,0,0,100), border=0)
            self.drawInterfaceItem(shadow)
            t=ButtonP((x+120,640),(x+220,670), 5)
            t.xMargin=0
            t.function=function
            t.init(True)
            t.fill=color
            t.text.text = title
            self.drawInterfaceItem(t)
            self.buttons.append(t)
            x+=120
        self.fps = Text((60,20), "0.0")
        self.fps.visible=False
        self.drawInterfaceItem(self.fps)
        
        
        
