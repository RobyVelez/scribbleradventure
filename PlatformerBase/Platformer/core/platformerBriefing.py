from CommonFunctions.fancyTextFunctions import *
from CommonFunctions.makeShapeFunctions import *

PLATFORMER_BRIEFING_TEXT_Y_POS = 25
PLATFORMER_BRIEFING_TEXT_PAR_SKIP = 10

class PlatformerBriefing(object):
    """
    The briefing object, a convience function for writing to a blank
    brief screen.
    """
    def __init__(self):
        """
        Constructs a platformer briefing object.
        """
        self.briefing = [Wrap(475), SkipV(PLATFORMER_BRIEFING_TEXT_Y_POS)]
        self.localText = []
        self.template = RoundedRectangle((0,0),(500,400), 10, fill=Color(255,255,255, 220), outline=Color(0,0,0))
        self.first = True
        
        
    def add(self, *parts):
        for part in parts:
            self.localText.append(part)
        
    def bul(self, *parts):
        """
        Adds a line of text to the briefing.
        
        Note that, internally, the briefing object uses the :mod:`fancyTextFunctions`
        module, so all commands found there can be added here.
        
        Args:
            parts: the text and objects to be added to the briefing.
        """
        self.flush()   
        self.localText = []
        for part in parts:
            self.localText.append(part)
        
    def pln(self, *parts):
        self.flush()
        for part in parts:
            self.briefing.append(part)
        
    def flush(self):
        if len(self.localText) > 0:
            if not self.first:
                self.briefing += [SkipV(PLATFORMER_BRIEFING_TEXT_PAR_SKIP)]
            else:
                self.first = False
            self.briefing.append(Bullet("-", self.localText))  
            self.localText = []  
        
    def par(self, *parts):
        """
        Same as line, but adds some white-space.
        
        Args:
            parts: the text and objects to be added to the briefing.
        """
        self.briefing += [SkipV(PLATFORMER_BRIEFING_TEXT_PAR_SKIP)]
        self.line(*parts)
        
        
    def write(self):
        """
        Actually writes all the text to the blank brief screen.
        
        Return:
            The figure with all text written to it.
        """
        self.flush()
        writeText(self.template, self.briefing)
        header = makeText("Briefing", size=24, yjust="top")
        header.moveTo(-246, -196)
        line = Line((-246, -170), (246, -170))
        header.draw(self.template)
        line.draw(self.template)
        return self.template