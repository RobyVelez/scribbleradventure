import os
from platformerConstants import *

class LevelInfo:
    def __init__(self, name=None, filename=None, group=None, description=None):
        #: Handle to the persistent variables library of calico
        self.persistentVars = calico.manager["python"].engine.persistentVariables
        #: Name of the level
        self.name = name
        #: Name of the file of this level
        self.filename = filename
        #: Number of the stage of this level
        self.group = group
        #: Description of this level
        self.description = description
        #: The stage directory for this level
        if group is not None:
            self.stagedir = STAGE_DIR_PREFIX + str(group)
        else:
            self.stagedir = None
        #: The full path to the file for this level
        self.fullpath = None
        #: The index of this level in the overall list
        self.index = -1
        #: The level to unlock when this level is completed
        self.nextLevel = -1

    def getNextLevel(self):
        if self.nextLevel == -1 and self.filename:
            for i, levelInfo in enumerate(PLATFORMER_LEVELS):
                levelInfo.index = i
                levelInfo.nextLevel = i+2
        return self.nextLevel

    def getSourcePath(self):
        if "studentDir" in self.persistentVars and self.stagedir and self.filename:
            studentDir = self.persistentVars["studentDir"]
            return os.path.join(studentDir, STAGES_DIR, self.stagedir, self.filename)
        else:
            return None


    def getTargetPath(self):
        if self.fullpath is None:
            if "studentDir" in self.persistentVars and self.stagedir and self.filename:
                studentDir = self.persistentVars["studentDir"]
                self.fullpath = os.path.join(studentDir, USER_DATA_DIR, STAGES_DIR, self.stagedir, self.filename)
        return self.fullpath


PLATFORMER_LEVELS = [
    LevelInfo("Platforms", "level1.py", 1, "Add platforms to your game."),
    LevelInfo("Hazards", "level2.py", 1, "Add hazards to your game."),
    LevelInfo("Enemies", "level3.py", 1, "Add enemies to your game."),
    LevelInfo("Goals", "level4.py", 1, "Add a goal to your game."),
    LevelInfo("Larger platforms", "level5.py", 1, "Be more flexible with platforms of different sizes."),
    LevelInfo("Setting the player", "level6.py", 1, "Set the position of the player."),
    LevelInfo("Level 1", "level7.py", 1, "Create any level you want with the commands you just learned!"),
    LevelInfo("Creating variables", "level1.py", 2, "Create variables to make platforms appear."),
    LevelInfo("Changing variables", "level2.py", 2, "Change variables to move platforms."),
    LevelInfo("Math with variables", "level3.py", 2, "Use variables to calculate sums."),
    LevelInfo("For loops", "level4.py", 2, "Use for loops to add multiple crates to your game."),
    LevelInfo("For loops and variables", "level5.py", 2, "Use for loops and variables to add jumppads to your game."),
    LevelInfo("For loops, math, and variables", "level6.py", 2, "Combine for loops, variables, and math to create a stairway to the goal."),
    LevelInfo("Level 2", "level7.py", 2, "Create any level you want, now with more options!"),
    LevelInfo("Changing colors", "level1.py", 3, "Assign platforms to variables in order to change their color."),
    LevelInfo("Moving blades", "level2.py", 3, "Assign spinning blades to variables in order to make them move."),
    LevelInfo("Moving platforms", "level3.py", 3, "Assign platforms to variables in order to make them move."),
    LevelInfo("Patrolling enemies", "level4.py", 3, "Assign enemies to variables to make them patrol."),
    LevelInfo("Changing the player", "level5.py", 3, "Assign the player to a variable to change its attributes."),
    LevelInfo("Changing the game", "level6.py", 3, "Assign the game to a variable to change the world."),
    LevelInfo("Level 7", "level7.py", 3, "Create any level you want, now with even more options!"),
    LevelInfo("Level 1", "level1.py", 4, "To be created."),
    LevelInfo("Final", "final.py", 4, "A longer example level."),
    LevelInfo("Blades", "spikes.py", 4, "Example level with lots of saw-blades."),
    # LevelInfo("Level 1", "level1.py", 4, "To be created."),
    # LevelInfo("Level 2", "level2.py", 4, "To be created."),
    # LevelInfo("Level 3", "level3.py", 4, "To be created."),
    # LevelInfo("Level 4", "level4.py", 4, "To be created."),
    # LevelInfo("Level 5", "level5.py", 4, "To be created."),
    # LevelInfo("Level 6", "level6.py", 4, "To be created."),
    # LevelInfo("Level 7", "level7.py", 4, "To be created."),
]
