###################################
############ CONSTANTS ############
###################################
IDLE = 0
RUN = 1
JUMP = 2
NR_OF_COSTUMES = 3
IDLE_LEFT = IDLE + NR_OF_COSTUMES
RUN_LEFT = RUN + NR_OF_COSTUMES
JUMP_LEFT = JUMP + NR_OF_COSTUMES
NO_HIT = 0
LEFT = 1
RIGHT = 2
TOP = 3
BOTTOM = 4

# Constants
USER_DATA_DIR = "user"
USER_DATA_FILE = "PlatformerData"
STAGES_DIR = "stages"
STAGE_DIR_PREFIX = "stage"
