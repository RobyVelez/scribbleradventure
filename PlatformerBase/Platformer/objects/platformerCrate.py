from platformerSprite import PlatformerSprite
from Platformer.core import *

class Crate(PlatformerSprite):
    def __init__(self, x=0, y=0):
        PlatformerSprite.__init__(self, x, y, 50, 50)
        self.shape.bodyType = "dynamic"
        self.initFrames()


    def initFrames(self):
        self.addFrame("crate/crate.png")


    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.Friction=1
        
