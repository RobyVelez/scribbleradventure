from platformerObject import PlatformerObject

class Decor(PlatformerObject):
     def __init__(self, shape):
        PlatformerObject.__init__(self)
        self.shape = shape
        
     def init(self, game):
        PlatformerObject.init(self, game)
        game.draw(self.shape)
        
     def undraw(self):
        self.shape.undraw()