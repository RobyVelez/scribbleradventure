from Graphics import *

class PlatformerObject(object):
    def __init__(self):
        self.game = None
        self.tag = ""
        self.z = 0
        
    def init(self, game):
        self.game = game