from platformerObstacle import Obstacle
from Platformer.core import *

class Platform(Obstacle):
     def __init__(self, x, y, width=100, height=20, fill=Color("brown"), outline=Color("brown"), texture=None):
        Obstacle.__init__(self, makeRectangle(x=x, y=y, w=width, h=height, fill=fill, outline=outline), texture)