"""
This module holds all the user convience functions for creating a level.
"""
# Standard imports
import inspect

# Calico imports
from Graphics import *

# Portal window imports
from CommonFunctions.makeShapeFunctions import *

# Platformer imports
from Platformer.core import *
from Platformer.objects import *

# Global variables
localGame = None
localLevel = None
ALL_DOCUMENTED_FUNCTIONS = []

def setGame(game):
    global localGame
    localGame = game
    

def setLevel(level):
    global localLevel
    localLevel = level


def printFunctions():
    for function in ALL_DOCUMENTED_FUNCTIONS:
        argSpec = inspect.getargspec(function)
        functionStr = function.__name__
        functionStr += "("
        for i, arg in enumerate(argSpec.args):
            functionStr += arg
            if i < len(argSpec.args) -1:
                functionStr += ", "
        functionStr += ")"        
        print(functionStr)
        print(function.__doc__)
        

def addPlatform(x, y, width=100, height=20, fill=Color("brown"), outline=Color("brown"), texture=None):
    """
    Adds a stationary platform to the current level.
    
    Args:
        x: The horizontal position of the platform
        y: The vertical position of the platform
        width: The width of the platform
        height: The height of the platform
        fill: The internal color of the platform
        outline: The color of the border of the platform
        texture: The texture of the platform
    """
    global localLevel
    platform = Platform(x, y, width, height, fill=fill, outline=outline, texture=texture)
    localLevel.objects.append(platform)
    return platform
ALL_DOCUMENTED_FUNCTIONS.append(addPlatform)

    
def addRectangle(x, y, width=100, height=20, fill=Color("brown"), outline=Color("brown"), back=False):
    """
    Adds a rectangle to the current level.
    
    The rectangle is purely visual; the player and or monsters
    can simply walk straight throught the rectangle.
    
    Args:
        x: The horizontal position of the rectangle
        y: The vertical position of the rectangle
        width: The width of the rectangle
        height: The height of the rectangle
        fill: The internal color of the rectangle
        outline: The color of the border of the rectangle
        back: If set to True, the rectangle will be placed in
            the background.
    """
    global localLevel
    rec = Decor(makeRectangle(x=x, y=y, w=width, h=height, fill=fill, outline=outline))
    if back:
        localLevel.objects.insert(0, rec)
    else:
        localLevel.objects.append(rec)
    return rec
ALL_DOCUMENTED_FUNCTIONS.append(addRectangle)

    
def addHazard(x, y):
    """
    Adds a hazard (saw blade) to the current level.
        
    Args:
        x: The horizontal position of the hazard
        y: The vertical position of the hazard
    """
    global localLevel
    hazard = Hazard(x, y)
    localLevel.objects.append(hazard)
    return hazard
ALL_DOCUMENTED_FUNCTIONS.append(addHazard)

    
def addDecor(shape):
    """
    Adds decor to the current level.
    
    Takes any calico shape, a like Rectangle, Circle, Figure, or 
    Text object, and adds it to the level as a visual object. The 
    player and monsters will not be able to interact with it.
    
    Args:
        shape: The shape to be added to the level.
    """
    global localLevel
    decor = Decor(shape)
    localLevel.objects.append(decor)
    return decor
ALL_DOCUMENTED_FUNCTIONS.append(addDecor)

    
def addEnemy(x, y):
    """
    Adds an enemy (green round monster) to the current level.
        
    Args:
        x: The horizontal position of the enemy
        y: The vertical position of the enemy
    """
    global localLevel
    enemy = Enemy(x, y)
    localLevel.objects.append(enemy)
    return enemy
ALL_DOCUMENTED_FUNCTIONS.append(addEnemy)
    
    
def addGoal(x, y, width=50, height=50):
    """
    Adds an goal to the current level.
        
    Args:
        x: The horizontal position of the goal
        y: The vertical position of the goal
    """
    global localLevel
    goal = Goal(x, y, width, height)
    localLevel.objects.append(goal)
    return goal
ALL_DOCUMENTED_FUNCTIONS.append(addGoal)
 

def addPlayer(x, y):
    global localLevel
    player = Player(x, y)
    localLevel.player = player
    return player
   
    
def setPlayer(x, y):
    global localLevel
    if not localLevel.player:
        addPlayer(x,y)
    localLevel.player.moveTo(x,y)


def getPlayer():
    global localLevel
    return localLevel.player


def getLevel():
    global localLevel
    return localLevel


def getGame():
    global localGame
    return localGame
 
 
def addBomb(x, y):
    global localLevel
    bomb = Bomb(x, y)
    localLevel.objects.append(bomb)
    return bomb
    

def addJumppad(x, y):
    global localLevel
    jumppad = Jumppad(x, y)
    localLevel.objects.append(jumppad)
    return jumppad


def addCrate(x, y):
    global localLevel
    crate = Crate(x, y)
    localLevel.objects.append(crate)
    return crate
          
    
def addText(x, y, text, color=Color("black"), size=26, xjust="center", yjust="center"):
    global localLevel
    textObj = Decor(makeText(text, color=color, size=size, xjust=xjust, yjust=yjust, pos = (x,y)))
    localLevel.objects.append(textObj)
    return textObj
  
    
def addMovingPlatform(x, y, width=100, height=20, fill=Color("brown"), outline=Color("brown")):
    global localLevel
    platform = MovingPlatform(x, y, width, height, fill=fill, outline=outline)
    localLevel.objects.append(platform)
    return platform
    
    
def setBackground(colorOrImage):
    global localLevel
    localLevel.background = colorOrImage
    return colorOrImage
    
    
def addObject(platformerObject):
    global localLevel
    localLevel.objects.append(platformerObject)
    return platformerObject
