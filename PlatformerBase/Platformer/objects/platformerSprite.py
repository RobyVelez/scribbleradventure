#Perform imports
import copy
from Graphics import *
from Platformer.core import *
from platformerObject import PlatformerObject

class PlatformerSprite(PlatformerObject):
    def __init__(self, x=0, y=0, w=15, h=45, debug = False, shape = None):
        PlatformerObject.__init__(self)
        if shape:
            self.shape = shape
        else:
            self.shape = Rectangle((x, y), (x+w, y+h))
        self.shape.color = Color(0,0,0,0)
        if debug: self.shape.color = Color(0,0,0,125)
        self.points = self.shape.points
        self.costumes = []
        self.currentFrameId = 0
        self.currentCostume = 0
        self.currentFrame = LRC_Frame()
        #self.currentFrame.frame = self.body
        self.currentFrame.draw(self.shape)
        self.nextCostume = 0
        self.animateCounter = 0
        self.animateTime = 0
        self.costumes.append([])
        self.tag = ""
        self.physicsObject = True
        self.debug = debug
        
    def init(self, game):
        self.game = game
        game.draw(self.shape)
        if self.physicsObject:
            self.shape.bounce=0
            self.shape.addToPhysics()
            self.shape.body.LinearDamping = 0
            self.shape.body.Friction = 0
            self.shape.body.Mass = 1
            self.shape.body.FixedRotation = True
        
    def addFrame(self, filename, costumeId=0, flip=False, scale=1.0, pos=(0,0)):
        while costumeId >= len(self.costumes):
            self.costumes.append([])
        if isinstance(filename, str):
            frame = Picture(getImagePath(filename))
        else:
            frame = filename
        if flip: frame.flipHorizontal()
        if self.debug: frame.setAlpha(128)
        frame.border = 0
        frame.moveTo(pos[0],pos[1])
        frame.scaleTo(scale)
        if not self.currentFrame.frame:
            self.currentFrame.frame = frame
        self.costumes[costumeId].append(frame)

    def setFrames(self, filename, costumeId=0, flip=False, scale=1.0, pos=(0,0)):
        while costumeId >= len(self.costumes):
            self.costumes.append([])
        self.costumes[costumeId] = []
        if "%" in filename:
            splitname = filename.split("%")
            base = splitname[0]
            ext = splitname[1]
            start = None
            if getImagePath(base + "0" + ext):
                start = 0
            elif getImagePath(base + "1" + ext):
                start = 1
            if start is None:
                print("ERROR: Could not find: " + base + "1" + ext)
            while getImagePath(base + str(start) + ext):
                self.addFrame(base + str(start) + ext, costumeId, flip, scale, pos)
                start += 1
        else:
            self.addFrame(filename, costumeId, flip, scale, pos)
              
    def animate(self):
        self.animateCounter += 1
        if self.animateCounter > self.animateTime:
            self.currentCostume = self.nextCostume
            self.currentFrameId = (self.currentFrameId + 1) % len(self.costumes[self.currentCostume])
            self.animateCounter = 0
            self.currentFrame.frame = self.costumes[self.currentCostume][self.currentFrameId]
        
    def flip(self):
        self.currentFrame.flipped = not self.currentFrame.flipped
        
    def top(self):
        return self.shape.getP1().getY()
        
    def bottom(self):
        return self.shape.getP2().getY()
        
    def left(self):
        return self.shape.getP1().getX()
        
    def right(self):
        return self.shape.getP2().getX()
        
    def getP1(self):
        return self.shape.getP1()
        
    def getP2(self):
        return self.shape.getP2()
        
    def draw(self, shape):
        self.shape.draw(shape)
        
    def undraw(self):
        self.shape.undraw()
        
    def move(self, x, y):
        self.shape.move(x, y)

    def moveTo(self, x, y):
        self.shape.moveTo(x, y)
        
    def getX(self):
        return self.shape.getX()

    def getY(self):
        return self.shape.getY()

    def hide(self):
        self.shape.visible=False

    def show(self):
        self.shape.visible=True

    def isVisible(self):
        return self.shape.visible
##     def __copy__(self):
##         other = PlatformerSprite()
##         tmp = other.shape
##         other.__dict__.update(self.__dict__)
##         other.shape = tmp
##         other.shape.moveTo(self.shape.getX(), self.shape.getY())
##         return other
