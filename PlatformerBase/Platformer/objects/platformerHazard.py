from platformerSprite import PlatformerSprite
from Platformer.core import *


class Hazard(PlatformerSprite):
    def __init__(self, x, y, w=65, h=75):
        PlatformerSprite.__init__(self, x, y, shape=Circle((x,y), 45), debug=False)
        self.shape.bodyType = "static"
        self.initFrames()
        self.waypoints = [(x, y)]
        self.currentWaypointIndex = 0
        self.speed = 1


    def initFrames(self):
        for i in range(1, 4): self.addFrame("grinder/re_grinder_" + str(i) + ".png")
        
    def route(self, waypoints):
        self.waypoints = waypoints
        
    def addWaypoint(self, x, y):
        self.waypoints.append((x,y))
        
    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        self.shape.body.IsSensor = True
        
        
    def onCollision(self, myfixture, otherfixture, contact):
        if otherfixture.UserData == self.game.player.shape:
            self.game.lose()
        return True
       
    def update(self, game):
        self.animate()
        if len(self.waypoints) <= 0:
            return
        currentWaypoint = self.waypoints[self.currentWaypointIndex]
        target = Vector(currentWaypoint[0], currentWaypoint[1])
        current = Vector(self.shape.center.x, self.shape.center.y)
        direction = target - current
        if direction.Length() <= (self.speed + 0.0001):
            self.shape.moveTo(target.X,target.Y)
            self.currentWaypointIndex = (self.currentWaypointIndex + 1) % len(self.waypoints) 
        else:
            direction.Normalize()
            step = direction*self.speed
            self.shape.move(step.X,step.Y)