"""
All objects that can be added to the game.
"""
from platformerObject import PlatformerObject
from platformerSprite import PlatformerSprite
from platformerDecor import Decor
from platformerObstacle import Obstacle
from platformerBomb import Bomb
from platformerCrate import Crate
from platformerGoal import Goal
from platformerHazard import Hazard
from platformerPlatform import Platform
from platformerEnemy import Enemy
from platformerPlayer import Player
from platformerJumppad import Jumppad
from platformerMovingPlatform import MovingPlatform
from platformerPickup import Pickup
import platformerUserFunctions as pf
