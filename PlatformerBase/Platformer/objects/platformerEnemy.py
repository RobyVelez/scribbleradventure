from platformerSprite import PlatformerSprite
from Platformer.core import *


class Enemy(PlatformerSprite):
    def __init__(self, x, y):
        PlatformerSprite.__init__(self, x, y, 65, 65)
        self.shape.bodyType = "dynamic"
        self.initFrames()
        self.animateTime = 5
        self.speed = 5
        self.horizontalVelocity = self.speed
        self.patrol = None


    def setPatrol(self, x1, x2):
        self.patrol = (x1, x2)
        
        
    def setDirection(self, direction):
        if direction == LEFT:
            self.horizontalVelocity = self.speed
            self.currentFrame.flipped = True
        elif direction == RIGHT:
            self.horizontalVelocity = -self.speed
            self.currentFrame.flipped = False

    def initFrames(self):
        for i in range(1, 9): self.addFrame("enemy/re_frame_" + str(i) + ".png", pos=(0,-9))
        self.currentFrame.flipped = True
        
        
    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        
        
    def onCollision(self, myfixture, otherfixture, contact):
        if otherfixture.UserData == self.game.player.shape:
            direction = testCollDir(contact, myfixture.UserData)
            if direction == TOP:
                self.game.player.shape.body.LinearVelocity = Vector(0, -20)
            else:
                self.game.lose()
        else:
            direction = testCollDir(contact, myfixture.UserData)
            self.setDirection(direction)
        return True
        
        
    def update(self, game):
        self.animate()
        if self.patrol:
            if self.shape.getX() <= self.patrol[0]:
                self.horizontalVelocity = self.speed
                self.currentFrame.flipped = True
            elif self.shape.getX() >= self.patrol[1]:
                self.horizontalVelocity = -self.speed
                self.currentFrame.flipped = False
        verticalVelocity = self.shape.body.LinearVelocity.Y
        self.shape.body.LinearVelocity = Vector(self.horizontalVelocity, verticalVelocity)

        
        
    def __copy__(self):
        return Enemy(self.getX(), self.getY())
       
