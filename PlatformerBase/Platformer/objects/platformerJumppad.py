from platformerSprite import PlatformerSprite
from Platformer.core import *

class Jumppad(PlatformerSprite):
    def __init__(self, x=0, y=0):
        PlatformerSprite.__init__(self, x, y, 50, 20)
        self.shape.bodyType = "static"
        self.initFrames()


    def initFrames(self):
        self.addFrame("jumppad/jumppad.png")


    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        

    def onCollision(self, myfixture, otherfixture, contact):
        if otherfixture.UserData == self.game.player.shape:
            direction = testCollDir(contact, myfixture.UserData)
            if direction == TOP:
                self.game.player.shape.body.LinearVelocity = Vector(0, -20)
        return True
