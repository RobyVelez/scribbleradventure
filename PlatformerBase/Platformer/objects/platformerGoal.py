from platformerObstacle import Obstacle
from Platformer.core import *

class Goal(Obstacle):
    def __init__(self, x, y, width=50, height=50):
        Obstacle.__init__(self, makeRectangle(x=x, y=y, w=width, h=height, fill=Color("green"), outline=Color("green")))
        
        
    def init(self, game):
        Obstacle.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        self.shape.body.IsSensor = True
        
        
    def onCollision(self, myfixture, otherfixture, contact):
        if otherfixture.UserData == self.game.player.shape:
            self.game.win()
        return True
        
        
    def undraw(self):
        self.shape.undraw()
