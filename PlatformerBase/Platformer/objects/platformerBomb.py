from platformerSprite import PlatformerSprite
from Platformer.core import *

class Bomb(PlatformerSprite):
    def __init__(self, x=0, y=0):
        PlatformerSprite.__init__(self, x, y, 50, 50, debug=True)
        self.shape.bodyType = "dynamic"
        self.initFrames()

    def initFrames(self):
        for i in range(1, 9): self.addFrame("enemy/re_frame_" + str(i) + ".png", pos=(0,-9))

    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.Friction=1
        

    def __copy__(self):
        return Bomb(self.getX(), self.getY())
##         
##         self.gravity = 0.4
##         self.horizontalVelocity = 0
##         self.verticalVelocity = 0
##         self.onGround = False
##         
##         #Set previous positions
##         self.prevTop = self.top()
##         self.prevBottom = self.bottom()
##         self.prevLeft = self.left()
##         self.prevRight = self.right()
##         
##     def update(self, game):
##         for obstacle in game.obstacles:
##             if self.collides(obstacle):
##                 self.shunt(obstacle)
##         for sprite in game.sprites:
##             if sprite == self:
##                 continue
##             if self.collides(sprite):
##                 self.shunt(sprite)
##         if self.collides(game.player):
##             if game.player.hitTop(self):
##                 game.player.shunt(self)
##             else:
##                 self.shunt(game.player)
##         if not self.onGround:
##             self.verticalVelocity += self.gravity
##         self.prevTop = self.top()
##         self.prevBottom = self.bottom()
##         self.prevLeft = self.left()
##         self.prevRight = self.right()
##         self.body.move(self.horizontalVelocity, self.verticalVelocity)
##         self.onGround = False
##             
##     def collides(self, rectangle):
##         return collision(self.body, rectangle)
##                 
##     def shunt(self, rectangle):
##         if self.hitTop(rectangle):
##             self.body.setY(rectangle.getP1().getY() - self.body.height/2)
##             self.onGround = True
##             self.verticalVelocity = 0
##         elif self.hitBottom(rectangle):
##             self.body.setY(rectangle.getP2().getY() + self.body.height/2)
##             self.verticalVelocity = 0
##         elif self.hitLeft(rectangle):
##             self.body.setX(rectangle.getP1().getX() - self.body.width/2)
##         elif self.hitRight(rectangle):
##             self.body.setX(rectangle.getP2().getX() + self.body.width/2)
##             
##     def hitTop(self, rectangle):
##         return self.prevBottom <= rectangle.getP1().getY()
##         
##     def hitBottom(self, rectangle):
##         return self.prevTop >= rectangle.getP2().getY()
##         
##     def hitLeft(self, rectangle):
##         return self.prevLeft <= rectangle.getP1().getX()
##         
##     def hitRight(self, rectangle):
##         return self.prevRight >= rectangle.getP2().getX()
