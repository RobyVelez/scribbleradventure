from platformerObstacle import Obstacle
from Platformer.core import *

class MovingPlatform(Obstacle):
    def __init__(self, x, y, width=100, height=20, fill=Color("brown"), outline=Color("brown")):
        Obstacle.__init__(self, makeRectangle(x=x, y=y, w=width, h=height, fill=fill, outline=outline))
        #self.shape.bodyType = "dynamic"
        self.waypoints = [(x+width/2, y+height/2)]
        self.currentWaypointIndex = 0
        self.speed = 1
        self.playerOnTop = False
        self.playerContact = None
        
    def init(self, game):
        Obstacle.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        
    def route(self, waypoints):
        self.waypoints = waypoints
        
    def addWaypoint(self, x, y):
        self.waypoints.append((x, y))
        
    def update(self, game):
        if len(self.waypoints) <= 0:
            return
        currentWaypoint = self.waypoints[self.currentWaypointIndex]
        target = Vector(currentWaypoint[0], currentWaypoint[1])
        current = Vector(self.shape.center.x, self.shape.center.y)
        direction = target - current
        if direction.Length() <= (self.speed + 0.0001):
            self.shape.moveTo(target.X,target.Y)
            self.currentWaypointIndex = (self.currentWaypointIndex + 1) % len(self.waypoints) 
        else:
            direction.Normalize()
            step = direction*self.speed
            self.shape.move(step.X,step.Y)
            if self.playerOnTop:
                self.game.player.shape.move(step.X,step.Y)
        if not self.playerContact:
            self.playerOnTop = False
        elif not self.playerContact.IsTouching():
            self.playerOnTop = False
        
    def onCollision(self, myfixture, otherfixture, contact):
        if otherfixture.UserData == self.game.player.shape:
            direction = testCollDir(contact, myfixture.UserData)
            if direction == TOP:
                self.playerOnTop = True
                self.playerContact = contact
        return True