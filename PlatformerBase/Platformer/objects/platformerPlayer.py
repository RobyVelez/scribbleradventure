# Perform imports
from Platformer.core import *
from platformerSprite import PlatformerSprite

class Player(PlatformerSprite):
    def __init__(self, x=0, y=0, w=15, h=45, debug = False):
        PlatformerSprite.__init__(self, x, y, w, h, debug)
                
        #Set attributes that determine the properties of the player
        self.jumpHeight = 14
        self.speed = 10
        
        #Set book-keeping attributes
        self.horizontalVelocity = 0
        self.verticalVelocity = 0
        self.onGround = False
        self.facing = RIGHT
        
        self.initFrames()
       
        
    def initFrames(self):
        for i in range(1, 11): self.addFrame("adventurer/re_Idle_" + str(i) + ".png", IDLE)
        for i in range(1, 9): self.addFrame("adventurer/re_Run_" + str(i) + ".png", RUN)
        for i in range(1, 11): self.addFrame("adventurer/re_Jump_" + str(i) + ".png", JUMP)

    def update(self, game):
        testOnGround(self)
        
        # Check if dead
        if self.getY() > game.levelHeight:
            game.lose()
            return
    
        # Move the player
        self.horizontalVelocity = self.shape.body.LinearVelocity.X
        self.verticalVelocity = self.shape.body.LinearVelocity.Y
        if game.window.getKeyPressed('Up') and self.onGround:
            self.verticalVelocity = -self.jumpHeight
            self.onGround = False
            self.currentFrameId = 0
        if game.window.getKeyPressed('Left'):
            self.currentFrame.flipped = True
            self.horizontalVelocity = -self.speed
            if self.onGround: self.currentCostume = RUN
            else: self.currentCostume = JUMP
            self.facing = LEFT
        elif game.window.getKeyPressed('Right'):
            self.currentFrame.flipped = False
            self.horizontalVelocity = self.speed 
            if self.onGround: self.currentCostume = RUN
            else: self.currentCostume = JUMP
            self.facing = RIGHT
        elif self.facing == LEFT:
            self.currentFrame.flipped = True
            self.horizontalVelocity = 0
            if self.onGround: self.currentCostume = IDLE
            else: self.currentCostume = JUMP
        else:
            self.currentFrame.flipped = False
            self.horizontalVelocity = 0
            if self.onGround: self.currentCostume = IDLE
            else: self.currentCostume = JUMP

        # Update costume
        nbCostumes = len(self.costumes[self.currentCostume])
        if self.onGround:
            self.currentFrameId = (self.currentFrameId + 1) % nbCostumes
        else:
            self.currentFrameId = min(max(int(self.horizontalVelocity + self.jumpHeight), 0), nbCostumes-1)
        self.currentFrame.frame = self.costumes[self.currentCostume][self.currentFrameId]
        
        # Set velocity
        self.shape.body.LinearVelocity = Vector(self.horizontalVelocity, self.verticalVelocity)
        

    # def __copy__(self):
    #     other = Player()
    #     tmp = other.shape
    #     other.__dict__.update(self.__dict__)
    #     other.shape = tmp
    #     other.shape.moveTo(self.shape.getX(), self.shape.getY())
    #     return other
