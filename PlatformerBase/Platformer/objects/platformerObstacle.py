from platformerObject import PlatformerObject
from Platformer.core import *
import traceback
import Cairo
import math


class Texture(Shape):
    def __init__(self):
        self.texture = None
        self.localPoints = None
        
    def setup(self, path, p1, p2):
        surface = Cairo.ImageSurface(getImagePath(path))
        self.texture = Cairo.SurfacePattern(surface)
        self.texture.Filter=Cairo.Filter.Fast
        self.texture.Extend = Cairo.Extend.Repeat
        m=Cairo.Matrix()
        m.Translate(-p1.x, -p1.y)
        self.texture.Matrix = m
        self.localPoints = [(p1.x,p1.y), (p2.x,p1.y), (p2.x,p2.y), (p1.x,p2.y)]

    def render(self, context):
        try:
            context.Save()
            context.LineWidth = 10
            context.LineCap = Cairo.LineCap.Round
            
            # Draw fill
            context.NewPath()
            p = self.localPoints[0]
            context.MoveTo(p[0], p[1])
            for i in xrange(len(self.localPoints)):
                p = self.localPoints[(i+1)%len(self.localPoints)]  
                context.LineTo(p[0], p[1])
            context.Pattern = self.texture
            context.Fill()
            context.Restore()
        except:
            print(traceback.format_exc())


class Obstacle(PlatformerObject):
     def __init__(self, shape, texturePath=None):
        PlatformerObject.__init__(self)
        self.shape = shape
        self.shape.bodyType = "static"
        self.shape.bounce=0
        #p1 = self.shape.getP1()
        #p2 = self.shape.getP2()
        self.texture=None
        if texturePath:
            self.texture = Texture()
            self.texture.setup(texturePath, self.shape.getP1(), self.shape.getP2())
        #self.texture.localPoints = [(p1.x,p1.y), (p2.x,p1.y), (p2.x,p2.y), (p1.x,p2.y)]
        #print(self.texture.localPoints)
        
     def init(self, game):
        PlatformerObject.init(self, game)
        game.draw(self.shape)
        if self.texture:
            game.addDecor(self.texture)
            self.shape.visible=False
        self.shape.addToPhysics()
        self.shape.body.LinearDamping = 0
        self.shape.body.Friction = 1
        self.shape.body.Mass = 1
        self.shape.body.FixedRotation = True
        
     def undraw(self):
        self.shape.undraw()
        
     def setColor(self, *args):
        self.shape.color = Color(*args)
