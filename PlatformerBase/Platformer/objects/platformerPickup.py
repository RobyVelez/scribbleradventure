from platformerSprite import PlatformerSprite
from Platformer.core import *


class Pickup(PlatformerSprite):
    def __init__(self, x, y, w=50, h=50):
        PlatformerSprite.__init__(self, x, y, shape=Circle((x,y), 45), debug=False)
        self.shape.bodyType = "static"
        self.initFrames()


    def initFrames(self):
        self.setFrames("pickup/s.png")

        
    def init(self, game):
        PlatformerSprite.init(self, game)
        self.shape.body.OnCollision += self.onCollision
        self.shape.body.IsSensor = True
        
        
    def onCollision(self, myfixture, otherfixture, contact):
        if not self.isVisible: return
        if otherfixture.UserData == self.game.player.shape:
            self.onPickup(self.game.player)
        return True
       

    def onPickup(self, player):
        pass


    def update(self, game):
        self.animate()
