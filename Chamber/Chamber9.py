import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
    
class chamber9Window(LevelWindow):
    def __init__(self,gameData,parent):        
        self.rows=[]
        self.portal=[]
        self.endLocations=[0,1,2,3]
        self.ans=[]
        self.maxRows=6
        
        lastChoice=None
        for i in range(self.maxRows):
            c=choice(self.endLocations)
            
            if lastChoice!=None:
                while c==lastChoice:
                    c=choice(self.endLocations)
            lastChoice=c
            self.ans.append(c)    
            
        print(self.ans)
        
        self.doorSwitch=None
        self.door=None
        
        self.endTestEvent=None
        self.testTime=100
        self.timer=None
        
        self.testPad=[]
        self.beforeTest=True        
        self.changeWalls=0
        
        LevelWindow.__init__(self,"Chamber9",gameData=gameData,parent=parent,debug=False)        
        self.setup()
        
    def liftDoor(self):
        self.door.move(0,-5)
        if self.door.y<-50:
            self.setGameOver(1)
            return False
        else:
            return True
            
    #Functions that control behavior of the testKeyPads
    
    
    def hitKeyPad(self):
        self.pauseRobot(True)
        #for t in self.testPad:
        #    t.setTestCase(choice(self.endLocations))
        
    def startTest(self):    
        #lock in correct test cases
        #for t in self.testPad:
        #    t.setTestCase(choice(self.endLocations))
        self.beforeTest=False
        
        #start test timer
        self.pauseRobot(True)
        self.endTestEvent=TimedEvent(self.testTime,self.endTest)
        self.addStepEvent(self.endTestEvent)
        self.timer.run=True
        
    def wrongCode(self):
        self.setGameOver(-1)    
    def correctCode(self):
        self.pauseRobot(False)
        
    #Functions that control non test pad events in the level
    def endTest(self):
        self.setGameOver(-1)
            
    def runDisplay(self):
        if self.timer.run:
            self.timer.updateTimerDisplay(self.endTestEvent)
        return True
    def preTest(self):
        if self.beforeTest:        
            if self.changeWalls==0:
                self.changeWalls=35
                for t in self.testPad:
                    t.setTestCase(choice(self.endLocations))
            else:
                self.changeWalls-=1
                  
            return True
        else:
            return False    
                    
    
    
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Use getIR() to enter correct security codes.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Drive to the orange mark and do leftIR,rightIR=getIR() to read you're ",self.getR("getIR()")," sensor."])
        text.append(["Use ",self.getR("if")," statements to check the values of leftIR and rightIR in order to ",self.getR("use()")," the right code."])
        text.append(["For example: \nbackward(4,1)\nleftIR,rightIR=getIR()\nif leftIR==0 and rightIR==0:\n     use(0)\nelif leftIR==0 and rightIR==1:\n     use(1)"])
        
        b=simpleBrief(text,[self.getR("getIR()"),self.getR("if"),self.getR("compare"),self.getR("use()")])                    
        self.addBriefShape(b,"Use getIR() to enter correct key code.")
        
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(scribbyPort, "Wait another set of security?")
        self.addResponse(curiosityPort, "Yeah, but it looks like this one needs to you use you're getLine() sensor.")
        self.addResponse(scribbyPort, "getLine() sensor?")
        self.addResponse(curiosityPort, "You have a sensor underneath you're chasis, halfway betwen your center and back. If you look closely you should be able to see two small lines which indicate where the sensors are.",lambda:setOption("show-sensors",True))
        self.addResponse(curiosityPort, "You can use getLine() the same way you use getIR(). For example leftLine,rightLine=getLine().")
        self.addResponse(curiosityPort, "Look at the values in leftLine and leftRight to enter the correct key code. For example if leftLine==0 and rightLine=0: use(0)",lambda:self.launchBriefScreen(0))
        self.addResponse(curiosityPort, " ",self.endConvo)
        
             
    def onLevelStart(self):
       self.helpWidgets[3].setButtonsInvisible(["getIR()","getLine()"]) 
       self.buildBriefings()
       self.pauseRobot(True)
       self.buildConvo()
       talk()
       
       self.addStepEvent(self.runDisplay)
        
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createStartPad(350,650)
        
        
        self.addObstacle(Rectangle((0,100),(275,600)))
        self.addObstacle(Rectangle((375,100),(700,600)))
        
        self.curiosity=self.quickActor(300,650,IMAGE_PATH+"curiosityPortrait.png")
        
        
        for i in range(5):
            if i==0:
                self.testPad.append(TestKeyPad((350,550-i*100),touchAction=self.startTest,wrongCodeAction=self.wrongCode,testType=1))
            else:            
                self.testPad.append(TestKeyPad((350,550-i*100),touchAction=self.hitKeyPad,wrongCodeAction=self.wrongCode,testType=1))
        self.addStepEvent(self.preTest)
        self.timer=TimerDisplay((650,50))
        
        #create the robot
        self.createRobot(350,650,90)
        
        
        self.addObstacle(Text((150,575),"0 0\n  0",color=Color("black"),fontSize=12))
        self.addObstacle(Text((200,575),"0 1\n  1",color=Color("black"),fontSize=12))
        self.addObstacle(Text((250,575),"1 0\n  2",color=Color("black"),fontSize=12))
        self.addObstacle(Text((300,575),"1 1\n  3",color=Color("black"),fontSize=12))
        
def startLevel(gameData=None, parent=None):
    level=chamber9Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

