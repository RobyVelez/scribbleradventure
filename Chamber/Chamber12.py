import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.curiosity=None
        self.sojourner=None
        LevelWindow.__init__(self,"Chamber12",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
          
           
       
    def endConvo(self):
        self.setGameOver(1)
        
        #self.pauseRobot(False)
        #self.launchBriefScreen(0)
        #self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup the sample with sample=pickup()."])
        
        text.append(["Check the contents of the sample with if statments."])
        text.append(["If the sample is 0 drive into the left most test area, if it is something else drive into right most test area."])
        text.append(["Here's an example: \nforward(4,1)\nsample=pickup()\nif sample==0:\n     print('foo')\nelse:\n     print('bar')"])
        text.append(["See ",self.getI("if")," page for more information on if statments."])
        
        b=simpleBrief(text,[self.getR("if")])                    
        self.addBriefShape(b,"Pickup sample & use 'if' to see what it is")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        computerPort=IMAGE_PATH+"computerTerminal.png"
        
        self.addResponse(sojournerPort, "Pathfinder! This has gone to far. Stop this now.")
        self.addResponse(pathfinderPort, "Sojourner. You've returned and you've brought Curiosity and... and Scribby.")
        self.addResponse(pathfinderPort, "So that was you in the test chamber. I should have known. I was never able to create a clone as good as the original.")
        self.addResponse(pathfinderPort, "My family. Here with me at my moment of success.")
        self.addResponse(curiosityPort, "We are not you're family. You are a crazy old robot and we're here to stop you.")
        self.addResponse(pathfinderPort, "Stop me? Why? I only wish to create a perfect robot. A robot that could defend us all robots.")
        self.addResponse(sojournerPort, "By defend us you mean destroy the humans. We can't let you do that.")
        self.addResponse(pathfinderPort, "My dear Sojourner. So lost and confused. Do not worry it will be ok.")
        self.addResponse(pathfinderPort, "Goodbye my family. When you see me again I will be reborn.")
        self.addResponse(computerPort, "Upload complete.")
        self.addResponse(curiosityPort, "What was that?")
        self.addResponse(sojournerPort, "We are too late. He's already uploaded himself into the robot... the Scribby clone.")
        self.addResponse(scribbyPort, "Someone needs to explain to me what's going on and now.")
        self.addResponse(sojournerPort, "Pathfinder was going to use you as a weapon. It's part of the reason I hide you all from him.")
        self.addResponse(sojournerPort, "These past years he's been trying to recreate you and I think he's succeeded.")
        self.addResponse(sojournerPort, "And because creating a unique Artificial Intelligence is difficult he downloaded his own conscienous into the clone.")
        self.addResponse(curiosityPort, "Do you know where the clone is now?")
        self.addResponse(sojournerPort, "No, I don't.")
        self.addResponse(scribbyPort, "I... I was meant to be a weapon?... to destroy? That... that can't be...")
        self.addResponse(curiosityPort, "Oh, Scribby.")
        self.addResponse(sojournerPort, "Let's get out of here.",lambda:self.setGameOver(1))
 
         
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        talk()
        
        
    def createLevel(self):
        
        #glorified event pads
        #self.createEndPad(350,50)
        #self.createEndPad(650,450)
        
        self.createStartPad(325,450)
        
        #create the robot
        self.createRobot(325,450,-90)
        
        self.quickActor(150,275,IMAGE_PATH+"computerTerminal.png",scale=0.25)
        self.pathfinder=self.quickActor(150,350,IMAGE_PATH+"pathfinderPortrait.png",scale=0.75)
        self.curiosity=self.quickActor(375,450,IMAGE_PATH+"curiosityPortrait.png")
        self.sojourner=self.quickActor(350,400,IMAGE_PATH+"sojournerPortrait.png")
        
        self.addObstacle(Rectangle((0,0),(700,200)))
        self.addObstacle(Rectangle((0,500),(700,700)))
        
        
        
        
def startLevel(gameData=None, parent=None):
    level=chamber1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()
                                         

