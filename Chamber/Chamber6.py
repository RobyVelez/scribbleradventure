import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber6Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.pathfinder=None
        self.trapDoor=[]     
        self.mask=None  
        
        self.roundNum=0
        self.testTime=100 #number of simulation ticks
        self.coolDown=10 #number of ticks before next test
        
        self.maxRounds=6  
        self.ans=[]
        
        #self.endLocations=[0,1,2,3,4]
        self.sample=[]        
        self.endLocations=[0,1,2,3,4]
        
        self.equalValues=[[0,0,1,1,TrollCompare()],[0,1,0,1,TrollCompare()]]
        #self.displayValues=["if s1==0 and s2==0:","elif s1==0 and s2==1:","elif s1==1 and s2==0:","elif s1==1 and s2==1 ","else:"]
        self.displayValues=["if sample1==0 and sample2==0:","elif sample1==0 and sample2==1:","elif sample1==1 and sample2==0:","elif sample1==1 and sample2==1:","else:"]
        
        
        self.testDisplayText=[]
        
        for i in range(self.maxRounds):
            self.ans.append(choice(self.endLocations))
            
        #print(self.ans)
        
        self.startTestEvent=None
        self.endTestEvent=None
        self.timerDisplay=None
        
        #test chamber decorations
        self.testNum=5
        self.sampleText=[]
        
        self.vent=None
        self.endPad=None
        
        self.testIndicators=[]
        
        LevelWindow.__init__(self,"Chamber6",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
    def updateTimerDisplay(self):
        s=None
        if self.resetEvent:            
            s=str(self.resetEvent.ticks)            
        elif self.evaluateEvent:
            s=str(self.evaluateEvent.ticks)
        
        if s:    
            if len(s)==1:
                s="0"+s
            #add a decimal to the number             
            d=s[:-1]+"."+s[-1:]
            self.timerDisplay.setText(d)
        
        return True
        
    def teleportStep(self):
        angle=-90
        destination=350,650
        robot = getRobot()
        robot.stop()
        
        #The contacts do not like the robot suddenly teleporting. Clear all of them (they will be recreated).
        del self.interactContactList[:]
        del self.pickupContactList[:]
        #del self.levelWindow.talkContactList[:]
        robot.setPose(destination[0], destination[1], angle)
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
        return False
   
    def createSample(self):
        val=None
        for i in range(len(self.sample)):
        
            self.sample[i].show()
            ansIndex=self.endLocations.index(self.ans[self.roundNum]) 
            eVal=self.equalValues[i][ansIndex]
        
            #is a function        
            if hasattr(eVal, '__call__'):
                self.sample[i].returned=eVal()
            else:
                self.sample[i].returned=eVal
        
    def evaluateTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)        
        
        if self.roundNum<self.maxRounds:
            print("Test results")
            
            #opens trap doors.
            for i in range(len(self.trapDoor)):
                if i!=self.ans[self.roundNum]:                    
                    self.trapDoor[i].move(0,100)  
            
            #sees if Scribby in is test area.
            #if he isn't game over man
            if self.robots[0].frame.y<300 or self.robots[0].frame.y>400:
                self.setGameOver(-1)              
                
            #not dead    
            if self.gameOver==0:
                self.testIndicators[self.roundNum].fill=Color("green")
                
                if self.roundNum>=self.maxRounds:
                    
                    print("Error. roundNum is bigger than maxRounds for some reason.")
                elif self.roundNum==self.maxRounds-1:
                    self.lastConvo()
                    talk()
                    #self.setGameOver(1)
                else:
                    self.roundNum+=1
                    self.resetEvent=TimedEvent(self.coolDown,self.resetTest)
                    self.addStepEvent(self.resetEvent)
            
    
    def startTest(self):
        self.evaluateEvent=TimedEvent(self.testTime,self.evaluateTest)
        self.addStepEvent(self.evaluateEvent)
            
    def resetTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)
        
        #set the display timer
        s=str(self.testTime)
        if len(s)==1:
            s="0"+s
        self.timerDisplay.setText(s[:-1]+"."+s[-1:])
            
        self.createSample()
        
        #teleports the robot back to the start location
        self.addStepEvent(self.teleportStep) 
        
        #resets the trapdoors
        for i in range(len(self.trapDoor)):
            origX=self.trapDoor[i].getX()
            self.trapDoor[i].moveTo(origX,250)
        
        #test starts as soon as robot movoes
        self.addStepEvent(OnMoveEvent(self.startTest))
        
        
    def hidePathfinder(self):
        self.pathfinder.hide()
        self.curiosity.show()
        self.vent.visible=True
        
    def lastConvo(self):
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        alertPort=IMAGE_PATH+"alertLight.png"
        
        self.addResponse(pathfinderPort, "You have done well in this test. You should be proud to know you are the last in a fine line of clones and your sacrifice was for the greater good.")
        self.addResponse(pathfinderPort, "But things must come to an end. It's a shame I must send you to the incinerator, but there's no reason to leave random clones around.")
        self.addResponse(alertPort, "Intruder alert! Intruder alert!")
        self.addResponse(pathfinderPort, "What is this. Hmm, it seems as though they are here to stop me. There's no time to spare. I must go and complete the upload.",self.hidePathfinder)
        self.addResponse(curiosityPort, "Scribby! Hey over here. He's gone. Let's get out of here.")
        self.addResponse(curiosityPort, " ",lambda:self.setGameOver(1))
        
    
    def endConvo(self):
        self.pauseRobot(False)
        self.resetTest()
        self.addStepEvent(self.updateTimerDisplay)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
    
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup both samples and save them as different variables."])
        
        text.append(["Check to see if both sample1==0 and sample2==0."])
        text.append(["See the ",self.getI("compare")," page for info on how to use 'and'."])
        b=simpleBrief(text,[self.getR("compare"),self.getR("if")])                    
        self.addBriefShape(b,"Use 'and' to compare both samples")
    
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(pathfinderPort, "Unfortunately it's time to wrap this up. The upload will soon be ready and I will have to go.")
        self.addResponse(pathfinderPort, "In this last test you will be checking the contents of the samples against all possible combinations.")
        self.addResponse(pathfinderPort, "You can begin when you are ready.",lambda:self.launchBriefScreen(0))
        self.addResponse(pathfinderPort, " ", self.endConvo)
        
        
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        self.vent.removeFromPhysics()
        for t in self.testDisplayText:
            t.removeFromPhysics()
        talk()
 
    def hideText(self,items):
        self.sampleText[0].visible=False
    def createLevel(self):
        
        #glorified event pads
        self.endPad=self.createEndPad(50,650)
        self.endPad.hide()
        self.createStartPad(350,650)
        
        
    
        #main area
        self.addObstacle(Rectangle((95,95),(100,700)))
        self.addObstacle(Rectangle((595,95),(600,700)))
        self.addObstacle(Rectangle((95,95),(600,100)))        
        mirror=self.addObstacle(Rectangle((265,95),(425,100),color=Color("silver")))        
        mirror.setWidth(2)
        mirror.outline=Color("black")        
        self.addObstacle(Rectangle((95,295),(600,300)))
        
        #pathfinder and his office/viewing room
        self.addObstacle(Rectangle((100,195),(200,200)))
        self.addObstacle(Rectangle((195,100),(200,200)))
        #window to office
        window=self.addObstacle(Rectangle((115,195),(185,200),color=Color("gray")))        
        #window.fill.alpha=0
        window.setWidth(1)
        window.outline=Color("black")        
        
        
        #door to office
        door=self.addObstacle(Rectangle((90,125),(105,175),color=Color("gray")))
        door.setWidth(0)        
        
        self.curiosity=self.quickActor(50,600,IMAGE_PATH+"curiosityPortrait.png",visible=False)
        self.vent=self.addObstacle(Rectangle((90,575),(105,625),color=Color("gray")))
        self.vent.setWidth(0)
        self.vent.visible=False
        self.pathfinder=self.quickActor(150,150,IMAGE_PATH+"pathfinderPortrait.png",scale=0.75,visible=True)
       
        
        #timer display
        self.addObstacle(Rectangle((495,100),(500,150)))
        self.addObstacle(Rectangle((495,145),(595,150)))        
        self.timerDisplay=self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right"))
        
        #sign with test number on it
        self.addObstacle(Text((200,110),"Test "+str(self.testNum),color=Color("black"),fontSize=18,xJustification="left"))
        
        
        
        
        #trapdoors and mask
        for i in range(5):
            self.trapDoor.append(self.quickActor(150+i*100,250,Rectangle((155+i*100,255),(245+i*100,345),color=Color("black")),obstructs=False,collision=self.deathCollide))
        self.mask=self.quickActor(349,248,Rectangle((145,202),(635,295),color=Color("gray")))
        
        #sample object(s)
        #self.sample.append(self.quickActor(350,550,IMAGE_PATH+"unknownSample1.png",pickup=True,scale=0.25,description=-1))
        #self.sample.append(self.quickActor(350,450,IMAGE_PATH+"unknownSample2.png",pickup=True,scale=0.25,description=-1))
        self.sample.append(Sample(350,550,IMAGE_PATH+"unknownSample1.png",scale=0.25,code=-1))
        self.sample.append(Sample(350,450,IMAGE_PATH+"unknownSample2.png",scale=0.25,code=-1))
        for s in self.sample:
            self.addActor(s)
            
        #test spaces
        self.addObstacle(Rectangle((195,295),(200,400)))
        self.addObstacle(Rectangle((295,295),(300,400)))
        self.addObstacle(Rectangle((395,295),(400,400)))
        self.addObstacle(Rectangle((495,295),(500,400)))
        #text above the test spaces/boxes
        for i in range(5):
            if i in self.endLocations:
                #self.testDisplayText.append(self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right")))
                self.testDisplayText.append(self.addObstacle(Text((105+i*100,305),self.displayValues[i],color=Color("black"),xJustification="left",fontSize=12)))
                self.testDisplayText[-1].rotation=50
        
               
        #create the robot
        self.createRobot(350,650,-90)        

        for i in range(self.maxRounds):
            self.testIndicators.append(self.addObstacle(Circle((210+i*15,125),5,color=Color("red"))))
            self.testIndicators[-1].setWidth(0)    


def startLevel(gameData=None, parent=None):
    level=chamber6Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

                                         

