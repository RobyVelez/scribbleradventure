import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
    
class chamber8Window(LevelWindow):
    def __init__(self,gameData,parent):        
        self.rows=[]
        self.portal=[]
        self.endLocations=[0,1,2,3]
        self.ans=[]
        self.maxRows=6
        
        lastChoice=None
        for i in range(self.maxRows):
            c=choice(self.endLocations)
            
            if lastChoice!=None:
                while c==lastChoice:
                    c=choice(self.endLocations)
            lastChoice=c
            self.ans.append(c)    
            
        #print(self.ans)
        
        self.doorSwitch=None
        self.door=None
        
        self.endTestEvent=None
        self.testTime=100
        self.timer=None
        
        self.testPad=[]
        self.beforeTest=True        
        self.changeWalls=0
        
        LevelWindow.__init__(self,"Chamber8",gameData=gameData,parent=parent,debug=False)        
        self.setup()
        
    def liftDoor(self):
        self.door.move(0,-5)
        if self.door.y<-50:
            self.setGameOver(1)
            return False
        else:
            return True
            
    #Functions that control behavior of the testKeyPads
    
    
    def hitKeyPad(self):
        self.pauseRobot(True)
        #for t in self.testPad:
        #    t.setTestCase(choice(self.endLocations))
        
    def startTest(self):    
        #lock in correct test cases
        #for t in self.testPad:
        #    t.setTestCase(choice(self.endLocations))
        self.beforeTest=False
        
        #start test timer
        self.pauseRobot(True)
        self.endTestEvent=TimedEvent(self.testTime,self.endTest)
        self.addStepEvent(self.endTestEvent)
        self.timer.run=True
        
    def wrongCode(self):
        self.setGameOver(-1)    
    def correctCode(self):
        self.pauseRobot(False)
        
    #Functions that control non test pad events in the level
    def endTest(self):
        self.setGameOver(-1)
            
    def runDisplay(self):
        if self.timer.run:
            self.timer.updateTimerDisplay(self.endTestEvent)
        return True
    def preTest(self):
        if self.beforeTest:        
            if self.changeWalls==0:
                self.changeWalls=35
                for t in self.testPad:
                    t.setTestCase(choice(self.endLocations))
            else:
                self.changeWalls-=1
                  
            return True
        else:
            return False    
                    
    
    
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Use getIR() to enter correct security codes.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Drive to the orange mark and do leftIR,rightIR=getIR() to read you're ",self.getR("getIR()")," sensor."])
        text.append(["Use ",self.getR("if")," statements to check the values of leftIR and rightIR in order to ",self.getR("use()")," the right code."])
        text.append(["For example: \nbackward(4,1)\nleftIR,rightIR=getIR()\nif leftIR==0 and rightIR==0:\n     use(0)\nelif leftIR==0 and rightIR==1:\n     use(1)"])
        
        b=simpleBrief(text,[self.getR("getIR()"),self.getR("if"),self.getR("compare"),self.getR("use()")])                    
        self.addBriefShape(b,"Use getIR() to enter correct key code.")
        
    def turnRobot(self):
        self.robots[0].frame.rotateTo(90)
        
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(scribbyPort, "What is this?")
        self.addResponse(curiosityPort, "It looks like a bunch of security doors. We have to get pass them to get to Pathfinder's office.")
        self.addResponse(scribbyPort, "How?")
        self.addResponse(curiosityPort, "I think you can use you're IR sensor. Here I'll turn on the visualization so you can see them better.",lambda:setOption("show-sensors",True))
        self.addResponse(curiosityPort, "Those are all of you're sensors. The ones on your back are IR sensors and can detect the presence of walls.")
        self.addResponse(curiosityPort, "You'll have to drive backwards. Here I'll turn you around.",self.turnRobot)
        self.addResponse(scribbyPort, "I have to drive backwards?")        
        
        self.addResponse(curiosityPort, "Yes. Now when you drive onto the red marks you'll be locked in place. Use your IR sensors. Do leftIR,rightIR=getIR()",lambda:self.helpWidgets[3].setButtonsVisible(["getIR()"],8))
        self.addResponse(curiosityPort, "Then check the values of leftIR and rightIR like you did before. For example if left==0 and right==0 I think you need to do a use(0) to enter '0' into the keypad.")
        self.addResponse(curiosityPort, "It looks like Pathfinder left himself a note on the wall here. I believe you're sensor values correspond to the correct code.")
        
        self.addResponse(scribbyPort, "I don't know.")
        self.addResponse(curiosityPort, "Just remember what you did in the previous test with sample. Instead of sample1=pickup() and sample2=pickup() you're going to do leftIR,rightIR=getIR()")
        self.addResponse(curiosityPort, "And be careful. You'll have limited time, and if you enter the wrong code you'll be scrapped.")
        self.addResponse(scribbyPort, "Alright.",lambda:self.launchBriefScreen(0))
        self.addResponse(scribbyPort, " ",self.endConvo)
        
        
    def onLevelStart(self):
       self.helpWidgets[3].setButtonsInvisible(["getIR()","getLine()"]) 
       self.buildBriefings()
       self.pauseRobot(True)
       self.buildConvo()
       talk()
       
       self.addStepEvent(self.runDisplay)
        
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createStartPad(350,650)
        
        #create the robot
        self.createRobot(350,650,-90)
        
        self.addObstacle(Rectangle((0,100),(275,600)))
        self.addObstacle(Rectangle((375,100),(700,600)))
        
        self.curiosity=self.quickActor(300,650,IMAGE_PATH+"curiosityPortrait.png")
        
        
        for i in range(5):
            if i==0:
                self.testPad.append(TestKeyPad((350,550-i*100),touchAction=self.startTest,wrongCodeAction=self.wrongCode))
            else:            
                self.testPad.append(TestKeyPad((350,550-i*100),touchAction=self.hitKeyPad,wrongCodeAction=self.wrongCode))
        self.addStepEvent(self.preTest)
        self.timer=TimerDisplay((650,50))
        
        self.addObstacle(Text((150,575),"0 0\n  0",color=Color("black"),fontSize=12))
        self.addObstacle(Text((200,575),"0 1\n  1",color=Color("black"),fontSize=12))
        self.addObstacle(Text((250,575),"1 0\n  2",color=Color("black"),fontSize=12))
        self.addObstacle(Text((300,575),"1 1\n  3",color=Color("black"),fontSize=12))
        
def startLevel(gameData=None, parent=None):
    level=chamber8Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

