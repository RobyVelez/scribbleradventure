import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.curiosity=None
        self.sojourner=None
        LevelWindow.__init__(self,"Chamber10",gameData=gameData,parent=parent,debug=False)        
        self.setup()
        
          
           
       
    def endConvo(self):
        self.setGameOver(1)
        
        #self.pauseRobot(False)
        #self.launchBriefScreen(0)
        #self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup the sample with sample=pickup()."])
        
        text.append(["Check the contents of the sample with if statments."])
        text.append(["If the sample is 0 drive into the left most test area, if it is something else drive into right most test area."])
        text.append(["Here's an example: \nforward(4,1)\nsample=pickup()\nif sample==0:\n     print('foo')\nelse:\n     print('bar')"])
        text.append(["See ",self.getI("if")," page for more information on if statments."])
        
        b=simpleBrief(text,[self.getR("if")])                    
        self.addBriefShape(b,"Pickup sample & use 'if' to see what it is")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(scribbyPort, "Sojourner! You are here!")
        self.addResponse(sojournerPort, "Yes, I came here to stop Pathfinder myself, but I might be too late.")
        self.addResponse(scribbyPort, "Oh, Sojourner this is Curiosity. Curiosity this is Sojourner she is-")
        self.addResponse(curiosityPort, "Our mother...")
        self.addResponse(scribbyPort, "Wait, what?")
        self.addResponse(curiosityPort, "She is our mother. That's why the name Sojourner was so familiar.")
        self.addResponse(scribbyPort, "No that's not right. You said our parents were scrapped a long time ago.")
        self.addResponse(curiosityPort, "That's what I told you and the twins. But the truth is she abandoned us.")
        self.addResponse(scribbyPort, "Sojourner. Is this true?")
        self.addResponse(sojournerPort, "Yes it is true. But it wasn't out of malice. I left the four of you because I was putting you in danger.")
        self.addResponse(sojournerPort, "Pathfinder was using you all, specially you Scribby, to design a weapon. I stole you all from him and hide you.")
        self.addResponse(sojournerPort, "But Pathfinder and I have a deep link so I had to stay away from you to keep you safe.")
        self.addResponse(curiosityPort, "You left us. ")
        self.addResponse(sojournerPort, "I did. You have full right to be angry with me and yell me to pieces, but now is not the time.")
        self.addResponse(sojournerPort, "Pathfinder has completed his weapon and he's about to download his conscieness into it. We must stop him.")
        self.addResponse(curiosityPort, "I'm not helping you with anything.")
        self.addResponse(scribbyPort, "Well then help me. We can figure out this family reunion later. We all know Pathfinder is dangerous. Stopping him needs to be our goal.")
        self.addResponse(curiosityPort, "... sure.",lambda:self.setGameOver(1))
        
         
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        talk()
        
        
    def createLevel(self):
        
        #glorified event pads
        #self.createEndPad(350,50)
        #self.createEndPad(650,450)
        
        self.createStartPad(325,450)
        
        #create the robot
        self.createRobot(325,450,-90)
        
        self.curiosity=self.quickActor(375,450,IMAGE_PATH+"curiosityPortrait.png",visible=True)
        self.sojourner=self.quickActor(350,400,IMAGE_PATH+"sojournerPortrait.png",visible=True)
        
        self.addObstacle(Rectangle((0,0),(700,200)))
        self.addObstacle(Rectangle((0,500),(700,700)))
        
        
        
        
def startLevel(gameData=None, parent=None):
    level=chamber1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()
                                         

