import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber4Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.pathfinder=None
        self.trapDoor=[]     
        self.mask=None  
        
        self.roundNum=0
        self.testTime=100 #number of simulation ticks
        self.coolDown=10 #number of ticks before next test
        
        self.maxRounds=6   
        self.ans=[]
        
        #self.endLocations=[0,1,2,3,4]
        self.sample=[]        
        self.endLocations=[0,4]
        
        #forces user to look at both samples b/c the else cause only contains zeros
        if random()>0.5:
            self.equalValues=[[0,lambda:randint(0,1)],[0,1,lambda:randint(1,2)]]
        else:
            self.equalValues=[[0,lambda:randint(1,2)],[0,1,lambda:randint(0,1)]]
        
        
        self.displayValues=["if sample1==0 and sample2==0:"," "," "," ","else:"]
        self.testDisplayText=[]
        
        for i in range(self.maxRounds):
            self.ans.append(choice(self.endLocations))
            
        self.startTestEvent=None
        self.resetEvent=None
        self.endTestEvent=None
        self.timerDisplay=None
        
        #test chamber decorations
        self.testNum=3
        self.sampleText=[]
        
        self.testIndicators=[]
        
        LevelWindow.__init__(self,"Chamber4",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
    def updateTimerDisplay(self):
        s=None
        if self.resetEvent:            
            s=str(self.resetEvent.ticks)            
        elif self.evaluateEvent:
            s=str(self.evaluateEvent.ticks)
        if s:    
            if len(s)==1:
                s="0"+s
            #add a decimal to the number             
            d=s[:-1]+"."+s[-1:]
            self.timerDisplay.setText(d)
        
        return True
        
    def teleportStep(self):
        angle=-90
        destination=350,650
        robot = getRobot()
        robot.stop()
        
        #The contacts do not like the robot suddenly teleporting. Clear all of them (they will be recreated).
        del self.interactContactList[:]
        del self.pickupContactList[:]
        #del self.levelWindow.talkContactList[:]
        robot.setPose(destination[0], destination[1], angle)
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
        return False
   
    def createSample(self):
        val=None
        for i in range(len(self.sample)):
        
            self.sample[i].show()
            ansIndex=self.endLocations.index(self.ans[self.roundNum]) 
            eVal=self.equalValues[i][ansIndex]
        
            #is a function        
            if hasattr(eVal, '__call__'):
                self.sample[i].returned=eVal()
            else:
                self.sample[i].returned=eVal
        
    def evaluateTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)        
        
        if self.roundNum<self.maxRounds:
            
            #opens trap doors.
            for i in range(len(self.trapDoor)):
                if i!=self.ans[self.roundNum]:                    
                    self.trapDoor[i].move(0,100)  
            
            #sees if Scribby in is test area.
            #if he isn't game over man
            if self.robots[0].frame.y<300 or self.robots[0].frame.y>400:
                self.setGameOver(-1)              
                
            #not dead    
            if self.gameOver==0:
                self.testIndicators[self.roundNum].fill=Color("green")
                
                if self.roundNum>=self.maxRounds:
                    print("Error. roundNum is bigger than maxRounds for some reason.")
                elif self.roundNum==self.maxRounds-1:
                    self.setGameOver(1)
                else:
                    self.roundNum+=1
                    self.resetEvent=TimedEvent(self.coolDown,self.resetTest)
                    self.addStepEvent(self.resetEvent)
            
    
    def startTest(self):
        
        self.evaluateEvent=TimedEvent(self.testTime,self.evaluateTest)
        self.addStepEvent(self.evaluateEvent)
            
    def resetTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)
        
        #set the display timer
        s=str(self.testTime)
        if len(s)==1:
            s="0"+s
        self.timerDisplay.setText(s[:-1]+"."+s[-1:])
            
        self.createSample()
        
        #teleports the robot back to the start location
        self.addStepEvent(self.teleportStep) 
        
        #resets the trapdoors
        for i in range(len(self.trapDoor)):
            origX=self.trapDoor[i].getX()
            self.trapDoor[i].moveTo(origX,250)
        
        #test starts as soon as robot movoes
        self.addStepEvent(OnMoveEvent(self.startTest))
        
        
    
    def revealPathfinder(self):
        self.vent.visible=False
        self.pathfinder.show()
        self.curiosity.hide()
        
    def endConvo(self):
        self.pauseRobot(False)
        self.resetTest()
        self.addStepEvent(self.updateTimerDisplay)
        dismiss()
        #self.launchBriefScreen(0)
        self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
    
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup both samples and save them as different variables."])
        
        text.append(["Check to see if both sample1==0 and sample2==0."])
        text.append(["See the ",self.getI("compare")," page for info on how to use 'and'."])
        b=simpleBrief(text,[self.getR("compare"),self.getR("if")])                    
        self.addBriefShape(b,"Use 'and' to compare both samples")
    
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(curiosityPort, "Pssst! Scribby. Are you ok.")
        self.addResponse(scribbyPort, "Yeah. Who is this nut job.")
        self.addResponse(curiosityPort, "I don't kno-...he's coming back. I'm trying to find a way out of here. Just hold on a little longer.",self.revealPathfinder)
        
        self.addResponse(pathfinderPort, "Sorry about that, I thought I heard something in the other room. I have to admit. As we get close to our goal I get more and more paranoid.")
        self.addResponse(pathfinderPort, "There have been so many that have tried to stop us. From our enemies to those... to those we held the closest.")
        self.addResponse(pathfinderPort, "But little did they know that we would not give up. You are living proof of that.")
        self.addResponse(pathfinderPort, "Well we still have some time so let's get on with the next test.")
        self.addResponse(pathfinderPort," In this test there are two samples. So you will have to pickup and check both.")
        self.addResponse(pathfinderPort, "Now if sample1 == 0 and sample2 == 0 go to the left most test area. else go to the right most.",lambda:self.helpWidgets[3].setButtonsVisible(["compare"],8))
        self.addResponse(pathfinderPort, "That shouldn't be too difficult.",lambda:self.launchBriefScreen(0))
        self.addResponse(pathfinderPort, " ",self.endConvo)
        
   
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildBriefings()
        
        self.buildConvo()
        self.vent.removeFromPhysics()
        for t in self.testDisplayText:
            t.removeFromPhysics()
        talk()
       
       #pass
       #self.runTest()
    

        
    def hideText(self,items):
        self.sampleText[0].visible=False
    def createLevel(self):
        
        #glorified event pads
        #self.createEndPad(250,50,locked=True)
        self.createStartPad(350,650)
        
        #create the robot
        self.createRobot(350,650,-90)
    
        #main area
        self.addObstacle(Rectangle((95,95),(100,700)))
        self.addObstacle(Rectangle((595,95),(600,700)))
        self.addObstacle(Rectangle((95,95),(600,100)))        
        self.addObstacle(Rectangle((95,295),(600,300)))
        
        #pathfinder and his office/viewing room
        self.addObstacle(Rectangle((100,195),(200,200)))
        self.addObstacle(Rectangle((195,100),(200,200)))
        #door to office
        door=self.addObstacle(Rectangle((90,125),(105,175),color=Color("gray")))
        door.setWidth(0)        
        
        self.curiosity=self.quickActor(625,450,IMAGE_PATH+"curiosityPortrait.png",visible=True)
        self.vent=self.addObstacle(Rectangle((575,425),(605,475),color=Color("gray")))
        self.vent.setWidth(0)
        self.pathfinder=self.quickActor(150,150,IMAGE_PATH+"pathfinderPortrait.png",scale=0.75,visible=False)
        
        #timer display
        self.addObstacle(Rectangle((495,100),(500,150)))
        self.addObstacle(Rectangle((495,145),(595,150)))        
        self.timerDisplay=self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right"))
        
        #sign with test number on it
        self.addObstacle(Text((200,110),"Test "+str(self.testNum),color=Color("black"),fontSize=18,xJustification="left"))
        
        
        #test spaces
        self.addObstacle(Rectangle((195,295),(200,400)))
        self.addObstacle(Rectangle((295,295),(300,400)))
        self.addObstacle(Rectangle((395,295),(400,400)))
        self.addObstacle(Rectangle((495,295),(500,400)))
        
        
        #trapdoors and mask
        for i in range(5):
            self.trapDoor.append(self.quickActor(150+i*100,250,Rectangle((155+i*100,255),(245+i*100,345),color=Color("black")),obstructs=False,collision=self.deathCollide))
        self.mask=self.quickActor(349,248,Rectangle((145,202),(635,295),color=Color("gray")))
        
        #sample object(s)
        #self.sample.append(self.quickActor(350,550,IMAGE_PATH+"unknownSample1.png",pickup=True,scale=0.25,description=-1))
        #self.sample.append(self.quickActor(350,450,IMAGE_PATH+"unknownSample2.png",pickup=True,scale=0.25,description=-1))
        self.sample.append(Sample(350,550,IMAGE_PATH+"unknownSample1.png",scale=0.25,code=-1))
        self.sample.append(Sample(350,450,IMAGE_PATH+"unknownSample2.png",scale=0.25,code=-1))
        for s in self.sample:
            self.addActor(s)
        
        
        #text above the test spaces/boxes
        for i in range(5):
            if i in self.endLocations:
                #self.testDisplayText.append(self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right")))
                self.testDisplayText.append(self.addObstacle(Text((105+i*100,305),self.displayValues[i],color=Color("black"),xJustification="left",fontSize=12)))
                self.testDisplayText[-1].rotation=50
            
        for i in range(self.maxRounds):
            self.testIndicators.append(self.addObstacle(Circle((210+i*15,125),5,color=Color("red"))))
            self.testIndicators[-1].setWidth(0)
        
def startLevel(gameData=None, parent=None):
    level=chamber4Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

'''
        self.sample.append(self.quickActor(350,550,LEVEL_PATH+"unknownSample.png",pickup=True,self.use=self.testSample,scale=0.25,description=-1))

    def testSample(self,items):
        print(items)
        
        print("Compare to 0",items==0)
        print("Compare to 1",items==1)
        
        items.description=1
        print("Change description to 1")
        
        print("Compare to 0",items==0)
        print("Compare to 1",items==1)
'''
       
                                         

