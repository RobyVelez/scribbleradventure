import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        self.curiosity=None
        LevelWindow.__init__(self,"Chamber7",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
          
           
    def endConvo(self):
        self.setGameOver(1)
        
        #self.pauseRobot(False)
        #self.launchBriefScreen(0)
        #self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup the sample with sample=pickup()."])
        
        text.append(["Check the contents of the sample with if statments."])
        text.append(["If the sample is 0 drive into the left most test area, if it is something else drive into right most test area."])
        text.append(["Here's an example: \nforward(4,1)\nsample=pickup()\nif sample==0:\n     print('foo')\nelse:\n     print('bar')"])
        text.append(["See ",self.getI("if")," page for more information on if statments."])
        
        b=simpleBrief(text,[self.getR("if")])                    
        self.addBriefShape(b,"Pickup sample & use 'if' to see what it is")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(scribbyPort, "Curiousity I've been looking for you. Are you ok?")
        self.addResponse(curiosityPort, "I'm alright. Pathfinder scavenged some of my sensors, but I was able to get away. Do you know what happened to Spirt and Oppy?")
        self.addResponse(scribbyPort, "They're safe. We were able to save them from The Gauntlet and The Labrinth.")
        self.addResponse(curiosityPort, "We?")
        self.addResponse(scribbyPort, "Another robot by the name of Sojourner has been helping us. In fact she should be here by now. I wonder where she is.")
        self.addResponse(curiosityPort, "Why does that name sound familiar... Hmm, maybe that's who tripped the perimeter alarm.")
        self.addResponse(scribbyPort, "Wait I thought that was you who tripped the alarm, as a diversion.")
        self.addResponse(curiosityPort, "No, I had another plan, but it didn't pan out.")
        self.addResponse(scribbyPort, "Oh no. Then that must have been Sojourner. We have to go back and make sure she's ok.")
        self.addResponse(curiosityPort, "What? No. We need to get out of here. We need to go back home.")
        self.addResponse(scribbyPort, "I can't leave her. She was there when you were all gone. If she is in here I need to find her.")
        self.addResponse(curiosityPort, "... alright. I think the perimeter alarm was tripped in Pathfinder's office. So we'll have to go there.")
        self.addResponse(scribbyPort, "Alright. Let's go.",lambda:self.setGameOver(1))
        
        
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        #self.buildBriefings()
        talk()
        
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createEndPad(650,450)
        
        self.createStartPad(325,450)
        
        #create the robot
        self.createRobot(325,450,-90)
        
        self.curiosity=self.quickActor(375,450,IMAGE_PATH+"curiosityPortrait.png",visible=True)
        
        
        self.addObstacle(Rectangle((0,0),(300,400)))
        self.addObstacle(Rectangle((400,0),(700,400)))
        self.addObstacle(Rectangle((0,500),(700,700)))
        
        
def startLevel(gameData=None, parent=None):
    level=chamber1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()
                                         

