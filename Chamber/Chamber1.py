import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class chamber1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        LevelWindow.__init__(self,"Chamber1",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
          
           
    def endConvo(self):
        self.setGameOver(1)
        #bring back up the win screen
        self.showPanel1()
        #self.pauseRobot(False)
        #self.launchBriefScreen(0)
        #self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup the sample with sample=pickup()."])
        
        text.append(["Check the contents of the sample with if statments."])
        text.append(["If the sample is 0 drive into the left most test area, if it is something else drive into right most test area."])
        text.append(["Here's an example: \nforward(4,1)\nsample=pickup()\nif sample==0:\n     print('foo')\nelse:\n     print('bar')"])
        text.append(["See ",self.getI("if")," page for more information on if statments."])
        
        b=simpleBrief(text,[self.getR("if")])                    
        self.addBriefShape(b,"Pickup sample & use 'if' to see what it is")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        #self.conversation.setDebug(True)
        self.addResponse(scribbyPort, "I hope curiosity is ok. She's been held for a while now.")
        self.addResponse(scribbyPort, "I wonder where did Sojourner go? Who is she going to meet?")
        self.addResponse(scribbyPort, "This has been a strange adventure. Seems like yesterday I was back at home.")
        self.addResponse(scribbyPort, "Wait, what's that noise?")
        self.addResponse(scribbyPort, "<blackout>",lambda:self.setGameOver(1))
        
        
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        #self.buildBriefings()
        talk()
        
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createStartPad(350,650)
        
        #create the robot
        self.createRobot(350,650,-90)
        
        
def startLevel(gameData=None, parent=None):
    level=chamber1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()
                                         

