import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
    
class chamber11Window(LevelWindow):
    def __init__(self,gameData,parent):        
        self.rows=[]
        self.portal=[]
        self.endLocations=[3,4,5,6]
        self.ans=[]
        self.maxRows=5
        
        lastChoice=None
        for i in range(self.maxRows):
            c=choice(self.endLocations)
            
            if lastChoice!=None:
                while c==lastChoice:
                    c=choice(self.endLocations)
            lastChoice=c
            self.ans.append(c)    
            
        self.doorSwitch=None
        self.door=None
        
        self.timer=None
        self.endTestEvent=None
        self.testTime=250
        
        LevelWindow.__init__(self,"Chamber11",gameData=gameData,parent=parent,debug=False)        
        self.setup()
    
    def reminderSwitch(self):
        self.pauseRobot(True)
        #self.conversation.setDebug(True)
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        #self.setResponse(sojournerPort,"Alright now just drive onto the switch and do use().")
        self.addResponse(sojournerPort,"Alright now just drive onto the switch and do use().")
        talk()
        self.pauseRobot(False)
    
    def runDisplay(self):
        if self.timer.run:
            self.timer.updateTimerDisplay(self.endTestEvent)
        return True
        
    def startLift(self,foo,bar):
        self.addStepEvent(self.liftDoor)
        
    def liftDoor(self):
        self.door.move(0,-5)
        if self.door.y<-50:
            self.setGameOver(1)
            return False
        else:
            return True
    
    def endTest(self):
        self.timer.run=False
        
    def endGame(self):
        if self.timer.run:
            self.setGameOver(-1)
                        
    def lockInChoice(self): 
        self.endTestEvent=TimedEvent(self.testTime,self.endGame)
        self.addStepEvent(self.endTestEvent)
        self.timer.run=True
               
        for i in range(len(self.rows)):
            self.rows[i].setSafePortal(self.ans[i])
        
        
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Try using the wait() command to time your movements.")
        
    def buildBriefings(self):
        text=[]
        text.append(["A ",self.getI("Script")," will execute commands one after another without a pause."])
        text.append(["Use the ",self.getI("wait()")," command to add a pause into your script."])
        text.append(["For example:\n\n     forward(4,3)\n     turnRight(4,1)\n     wait(0.5)\n     forward(4,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        
        self.addResponse(sojournerPort,"The upload station is beyond these security corridors. Use you're getIR() sensors to read the wall configuration and drive into the right portal.")
        self.addResponse(scribbyPort,"Uh, ok what about you two? ")
        self.addResponse(sojournerPort,"There's a switch on the other side of the wall. Pull it and the wall with open.")
        self.addResponse(scribbyPort,"Curiosity?")
        self.addResponse(curiosityPort,"You can do this. Once you use() the portal and teleport get ready to turn, back up to the test wall, read you're sensors, and then drive onto the correct portal.")
        self.addResponse(scribbyPort,"Alright.",lambda:self.launchBriefScreen(0))        
        self.addResponse(scribbyPort,"  ",self.endConvo)
        
        
   
    def onLevelStart(self):
        
        
        self.pauseRobot(True)
        self.addStepEvent(self.runDisplay)
        self.buildBriefings()
        self.buildConvo()
        talk() 
        
    def createLevel(self):
            
        #glorified event pads
        #self.createEndPad(50,150)
        self.createStartPad(500,150)
        
        #Create portals
        self.portal.append(Portal((400,150), (250,650), 0, 0,debug=False,additionalAction=self.lockInChoice))
        #self.portal.append(Portal((250,150), (250,350), -90, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)       
        
        try:
            for i in range(self.maxRows):
                if i==self.maxRows-1:
                    self.rows.append(TestRow((50,650-i*100),(250,550-i*100),useAction=self.endTest))                    
                else:                
                    self.rows.append(TestRow((50,650-i*100),(250,550-i*100)))
                self.addObstacle(Rectangle((0,600-i*100),(700,605-i*100)))
        except:
            print("error")
        
        
        for i in range(len(self.rows)):
            self.rows[i].setSafePortal(choice(self.endLocations))
        
        
        self.doorSwitch=self.addActor(EventPad((150,150),150,75,avatar=[LEVEL_PATH+"switchLeft.png",LEVEL_PATH+"switchRight.png"],scale=0.35,interact=self.startLift,debug=False))
        
        #create the robot
        self.createRobot(500,150,0)
        
        self.door=self.addObstacle(Rectangle((350,100),(355,200)))  

        self.addObstacle(Rectangle((0,0),(700,100)))        
        self.addObstacle(Text((450,50),"0 0\n  0",color=Color("black"),fontSize=12))
        self.addObstacle(Text((500,50),"0 1\n  1",color=Color("black"),fontSize=12))
        self.addObstacle(Text((550,50),"1 0\n  2",color=Color("black"),fontSize=12))
        self.addObstacle(Text((600,50),"1 1\n  3",color=Color("black"),fontSize=12))
              
        self.timer=TimerDisplay((50,50))
        
        self.curiosity=self.quickActor(575,150,IMAGE_PATH+"curiosityPortrait.png",visible=True)
        self.sojourner=self.quickActor(625,150,IMAGE_PATH+"sojournerPortrait.png",visible=True)
        
        self.addActor(EventPad((250,150),50,50,avatar=LEVEL_PATH+"portalOrange.png",collisionAction=self.reminderSwitch,debug=False))
       
        
def startLevel(gameData=None, parent=None):
    level=chamber11Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

