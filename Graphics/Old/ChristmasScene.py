from Graphics import *
from Myro import doTogether
from math import *
from random import *

#Make a window that we will draw on
#String in quotes is the name of window
#Numbers are the width and height respectively
win = Window("Christmas_Scene",600,400)

#Set background of floor and make the floor
win.setBackground(Color("darkorange"))
floor = Rectangle((0,250),(600,400))
floor.fill=Color("gray")
floor.draw(win)
    
#Create a tree with a polygon
def createTree():
    #Specify the top, bottom left, and bottom right points

    #Top point of tree. x,y
    treeTop = [100,100]
    #tree height (y) and width(x)
    tHeight = 200 
    tWidth = 150
    tree = Polygon((treeTop[0],treeTop[1]),
        (treeTop[0]-tWidth/2,treeTop[1]+tHeight),
        (treeTop[0]+tWidth/2,treeTop[1]+tHeight))
    #tree = Polygon((100,50),(25,300),(175,300))

    #Make tree green
    tree.fill=Color("green")
    #Draw tree to the window
    tree.draw(win)


#create a stocking and specify where it's attached to
def createStocking(x,y):
    
    #We are going to specify starting positions for
    #the different parts of the stocking. This is
    #not the final position, but rather will allow 
    #us to construct it. After we construct it we will
    #move it into position
    
    #starting coordinates for top fluffy part
    x1=150
    y1=150    
    #Width and height of the top,fluffy part of the stocking
    tW =50
    tH =25    
    top = RoundedRectangle((x1,y1),(x1+tW,y1+tH),5)
    top.fill=Color("ivory")
    
    #starting coords for first foot section
    x2 = 160
    y2 = 170
    #first foot section is a round rectangle
    #specifies the width,height and roundness
    f1W=30
    f1H=50
    rN = 5    
    f1 = RoundedRectangle((x2,y2),(x2+f1W,y2+f1H),rN)
    f1.fill=Color("red")
    f1.outline=Color("red")
    
    #second foot section will be an oval
    #center of the oval and the radius of x and y
    x3=165
    y3=220
    xRad = 25
    yRad = 15    
    f2 = Oval((x3,y3),xRad,yRad)
    f2.fill=Color("red")
    f2.outline=Color("red")
    #f2.rotate(5)
    
    #top.rotate(45)
    #draw the f1 before top so that it will appear behind it
    f1.draw(win)
    f2.draw(win)
    top.draw(win) 
    
    f1.rotate(45)
    f2.rotate(45)
    top.rotate(45)    
    
    f1.moveTo(x+20,y+20)
    f2.moveTo(x+26,y+42)
    top.moveTo(x,y)
    
#Creates a fire place with a few stockings
def createFirePlace():
    x1=225
    y1=150
    x2=255
    y2=280
    
    x3=355
    y3=y1
    x4=385
    y4=y2
    
    x5=200
    y5=y1-30
    x6=410
    y6=y1
    
    x7=x2
    y7=y1
    x8=355
    y8=y2
    
    x9=260
    y9=0
    x10=350
    y10=120
    
    leftPillar = Rectangle((x1,y1),(x2,y2))
    rightPillar = Rectangle((x3,y3),(x4,y4))
    mantel = Rectangle((x5,y5),(x6,y6))
    hearth = Rectangle((x7,y7),(x8,y8))
    chimney = Rectangle((x9,y9),(x10,y10))
    
    leftPillar.fill=Color("firebrick")
    rightPillar.fill=Color("firebrick")
    mantel.fill=Color("firebrick")
    hearth.fill=Color("black")
    chimney.fill=Color("firebrick")
    
    leftPillar.draw(win)
    rightPillar.draw(win)
    mantel.draw(win)  
    hearth.draw(win)
    chimney.draw(win)
    
    createStocking(375,145)
    createStocking(300,145)                       
    createStocking(225,145)                    



#variables to hold the box and lid for a present
#We put them here because we want them to be global                
box=None
lid=None
toy=None

def createPresent():
    #We have to explicity call these variables
    #global or else the changes we make to them
    #won't take hold everywhere.
    global box,lid,toy
    #base of present
    x1=150
    x2=180
    y1=320
    y2=290
    
    #lid of present
    x3=x1
    x4=x2
    y3=280
    y4=y2
    
    #position of toy
    x5=165
    y5=280
    
    box = Rectangle((x1,y1),(x2,y2))
    box.fill=Color("red")
    box.draw(win)
    
    lid = Rectangle((x3,y3),(x4,y4))
    lid.fill=Color("brown")
    lid.draw(win)
    
    #Just a simple ball for a toy
    #Note we are not going to draw it until someone
    #clicks on the present
    toy = Circle((x5,y5),10)
    toy.fill=Color("pink")
    
def mouseDown(obj, event):
    print(event.x,event.y)
    
    #Will open the present and reveal the toy
    global lid,toy
    if box.hit(event.x,event.y) or lid.hit(event.x,event.y):
        if lid.rotation == 0:
            lid.rotate(90)
            lid.moveTo(185,275)
            toy.draw(win)
        else:
            lid.rotate(-90)
            lid.moveTo(165,285)
            toy.undraw()
        
    
win.onMouseDown(mouseDown)

#Create a star atop of the tree that blinks
def blinkingStar():

    colors=["yellow","gold","orange"]
    numShapes=5
    shapeList=[]  
        
    #dimensions of you base shape
    #x=[75,125,125,75]
    #y=[25,25,75,75]
    x=[100,150,100,50]
    y=[90,100,110,100]    
    
    
    
    #makes all the shapes that constitute the star
    for i in range(numShapes):
        #Create the base shape
        s=Polygon((x[0],y[0]),(x[1],y[1]),(x[2],y[2]),
            (x[3],y[3]))
        #rotate it, add color, and draw it
        s.rotate((360/numShapes)*i)
        s.fill=Color(colors[i%2])
        s.draw(win)
        
        #store shapes in a list
        shapeList.append(s)
    
    cX=shapeList[0].getX()
    cY=shapeList[0].getY()    
    c=Circle((cX,cY),10)
    c.fill=Color(colors[2])
    c.draw(win)
    blinkImages=[]
    while True:
        for s in shapeList:
            #toggles the color of the star shapes
            #from colors[0] to colors[1]
            if s.fill.Equals(Color(colors[0])):
                s.fill=Color(colors[1])
            else:
                s.fill=Color(colors[0])
            #if len(blinkImages)<numShapes:
            #    blinkImages.append(Picture(win))
            #else:
            #    savePicture(blinkImages,"blinkStar.gif",0,True)
            #    print("wrote gif")

def windowSnowing():

    #coordinates of night sky and width/height
    x1=445
    y1=40    
    nW=100
    nH=100
    night=Rectangle((x1,y1),(x1+nW,y1+nH))
    night.fill=Color("midnightblue")
    
    #thickness of the frame
    fT=5
    x2=x1-fT
    y2=y1-fT
    x3=x1+nH+fT
    y3=y1+nH+fT    
    frameBack = Rectangle((x1-fT,y1-fT),(x1+nH+fT,y1+nH+fT))
    frameBack.fill=Color("sienna")
    
    #origin for cross pieces of frame
    x4=x1+nW/2-fT/2
    y4=y1
    x5=x4+fT
    y5=y1+nH
    
    c1=Rectangle((x4,y4),(x5,y5))
    c1.fill=Color("sienna")
    c1.outline=Color("sienna")
    c2=Rectangle((x4,y4),(x5,y5))
    c2.fill=Color("sienna")
    c2.outline=Color("sienna")
    c2.rotate(90)
        
    #order we draw these objects matters
    frameBack.draw(win) 
    night.draw(win)
    c1.draw(win)
    c2.draw(win)
    
    minSize=1
    maxSize=2.5
    maxSpeed=0.01    
    maxFlakes = 45
    maxDrift=0.1
    #How close can snow get to the frame before we
    #redraw it
    buff=fT/2
    #spacing between flakes
    s = nW/(maxFlakes+1)
    flakes=[]
    
    #in order for the snowflakes to be behind the cross
    #pieces we have to draw it in reference to the cross pieces
    for i in range(1,maxFlakes+1):
        rad=-minSize+2*maxSize*random()
        flakes.append(Circle((i*s-nW/2,-random()*nH*2),rad))
        flakes[-1].fill=Color("snow")
        flakes[-1].visible=False
        #flakes[-1].draw(night)#win)
    #c1.draw(win)
    #c2.draw(win)
    while True:

        for f in flakes:
            if f.getY() >= -buff+nH/2 or f.getX()<buff-nW/2 or f.getX()>-buff+nW/2:
                f.undraw()
                f.visible=False
                f.moveTo(f.getX(),y1-random()*nH*2)
            elif f.getY() > -nH/2 and not f.visible:
                f.visible=True
                f.draw(night)

                
            #random number from -1 to 1
            r=-1+2*random()
            f.move(r*maxDrift,random()*maxSpeed)
                   
    
    
def main():
    #createBackdrop()
    createTree()
    createPresent()
    createFirePlace()
    doTogether(blinkingStar,windowSnowing)
    #windowSnowing()
    #blinkingStar()
    
    
main()