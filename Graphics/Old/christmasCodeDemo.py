from Graphics import *
from random import*
from Myro import doTogether

win = Window("Testing",600,400)

#make a variable that will hold the box
#put it outside all the function so they can all
#see it. initialize it to essentially nothing
box = None
#make a ball object that will also appear when we click
ball = None

def createBox():
    #declare the box, defined above, as global
    #so we can modify it
    global box,ball    
        
    box = Rectangle((100,100),(300,200))
    box.draw(win)
    
    ball = Circle((300,300),10)
    ball.visible=False
    
    
    
#function that will be called everytime we click the mouse    
def mouseDown(obj,event):
    #gives you x,y coordinates of whereever you click
    #in of itself a very useful piece of code
    print(event.x,event.y)
    
    #to modify these two objects they have to be global
    global box,ball
    
    #if you click inside the box
    if box.hit(event.x,event.y):
        if box.rotation == 0:
            box.rotate(90)            
        else:
            box.rotate(-90)
    elif ball.hit(event.x,event.y):
        #moves the ball over by fix
        ball.move(5,0)
        if ball.fill.Equals(Color("green")):
            ball.fill=Color("red")
        else:
            ball.fill=Color("green")
    else: #if you don't click the box or ball
        if ball.visible:
            ball.undraw()
            ball.visible=False
        else:
            ball.draw(win)
            ball.visible=True
            
        
#assign the function that deals with mouse clicks
#to the window        
win.onMouseDown(mouseDown)              


def blinkingBox1():
    while True:
        if box.rotation == 0:
            if box.fill.Equals(Color("blue")):
                box.fill=Color("yellow")
            else:
                box.fill=Color("blue")

def blinkingBox2():
    while True:
        #print("here")
        if box.rotation == 90:
            if box.fill.Equals(Color("orange")):
                box.fill=Color("purple")
            else:
                box.fill=Color("orange")

    
def main():  
    createBox() 
    doTogether(blinkingBox1,blinkingBox2)
    
main()
    