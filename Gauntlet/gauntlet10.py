import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet10Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        LevelWindow.__init__(self,"Gauntlet10",gameData=gameData,parent=parent,debug=True)
        
        
        self.setup()
        
    
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def endConvo(self):
        dismiss()
        
        self.pauseRobot(False)
        
    def buildBriefings(self):
        text=[]
        text.append(["Build two ",self.getI("functions1")," one called lower() and another called upper()."])
        text.append(["lower() will have all the commands to get you to the blue portal and do a ",self.getI("use()")])
        text.append(["upper() will have the commands to get you from the red portal to the end."])
        text.append(["Call lower() first in the ",self.getI("Shell"),". Wait till you get through the portal. Then call upper() in the Shell.",self.getI("Shell")])
        b=simpleBrief(text,[self.getR("functions1"),self.getR("use()"),self.getR("Shell")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        self.addResponse(scribbyPort, "That was really close. I just had to do a single forward(), but still.")
        self.addResponse(scribbyPort,"The portal stopping my Script is problematic. What if I have to do a bunch of commands after a portal?")
        self.addResponse(spiritPort, "Put them into a function and then just execute the function.")
        self.addResponse(scribbyPort,"What is a function?")
        self.addResponse(spiritPort, "It's a way to group commands together and allows you to make your own custom commands.",lambda:self.helpWidgets[1].setButtonsVisible(["functions1"],8))
        self.addResponse(spiritPort, "So you can make two functions one called lowerPath() and one called upperPath(). lowerPath() has the forward(), turnRight(), etc commands that get you to the blue portal.")
        self.addResponse(spiritPort, "upperPath() would then have the forward(), turnRight(), etc commands to get you from the red portal to the end goal.")
        self.addResponse(spiritPort, "Here, I'll upload info about functions to your Help Pages.",lambda:self.launchBriefScreen(0))
        self.addResponse(spiritPort, " ",self.endConvo)
    
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.helpWidgets[1].setButtonsInvisible(["functions1"])
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        talk()
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet10back.png")
         #glorified event pads
        self.createEndPad(450,50)
        self.createStartPad(550,650)
        
        self.portal.append(Portal((50,650), (50,450), -90, 0,debug=False))
        self.portal.append(Portal((50,450), (50,650), -180, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)
        
        #create the robot
        self.createRobot(550,650,-180)
        
        self.addObstacle(Polygon((100,200),(700,200),(700,600),(500,600),(500,300),(150,300),(150,600),(0,600),(0,500),(100,500),(100,300)), vis=False)
        
        self.addObstacle(Rectangle((0,0),(400,100)), vis=False)
        self.addObstacle(Rectangle((500,0),(700,100)), vis=False)
        
        self.addObstacle(Rectangle((250,550),(400,700)), vis=False)
                
        self.thwomp.append(Thwomp((450,350),(450,650),1,self.deathCollide,run=False))
        #self.thwomp.append(Thwomp((325,450),(325,650),1,self.deathCollide,run=True))
        self.thwomp.append(Thwomp((200,350),(200,650),1,self.deathCollide,run=False))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
            
        self.fire.append(FireBall((700,150), (0,150),2, self.deathCollide,speed=20,bounce=False,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)

        self.kuka=Kuka((650,650),hitMessage=None,hitPause=20,hitAction=None, scale=0.70,speed=20,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        self.kuka.wayPoints.append([550,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
        
    
        
def startLevel(gameData=None, parent=None):
    level=gauntlet10Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

