import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class gauntlet1Window(LevelWindow):    
    def __init__(self,gameData,parent):      
        self.fire=[]
        
        LevelWindow.__init__(self,levelName="Gauntlet1",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
        
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Use forward() command to get pass the fireballs.",6) 
        
    def buildBriefings(self):
        text=[]
        text.append(["Use the ",self.getI("forward()")," or ",self.getI("backward()")," command to get down the corridor.\n"])   
        b=simpleBrief(text,[self.getR("forward()"),self.getR("backward()")])                    
        self.addBriefShape(b)
            
            
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(scribbyPort, "Ok, I'm here. Now what? S-Sojourner? Where are you?")
        self.addResponse(sojournerPort, "I'm here. I can't enter The Gauntlet myself, but I can communicate with you over you're radio.")
        self.addResponse(scribbyPort, "Wait, you said you were going to come with me? I can't do this on my own. There are fireballs infront of me. I don't want to be torched.")
        self.addResponse(sojournerPort, "No, I said I would be with you. I am with you. You're not on you're own. You can do this. ")
        self.addResponse(sojournerPort, "Small steps first. Use you're forward() command to get past these fireballs. Small steps climb the mountain.")
        self.addResponse(scribbyPort, "...o-ok...",lambda:self.launchBriefScreen(0))
        self.addResponse(scribbyPort, " ",self.endConvo)
        
        
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()           
        self.buildBriefings()
        self.buildConvo()
        self.robots[0].pauseRobot=True        
        talk()
        
    def startFire(self):
        for f in self.fire:
            f.run=True
            
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet1back2.png")   
        
        #glorified event pads
        self.createEndPad(350,0)
        self.createStartPad(350,650)
        
        #create the robot
        self.createRobot(350,650,-90)
        
        self.addObstacle(Polygon((0,0),(300,0),(300,100),(100,100),(100,200),(300,200),(300,300),
            (100,300),(100,400),(300,400),(300,500),(100,500),(100,600),(300,600),
            (300,700),(0,700)), vis=False)
        self.addObstacle(Polygon((400,0),(700,0),(700,700),(400,700),(400,600),
            (600,600),(600,500),(400,500),(400,400),(600,400),(600,300),
            (400,300),(400,200),(600,200),(600,100),(400,100)), vis=False)
        
        
        self.fire.append(FireBall((100,150), (550,150),3, self.deathCollide,speed=25,run=False,startLoc=[350,150]))
        self.fire.append(FireBall((100,350), (550,350),3, self.deathCollide,speed=25,run=False))
        self.fire.append(FireBall((550,550), (100,550),2, self.deathCollide,speed=25,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
            f.run=True
            
        #self.addStartEvent(self.startFire)    
        
def startLevel(gameData=None,parent=None):
    level=gauntlet1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
            
        
'''
#Create the briefing
text=[Wrap(475), SkipV(BRIEFING_TEXT_Y_POS)]
#text+=["1. Click the shell tab to bring the ",Inline2("Shell","helpPage:shellPage", Color("purple"))," forward.\n"]
#text+=["2. Type the ",Inline2("talk()","helpPage:talkPage", Color("red"))," command in the shell and then hit \nEnter to respond.\n"]

text+=[Bullet("1.",["Click the shell tab to bring the ",Inline2("Shell","helpPage:shellPage", Color("purple"))," forward.\n"])]
text+=[Bullet("2.",["Type the ",Inline2("talk()","helpPage:talkPage", Color("red"))," command in the shell and then hit Enter to respond.\n"])]

text+=[SetV(RELATED_PAGES_Y_POS)]
text+=[Related("talk()", "helpPage:talkPage", Color("red")), " "]
text+=[Related("shell", "helpPage:shellPage", Color("purple"))]           
b1=Picture(LEVEL_PATH+"intro1Brief_talkSeed.png")
writeText(b1, text)
self.addBriefShape(b1,"Respond to Sojourner with talk()")
''' 