import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet6Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.kuka=None
        self.hazards=[]        
        
        LevelWindow.__init__(self,"Gauntlet7",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
            
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Try using the wait() command to time your movements.")
        
    def buildBriefings(self):
        text=[]
        text.append(["A ",self.getI("Script")," will execute commands one after another without a pause."])
        text.append(["Use the ",self.getI("wait()")," command to add a pause into your script."])
        text.append(["For example:\n\n     forward(4,3)\n     turnRight(4,1)\n     wait(0.5)\n     forward(4,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(sojournerPort, "<static>... Scribb... Scribby can <static> hear me...<static>")
        self.addResponse(scribbyPort, "Yes I can hear you, but you're breaking up.")
        self.addResponse(sojournerPort, "As you get deeper in The <static>...harder for <staic>...communi-....")
        self.addResponse(scribbyPort, "Sojourner? Sojourner! Are you there? Sojourner?...",lambda:self.launchBriefScreen(0))
        self.addResponse(scribbyPort, " ",self.endConvo)
        
   
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.helpWidgets[1].setButtonsInvisible(["use()","functions1"]) 
        
        self.pauseRobot(True)
        
        self.buildConvo()
        self.buildBriefings()
        talk()
    
        
    def hitKuka(self):
        self.kuka.run=False
        self.kuka.stopMotion()
        self.pauseRobot(True)        
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        self.addResponse(self.kuka.getAvatar(), "...")        
        self.addResponse(scribbyPort, "He's stopped. I think I've stunned him. ")
        self.addResponse(scribbyPort, "Now's my chance. I have to save Spirit.")
        self.addResponse(scribbyPort, " ",lambda:self.setGameOver(1))        
        self.talk()
            
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def cantRun(self):
        if not self.kuka.damaged:
            
            scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
            self.pauseRobot(True)
            self.addResponse(scribbyPort,"I can't just run from it. I have to stand and fight and at least try to damage it. And now the exit is locked. I have to move off and back on it to try again.")
            talk()   
            self.pauseRobot(False)
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet7back.png")
        #glorified event pads
        self.createEndPad(250,50,locked=True)
        self.createStartPad(250,650)
        
        #create the robot
        self.createRobot(250,650,-90)
    
        #self.addObstacle(Polygon((280,700),(0,700),(0,0),(150,0),(150,330),(225,330),(200,510)))
        self.addObstacle(Rectangle((0,0),(200,600)), vis=False)
        self.addObstacle(Rectangle((300,0),(400,300)), vis=False)
        self.addObstacle(Rectangle((300,400),(400,600)), vis=False)
        self.addObstacle(Rectangle((500,0),(700,700)), vis=False)
        
        self.hazards.append(Thwomp((450,50),(450,650),1,self.deathCollide))
        self.addActor(self.hazards[0])
        self.addStepEvent(self.hazards[0].step)
                
        
        self.kuka=Kuka((50,650),hitMessage=None,hitPause=20,hitAction=self.hitKuka, scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([50,650])
        self.kuka.wayPoints.append([150,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
        
        self.addActor(EventPad((250,50),50,50,collisionAction=self.cantRun,debug=False))
        
def startLevel(gameData=None, parent=None):
    level=gauntlet6Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

