restart()
wait(0.25)
skip()

def gaunt4():

    #restart()
    forward(4,1)
    turnRight(4,1)
    #wait(1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,5)
    turnRight(4,1)
    forward(4,2)

def gaunt5():
    forward(4,3)
    turnRight(4,1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,2)
    wait(2)    
    #backward(4,1)
    #wait(10)
    #forward(4,1)
    
def gaunt7():
    forward(4,3)
    turnRight(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,3)
    turnRight(4,1)
    forward(4,2)
    turnRight(4,1)
    wait(10)
    forward(4,6)
def gaunt6b():
    turnRight(4,1)
    forward(4,2)
    turnLeft(4,1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,3)
    
    
def gaunt6():
    forward(4,3)
    motors(1,0.5)
    wait(48)
    stop()
    turnLeft(4,1)
    forward(4,1)
    
def gaunt9():
    turnRight(2,0.25)
    forward(4,2.5)
    turnLeft(2,0.75)
    forward(4,2)
    turnRight(2,0.75)
    forward(4,2)
    use()


def gaunt10a():
    forward(4,1)
    turnRight(4,1)
    forward(4,2)
    turnLeft(4,1)
    forward(4,3)
    turnLeft(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,2)
    use()
def gaunt10b():
    turnRight(4,1)
    forward(4,3)
    turnRight(4,1)
    forward(4,4)
    turnLeft(4,1)
    forward(4,1)
    
def gaunt11a():
    forward(4,1.5)
    turnLeft(4,1)
    forward(4,4)
    turnRight(4,1)    
    forward(4,3.25)
    turnLeft(4,1)
    forward(4,1)
    use()
def gaunt11b():
    turnLeft(4,1)
    forward(4,1)
    turnRight(4,1)
    forward(4,2.5)
    turnRight(4,1)
    forward(4,1)
#def gaunt10b():
#    forward
def gaunt9b():
    forward(4,3)
    turnRight(4,1)
    forward(4,4)
def gaunt12Right():
    forward(4,1)
    turnRight(4,1)
    forward(4,2)
    turnRight(4,1)
    forward(4,1)
    use()
    
    
def gaunt12Left():
    forward(4,1)
    turnLeft(4,1)
    forward(4,2)
    turnLeft(4,1)
    forward(4,1)
    use()
def foo():
    backward(4,2)
    use()
def bar():
    backward(4,1)
    turnLeft(4,1)
    forward(4,2)
    
def untitled1():
    forward(4,1)
    sample=pickup()
    print(sample)
    if sample==0:
        forward(4,1)
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    else:
        forward(4,1)
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)

def untitled2():
    forward(4,1)
    sample=pickup()
    print(sample)
    if sample==0:
        forward(4,1)
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif sample==1:
        forward(4,1)
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    else:
        forward(4,1)
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)        
        
def untitled3():
    forward(4,1)
    sample1=pickup()
    forward(4,1)
    sample2=pickup()
    
    print(sample1,sample2)
    if sample1==0 and sample2==0:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)        
s1=None
s2=None
def untitled4():
    forward(4,1)
    sample1=pickup()
    forward(4,1)
    sample2=pickup()
    '''    
    global s1,s2
    
    s1=sample1
    s2=sample2
    print(type(sample1))
    print(type(sample2))
    '''
    print(sample1,sample2)
    print(sample1<sample2)
    print(sample1>sample2)
    print(sample1==sample2)
    
    if sample1<sample2:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif sample1>sample2:
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    elif sample1==sample2:
        forward(4,1)        
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)         

def untitled5():
    forward(4,1)
    s1=pickup()
    forward(4,1)
    s2=pickup()
    
    if s1==0 and s2==0:
        turnLeft(4,1)
        forward(4,2)
        turnRight(4,1)
        forward(4,1)
    elif s1==0 and s2==1:
        turnLeft(4,1)
        forward(4,1)
        turnRight(4,1)
        forward(4,1)
    elif s1==1 and s2==0:
        forward(4,1) 
    elif s1==1 and s2==1:
        turnRight(4,1)
        forward(4,1)
        turnLeft(4,1)
        forward(4,1)
    else:
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)         
def untitled6():
    w=5
    left,right=getIR()
    print(left,right)
    if left==0 and right==0:
        use(0)
    elif left==0 and right==1:
        use(1)
    elif left==1 and right==0:
        use(2)
    elif left==1 and right==1:
        use(3)
    else:
        print("Error")    

def untitled7a():
    forward(4,3)
    use()
    
def untitled7():
    turnRight(4,1)
    backward(4,1)
    left,right=getIR()
    print(left,right)
    if left==0 and right==0:
        forward(4,2)
        use()
    elif left==0 and right==1:
        forward(4,3)
        use()
    elif left==1 and right==0:
        forward(4,4)
        use()
    elif left==1 and right==1:
        forward(4,5)
        use()
        
def hardcode():
    untitled7()
    wait(0.1)
    untitled7()
    wait(0.1)
    
    untitled7()
    wait(0.1)
    
    untitled7()
    wait(0.1)
    
    untitled7()
    wait(0.1)
    
    untitled7()
     
def untitled8():
    backward(4,1)
    left,right=getIR()
    print(left,right)
    if left==0 and right==0:
        use(0)
    elif left==0 and right==1:
        use(1)
    elif left==1 and right==0:
        use(2)
    elif left==1 and right==1:
        use(3)
        
def chamber9():
    backward(4,1)
    left,right=getLine()
    print(left,right)
    if left==0 and right==0:
        use(0)
    elif left==0 and right==1:
        use(1)
    elif left==1 and right==0:
        use(2)
    elif left==1 and right==1:
        use(3)   
    
     
     
     
     
    
                                                                