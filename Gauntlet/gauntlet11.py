import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet11Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        LevelWindow.__init__(self,"Gauntlet11",gameData=gameData,parent=parent)
        
        self.setup()
        
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def endConvo(self):
        
        self.pauseRobot(False)
        
        
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        self.addResponse(spiritPort, "We're almost there. Just keep using those functions and we'll get out of here.",self.endConvo)
        
        
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.pauseRobot(True)
        self.buildConvo()
        talk()
  
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet11back.png")
        #glorified event pads
        self.createEndPad(300,150)
        self.createStartPad(550,650)
        
        #Create portals
        self.portal.append(Portal((50,150), (550,150), -180, 0,debug=False))
        self.portal.append(Portal((550,150), (50,150), -180, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)
    
    
        #create the robot
        self.createRobot(550,650,-90)
        
        
        self.addObstacle(Rectangle((0,0),(100,100)), vis=False)
        self.addObstacle(Rectangle((350,100),(500,200)), vis=False)
     
        self.addObstacle(Rectangle((600,100),(700,200)), vis=False)
        self.addObstacle(Rectangle((0,200),(100,700)), vis=False)
        self.addObstacle(Rectangle((200,550),(500,700)), vis=False)
        self.addObstacle(Rectangle((600,550),(700,600)), vis=False)
        
        self.addObstacle(Polygon((200,0),(250,0),(250,300),(700,300),(700,450),(200,450)), vis=False)        
        
        
        self.thwomp.append(Thwomp((150,50),(150,650),1,self.deathCollide,run=False))
        
        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
            
        self.fire.append(FireBall((700,50), (300,50),2, self.deathCollide,speed=20,bounce=False,run=False))
        self.fire.append(FireBall((700,250), (300,250),2, self.deathCollide,speed=20,bounce=False,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
            
            
        self.kuka=Kuka((650,650),hitMessage=None,hitPause=20,hitAction=None, scale=0.70,speed=25,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([50,650])
        self.kuka.wayPoints.append([550,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
         
          

def startLevel(gameData=None, parent=None):
    level=gauntlet11Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

