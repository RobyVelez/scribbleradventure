import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet5Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.kuka=None
        self.hazards=[]        
        
        LevelWindow.__init__(self,"Gauntlet5",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
            
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Use motors() and stop() commands to get around grinders.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Trick Kuka into running into the hazards."])
        text.append(["You will need speed so typing ",self.getI("commands")," one at a time won't work."])
        text.append(["Build a ",self.getI("Script")," and enter the ",self.getI("commands")," you want to execute."])
        text.append(["When you run the ",self.getI("Script")," the ",self.getI("commands")," will be excecuted sequentially."])        
        b=simpleBrief(text,[self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Build Scripts for faster movements.")
        
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(scribbyPort, "Sojourner help! He's after me.")
        self.addResponse(sojournerPort, "You have to damage him. Trick him into getting hit by the hazards.")
        self.addResponse(scribbyPort, "How? He's too fast.")
        self.addResponse(sojournerPort, "You have to automate your movements, your commands. I'm sending you information now on how to build Scripts and on commands.",lambda:self.helpWidgets[1].setButtonsVisible(["Script","commands"],8))
        self.addResponse(scribbyPort, "Commands? Scripts?")
        self.addResponse(sojournerPort,"Commands are what you've been doing this whole time. talk(), forward(), turnRight(), skip(), restart(), etc. You can type commands one at a time in the Shell or put a bunch of them in a Script.")
        self.addResponse(sojournerPort,"When you run a Script all the commands are executed one after another from top to bottom, which allows you to automate your movements.")
        self.addResponse(sojournerPort, "Using a Script will be much faster than typing commands one at a time in the Shell.",lambda:self.launchBriefScreen(0))
        self.addResponse(sojournerPort, " ",self.endConvo)
            
   
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()       
        self.buildConvo()
        self.buildBriefings()
        talk()
    
    def hitKuka(self):
        self.unlockEndPad()
        if self.onEndPad():
            self.setGameOver(1)
            
        
    def startKuka(self):
        self.kuka.show()
        #print("starting")
        self.kuka.run=True

    def cantRun(self):
        if not self.kuka.damaged:
            self.pauseRobot(True)        
            scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
            #self.pauseRobot(True)
            self.addResponse(scribbyPort,"I can't just run from it. I have to stand and fight and at least try to damage it.")
            talk()  
            #self.printToSpeechBox(scribbyPort, "I can't just run from it. I have to stand and fight and at least try to damage it.", closeOnMove=True)
            self.pauseRobot(False)
        
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet5back.png")   
        #print("Creating level")
        #glorified event pads
        self.createEndPad(650,150,locked=True)
        self.createStartPad(150,650)
        
        #create the robot
        self.createRobot(150,650,-90)
    
        #self.addObstacle(Polygon((280,700),(0,700),(0,0),(150,0),(150,330),(225,330),(200,510)))
        self.addObstacle(Rectangle((0,400),(100,600)), vis=False)
        self.addObstacle(Polygon((0,0),(700,0),(700,100),(400,100),(400,300),(0,300)), vis=False)
        self.addObstacle(Polygon((500,200),(600,200),(600,600),(200,600),
            (200,400),(500,400)), vis=False)
        
        self.hazards.append(FireBall((0,350), (500,350),3, self.deathCollide,speed=20))
        self.addActor(self.hazards[0])
        self.addStepEvent(self.hazards[0].step)        
        
        self.kuka=Kuka((50,650),hitMessage=None,hitPause=20,hitAction=self.hitKuka, scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([50,650])
        self.kuka.wayPoints.append([150,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
        
        self.addActor(EventPad((650,150),50,50,collisionAction=self.cantRun,debug=False))
        
def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet5Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

