import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet4Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        LevelWindow.__init__(self,"Gauntlet4",gameData=gameData,parent=parent)
        
        #NewLevelWindow.__init__(self,"Gauntlet4",rStartX=225,rStartY=145,rStartT=0,
        #rEndX=150,rEndY=650,gameData=gameData,parent=parent)
        self.setup()
        #self.briefScreen.visible=True 
        
    def startBoulder(self):
        self.gate.rotateTo(45)
        self.boulder.shape.body.AngularVelocity=25
        self.boulder.shape.body.LinearVelocity=Vector(0,25)
    def boulderMovement(self):        
        if not self.gameOver and self.runTimer:
            if self.boulder.getY()>350:
                self.boulder.shape.body.AngularVelocity=-25                
                self.boulder.shape.body.LinearVelocity=Vector(-15,15)
            else:
                self.boulder.shape.body.AngularVelocity=25
                self.boulder.shape.body.LinearVelocity=Vector(15,15)
        else:
            self.boulder.shape.body.LinearVelocity=Vector(0,0)
            self.boulder.shape.body.AngularVelocity=0
           
        return True        
    def stopRobot(self):
       self.robot.frame.body.ResetDynamics()
       disableContactList(self.robot.frame.body.ContactList)       
    
    def onLevelStart(self):
        pass
    
    
       
    def createLevel(self):    
        #glorified event pads
        self.createEndPad(150,650)
        self.createStartPad(225,145)
        
        #create the robot
        self.createRobot(225,145,-0)
        
        self.addObstacle(Polygon((0,0),(100,100),(200,200),(450,200),(550,300),(550,400),
            (450,500),(200,500),(100,600),(100,700),(0,700)))
        self.addObstacle(Polygon((150,0),(200,50),(250,100),(500,100),(650,250),
            (650,450),(500,600),(250,600),(200,650),(200,700),(700,700),
            (700,0)))
        self.gate=self.addObstacle(Polygon((0,95),(190,95),(190,100),(0,100)))
        
        self.boulder=Boulder((120,50), self.deathCollide, speed=10,scale=0.75,debug=False,obstructs=True)
        self.addActor(self.boulder)
        self.addStepEvent(self.boulderMovement)
        self.addStartEvent(self.startBoulder)
        self.addEndEvent(self.stopRobot)
        
    '''    
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet4BriefScreen.png"
        
    def setStatusText(self):        
        self.addStatusPad("Not sure what to expect here. I Have to move and turn fast. Pretty sure I need a script to get past this level.",Circle((225,150),10),False)
        self.addStatusPad("You have to be kidding me. Gotta move!!!",Rectangle((300,125),(525,200)),False)
        
    def createObstacles(self):
        self.addObstacle(Polygon((0,0),(100,100),(200,200),(450,200),(550,300),(550,400),
            (450,500),(200,500),(100,600),(100,700),(0,700)))
        self.addObstacle(Polygon((150,0),(200,50),(250,100),(500,100),(650,250),
            (650,450),(500,600),(250,600),(200,650),(200,700),(700,700),
            (700,0)))
        self.gate=self.addObstacle(Polygon((0,95),(190,95),(190,100),(0,100)))
        
    
       
    def createHazards(self):
        self.boulder=Boulder((120,50), self.deathCollide, speed=10,scale=0.75,debug=False,obstructs=True)
        self.addActor(self.boulder)
        self.addStepEvent(self.boulderMovement)
        self.addStartEvent(self.startBoulder)
        self.addEndEvent(self.stopRobot)
    '''
def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet4Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

