import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet8Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        NewLevelWindow.__init__(self,"Gauntlet8",rStartX=650,rStartY=500,rStartT=-180,
        rEndX=300,rEndY=150,gameData=gameData,parent=parent)
        self.setup()
        self.briefScreen.visible=True 
        
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet8BriefScreen.png"
        
    def setStatusText(self):        
        self.addStatusPad("Alright just wait until this thing passes.",Circle((250,500),10),False)
        self.addStatusPad("Hmm, I have to be quick here.",Circle((550,150),10),False)
    def createObstacles(self):
        self.addObstacle(Polygon((0,0),(100,0),(100,100),(0,100)))
        self.addObstacle(Polygon((350,100),(500,100),(500,200),(350,200)))
     
        self.addObstacle(Polygon((600,100),(700,100),(700,200),(600,200)))
        self.addObstacle(Polygon((0,200),(100,200),(100,700),(0,700)))
        self.addObstacle(Polygon((200,550),(700,550),(700,700),(200,700)))
        self.addObstacle(Polygon((200,0),(250,0),(250,300),(700,300),(700,450),(200,450)))        
        
        #Create portals
        self.portal.append(Portal((50,150), (550,150), -180, 0,debug=False))
        self.portal.append(Portal((550,150), (50,150), -180, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)
    def startThwomp(self):
        for t in self.thwomp:
            t.run=True
            
    def createHazards(self):
        self.thwomp.append(Thwomp((150,50),(150,650),1,self.deathCollide,run=False))
        
        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
        self.addStartEvent(self.startThwomp)
            
        self.fire.append(FireBall((700,50), (300,50),2, self.deathCollide,speed=25,bounce=False))
        self.fire.append(FireBall((700,250), (300,250),2, self.deathCollide,speed=25,bounce=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
          

def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet8Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

