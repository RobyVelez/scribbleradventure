import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet7Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        NewLevelWindow.__init__(self,"Gauntlet7",rStartX=550,rStartY=650,rStartT=-180,
        rEndX=50,rEndY=50,gameData=gameData,parent=parent)
        self.setup()
        self.briefScreen.visible=True 
        
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet7BriefScreen.png"
        
    def setStatusText(self):       
        self.addStatusPad("I think I can run past these dudes.",Circle((550,650),10),False)
        self.addStatusPad("Woah. What is this thing. What happens if I use() it?",Circle((50,650),10),False)
        self.addStatusPad("I teleported! That's convenient.",Circle((650,450),10),False)
        
         
    def dummy(self):
        pass
    def createObstacles(self):
        
        self.addObstacle(Polygon((0,200),(600,200),(600,500),(700,500),(700,700),(600,700),(600,600),(500,600),(500,500),(375,500),(375,400),(250,400),(250,300),(150,300),(150,600),(0,600),(0,300)))
        self.addObstacle(Polygon((100,0),(700,0),(700,100),(100,100)))
        
        self.portal.append(Portal((50,650), (650,450), -180, 0,debug=False))
        self.portal.append(Portal((650,450), (50,650), -90, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)

    def startThwomp(self):
        for t in self.thwomp:
            t.run=True
            
    def createHazards(self):
        self.thwomp.append(Thwomp((450,550),(450,650),1,self.deathCollide,run=False))
        self.thwomp.append(Thwomp((325,450),(325,650),1,self.deathCollide,run=False))
        self.thwomp.append(Thwomp((200,350),(200,650),1,self.deathCollide,run=False))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
        self.addStartEvent(self.startThwomp)
            
        self.fire.append(FireBall((700,150), (0,150),2, self.deathCollide,speed=25,bounce=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)

def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet7Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

