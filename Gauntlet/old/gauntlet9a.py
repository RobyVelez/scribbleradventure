import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
  

class gauntlet8Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.fire=[]
        self.saw=[]
        self.portal=[]
        NewLevelWindow.__init__(self,"Gauntlet9",rStartX=450,rStartY=650,rStartT=-90,
        rEndX=225,rEndY=150,gameData=gameData,parent=parent)
        self.setup()
        self.briefScreen.visible=True       

    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet9BriefScreen.png"
  
  
    def setStatusText(self):
        pass
        
    def createObstacles(self):
        self.addObstacle(Rectangle((0,0),(50,700)))        
        self.addObstacle(Rectangle((650,0),(700,700)))
        self.addObstacle(Rectangle((150,0),(550,100)))
        self.addObstacle(Rectangle((150,600),(200,700)))
        self.addObstacle(Rectangle((500,600),(550,700)))
        
        self.addObstacle(Rectangle((150,400),(200,500)))
        self.addObstacle(Rectangle((500,400),(550,500)))
        
        self.addObstacle(Rectangle((150,200),(550,400)))
        self.addObstacle(Rectangle((300,500),(400,700)))        
        
        #Create portals
        self.portal.append(Portal((475,150), (250,650), -90, 0,debug=False))
        self.portal.append(Portal((250,650), (475,150), 90, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)

    
    def createHazards(self):
        self.saw.append(Grinder((350,150), self.deathCollide, rotationSpeed=-10,scale=0.2,debug=False))
        self.saw.append(Grinder((350,450), self.deathCollide, rotationSpeed=10,scale=0.2,debug=False))

        for s in self.saw:
            self.addActor(s)
            self.addStepEvent(s.step)
            
        self.fire.append(FireBall((600,700), (600,0),0, self.deathCollide,speed=25,bounce=False))
        self.fire.append(FireBall((100,700), (100,0),0, self.deathCollide,speed=25,bounce=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
            
def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    return gauntlet8Window(gameData,parent)
if __name__ == "__main__":
    startLevel()
    
    