import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet10Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.saw=[]
        self.portal=[]
        self.gate=None
        NewLevelWindow.__init__(self,"Gauntlet10",rStartX=50,rStartY=550,rStartT=0,
        rEndX=50,rEndY=150,gameData=gameData,parent=parent)
        self.setup()
        self.briefScreen.visible=False 
        
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet6BriefScreen.png"
        
    def setStatusText(self):
        self.addStatusPad("Not this again. I wonder if this is a reoccuring nightmare of Indiana Jones.",
            Circle((50,550),10),False)
        self.addStatusPad("Between a sharp thing and a sharper thing.",Rectangle((400,100),(600,200)),False)

    def createObstacles(self):
        
        self.addObstacle(Polygon((0,0),(200,0),(200,100),(0,100)))
        self.addObstacle(Polygon((500,0),(700,0),(700,100),(500,100)))
        self.addObstacle(Polygon((300,100),(400,100),(400,200),(300,200)))        
        self.addObstacle(Polygon((0,200),(200,200),(200,300),(500,300),
        (500,200),(700,200),(700,400),(0,400)))
        
        self.addObstacle(Polygon((0,600),(100,600),(300,650),(500,650),(700,700),(0,700)))
        self.addObstacle(Polygon((100,400),(100,525),(300,575),(500,575),(700,625),(700,400)))
        self.gate=self.addObstacle(Polygon((-100,500),(100,500),(100,502),(-100,502)))
        
        #Create portals
        self.portal.append(Portal((650,650), (650,150), 0, 0,debug=False))
        self.portal.append(Portal((650,150), (650,650), 0, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)       
            
    def startBoulder(self):
        self.gate.rotateTo(90)
        self.boulder.shape.body.AngularVelocity=25
        self.boulder.shape.body.LinearVelocity=Vector(0,25)
    def boulderMovement(self):        
        if not self.gameOver and self.runTimer:
            self.boulder.shape.body.AngularVelocity=25
            self.boulder.shape.body.LinearVelocity=Vector(25,15)
        else:
            self.boulder.shape.body.LinearVelocity=Vector(0,0)
            self.boulder.shape.body.AngularVelocity=0
           
        return True        
    def stopRobot(self):
        robot = getRobot()
        robot.stop()
        
        del self.interactContactList[:]
        del self.pickupContactList[:]
        del self.talkContactList[:]
        robot.setPose(robot.getLocation[0], robot.getLocation[1], robot.getAngle())
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
       
    def createHazards(self):
        #saws that rotate around some common point
        self.saw.append(Grinder((465,150), self. deathCollide, rotationSpeed=10,scale=0.2,
            orbit=True, orbitCenter=(350,150), orbitRadius=115, orbitStep=0.09, currentAngle=0,debug=False))
        
        self.saw.append(Grinder((235,150), self. deathCollide, rotationSpeed=-10,scale=0.2,
            orbit=True, orbitCenter=(350,150), orbitRadius=115, orbitStep=0.09, currentAngle=3.14,debug=False))
            
        for s in self.saw:
            self.addActor(s)
            self.addStepEvent(s.step)
            
        self.boulder=Boulder((50,475), self.deathCollide, speed=10,scale=0.5,debug=False,obstructs=True)
        self.addActor(self.boulder)
        self.addStepEvent(self.boulderMovement)
        self.addStartEvent(self.startBoulder)  


def startLevel(gameData=None, parent=None):    
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    return gauntlet10Window(gameData,parent)    
if __name__ == "__main__":
    startLevel()

