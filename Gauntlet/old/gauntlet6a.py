import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
class gauntlet6Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        NewLevelWindow.__init__(self,"Gauntlet6",rStartX=200,rStartY=625,rStartT=-90,
        rEndX=200,rEndY=75,gameData=gameData,parent=parent)
        self.fire=[]
        self.gate=[]
        self.setup()
        self.briefScreen.visible=True
  
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet6BriefScreen.png"
        
    def setStatusText(self):
        self.addStatusPad("Oh, dodge ball... I mean dodge fire.",Circle((200,625),10),True)
        self.addStatusPad("Turn and move, turn and move.",Circle((500,500),10),True)
        self.addStatusPad("I need to wait().",Circle((500,275),10),True)
    
    def createObstacles(self):
        self.addObstacle(Polygon((0,0),(150,0),(150,150),(25,150),
        (25,250),(450,250),(450,450),(25,450),(25,550),
        (150,550),(150,700),(0,700)))
        self.addObstacle(Polygon((250,0),(700,0),(700,700),(250,700),
        (250,550),(675,550),(675,450),(550,450),(550,250),(675,250),
        (675,150),(250,150)))        
           
        self.gate.append(self.addObstacle(Line((100,450),(100,550))))
        self.gate.append(self.addObstacle(Line((600,150),(600,250))))
        for g in self.gate:
            #self.sim.window.draw(g)
            g.setWidth(10)
        
    def startFire(self):
        for g in self.gate:
            g.outline.alpha=0
        for f in self.fire:
            f.run=True
    
    def createHazards(self):
        self.fire.append(FireBall((50,500), (650,500),3, self.deathCollide,speed=20,run=False))
        self.fire.append(FireBall((650,200), (50,200),2, self.deathCollide,speed=20,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
        self.addStartEvent(self.startFire)
              
def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet6Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()
