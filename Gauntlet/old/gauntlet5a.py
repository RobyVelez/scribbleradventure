import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"

 
class gauntlet5Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        NewLevelWindow.__init__(self,"Gauntlet5",rStartX=650,rStartY=450,rStartT=-180,
        rEndX=25,rEndY=450,gameData=gameData,parent=parent)
        self.thwomp=[]
        self.setup()
        self.briefScreen.visible=True 
    
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet5BriefScreen.png"
        
    def setStatusText(self):
        self.addStatusPad("I'm just going to wait() here until this thwomp passes.",Circle((550,450),10),False)
        self.addStatusPad("Have to be patient",Circle((250,250),10),False)
        self.addStatusPad("Turn and move! Turn and move!",Circle((150,250),10),False)
    
    def createObstacles(self):
        self.addObstacle(Polygon((0,0),(0,400),(100,400),(100,15),(200,15),(200,200),
            (400,200),(400,15),(500,15),(500,400),(700,400),(700,0)))
        self.addObstacle(Polygon((0,700),(0,500),(100,500),(100,675),(200,675),
            (200,300),(400,300),(400,675),(500,675),(500,500),(700,500),(700,700)))

    def startThwomp(self):
        for t in self.thwomp:
            t.run=True
            
    def createHazards(self):  
        self.thwomp.append(Thwomp((150,55),(150,625),1,self.deathCollide,run=False))
        self.thwomp.append(Thwomp((450,55),(450,625),1,self.deathCollide,run=False))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
        self.addStartEvent(self.startThwomp) 


def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet5Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

    
