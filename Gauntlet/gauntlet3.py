import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet3Window(LevelWindow):
    def __init__(self,gameData,parent):        
        self.saw=[]
        
        LevelWindow.__init__(self,"Gauntlet3",gameData=gameData,parent=parent)
        
        self.setup()
        
    def theApproach(self):
        self.pauseRobot(True)
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        self.addResponse(scribbyPort, "I made it! Wow, The Gauntlet terrified me before, but it doesn't seem so bad.")
        self.addResponse(sojournerPort, "You have made progress, but it's going to get much harder. ")
        self.addResponse(sojournerPort, "I know I said you had to go on this quest to save you're family, but if you want to turn back now is the time.")
        self.addResponse(scribbyPort, "...no. I can do this. I can keep going.")
        self.addResponse(sojournerPort, "Alright. Onward.")
        self.addResponse(sojournerPort, " ",lambda:self.setGameOver(1))
        talk()
    
    def endConvo(self):
        self.pauseRobot(False)
        self.launchBriefScreen(0)
        self.setObjectiveText("-Use motors() and stop() commands to get around grinders.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Use the ",self.getI("motors()")," to set the speed of your wheels and move in curves.\n"])
        text.append(["Try motors(1,0.5) in the shell.\n"])
        text.append(["Note you have to manually stop you're wheels in order to stop. You can use the stop() or motors(0,0) command to halt.\n"])
        text.append(["Remember the Help Pages on the right for more info on motors().\n"])
        b=simpleBrief(text,[self.getR("motors()")])                    
        self.addBriefShape(b,"motors() for moving in curves")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(scribbyPort, "Whew! That was close. I was afraid at first, but this isn't too bad.")
        self.addResponse(sojournerPort, "Don't get over confident. There's still a long road ahead. You should be in the saw room now.")
        self.addResponse(scribbyPort, "Yeah. Hmm... this is going to take forever with forward and turn moves.")
        self.addResponse(sojournerPort, "Expand you're mind. Use you're motors() command which will let you move in curves.")
        self.addResponse(scribbyPort, "motors() command? How does that work.")
        self.addResponse(sojournerPort, "Try motors(1,0.5). It sets the speed of your left and right wheels to 1 and 0.5 respectively.")
        self.addResponse(scribbyPort, "That seems useful. May take a bit to get comfortable with that type of moving.")
        self.addResponse(sojournerPort, "Yup, but it's a powerful command. ")
        self.addResponse(sojournerPort, "Also to stop you have to set the speed of you're motors to 0, motors(0,0), or use the command stop()",lambda:self.pauseRobot(False))
    
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        talk()
    
        
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet3back.png")
        #glorified event pads
        self.createEndPad(615,95,locked=True)
        self.createStartPad(425,665)
        
        #create the robot
        self.createRobot(425,665,-180)
    
        #self.addObstacle(Polygon((280,700),(0,700),(0,0),(150,0),(150,330),(225,330),(200,510)))
        #self.addObstacle(Rectangle((0,0),(250,115)))
        
        #self.addObstacle(Rectangle((600,425),(700,700)))
        
        #self.saw.append(Grinder((100,275), self.deathCollide, rotationSpeed=-10,scale=0.70,debug=False))
        #self.saw.append(Grinder((100,675), self.deathCollide, rotationSpeed=-10,scale=0.70,debug=False))
        
        #self.saw.append(Grinder((765,275), self.deathCollide, rotationSpeed=-10,scale=0.70,debug=False))
        
        self.saw.append(Grinder((425,480), self.deathCollide, rotationSpeed=-10,scale=0.70,debug=False))
        self.saw.append(Grinder((425,95), self.deathCollide, rotationSpeed=-10,scale=0.70,debug=False))
        try:
            t=400
            self.addObstacle(curvedShape2((425,480),250,90,245,convex=False,edgeThickness=t), vis=False)
            self.addObstacle(curvedShape2((425,95),260,0,65,convex=False,edgeThickness=t),vis=False)
        
        except:
            print("error")
            
        for s in self.saw:
            self.addActor(s)
            self.addStepEvent(s.step)
            
        self.addActor(EventPad((615,95),50,50,collisionAction=self.theApproach,debug=False))
        


def startLevel(gameData=None, parent=None):
    level=gauntlet3Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

