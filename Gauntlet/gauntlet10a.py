import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet9Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        LevelWindow.__init__(self,"Gauntlet9",gameData=gameData,parent=parent,debug=True)
        
        
        self.setup()
        
    
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def endConvo(self):
        pass
        
    def buildBriefings(self):
        text=[]
        text.append(["A ",self.getI("Script")," will execute commands one after another without a pause."])
        text.append(["Use the ",self.getI("wait()")," command to add a pause into your script."])
        text.append(["For example:\n\n     forward(4,3)\n     turnRight(4,1)\n     wait(0.5)\n     forward(4,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        self.addResponse(scribbyPort, "That we really close. I just had to do a single forward(), but still.")
        self.addResponse(scribbyPort,"The portal stopping my Script is problematic. What if I have to do a bunch of commands after a portal?")
        self.addResponse(spiritPort, "Put them into a function and then just execute the function.")
        self.addResponse(scribbyPort,"What is a function?")
        self.addResponse(spiritPort, "It's a way to group commands together and allows you to make your own custom commands.")
        self.addResponse(spiritPort, "So you can make two functions one called lowerPath() and one called upperPath(). lowerPath() has the forward(), turnLeft(), etc commands that get you to the blue portal.")
        self.addResponse(spiritPort, "upperPath() would then have the turnRight(), forward(), etc commands to get you from the red portal to the end goal.")
        self.addResponse(spiritPort, "Here, I'll upload info about functions to your Help Pages.",lambda:self.helpWidgets[1].setButtonsVisible(["functions1"],8))
       
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible(["functions1"])
        #self.pauseRobot(True)
        #self.buildConvo()
        #talk()
    
    def createLevel(self):
    
         #glorified event pads
        self.createEndPad(550,50)
        self.createStartPad(550,650)
        
        self.portal.append(Portal((50,650), (50,450), -180, 0,debug=False))
        self.portal.append(Portal((50,450), (50,650), -90, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)
        
        #create the robot
        self.createRobot(550,650,-180)
        
        self.addObstacle(Polygon((100,200),(700,200),(700,600),(500,600),(500,300),(150,300),(150,600),(0,600),(0,500),(100,500),(100,300)))
        
        self.addObstacle(Rectangle((0,0),(500,100)))
        self.addObstacle(Rectangle((600,0),(700,100)))
        
        self.addObstacle(Rectangle((250,550),(400,700)))
                
        self.thwomp.append(Thwomp((450,350),(450,650),1,self.deathCollide,run=False))
        #self.thwomp.append(Thwomp((325,450),(325,650),1,self.deathCollide,run=True))
        self.thwomp.append(Thwomp((200,350),(200,650),1,self.deathCollide,run=False))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
            
        self.fire.append(FireBall((700,150), (0,150),2, self.deathCollide,speed=20,bounce=False,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)

        #self.kuka=Kuka((650,650),hitMessage=None,hitPause=20,hitAction=None, scale=0.70,speed=10,visible=False,debug=False)
        #self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([550,650])
        
        #self.addStartEvent(self.startKuka)
        #self.addStepEvent(self.kuka.step)
        
    
        
def startLevel(gameData=None, parent=None):
    level=gauntlet9Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

