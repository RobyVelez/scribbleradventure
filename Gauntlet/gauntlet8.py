import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet8Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.spirit=None
        self.kuka=None
        self.wayPoints=[]
        LevelWindow.__init__(self,"Gauntlet8",gameData=gameData,parent=parent,debug=True)        
        self.setup()
    
    def endConvo(self):
        self.spirit.hide()
        self.pauseRobot(False)
        self.setObjectiveText("-Escape with Spirit",8)
    
    def setRun(self):
        self.kuka.run=True
        self.pauseRobot(False)
    
    def kukaConvo(self):
        self.addResponse(self.kuka.getAvatar(), "AAARAAARRRRGGGGHHHHHH!!!!")
        self.addResponse(self.kuka.getAvatar(), "You think you're clever. I'll recycle both of you!")
        self.addResponse(self.kuka.getAvatar(), " ",self.setRun)
                
    def flight(self):
        self.pauseRobot(True)
        self.kuka.moveTo(50,150)
        self.kuka.show()
        self.kukaConvo()
        talk()
        
        
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        self.addResponse(scribbyPort, "Spirit!")
        self.addResponse(spiritPort, "How, how did you survive? How are you not scrap?")
        self.addResponse(scribbyPort, "I'll explain later. Right now we need to get out of here.")
        self.addResponse(spiritPort, "I can't move. Kuka took my wheels.")
        self.addResponse(scribbyPort, "Don't worry I'll carry you, but we have to hurry. I stunned Kuka. I don't know how long he'll be out.",self.endConvo)
        
              
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()        
        
        self.helpWidgets[1].setButtonsInvisible(["use()","functions1"])
        self.pauseRobot(True)
        self.buildConvo()
        talk()
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet8back.png")
        #glorified event pads
        self.createEndPad(50,650)
        self.createStartPad(550,150)
        
        #create the robot
        self.createRobot(550,150,0)
        
        #self.addObstacle(Polygon((280,700),(0,700),(0,0),(150,0),(150,330),(225,330),(200,510)))
        self.addObstacle(Rectangle((0,0),(700,100)), vis=False)
        self.addObstacle(Rectangle((100,200),(700,700)), vis=False)
        
        self.spirit=self.quickActor(650, 150, IMAGE_PATH+"spiritPortrait.png",  visible=True, obstructs=True,debug=False)
        #self.kuka=self.quickActor(600, 150,IMAGE_PATH+"kuka.png",  scale=0.75,visible=True, obstructs=False,bodyType="dynamic",debug=False)
        
        self.kuka=Kuka((50,50), self.deathCollide,scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)
        self.addStepEvent(self.kuka.step)
        
        self.addActor(EventPad((50,550),50,50,collisionAction=self.flight,debug=False))
        

def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet8Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

