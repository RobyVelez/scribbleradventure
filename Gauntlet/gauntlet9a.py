import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet10Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        LevelWindow.__init__(self,"Gauntlet10",gameData=gameData,parent=parent)
        
        self.setup()
        
    '''
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet8BriefScreen.png"
        
    def setStatusText(self):        
        self.addStatusPad("Alright just wait until this thing passes.",Circle((250,500),10),False)
        self.addStatusPad("Hmm, I have to be quick here.",Circle((550,150),10),False)
    def buildBriefings(self):
        text=[]
        text.append(["A ",self.getI("Script")," will execute commands one after another without a pause."])
        text.append(["Use the ",self.getI("wait()")," command to add a pause into your script."])
        text.append(["For example:\n\n     forward(4,3)\n     turnRight(4,1)\n     wait(0.5)\n     forward(4,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    '''
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def endConvo(self):
        pass
        
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        self.addResponse(scribbyPort, "That we really close. I just had to do a single forward(), but still.")
        self.addResponse(scribbyPort,"The portal stopping my Script is problematic. What if I have to do a bunch of commands after a portal?")
        self.addResponse(spiritPort, "Put them into a function and then just execute the function.")
        self.addResponse(scribbyPort,"What is a function?")
        self.addResponse(spiritPort, "It's a way to group commands together and allows you to make your own custom commands.")
        self.addResponse(spiritPort, "So you can make two functions one called lowerPath() and one called upperPath(). lowerPath() has the forward(), turnLeft(), etc commands that get you to the blue portal.")
        self.addResponse(spiritPort, "upperPath() would then have the turnRight(), forward(), etc commands to get you from the red portal to the end goal.")
        self.addResponse(spiritPort, "Here, I'll upload info about functions to your Help Pages.",lambda:self.helpWidgets[1].setButtonsVisible(["functions1"],8))
        
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible(["functions1"])
        self.pauseRobot(True)
        self.buildConvo()
        talk()
  
    
    def createLevel(self):
    
        
        #glorified event pads
        self.createEndPad(300,150)
        self.createStartPad(550,650)
        
        #create the robot
        self.createRobot(550,650,-90)
        
        
        self.addObstacle(Rectangle((0,0),(100,100)))
        self.addObstacle(Rectangle((350,100),(500,200)))
     
        self.addObstacle(Rectangle((600,100),(700,200)))
        self.addObstacle(Rectangle((0,200),(100,700)))
        self.addObstacle(Rectangle((200,550),(500,700)))
        self.addObstacle(Rectangle((600,550),(700,600)))
        
        self.addObstacle(Polygon((200,0),(250,0),(250,300),(700,300),(700,450),(200,450)))        
        
        #Create portals
        self.portal.append(Portal((50,150), (550,150), -180, 0,debug=False))
        self.portal.append(Portal((550,150), (50,150), -180, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)
    
        self.thwomp.append(Thwomp((150,50),(150,650),1,self.deathCollide,run=False))
        
        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
            
        self.fire.append(FireBall((700,50), (300,50),2, self.deathCollide,speed=20,bounce=False,run=False))
        self.fire.append(FireBall((700,250), (300,250),2, self.deathCollide,speed=20,bounce=False,run=False))
        
        for f in self.fire:
            self.addActor(f)
            self.addStepEvent(f.step)
            
            
        self.kuka=Kuka((650,650),hitMessage=None,hitPause=20,hitAction=None, scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([50,650])
        self.kuka.wayPoints.append([550,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
         
          

def startLevel(gameData=None, parent=None):
    level=gauntlet10Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()

