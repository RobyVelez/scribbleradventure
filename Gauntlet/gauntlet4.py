import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet4Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.spirit=None
        self.kuka=None
        self.wayPoints=[]
        LevelWindow.__init__(self,"Gauntlet4",gameData=gameData,parent=parent,debug=True)        
        self.setup()
    
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        self.setObjectiveText("-Don't let Kuka get you.")
        self.kuka.run=True
        
    def buildBriefings(self):
        text=[]
        text.append(["Use the ",self.getI("motors()")," to set the speed of your wheels and move in curves.\n"])
        text.append(["Try motors(1,0.5) in the shell.\n"])
        text.append(["Note you have to manually stop you're wheels in order to stop. You can use the stop() or motors(0,0) command to halt.\n"])
        text.append(["Remember the Help Pages on the right for more info on motors().\n"])
        b=simpleBrief(text,[self.getR("motors()")])                    
        self.addBriefShape(b,"motors() for moving in curves")
    
    def showScribby(self):
        self.robots[0].frame.visible=True
        
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        spiritPort = IMAGE_PATH+"spiritPortrait.png"
        self.addResponse(self.kuka.getAvatar(), "Why struggle so hard. Why resist? Just give in.")
        self.addResponse(spiritPort, "Never! I'll never cooperate with you. I'll never give you my parts.")
        self.addResponse(self.kuka.getAvatar(), "That's where you're wrong my friend. You will cooperate and hand over you're parts. Either willing...")
        self.addResponse(self.kuka.getAvatar(), "...or by other means.",self.showScribby)
        #self.robots[0].frame.visible=True
        self.addResponse(scribbyPort, "Those blades were close... Hmm, there doesn't seem to be any hazards in he-... Spirit! I found you! Who is that with you?")
        self.addResponse(sojournerPort, "What is it? What do you see?")
        self.addResponse(scribbyPort, "It looks like a robotic arm; orange and yellow.")
        self.addResponse(sojournerPort, "Kuka?! Is he behind this? He's not that smart.")
        self.addResponse(scribbyPort, "Who's Kuka.")
        self.addResponse(sojournerPort, "He's a scavenger of parts. Don't let him get near you.")
        self.addResponse(self.kuka.getAvatar(), "Hmm... who do we have here.")
        self.addResponse(spiritPort, "Scribby? Can it be. How did you survive The Gauntlet.")
        self.addResponse(scribbyPort, "I'll tell you about it later. I'm here to get you out of here.")
        self.addResponse(self.kuka.getAvatar(), "Oh I don't think that's necessary. You just got here.")
        self.addResponse(self.kuka.getAvatar(), "And you're servos look oh so nice.")
        self.addResponse(spiritPort, "Scribby run!",self.endConvo)
    
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        #self.buildBriefings()
        talk()
    
        #self.addStepEvent(self.recordWayPoints)
        #print(self.robots[0].getLocation())
        #pass
        #self.addStatusPad("Halfway there",Circle((350,350),10),False)
        
        
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet4back.png")
        #glorified event pads
        self.createEndPad(50,650)
        self.createStartPad(50,150)
        
        #create the robot
        self.createRobot(50,150,90)
        self.robots[0].frame.visible=False
    
        #self.addObstacle(Polygon((280,700),(0,700),(0,0),(150,0),(150,330),(225,330),(200,510)))
        self.addObstacle(Rectangle((0,0),(700,100)), vis=False)
        self.addObstacle(Rectangle((100,200),(700,700)), vis=False)
        
        self.spirit=self.quickActor(650, 150, IMAGE_PATH+"spiritPortrait.png",  visible=True, obstructs=True,debug=False)
        #self.kuka=self.quickActor(600, 150,IMAGE_PATH+"kuka.png",  scale=0.75,visible=True, obstructs=False,bodyType="dynamic",debug=False)
        
        self.kuka=Kuka((600,150), self.deathCollide,scale=0.70,speed=10,debug=False)
        self.addActor(self.kuka)
        self.addStepEvent(self.kuka.step)
        

def startLevel(gameData=None, parent=None):
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet4Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

