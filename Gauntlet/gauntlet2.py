import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
class gauntlet2Window(LevelWindow):

    def __init__(self,gameData,parent):
        self.thwomp=[]        
        
        LevelWindow.__init__(self,levelName="Gauntlet2",gameData=gameData,parent=parent,debug=True)                    
        
        #LevelWindow.__init__(self,"Gauntlet2",rStartX=665,rStartY=665,rStartT=-180,       
        #rEndX=50,rEndY=50,gameData=gameData,parent=parent)
        
        self.setup()
        
    def endConvo(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
        self.setObjectiveText("-Use turnRight() and turnLeft() commands to navigate corners.",6)
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(sojournerPort, "See, that wasn't that bad.")        
        self.addResponse(scribbyPort, "I nearly melted the solder off my circuit boards...")
        self.addResponse(sojournerPort, "It builds character.")
        self.addResponse(scribbyPort, "...")
        self.addResponse(sojournerPort, "What do you see infront of you. What's up ahead.")
        self.addResponse(scribbyPort, "There are stone faces going up and down. I'm going to get flatten!")
        self.addResponse(sojournerPort, "They're thwomps. You'll be fine as long as you keep moving. Take it slow and remember you're turning commands. turnRight(2,2) or turnLeft(2,2) should produce turns close to 90 degrees.")
        self.addResponse(sojournerPort, "Small steps remember. Small steps climb the mountain",lambda:self.launchBriefScreen(0))
        self.addResponse(sojournerPort, " ",self.endConvo)
        
        
    def buildBriefings(self):
        text=[]
        text.append(["Keep using the ",self.getI("forward()")," or ",self.getI("backward()")," commands to move.\n"])           
        text.append(["Use the ",self.getI("turnRight()")," or ",self.getI("turnLeft()")," commands to turn the corners.\n"])   
        b=simpleBrief(text,[self.getR("turnRight()"),self.getR("turnLeft()"),self.getR("forward()"),self.getR("backward()")])                    
        self.addBriefShape(b,"Turn commands to get around corners")
    
    
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.buildBriefings()
        self.buildConvo()
        talk()       
    
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet2back.png")   
        #glorified event pads
        self.createEndPad(50,50)
        self.createStartPad(150,650)
        
        #create the robot
        self.createRobot(150,650,-90)
        
        self.addObstacle(Polygon((300,0),(700,0),(700,115),(300,115)), vis=False)
        self.addObstacle(Polygon((0,200),(400,200),(500,200),(500,233),(400,233),(400,333),(0,333)), vis=False)
        self.addObstacle(Rectangle((200,500),(700,700)), vis=False)
        self.addObstacle(Rectangle((0,500),(100,700)), vis=False)
        
        #self.addObstacle(Polygon((200,466),(700,466),(700,625),(300,625),(300,500),(200,500)))
    
        self.thwomp.append(Thwomp((250,50),(250,150),1,self.deathCollide,run=True))
        self.thwomp.append(Thwomp((450,275),(450,425),1,self.deathCollide,run=True))
        #self.thwomp.append(Thwomp((250,550),(250,650),1,self.deathCollide,run=True))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
               

def startLevel(gameData=None, parent=None):
    level=gauntlet2Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                
