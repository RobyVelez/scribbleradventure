import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet12Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.boulder=None
        self.thwomp=[]
        self.fire=[]
        self.portal=[]
        self.endPad=None
        self.runElevator=False
        self.elevator=[]
        self.elevSwitch=None
        self.modeSwitch=None
        self.testLine=None
        
        LevelWindow.__init__(self,"Gauntlet12",gameData=gameData,parent=parent,debug=True)
        
        self.setup()
        
        
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def hitKuka(self):
        self.thwomp[0].show()
        self.kuka.run=False
        self.kuka.stopMotion()
        self.lastConvo()
        talk()
        
    def elevatorMove(self):
        if self.runElevator:
            if self.endPad.getY()>550:
                self.runElevator=False
                return False
            else:
                self.endPad.move(0,3)
                for e in self.elevator:
                    e.move(0,3)
            
        return True
            
    def startElevator(self,foo,bar):
        self.runElevator=True
        return True
        
    def dropThwomp(self,foo,bar):
        self.modeSwitch.changeCostume(1)
        self.thwomp[0].run=True
        
    def lastConvo(self):        
        self.pauseRobot(True)
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png" 
        
        self.addResponse(self.kuka.getAvatar(), "NOOOO-...<crush>...")
        self.addResponse(scribbyPort, "Is... is he scrapped?")
        self.addResponse(spiritPort, "I think so. ")
        self.addResponse(spiritPort, "Let's get out of here.",lambda:self.setGameOver(1))
        
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        self.addResponse(spiritPort, "We're almost there. Just keep using those functions and we'll get out of here.")
        
        
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.buildConvo()
        talk()
  
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet12back2.png")  
        
        #glorified event pads
        self.endPad=self.createEndPad(650,50)
        self.createStartPad(50,150)
        
        #Create portals
        self.portal.append(Portal((50,350), (350,50), 90, 0,debug=False))
        self.portal.append(Portal((350,50), (50,350), 180, 1,debug=False))
        
        #Create portals
        self.portal.append(Portal((550,50), (50,650), 0, 0,debug=False))
        self.portal.append(Portal((50,650), (550,50), -90, 1,debug=False))
        
        #Create portals
        self.portal.append(Portal((50,450), (350,650), -90, 0,debug=False))
        self.portal.append(Portal((350,650), (50,450), -180, 1,debug=False))
        
        
        for p in self.portal:
            self.addActor(p)
    
        self.modeSwitch=self.addActor(EventPad((550,450),50,75,avatar=[LEVEL_PATH+"switchEmergency.png",LEVEL_PATH+"switchNormal.png"],scale=0.35,interact=self.dropThwomp,debug=False))
        self.elevSwitch=self.addActor(EventPad((550,650),50,75,avatar=LEVEL_PATH+"elevatorSwitch.png",scale=0.25,interact=self.startElevator,debug=False))
        
        self.addStepEvent(self.elevatorMove)
        
        #self.testLine=self.addObstacle(Rectangle((125,125),(175,175)))         
        #self.testLine.body.IsSensor=True
        #self.testLine.tag="line"
        
        #create the robot
        self.createRobot(50,150,0)
        
        
        self.addObstacle(Rectangle((0,100),(100,102)), vis=False)
        self.addObstacle(Rectangle((0,200),(100,300)), vis=False)
        self.addObstacle(Rectangle((0,500),(100,600)), vis=False)
        self.addObstacle(Rectangle((200,0),(300,700)), vis=False)
        self.addObstacle(Rectangle((400,0),(500,100)), vis=False)
        self.addObstacle(Rectangle((400,600),(500,700)), vis=False)
        #self.addObstacle(Polygon((
        texture = Picture(LEVEL_PATH+"ElevatorTexture.png")
        self.elevator.append(self.addObstacle(Rectangle((600,-600),(700,0), color=Color(91, 91, 91))))
        texture.draw(self.elevator[-1])
        texture.moveTo(0, 0)
        texture.border=0
        self.elevator.append(self.addObstacle(Rectangle((600,100),(700,700), color=Color(91, 91, 91))))
        texture.draw(self.elevator[-1])
        texture.moveTo(0, 0)
        texture.border=0
        
        self.addObstacle(Rectangle((0,400),(200,401)), vis=False)
        self.addObstacle(Rectangle((600,0),(601,500)), vis=False)
        self.addObstacle(Rectangle((500,200),(501,500)), vis=False)
        self.addObstacle(Rectangle((500,200),(600,400)), vis=False)
        
        
        self.addObstacle(Rectangle((300,200),(600,201)))
        
        self.thwomp.append(Thwomp((400,335),(400,460),1,self.deathCollide,scale=2,run=False,debug=False, hitbox=makeColRec(180, 220, "dynamic")))
        
        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
        
        self.kuka=Kuka((50,50),hitMessage=None,hitPause=20,hitAction=self.hitKuka, scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        self.kuka.wayPoints.append([150,50])
        #self.kuka.wayPoints.append([550,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
          

def startLevel(gameData=None, parent=None):
    level=gauntlet12Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()
    
#from FarseerPhysics.Dynamics import RayCastCallback

'''
#class testCallBack(RayCastCallback):
#    def __init__(self):
#        pass
#    def reportFixture(self):
#        print("foo")    

#fixture
#point
#normal
#fraction
def testCallBack(fixture,v1,v2,hit):
    print(fixture.UserData.x)
    print(fixture.UserData.y)
    fixture.UserData.fill=Color("blue")
    fixture.UserData.outline=Color("yellow")
    print(fixture.IsSensor)
    
    print(v1)
    print(v2)
    print(hit)
    return -1.0
    
#-1 means ignore and keep going
#1 means unknown, possibly record and keep going
#0 means stops/terminate, but doesn't mean yuo've hit the closest objects
#fraction means a hit and go no further down th eline from this object    
c=RayCastCallback(testCallBack)   
L=getLevelObject()
L.sim.window.canvas.world.RayCast(c,Vector(0,0),Vector(700,700))
''' 

