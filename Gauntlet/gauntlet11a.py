import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
  

class gauntlet11Window(LevelWindow):
    def __init__(self,gameData,parent):
        self.fire=[]
        self.saw=[]
        self.portal=[]
        LevelWindow.__init__(self,"Gauntlet9",gameData=gameData,parent=parent,debug=True)
        
        self.setup()
    
    def buildConvo(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"        
        
        self.addResponse(scribbyPort, "We need to get out of here. Spirit do you know where the exit is.")
        self.addResponse(spiritPort, "Not exactly but we can follow those portals.")
        self.addResponse(scribbyPort, "Wait, where did those portals come from. I hadn't seen them before.")
        self.addResponse(spiritPort, "It looks like Kuka activated The Gauntlet's emergency mode. Notice the Hazards have stopped moving.")
        self.addResponse(spiritPort, "In emergency mode the Hazards shut down and the portals appear. We can follow them to the exit.")
        self.addResponse(scribbyPort, "How do we use them?")
        self.addResponse(spiritPort, "Step on the portal and execute the use() command. You can type it in the Shell or put it in a Script.",lambda:self.helpWidgets[1].setButtonsVisible(["use()"],8))
        self.addResponse(spiritPort, "But be careful, the portal will short circuit you and stop any current command or Script. It will stop you cold and you'll have to re-execute your commands or Scripts.")
        self.addResponse(scribbyPort, "Uh, isn't being stopped cold a problem?! We have to move here.")
        self.addResponse(spiritPort, "I'll show you a better way in a second. For now just go. Just be ready to execute a forward() command in the Shell after going through the portal.")
    def onLevelStart(self):
        self.helpWidgets[1].setButtonsInvisible(["use()","functions1"])        
        self.buildConvo()
        talk()
    
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(50,150)
        self.createStartPad(50,550)
        
        #create the robot
        self.createRobot(50,550,0)
        
        self.addObstacle(Rectangle((0,0),(200,100)))
        self.addObstacle(Rectangle((500,0),(700,100)))        
        #self.addObstacle(Polygon((300,100),(400,100),(400,200),(300,200)))        
        self.addObstacle(Polygon((0,200),(200,200),(200,300),(500,300),
        (500,200),(700,200),(700,400),(0,400)))
        
        self.addObstacle(Polygon((0,600),(100,600),(300,650),(500,650),(700,700),(0,700)))
        self.addObstacle(Polygon((100,400),(100,525),(300,575),(500,575),(700,625),(700,400)))
        self.gate=self.addObstacle(Polygon((-100,500),(100,500),(100,502),(-100,502)))
        
        #Create portals
        self.portal.append(Portal((650,650), (650,150), 0, 0,debug=False))
        self.portal.append(Portal((650,150), (650,650), 180, 1,debug=False))
        
        for p in self.portal:
            self.addActor(p)       
            
        self.saw.append(Grinder((350,250), self. deathCollide, rotationSpeed=-10,scale=0.2,
            orbit=True, orbitCenter=(350,150), orbitRadius=115, orbitStep=0.09, currentAngle=3.14,run=False,debug=False))
        self.saw.append(Grinder((350,50), self. deathCollide, rotationSpeed=-10,scale=0.2,
            orbit=True, orbitCenter=(350,150), orbitRadius=115, orbitStep=0.09, currentAngle=3.14,run=False,debug=False))
        
                
        for s in self.saw:
            self.addActor(s)
            
        self.boulder=Boulder((50,475), self.deathCollide, speed=10,scale=0.5,debug=False,obstructs=True)
        self.addActor(self.boulder)
            
def startLevel(gameData=None, parent=None):
    return gauntlet11Window(gameData,parent)
if __name__ == "__main__":
    startLevel()
    
    