import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet6Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.kuka=None
        self.hazards=[]        
        
        LevelWindow.__init__(self,"Gauntlet6",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
            
    def endConvo(self):
        self.pauseRobot(False)
        dismiss()
        self.setObjectiveText("-Use motors(), wait(), and stop() to get around curves")
        
    def buildBriefings(self):
        text=[]
        text.append(["Use ",self.getI("wait()")," in conjunction with ",self.getI("motors()")," and ",Inline2("stop()","helpPage:motorsPage",Color("red"))," command to get around these curves."])
        text.append(["For example:\n\n     forward(4,3)\n     motors(1,0.5)\n     wait(10)\n     stop()\n     turnRight(2,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("motors()")])                    
        self.addBriefShape(b,"wait(), motors(), and stop() in Scripts")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(scribbyPort, "He almost got me! I can't do this. He's going to get me!")
        self.addResponse(sojournerPort, "You can do this. He hasn't gotten you yet. Just have to keep going.")
        self.addResponse(scribbyPort, "How am I going to get around these curves?")
        self.addResponse(sojournerPort, "Use motors() commands in your script.")
        self.addResponse(scribbyPort, "But motors() needs stop() or motors(0,0) to halt movement.")
        self.addResponse(scribbyPort, "If I put motors(1,0.5) and then stop() in a Script they'll be executed one after another and I won't go anywhere. I can't wait and hit stop() myself if I'm using a Script.")
        self.addResponse(scribbyPort, "How do I time my movements?")    
        
        self.addResponse(sojournerPort, "I'm sending you info on the wait() command.",lambda:self.helpWidgets[1].setButtonsVisible(["wait()"],8))
        self.addResponse(sojournerPort, "The wait() command will allow you to add pauses and timing to your movements. For example to pause for 1.5 seconds do wait(1.5)")
        self.addResponse(sojournerPort, "See the example in the briefing.",lambda:self.launchBriefScreen(0))
        self.addResponse(sojournerPort, " ",self.endConvo)
        
   
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.helpWidgets[1].setButtonsInvisible(["wait()","use()","functions1"]) 
        self.pauseRobot(True)
        self.buildConvo()        
        self.buildBriefings()        
        talk()
    
    def hitKuka(self):
        self.unlockEndPad()
        if self.onEndPad():
            self.setGameOver(1)
        
    def startKuka(self):
        self.kuka.show()
        self.kuka.run=True
        
    def cantRun(self):
        if not self.kuka.damaged:
            
            scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
            self.pauseRobot(True)
            self.addResponse(scribbyPort,"I can't just run from it. I have to stand and fight and at least try to damage it. And now the exit is locked. I have to move off and back on it to try again.")
            talk()   
            self.pauseRobot(False)
    
    
   
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"gauntlet6back.png")
        
        #glorified event pads
        self.createEndPad(325,650,locked=True)
        self.createStartPad(150,650)
        
        #create the robot
        self.createRobot(150,650,-90)
        try:
            self.addObstacle(curvedShape([350,350],250,180,360,convex=False,edgeThickness=250), vis=False)
            self.addObstacle(curvedShape([350,350],150,0,359), vis=False)        
            self.addObstacle(curvedShape([350,350],250,0,90,convex=False,edgeThickness=250), vis=False)
        except:
            print("Error with curve funcitons.")
            
        self.addObstacle(Rectangle((0,0),(100,600)), vis=False)        
        self.addObstacle(Rectangle((200,600),(275,700)), vis=False)        
        
        self.hazards.append(Grinder([350,550], self.deathCollide, scale=0.25, rotationSpeed=5, debug=False, orbit=True, orbitCenter=(350,350), orbitRadius=200, orbitStep=0.018, currentAngle=45))
        self.addActor(self.hazards[0])
        self.addStepEvent(self.hazards[0].step)
        
        
        self.kuka=Kuka((50,650),hitMessage=None,hitPause=20,hitAction=self.hitKuka, scale=0.70,speed=10,visible=False,debug=False)
        self.addActor(self.kuka)        
        #appends the kuka's and robot's start position to make the starting look better
        #self.kuka.wayPoints.append([50,650])
        self.kuka.wayPoints.append([150,650])
        
        self.addStartEvent(self.startKuka)
        self.addStepEvent(self.kuka.step)
        
        self.addActor(EventPad((325,650),50,50,collisionAction=self.cantRun,debug=False))
        
def startLevel(gameData=None, parent=None):
    level=gauntlet6Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()                                                

