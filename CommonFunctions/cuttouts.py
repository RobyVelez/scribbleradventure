"""
Deprecated code, should be removed at some point.
"""
class statObject():
    def __init__(self):
        self.score=0
        self.userName=None
        self.levelName=None
        self.restarts=0
        self.loses=0
        self.totalTime=0
        self.winTime=0
        self.code=None
        
    
    def writeToFile(self,fileName=None):
        data="\nUsername:"+str(self.userName)+"\n"
        data+="Level:"+str(self.levelName)+"\n"
        data+="Score:"+str(self.score)+"\n"
        data+="TimeBonus:"+str(self.winTime)+"\n"
        data+="TotalTime:"+str(self.totalTime)+"\n"
        data+="Restarts:"+str(self.restarts)+"\n"
        data+="Loses:"+str(self.loses)+"\n"
        data+="Code:"+str(self.code)+"\n"
        
        if fileName!=None:
            f=open(fileName,'a')
            f.write(data)
        else:
            pass
                
        
    def __str__(self):
        toWrite="Username:"+str(self.userName)+"\n"
        toWrite+="\tLevel:"+str(self.levelName)+"\n"
        toWrite+="\tScore:"+str(self.score)+"\n"
        toWrite+="\tTimeBonus:"+str(self.winTime)+"\n"
        toWrite+="\n"
        toWrite+="\tTotalTime:"+str(self.totalTime)+"\n"
        toWrite+="\tRestarts:"+str(self.restarts)+"\n"
        toWrite+="\tLoses:"+str(self.loses)+"\n"
        toWrite+="\n"
        toWrite+="\tCode:"+str(self.code)+"\n"

        return toWrite
