"""
Deprecated code, should be removed at some point.
"""

import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *


def intro1Briefa():
    b2=Picture(LEVEL_PATH+"intro1Briefa.png")
    text=[Wrap(475), SkipV(BRIEFING_TEXT_Y_POS)]
    text+=[Related("forward()", "helpPage:forwardPage", Color("red")), " "]
    text+=[Related("backward()", "helpPage:backwardPage", Color("red")), " "]        
    text+=[Related("shell", "helpPage:shellPage", Color("purple"))] 
    
    writeText(b2, text)
    
    
