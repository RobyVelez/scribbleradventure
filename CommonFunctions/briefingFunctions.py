"""
A set of functions and classes to create briefscreens.

More information on what can and can not be written to the briefscreen 
can be found in :mod:`fancyTextFunctions`.

This module is currently a little broken, but it may be usefull in the future.
"""

from Myro import *
from Graphics import *

from os.path import dirname, realpath
from constructionFunctions import *
from utilityFunctions import getWidth, getHeight
from fancyTextFunctions import *

IMAGE_PATH=dirname(dirname(realpath(__file__)))+"/CommonImages/"

BRIEFING_TEXT_Y_POS = 25
BRIEFING_TEXT_INDENT = 20
BRIEFING_TEXT_PAR_SKIP = 5

RELATED_PAGES_Y_POS = 345
RELATED_PAGES_BUTTON_WIDTH = 92
RELATED_PAGES_BUTTON_HEIGHT = 35

INLINE_BUTTON_WIDTH = 80
INLINE_BUTTON_HEIGHT = 20


def simpleBrief(briefText,relatedPages,templatePictureFile=None,bulletMarker=None):
    """
    Creates a simple briefing.

    Args:
        briefText: The text to be shown in the briefing.
        relatedPages: The related pages shown at the bottom of the briefing.
        templatePictureFile: String pointing to the image file to use as a 
            background for the briefing.
        bulletMarker: Charachter to be used as a bullet marker in the briefing.
    """
    #General baseline parameters for the briefing screen
    text=[Wrap(475), SkipV(BRIEFING_TEXT_Y_POS)]
    #print(briefText)
    for i in range(len(briefText)):
        if bulletMarker==None:
            text+=[Bullet(str(i+1)+".",briefText[i])]
        else:
            text+=[Bullet(bulletMarker,briefText[i])]
    
        #print(text)
    
    #set the vertical position of the related pages
    text+=[SetV(RELATED_PAGES_Y_POS)]
    for r in relatedPages:
        text+=r
        #Related(n,"helpPage:"+button.tag,button.fill)," "]
        
        #buttonNames:
        #buttonAndText=helpWidget.buttonAndText[n]
        #button=buttonAndText[0]
        
        #text+=[Related(n,"helpPage:"+button.tag,button.fill)," "]
        #print(text)
    if isinstance(templatePictureFile, Shape):
        template = templatePictureFile
    elif templatePictureFile:
        template=Picture(templatePictureFile)    
    else:
        template=Picture(IMAGE_PATH+"blankBriefWithText.png")
    
    writeText(template, text)
    
    return template    
    
class Inline(object):
    """
    Adds an inline button to the briefscreen.
    
    Args:
        text: the text to appear on the button.
        tag: the tag used to determine what should happen if 
             this button is clicked.
    """
    def __init__(self,text,tag=None,color=Color("white")):
        self.text = text
        if tag:
            self.tag = tag
        else:
            self.tag = text
        self.color=color
        


class Briefing(object):
    """
    The briefing object, a convience function for writing to a blank
    brief screen.
    """
    def __init__(self, levelWindow):
        """
        Constructs a briefing object.
        
        Args:
            levelWindow: a pointer to the level window object.
        """
        self.briefing = [SkipV(BRIEFING_TEXT_Y_POS)]
        self.target = Picture(IMAGE_PATH+"blankBriefWithText.png")
        self.levelWindow = levelWindow
        
    def line(self, *parts):
        """
        Adds a line of text to the briefing.
        
        Note that, internally, the briefing object uses the :mod:`fancyTextFunctions`
        module, so all commands found there can be added here.
        
        Args:
            parts: the text and objects to be added to the briefing.
        """
        for part in parts:
            if isinstance(part, Inline):
                #TODO JH: What is the correct way to access buttons by tag?
                #button = self.levelWindow.panelButtons[actualTag]
                #color = button.color
                newButton, _ = createPanelButton(self.target, 0, 0, INLINE_BUTTON_WIDTH, INLINE_BUTTON_HEIGHT, part.text, part.tag) 
                self.briefing.append(newButton)
                #buttonColor=color)
            else:  
                self.briefing.append(part)
        self.briefing += '\n'
        
        
    def par(self, *parts):
        """
        Same as line, but adds some white-space.
        
        Args:
            parts: the text and objects to be added to the briefing.
        """
        self.briefing += [SkipV(BRIEFING_TEXT_PAR_SKIP)]
        self.line(*parts)
            
            
    def relatedPages(self, pageDetails):
        """
        Constructs the related pages section of the briefscreen.
        
        Currenlty broken.
        
        Args:
            parts: the buttons to add to the briefscreen.
        """
        #Probably a much more elegant programming way of doing this, but in short
        #there needs to be the ability to set the text, tag, and color of the 
        #related page buttons. ideally there would also be the avatar option for the
        #future. RV
        self.briefing += [SetV(RELATED_PAGES_Y_POS)]
        for d in pageDetails:
            #for now the button avatar is just left as None
            #createPanelButton(panel,x,y,w,h,text,tag,avatar=None,buttonColor=Color("white"),textColor=Color("black"),fS=18)
            newButton, _ = createPanelButton(self.target, 
                                            0, 
                                            0, 
                                            RELATED_PAGES_BUTTON_WIDTH, 
                                            RELATED_PAGES_BUTTON_HEIGHT, 
                                            d[0], 
                                            d[1],
                                            avatar=None,
                                            buttonColor=d[2]) 
                                            #buttonColor=color)
            self.briefing += [newButton]

        '''
        for part in parts:
            #TODO JH: What is the correct way to access buttons by tag? 
            #button = window.panelButtons[tag]
            #color = button.color
            newButton, _ = createPanelButton(self.target, 
                                            0, 
                                            0, 
                                            RELATED_PAGES_BUTTON_WIDTH, 
                                            RELATED_PAGES_BUTTON_HEIGHT, 
                                            part, 
                                            part) 
                                            #buttonColor=color)
            self.briefing += [newButton]
        '''
        
    def write(self):
        """
        Actually writes all the text to the blank brief screen.
        
        Return:
            The figure with all text written to it.
        """
        writeText(self.target, self.briefing)
        return self.target
