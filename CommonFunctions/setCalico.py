"""
The only purpose of this module is to ensure that calico is found properly on 
Windows machines. To use it, import in the main file that needs to be loaded, 
and call: 
::

    initCalico(calico). 

Note: This text is verbatim, the word calico should be typed there.
"""

## import sys
## import os
## sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
## sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../GameObjects")
## sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../WindowObjects")
## 
## from levelWindow import *
## from hazards import * 
## from interactables import *
## from events import *
## from SpriteSheet import SpriteSheet

def initCalico(calc):
    """
    Sets calico as a global variable.

    By setting calico as a global veriable explicitly, calico will also be 
    available on Windows machines when loading modules manually. The only 
    correct way of calling this function is with initCalico(calico). Also
    disables shell reset on run, because Scribbler Adventure does not survive
    this operation, but this is scheduled for change in the future.

    Args:
        calc: A reference to the calico object automatically accesible when 
            running the python interpreter in Calico.
    """
    global calico
    calico = calc
    calico.config.SetValue('python-language','reset-shell-on-run',False)
