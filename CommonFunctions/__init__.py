"""
Module containing common utility functions used throughout the portal window.

This file may look useless, but it turns the CommonFunctions folder into a 
python package, allowing you to import files with CommonFunctions.<filename>.
"""
