"""
These functions are independent of the various levels or main world screeen and
simply make it easier to add items to panels or windows.
"""
from Graphics import *


#Button table object
#Grid of buttons

def makeSimplePanel(p1,p2,drawPlane,tag="panel",
                    color=Color("white"),clickFunction=None,visible=True):
    """
    Creates a simple panel.

    A Simple panel is simply a rectangle with a black border, usually with some
    on click function attached to it.

    Args:
        p1: Tuple (x, y) indicating the coordinate for the top left of the 
            panel.
        p2: Tuple (x, y) indicating the coordinate for the bottom right of the 
            panel.
        drawPlane: :class:`~Graphics.Shape` that this panel should be drawn to.
        color: :class:`~Graphics.Color` indicating the background color for this
            panel.
        clickFunction: :func:`onClick` callback function to be executed when 
            this panel is clicked.
        visible: boolean indicating whether this panel should be visible.
    """
    panel=Rectangle(p1,p2,color=color)
    panel.tag=tag
    panel.visible=visible
    
    panel.outline=Color("black")    
    panel.setWidth(2)
    panel.bodyType="static"
    
    panel.draw(drawPlane)
    if clickFunction !=None:
        panel.connect("click",clickFunction)
    
    return panel

def createPanelLine(panel,point1,point2,c=Color("black"),lw=None):
    """
    Draws a line to a panel.

    Args:
        panel: :class:`~Graphics.Rectangle` that the line will be drawn to.
        point1: Tuple (x, y) indicating the coordinate where the line should 
            start.
        point2: Tuple (x, y) indicating the coordinate where the line should 
            end.
        c: :class:`~Graphics.Color` of the line.
        lw: float indicating the width of the line.
    """
    #text is drawn to the center of the panel so it must be
    #offset to the upper left corner (0,0)
    oX=panel.width/2
    oY=panel.height/2
    
    lineObj=Line((point1[0]-oX,point1[1]-oY),(point2[0]-oX,point2[1]-oY),
                 color=c)
    if lw:
        lineObj.setWidth(lw)
    lineObj.draw(panel)    
    
    return lineObj
    
def createPanelText(panel,x,y,text,c=Color("black"),fS=18,yJust=None):
    """
    Draws text to a panel.

    Args:
        panel: :class:`~Graphics.Rectangle` that the text will be drawn to.
        x: float indicating the x coordinate of the text.
        y: float indicating the y coordinate of the text.
        text: string indicating the text to be drawn.
        c: :class:`~Graphics.Color` of the text.
        fS: integer indicating the font size of the text.
        yJust: string indicating the vertical alginment of the text. Options 
            are: "top", "bottom", or "center". Default is "bottom".
    """
    #text is drawn to the center of the panel so it must be
    #offset to the upper left corner (0,0)
    oX=panel.width/2
    oY=panel.height/2
    
    textObj=Text((x-oX,y-oY+fS),text,color=c,fontSize=fS)
    textObj.setXJustification("left")
    if yJust:
         textObj.setYJustification(yJust)
    else:
        textObj.setYJustification("bottom")
    
    textObj.draw(panel)
    
    return textObj

# FIXME (JH): Why do we have a special function for a `load` button?
def createLoadButton(panel,x,y,w,h,text,tag,avatar=None,
                     buttonColor=Color("white"),textColor=Color("black"),
                     fS=18,leftIcon=None,timeStamp=None):
    """
    Draws a load button to a panel.

    Args:
        panel: :class:`~Graphics.Rectangle` that the text will be drawn to.
        x: float indicating the x coordinate of the text.
        y: float indicating the y coordinate of the text.
        w: float indicating the width of the button.
        h: float indicating the height of the button.
        text: string indicating the text to be shown on the button.
        tag: string used to identify the action to be performed when this button
            is clicked.
        avatar: :class:`~Graphics.Picture` to be shown on the button.
        buttonColor: :class:`~Graphics.Color` of the background of the button.
        textColor: :class:`~Graphics.Color` of the text of the button.
        fS: integer indicating the font size of the text.
        leftIcon: :class:`~Graphics.Picture` to be shown at the left side of the
            button.
        timeStamp: value is ignored.
    """
    #text is drawn to the center of the panel so it must be
    #offset to the upper left corner (0,0)
    oX=panel.width/2
    oY=panel.height/2
    
    #text spacing with edge
    space=5
    
    #radius of rounded rectangle
    rad=0

    button=RoundedRectangle((x-oX+1,y-oY),(x-oX+w,y-oY+h),rad,color=buttonColor)
    button.setWidth(0)
    button.tag=tag #very important to tag this object
    button.draw(panel)

    icon=None
    s=0.1
    if leftIcon!=None:
        icon=Picture(leftIcon)
        #icon.scale(s)
        icon.outline.alpha=0
        icon.draw(button)
        icon.move(-w/2,-h/2)
        
    if icon!= None:
        space=space+icon.width
        
    if avatar!=None:
        pic=Picture(avatar)
        pic.draw(button)
    
    #button.move(oX-infoW,oY-2*infoH)

    textObj=Text((0,0),text,color=textColor,fontSize=fS)
    textObj.draw(button)
    textObj.move(-w/2+textObj.width/2+space,0)
        
    return [button,textObj]


def createPanelButton(panel,x,y,w,h,text,tag,avatar=None,
                      buttonColor=Color("white"),textColor=Color("black"),
                      fS=18):
    """
    Draws a button to a panel.

    Args:
        panel: :class:`~Graphics.Rectangle` that the text will be drawn to.
        x: float indicating the x coordinate of the text.
        y: float indicating the y coordinate of the text.
        w: float indicating the width of the button.
        h: float indicating the height of the button.
        text: string indicating the text to be shown on the button.
        tag: string used to identify the action to be performed when this button
            is clicked.
        avatar: :class:`~Graphics.Picture` to be shown on the button.
        buttonColor: :class:`~Graphics.Color` of the background of the button.
        textColor: :class:`~Graphics.Color` of the text of the button.
        fS: integer indicating the font size of the text.
    """
    #text is drawn to the center of the panel so it must be
    #offset to the upper left corner (0,0)
    oX=panel.width/2
    oY=panel.height/2
    
    #radius of rounded rectangle
    rad=5
    button=RoundedRectangle((x-oX,y-oY),(x-oX+w,y-oY+h),rad,color=buttonColor)
    button.outline=Color("black")
    button.tag=tag #very important to tag this object
    button.draw(panel)

    if avatar!=None:
        pic=Picture(avatar)
        pic.draw(button)
    
    #button.move(oX-infoW,oY-2*infoH)

    textObj=Text((0,5),text,color=textColor,fontSize=fS)
    textObj.yJustification = "bottom"
        
    textObj.draw(button)
    
    button.draw(panel)
    
    return [button,textObj]
    

def createNextButton(panel,x,y,w,h,text,tag,buttonColor=Color(255, 235, 100),
                     textColor=Color("black"),fS=18,shape="rectangle"):
    """
    Draws a 'next' button to a panel.

    The 'next' button is a button shown on the 
    :class:`~conversation.speechBox.SpeechBox`
    that indicates whether there are still responses left within the current 
    conversation. If there are still responses left, the 'next' button is 
    triangular. If there are no responses left, the 'next' button is a rounded 
    rectangle.

    Args:
        panel: :class:`~Graphics.Rectangle` that the text will be drawn to.
        x: float indicating the x coordinate of the text.
        y: float indicating the y coordinate of the text.
        w: float indicating the width of the button.
        h: float indicating the height of the button.
        text: string indicating the text to be shown on the button.
        tag: string used to identify the action to be performed when this button
            is clicked.
        buttonColor: :class:`~Graphics.Color` of the background of the button.
        textColor: :class:`~Graphics.Color` of the text of the button.
        fS: integer indicating the font size of the text.
        shape: string indicating the shape of the button. Options are:
            "rectangle" and "triangle".
    """
    #text is drawn to the center of the panel so it must be
    #offset to the upper left corner (0,0)
    oX=panel.width/2
    oY=panel.height/2
    
    #radius of rounded rectangle
    rad=5

    if shape == "rectangle":
        button=RoundedRectangle((x-oX,y-oY),(x-oX+w,y-oY+h),rad,
                                color=buttonColor)
        button.outline=Color("black")
        button.tag=tag #very important to tag this object
    else:
        button=Polygon((x-oX,y-oY), (x-oX+w,y-oY), (x-oX+w/2,y-oY+h))
        button.fill=buttonColor
        button.outline=Color("black")
        button.tag=tag #very important to tag this object

    textObj=Text((0,5),text,color=textColor,fontSize=fS)
    textObj.yJustification = "bottom"
    textObj.draw(button)
    
    button.draw(panel)
    
    return button
    
