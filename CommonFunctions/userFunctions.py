"""
These are functions that the user can use to control the robot or interact with 
the world. For the most part they use either getRobot(), getSimulation(), or 
getLevelObject() to obtain a pointer to the current objects they must modify.
"""

from Myro import *
from time import sleep,time
from math import sqrt,cos,sin,pi,copysign,radians
from random import random


'''
#Fixed the built in Calico getLine() function
#This should no longer be need. RV 10/25/15

#overridden functions for the user
def getLine():
    print("custom get line")
    leftSensor = 0;
    rightSensor = 0;
    robot=getRobot()
    if robot != None:
        IR_Loc1 = [-12,5]
        IR_Loc2 = [-12,-5]
        loc = [robot.frame.getX(),robot.frame.getY()]
        theta = -1*radians(robot.frame.rotation)
        newIR_Loc1 = [cos(theta)*IR_Loc1[0] - sin(theta)*IR_Loc1[1] + loc[0],
                        sin(theta)*IR_Loc1[0] + cos(theta)*IR_Loc1[1] + loc[1]]
        newIR_Loc2 = [cos(theta)*IR_Loc2[0] - sin(theta)*IR_Loc2[1] + loc[0],
                        sin(theta)*IR_Loc2[0] + cos(theta)*IR_Loc2[1] + loc[1]]
        #newIR_Loc1 = [cos(theta)*IR_Loc1[0] - sin(theta)*IR_Loc1[1],
        #                sin(theta)*IR_Loc1[0] + cos(theta)*IR_Loc1[1]]
        #newIR_Loc2 = [cos(theta)*IR_Loc2[0] - sin(theta)*IR_Loc2[1],
        #                sin(theta)*IR_Loc2[0] + cos(theta)*IR_Loc2[1]]
        sim=getSimulation()
        for s in sim.window.canvas.shapes:
            if s.ToString().find("Picture") != -1 and s.tag=="Tile":
                if s.hit(newIR_Loc1[0],newIR_Loc1[1]):
                    r,g,b,a=s.getRGBA(newIR_Loc1[0],newIR_Loc1[1])
                    if r==0 and g==0 and b==0 and a==255:                    
                        leftSensor = 1
                if s.hit(newIR_Loc2[0],newIR_Loc2[1]):
                    r,g,b,a=s.getRGBA(newIR_Loc2[0],newIR_Loc2[1])
                    if r==0 and g==0 and b==0 and a==255:
                        rightSensor = 1
                
    else:
        print("no robot for getline")
    print("test")
    return [rightSensor,leftSensor]
'''  
#########################################
#Commands that emulate the panel buttons#
#########################################

def restart():
    """Restarts the current level."""
    if getLevelObject()!=None:
        getLevelObject().restartGameNoStopScript()

        
def next():
    """Proceeds to the next level, if unlocked."""
    if getLevelObject()!=None:
        L=getLevelObject()        
        L.saveStats()
                
        if L.parent != None:
            nextLevel=L.parent.nextLevelExists()
            if nextLevel=="end of stage":
                menu()
            else:
                #moved saving of data to L.saveStats()
                #L.parent.saveGameData()
                L.parent.launchNextLevel() 
                
        #Not going to stop the script
        #assumption is that some is trying to solve SA
        #with a single script/program
        #L.stopScript()  
        
def prev():
    """Returns to the previous level."""
    if getLevelObject()!=None:
        L=getLevelObject()
        L.saveStats()
        if L.parent != None:
            #L.parent.saveGameData()        
            L.parent.launchPrevLevel() 
        #Not going to stop the script
        #assumption is that some is trying to solve SA
        #with a single script/program
        #L.stopScript()
        
def menu():
    """Exists the current level and returns to the Scribbler Adventure menu."""
    if getLevelObject()!=None:        
        if getLevelObject().parent==None:
            pass
        else:
            L=getLevelObject()
            L.saveStats()
            #save data
            if L.parent != None:
                #L.parent.saveGameData()
                L.parent.showWindow()
            L.sim.window.Destroy()
            if L.parent != None:
                L.parent.showWindow()
                #L.parent.onReturn()
                
def dismiss():
    """Hides any brief screens, help pages, or conversations."""
    if getLevelObject()!=None:
        return getLevelObject().dismiss()

################
#Game functions#
################               
def pickup():
    """Pickup an item that Scribby is currently on top of."""
    if getLevelObject()!=None:
        return getLevelObject().pickup()
    
def use(*items):
    """
    Uses items in the world or stored in variables.

    Interacts with the oject that Scribby is currenlty on top of, or uses the
    items passed as arguments to this function. If items are passed to this 
    function while Scribby is on top of an interactable object, this function
    attempts to use those items on the object.
    """
    if getLevelObject()!=None:
        return getLevelObject().use(list(items))
    
def talk():
    """Talk with NPCs in the area."""
    if getLevelObject()!=None:
        return getLevelObject().talk()

def skip():
    """Skip the current conversation."""
    if getLevelObject()!=None:
        getLevelObject().skip()
        #Catchall to all any events to finsih
        #See the initial Chamber levels and the Teleport step as an example
        wait(0.5)

######
#Misc#
######

def getDialog():
    """Returns the current text in the dialog box."""
    l=getLevelObject()
    return l.speechBox.text


def getGameState():
    """
    Returns the current game state.

    Return: A list with three elements. If there is currently a level open, it
    will return a string stating 'LevelObject', an integer indicating whether 
    we reached a game-over state, and a string containing the level name. If we
    are in the Scribbler Adventure window, this function will return the string
    'ScribblerActivity', and integer indicating the current stage, and and 
    integer indicating the current level.
    """
    #Does the levelObject exist and is it's window visible
    if getLevelObject()!=None and getLevelObject().sim.window.Visible:
        #print(getLevelObject().sim.window.Visible)
        L=getLevelObject()
        return ["LevelObject",L.gameOver,L.levelName]
    else:        
        #Find the handle to the Scribbler Activity Window if it exists
        scribHandle=None
            
        if existsUserFunc("portal"):
            portalHandle=calico.manager.scope.GetVariable("portal")
            if portalHandle.mainWindow.child!=None and portalHandle.mainWindow.child.getTitle()=="ScribblerAdventure":            
                #if isinstance(portal.mainWindow.child,ScribblerAdventureWindow):                
                scribHandle=portalHandle.mainWindow.child
        elif existsUserFunc("scribAdj"):
            scribHandle=calico.manager.scope.GetVariable("scribAdj")
        
        #print(scribHandle)
        if scribHandle !=None:
            if scribHandle.isVisible():
                return ["ScribblerActivity",scribHandle.currentStage,scribHandle.currentLevel]
        
    
         
        
def existsUserFunc(name=None):
    """
    Function to check if a variable is in scope.

    Args:
        name (string): The name of the function to check.
    """
    try:
        calico.manager.scope.GetVariable(name)
    except:
        return False
    else:  
        return True
            
def launchStage(name=None):
    """
    Launches a particular stage.

    Args:
        name (string): The name of the stage to launch.
    """
    scribHandle=None
    
    if existsUserFunc("portal"):
        portalHandle= calico.manager.scope.GetVariable("portal")
        if portalHandle.mainWindow.child!=None and portalHandle.mainWindow.child.getTitle()=="ScribblerAdventure":            
            scribHandle=calico.manager.scope.GetVariable("portal").mainWindow.child
    elif existsUserFunc("scribAdj"):
        scribHandle=calico.manager.scope.GetVariable("scribAdj")
    if scribHandle !=None:
        if name in scribHandle.stages:
            scribHandle.launchActivity(name)
        else:
                L.outputPrint("Error unknown stage "+str(name))
                L.outputPrint("Possible stages: \n"+str(scribAdj.stages))           
                
                

