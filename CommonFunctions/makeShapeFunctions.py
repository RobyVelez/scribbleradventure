"""
This is a set of convience functions for creating shapes.
"""

import os
from Graphics import *

IMAGE_PATH=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonImages/"


def makeDebugColor(r, g, b, a, debug):
    """
    Creates visible or invisible color depending on the debug flag

    If debug is True, creates the indicated color; otherwise returns a 
    transparant color.
    
    Args:
        r: The red component.
        g: The green component.
        b: The blue component.
        a: The alpha component.
        debug: Boolean, indicating whether this should be a 'visible' color.
 
    Returns:
        Returns the indicated color if debug is True; returns an invisible 
            transparant color if debug is False.
    """
    if debug:
        return makeColor(r, g, b, a)
    else:
        return makeColor(0, 0, 0, 0)
       
        

def makeDebugPicture(picture, debug):
    """
    Takes a picture and makes all pixels transparant.
    
    Args:
        avatar: Picture object.
        debug: Boolean. If false, this function return the avatar unmodified.
    """
    if debug:
        for pixel in picture.getPixels():
            if pixel.getAlpha() > 0:
                pixel.setAlpha(120)
    else:
        #Assuming that we did not change the pixels in the images manually,
        #this should revert the picture back to before debug-mode, preserving
        #any other transformations.
        #If we did change the pixels manually, then, well, I guess we should
        #not switch back out of debug mode.
        #originalPicture = Picture(picture.filename)
        #picture.setPixels(originalPicture)
        pass
    return picture


def makePic(fileName, scale=1.0):
    """
    Takes a filename and create a centered picture without border.
    
    Args:
        fileName: The name of a file containing a picture (.jpg and .png are 
            supported)
        scale: The scale of the picture.
        
    Returns:
        A Picture object.
    """
    if isinstance(fileName, Picture):
        pic = fileName
    elif isinstance(fileName, str) and os.path.isfile(fileName):    
        pic = Picture(fileName)
    else:
        pic = Picture(IMAGE_PATH + "error.png")    
    pic.border = 0
    pic.scale(scale)
    pic.moveTo(0,0)        
    return pic


def makeCircle(radius=50, color=Color("blue"), bodyType="static", x=0, y=0):
    """
    Creates a :class:`~Graphics.Circle` that can be added to the simulator.

    Args:
        radius: float indicating the radius of the circle.
        color: :class:`~Graphics.Color` for both the fill and border of the 
            rectangle.
        bodyType: string indicating whether this rectangle should be moved by 
            the simulator. Options are: "static" or "dynamic".
        x: float indicating the x coordinate of the rectangle.
        y: float indicating the y coordinate of the rectangle.
    """
    if isinstance(color, str): color = Color(color)
    if hasattr(color, "__getitem__"):
        if len(color) == 3:
            color = makeColor(color[0], color[1], color[2], 255)
        if len(color) == 4:
            color = makeColor(color[0], color[1], color[2], color[4])
    collisionCircle = Circle((x,y), radius, color=color, bodyType=bodyType)
    return collisionCircle
    
    
def makeRectangle(w=100, h=100, color=Color("blue"), outline=None, fill=None, 
                  bodyType="static", x=0, y=0):
    """
    Creates a :class:`~Graphics.Rectangle` that can be added to the simulator.

    Args:
        w: float indicating the width of the rectangle.
        h: float indicating the heigth of the rectangle.
        color: :class:`~Graphics.Color` for both the fill and border of the 
            rectangle.
        outline: :class:`~Graphics.Color` for the border of the rectangle.
        fill: :class:`~Graphics.Color` for the fill of the rectangle.
        bodyType: string indicating whether this rectangle should be moved by 
            the simulator. Options are: "static" or "dynamic".
        x: float indicating the x coordinate of the rectangle.
        y: float indicating the y coordinate of the rectangle.
    """
    if isinstance(color, str): color = Color(color)
    if hasattr(color, "__getitem__"):
        if len(color) == 3:
            color = makeColor(color[0], color[1], color[2], 255)
        if len(color) == 4:
            color = makeColor(color[0], color[1], color[2], color[4])
    rectangle = Rectangle((x,y), (x+w,y+h))
    rectangle.fill = color
    rectangle.outline = color
    if fill: rectangle.fill = fill
    if outline: rectangle.outline = outline
    return rectangle


def makeColCircle(radius, bodyType="static"):
    """
    Creates a transparant circle with the indicated radius
    
    Args:
        radius: The radius of the circle.
        bodyType: The body type of the circle, either 'dynamic' or 'static'.
        
    Returns:
        A Circle object.
    """
    collisionCircle = Circle((0,0), radius)
    collisionCircle.bodyType = bodyType
    collisionCircle.border = 0
    collisionCircle.fill = makeColor(0,0,0,0)
    return collisionCircle


def makeColRec(width, height, bodyType="static"):
    """
    Creates a transparant, centered rectangle of the indicated width and height.
    
    Args:
        width: The width of the rectangle
        height: The height of the rectangle
        bodyType: The body type of the rectangle, either 'dynamic' or 'static'.
    """
    rec = Rectangle((0,0), (width, height))
    rec.bodyType = bodyType
    rec.border = 0
    rec.fill = makeColor(0,0,0,0)
    rec.moveTo(0, 0)
    return rec


def makeColCircleAvatar(avatar, bodyType="static"):
    """
    Creates an invisible circle based on the size of an avatar.

    Creates a transparant circle with a radius equal to the smallest side of the avatar.
    
    Args:
        avatar: Usually a Picture object, must implement getP1() and getP2().
        
    Returns:
        A Circle object.
    """
    width = abs(avatar.getP1().x - avatar.getP2().x)
    height = abs(avatar.getP1().y - avatar.getP2().y)
    radius = min(width, height)/2
    return makeColCircle(radius, bodyType) 
    
    
def makeText(text=" ", size=16, color=Color(0,0,0), xjust="left", yjust="bottom",
             pos=(0,0), bodyType="static"):
    """
    Creates a text object with the Helvetica font.

    Not much different from creating a text object manually, but applies the 
    standard Scribbler Adventure font, which is currently Helvetica, and makes
    the color black, which is arguably more useful than Calico's purple.

    Args:
        text (string): The text to display in the text object.
        size (float): The font size of the text.
        color (:class:`.Color`): The color of the text.
        xjust (string): Horizontal alignment of the text. Options are: 'left', 
            'right', and 'center'.
        yjust (string): Vertical alignment of the text. Options are: 'top', 
            'bottom', and 'center'.
        bodyType (string): The body type of the text, used if the text is ever
            added to the physics simulator. Options are: 'static' and 'dynamic'.
    """
    text = Text(pos, str(text), color=color)
    text.bodyType = bodyType
    text.yJustification = yjust
    text.xJustification = xjust
    text.fontFace = "Helvetica Neue Light"
    text.fontSize = size
    return text
