"""
Imports all common Scribbler Adventure modules, and sets the appropriate paths
for those modules. Mostly a convenience file to avoid having to import all these
modules manually.

Also defines a global LRC_DEBUG variable, that may be useful to turn debuggin on
and off within the entirety of Scribbler Adventure (not currently in use).

Imported modules and objects are:
::

    from levelWindow import *
    from hazards import * 
    from interactables import *
    from events import *
    from spriteSheet import SpriteSheet
    from windowObjects import EntryBox,RoundedButton
"""
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../GameObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../WindowObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/..")

from levelWindow import *
from hazards import * 
from interactables import *
from events import *
from spriteSheet import SpriteSheet
#from fancyTextFunctions import *
from windowObjects import EntryBox,RoundedButton

# Creates the LRC_DEBUG global variable, which is only initialized if it did not exist yet.
if "LRC_DEBUG" not in globals():
    globals()["LRC_DEBUG"] = False
