import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class MinosMazeWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.passEntry=None
        
        LevelWindow.__init__(self,levelName="Loop Of Doom",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    
    def confirmCompletion(self):
        self.checkPassword(self.passEntry.trueText)
        
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor and ask them for the 'Loop Of Doom' challenge (see behind this brief screen) and a ",Bold("real")," Scribbler robot."])
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected to a ",Bold("real")," Scribbler robot create a ",self.getI("Script")," that includes ",
        self.getI("forward()")," ",self.getI("turnRight()")," and ",self.getI("turnLeft()"),
        " commands to get from the Start position to the End position on the 'Loop Of Doom'."])
        text.append(["When you're done, confirm with a mentor and they will enter the password to unlock the next level."])
        
        b=simpleBrief(text,[self.getR("Script"),self.getR("forward()"),self.getR("turnLeft()"),self.getR("turnRight()")])                    
        self.addBriefShape(b,"Getting Started")
        
        text=[]
        text.append(["Ask mentor to enter password and fastest time for the challenge (optional) once completed."])
        text.append(["\nEnter time in seconds. (optional)."])
        text.append(["\nEnter password."])
        
        
        b=simpleBrief(text,[],IMAGE_PATH+"blankBrief.png",bulletMarker="")                    
        
        timeEntry=EntryBox((40-b.x,130-b.y),(100-b.x,150-b.y))
        timeEntry.draw(b)
        timeEntry.setup(self.sim.window)
        
        self.passEntry=EntryBox((40-b.x,180-b.y),(200-b.x,200-b.y))
        self.passEntry.draw(b)
        self.passEntry.setup(self.sim.window,True,False)
        self.passEntry.maxChars=26
        self.passEntry.justNumbers=False
        
        self.addEntryObject(timeEntry)
        self.addEntryObject(self.passEntry)
        
        submitButton=RoundedButton((40-b.x,225-b.y),(150-b.x,275-b.y),5)
        submitButton.setup("Enter","PasswordEnterButton")
        submitButton.draw(b)
        submitButton.action=self.confirmCompletion
        self.addMouseDown(submitButton.mouseDownAlt)        
        
        self.addBriefShape(b,"Enter Password")
        
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Get through Loop Of Doom.")       
        self.launchBriefScreen(0)
        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"LoopOfDoom.png")   
        
        #glorified event pads
        self.createEndPad(25,325,locked=True)
        self.createStartPad(30,525)
        
        #create the robot
        self.createRobot(30,525,-45)  
        
        s=self.addDecor(Text((30,525),"Start",color=Color("black"),fontSize=36))
        e=self.addDecor(Text((25,325),"End",color=Color("black"),fontSize=36))
        
        
def startLevel(gameData=None,parent=None):
    level=MinosMazeWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    