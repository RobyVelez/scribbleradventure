restart()   
def intro(levelNum):
    if levelNum==1:
        skip()
        forward(5,5)
    elif levelNum==2:
        skip()
        forward(4,3.25)
        turnRight(4,1)
        forward(4,3)
        turnLeft(4,1)
        forward(4,4)
    elif levelNum==3:
        skip()
        turnLeft(4,1)
        forward(4,0.5)
        turnRight(4,1)
        forward(4,3.5)
        turnRight(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,1)   
    elif levelNum==4:
        skip()
        motors(0.5,0.9)
        wait(25)
        skip()
        turnLeft(1,1)
        motors(0.9,0.5)
        wait(35)
        skip()
        turnRight(1,1.1)
        forward(4,4)
        turnRight(4,1)
        forward(5,5)
    elif levelNum==5:
        skip()
        forward(4,4)
        skip()
        turnLeft(4,1)
        forward(4,3)
        skip()
        forward(4,1)
        skip()
        turnRight(4,1)
        forward(4,1)
        turnLeft(4,1)
        forward(4,2)
        turnLeft(4,1)
        forward(4,4)
        turnLeft(4,1)
        forward(4,3)
        turnRight(4,1)
        forward(4,1)
    elif levelNum==6:
        skip()
        forward(5,5)


def chamber2():
    skip()
    skip()
    for i in range(6):
        forward(4,1)
        sample=pickup()
        forward(4,1)
        if sample==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        else:
            turnLeft(-4,1)
            forward(4,2)
            turnRight(-4,1)
            forward(4,1)
        wait(20)
        print("going again")

def chamber3():
    skip()
    skip()
    for i in range(6):
        forward(4,1)
        sample=pickup()
        forward(4,1)
        if sample==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        elif sample==1:
            turnLeft(4,1)
            forward(4,1)
            turnRight(4,1)
            forward(4,1)
        
        else:
            turnLeft(-4,1)
            forward(4,2)
            turnRight(-4,1)
            forward(4,1)
        wait(20)
        print("going again")
                
def chamber4():
    skip()
    skip()
    for i in range(6):
        
        forward(4,1)
        sample1=pickup()
        forward(4,1)
        sample2=pickup()
        
        if sample1==0 and sample2==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        else:
            turnRight(4,1)
            forward(4,2)
            turnLeft(4,1)
            forward(4,1)
        wait(20)
        print("goint again")
    
def chamber5():
    skip()
    skip()
    for i in range(6):
        
        forward(4,1)
        sample1=pickup()
        forward(4,1)
        sample2=pickup()
        
        if sample1<sample2:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        elif sample1>sample2:
            turnLeft(4,1)
            forward(4,1)
            turnRight(4,1)
            forward(4,1)
        elif sample1==sample2:
            forward(4,1)    
        else:
            turnRight(4,1)
            forward(4,2)
            turnLeft(4,1)
            forward(4,1)
        wait(20)
        print("going again")
        

def chamber6():
    skip()
    skip()
    
    for i in range(6):
        forward(4,1)
        sample1=pickup()
        forward(4,1)
        sample2=pickup()
        
        if sample1==0 and sample2==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        elif sample1==0 and sample2==1:
            turnLeft(4,1)
            forward(4,1)
            turnRight(4,1)
            forward(4,1)
        elif sample1==1 and sample2==0:
            forward(4,1)    
        elif sample1==1 and sample2==1:
            turnRight(4,1)
            forward(4,1)
            turnLeft(4,1)
            forward(4,1)        
        else:
            turnRight(4,1)
            forward(4,2)
            turnLeft(4,1)
            forward(4,1)  
        wait(20)
        print("going again")
    skip()
    skip()

def chamber8():
    skip()
    for i in range(5):
        backward(4,1)
        left,right=getIR()
        #left,right=getLine()
        print("left and right ir are ",left,right)
        if left==0 and right==0:
            use(0)
            print("enterting ",0)
        elif left==0 and right==1:
            use(1)
            
            print("enterting ",1)
        elif left==1 and right==0:
            use(2)
            
            print("enterting ",2)
        else:
            use(3)
            
            print("enterting ",3)
        wait(1.5)
        print("going again")
    backward(4,1)

def chamber9():
    skip()
    for i in range(5):
        backward(4,1)
        #left,right=getIR()
        left,right=getLine()
        
        if left==0 and right==0:
            use(0)
        elif left==0 and right==1:
            use(1)
        elif left==1 and right==0:
            use(2)
        else:
            use(3)
        wait(1)
        print("going again")
    backward(4,1)            
def chamber3_no():
    for i in range(6):
        
        forward(4,1)
        sample=pickup()
        forward(4,1)
        if sample==0:
            turnLeft(4,1)
            forward(4,2)
            turnRight(4,1)
            forward(4,1)
        elif sample==1:
            turnLeft(4,1)
            forward(4,1)
            turnRight(4,1)
            forward(4,1)
        
        else:
            turnLeft(-4,1)
            forward(4,2)
            turnRight(-4,1)
            forward(4,1)
        wait(20)
        print("going again")
                    

def chamber11():
    skip()
    for i in range(5):        
        backward(4,1)
        left,right=getIR()
        #print(left,right)
        if left==0 and right==0:
            forward(4,2)
        elif left==0 and right==1:
            forward(4,3)
        elif left==1 and right==0:
            forward(4,4)
        else:
            forward(4,5)
        use(foo)
        wait(1)
        print("going_")    
    backward(4,1)
    use()
    #wait for door to rise
    wait(15)
    


def lab7p2(key):
    for _ in range(5): forward(3,0.01)
    for _ in range(5): turnRight(3.2,0.01)
    for _ in range(5): forward(4,0.01)
    use(key)
    for _ in range(5): turnLeft(3.2,0.01)
    for _ in range(5): forward(4,0.01)
def myFunction(x):
    forward(4,1)
    use(x)
    forward(4,1)

def grabKey(x=1):
    forward(4,x)
    return pickup()

def lab7p2(key):
    for _ in range(5): forward(3,0.01)
    for _ in range(5): turnRight(3.2,0.01)
    for _ in range(5): forward(4,0.01)
    use(key)
    for _ in range(5): turnLeft(3.2,0.01)
    for _ in range(5): forward(4,0.01)

#Level 9
def lab9():
    levelWindow = getLevelObject()
    key = lab9p1()
    skip()
    while levelWindow.fire5.getY() > 450:
        wait(0.1)
    key = lab9p2(key)
    while levelWindow.fire5.getY() < 250:
        wait(0.1)
    key = lab9p2(key)
    while levelWindow.fire5.getY() > 450:
        wait(0.1)
    key = lab9p2(key)
    while levelWindow.fire5.getY() < 250:
        wait(0.1)
    key = lab9p2(key)
        

def lab9p1():
    for _ in range(5):
        forward(4,0.01)
    key = pickup()
    for _ in range(5):
        forward(4,0.01)
    return key
    
def lab9p2(key):
    #forward(4,3)
    for _ in range(15):
        forward(4,0.01)
    for _ in range(5):
        turnRight(4,0.01)
    use(key)
    forward(4,2)
    newKey = pickup()
    forward(4,1)
    use(foo)
    return newKey

def lab11(term, key):
    forward(4,2)
    turnRight(4,1)
    forward(4,term)
    use(key)
    turnRight(4,1)
    forward(4,1)
    wait(3+term/2)
    use(foo)

#function to turn of portal short circuit
#pass to use() (e.g. use(foo)
def foo():
    return True

stages=["Intro","Gauntlet","Open_Loop_Control","Labyrinth","Chamber","Closed_Loop_Control"]
stageIndex=0

while True and stageIndex<len(stages):
    #if stageIndex==3:
    #    stageIndex=4
    
    [area,gameOver,levelName]=getGameState() 
    print(area) 
               
    if area=="ScribblerActivity":
        if stageIndex<len(stages):
            print(stages[stageIndex])
            launchStage(stages[stageIndex])    
    else:        
    
        #lose
        if gameOver==-1:
            print("lost restarting")
            restart()
        #won    
        elif gameOver==1:
            #debugging, testign individual levels
            if getLevelObject()!=None and getLevelObject().parent==None:
                break
            else:
                print("won, moving on")
                next()
        else:   
            if levelName=="Intro1":
                intro(1)
            elif levelName=="Intro2":
                intro(2)
            elif levelName=="Intro3":
                intro(3)
            elif levelName=="Intro4":
                intro(4)
            elif levelName=="Intro5":
                intro(5)
            elif levelName=="Intro6":
                intro(6)
                [area,gameOver,levelName]=getGameState()
                #won the last level
                if gameOver==1:
                    stageIndex+=1
                    
            elif levelName=="Gauntlet1":
                skip()
                for i in range(3):
                    while True:
                        left,center,right=getObstacle()
                        if center>0:
                            wait(0.5)
                            forward(4,2)
                            break
            elif levelName=="Gauntlet2":
                forward(4,2)
                turnRight(4,1)
                forward(4,4)
                turnLeft(4,1)
                forward(4,3)
                turnLeft(4,1)
                forward(4,2)
                wait(1)
                forward(8,1.25)
                turnRight(4,1)
                forward(4,2)
            elif levelName=="Gauntlet3":
                skip()
                motors(1,0.5)
                wait(32)
                stop()
                motors(0.5,1) 
                wait(20)
                skip()
            elif levelName=="Gauntlet4":
                skip()
                forward(5,5)
            elif levelName=="Gauntlet5":
                skip()
                wait(9.5)
                forward(4,3)
                turnRight(4,1)
                forward(4,3)
                turnLeft(4,1)
                forward(4,2)
                turnRight(4,1)
                forward(4,3)
                wait(10)
            elif levelName=="Gauntlet6":
                skip()
                forward(4,3)
                motors(1,0.5)
                wait(46)
                stop()
                turnLeft(4,1)
                forward(4,1)
                wait(2)
            elif levelName=="Gauntlet7":
                skip()
                wait(15)
                forward(4,3)
                turnRight(4,1)
                forward(4,2)
                turnRight(4,1)
                forward(4,3)
                turnRight(4,1)
                forward(4,2)
                turnRight(4,1)
                wait(10)
                forward(5,5)
                skip()
                wait(5)
            elif levelName=="Gauntlet8":
                skip()
                backward(4,5)
                turnRight(4,1)
                forward(4,4)
                skip()
                forward(4,4)
            elif levelName=="Gauntlet9":
                skip()
                turnRight(1,1)
                forward(4,2.5)
                turnLeft(2,1)
                forward(4,2) 
                turnRight(2,1)
                forward(4,2)
                use(foo)
                forward(5,5)
            elif levelName=="Gauntlet10":
                skip()
                forward(4,1)
                turnRight(4,1)
                forward(4,2)
                turnLeft(4,1)
                forward(4,3)
                turnLeft(4,1)
                forward(4,2)
                turnRight(4,1)
                forward(4,2)
                use(foo)
                forward(4,3)
                turnRight(4,1)
                forward(4,4)
                turnLeft(4,1)
                forward(4,1)
            elif levelName=="Gauntlet11":
                skip()
                forward(4,2)
                turnLeft(4,1)
                forward(4,4)
                turnRight(4,1)
                forward(4,3.2)
                turnLeft(4,1)
                forward(4,1)
                use(foo)
                turnLeft(4,1)
                forward(4,1)
                turnRight(4,1)
                forward(4,2.25)
                turnRight(4,1)
                forward(4,1)
            elif levelName=="Gauntlet12":
                forward(4,1)
                turnRight(4,1)
                forward(4,2)
                turnRight(4,1)
                forward(4,1)
                use(foo)
                forward(4,1)
                turnLeft(4,1)
                forward(4,2)
                turnLeft(4,1)
                forward(4,1)
                use(foo)
                forward(4,1)
                turnLeft(4,1)
                forward(4,2)
                turnLeft(4,1)
                forward(4,1)
                use(foo)
                forward(4,1)
                turnRight(4,1)
                forward(4,2)
                turnRight(4,1)
                forward(4,1)
                use(foo)
                wait(27)            
                backward(4,1)
                turnLeft(4,1)
                forward(4,1)
                [area,gameOver,levelName]=getGameState()
                #won the last level
                if gameOver==1:
                    stageIndex+=1
            elif levelName=="Minos Maze":
                wait(5) #needed or Calico will crash
                getLevelObject().setGameOver(1)               
                #use("lrcmentor")
                [area,gameOver,levelName]=getGameState()
                #won the last level
                if gameOver==1:
                    stageIndex+=1
            elif levelName=="Loop Of Doom":
                wait(5) #needed or Calico will crash                 
                getLevelObject().setGameOver(1)                               
                #use("lrcmentor")
            elif levelName=="Labyrinth 1":
                skip()
                forward(4,1)
                key=pickup()
                skip()
                forward(4,3)
                use(key)
                skip()
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 2":
                skip()
                [area,gameOver,levelName]=getGameState() 
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                stageIndex+=1
                
            elif levelName=="Labyrinth 3":
                skip()
                forward(4,1)
                t=pickup()
                forward(4,1)
                use(t,pickup())
                skip()
                forward(4,2)
                skip()
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 4":
                skip()
                forward(4,2)
                use(16)
                skip()
                forward(4,2)
                t=pickup()
                forward(4,1)
                use(t)
                skip()
                turnRight(4,1)
                forward(4,2)
                t2=pickup()
                forward(4,2)
                use(t2+15)
                skip()
                turnRight(4,1)
                forward(4,3)
                use(t+t2)
                skip()
                wait(1)
                forward(4,2)
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 5":
                skip()
                myFunction(1)
                skip()
                myFunction(374)
                skip()
                while True:
                    left,center,right=getObstacle()
                    print(left,center,right)
                    if center<501:
                        print("Waiting")
                        wait(1)
                        break
                myFunction(263)
                while True:
                    left,center,right=getObstacle()
                    print(left,center,right)
                    if center<501:
                        print("Waiting")
                        wait(1)
                        break
                myFunction(184)
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 6":
                skip()
                x7=grabKey()
                #wait(5)
                skip() # Skip pre-test dialog and start test
                skip() # Skip post-test dialog
                #wait(7)
                x4=grabKey(4)
                skip() # Skip pre-test dialog and start test
                skip() # Skip post-test dialog
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 7":
                skip()
                forward(4,2)
                x1=pickup()
                forward(4,2)
                x2=pickup()
                turnLeft(4,1)
                forward(4,2)
                use((x1*2)/x2)
                turnLeft(4,1)
                forward(4,2)
                use((x1-x2)+17)
                forward(-4,1)
                turnLeft(-4,1)
                forward(4,2)
                skip()
                forward(4,2)
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 8":
                skip()
                [area,gameOver,levelName]=getGameState() 
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()

                
            elif levelName=="Labyrinth 9":
                skip()
                levelWindow = getLevelObject()
                #talkAll(levelWindow.witch)
                forward(4,1)
                key = pickup()
                while levelWindow.thwomp1.getY() <= 350:
                    wait(0.1)
                while levelWindow.thwomp1.getY() > 350:
                    wait(0.1)
                lab7p2(key)
                #talkAll(levelWindow.youngWitch)
                while levelWindow.thwomp2.getY() <= 450:
                    wait(0.1)
                while levelWindow.thwomp2.getY() > 450:
                    wait(0.1)
                lab7p2(key)
                forward(4,2)
                skip()
                [area,gameOver,levelName]=getGameState() 
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()

                
            elif levelName=="Labyrinth 10":
                skip()
                lab9()
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                
            elif levelName=="Labyrinth 11":
                skip()
                levelWindow = getLevelObject()
                for _ in range(6):
                    wait(10)
                    t = levelWindow.activeTerminal+1
                    c = int(levelWindow.codeStr)
                    print(t, c)
                    lab11(t,c)
                    wait(5)
                skip()
                wait(3)
                while gameOver != 1:
                    [area,gameOver,levelName]=getGameState() 
                    wait(1)
                next()
                stageIndex+=1
            
            elif levelName=="Chamber1":
                skip()
            elif levelName=="Chamber2":
                chamber2()
            elif levelName=="Chamber3":
                chamber3()
            elif levelName=="Chamber4":
                chamber4()
            elif levelName=="Chamber5":
                chamber5()
            elif levelName=="Chamber6":
                chamber6()
            elif levelName=="Chamber7":
                skip()
            elif levelName=="Chamber8":
                chamber8()                
            elif levelName=="Chamber9":
                chamber9()  
            elif levelName=="Chamber10":
                skip()
            elif levelName=="Chamber11":
                skip()
                backward(4,1)
                use(foo)
                chamber11()
            elif levelName=="Chamber12":
                skip()
                [area,gameOver,levelName]=getGameState()
                #won the last level
                if gameOver==1:
                    stageIndex+=1 
            elif levelName=="Shy Party Robot":
                wait(5)
                use("lrcmentor")
                [area,gameOver,levelName]=getGameState()
                #won the last level
                #if gameOver==1:
                stageIndex+=1
                menu()
            elif levelName=="LineFollow" or levelName=="Navigate Maze" or levelName=="Stop At Wall-Distance" or levelName=="Stop At Wall-Obstacle" or levelName=="Follow Wall 1" or levelName=="Follow Wall 2" or levelName=="Clap and Dance":
                wait(5)
                use("lrcmentor")

            else:
                break
            
            
                    
                    
