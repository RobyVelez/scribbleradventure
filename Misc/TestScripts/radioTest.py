from Graphics import *

win = Window(300, 300)

statusOn=True

def toggleStatus(o, e):
    global statusOn
    if statusOn:
        statusOn=False
    else:
        statusOn=True
    print("Status on: ",statusOn)
    #print("Change!", o , e)

onButton = RadioButton((10, 150), "On")
offButton = RadioButton((10, 170), "Off",onButton)

onButton.draw(win)
offButton.draw(win)

onButton.connect("change-value", toggleStatus)
#offButton.connect("change-value", statusOff)
