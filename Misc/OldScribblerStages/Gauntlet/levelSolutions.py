from levelWindow import *

def g2():
    forward(10,2)
    turnRight(1,3.9)
    
    forward(2,3.5)
    turnRight(1,3.6)
    
    wait(1.5)
    
    forward(10,2.1)
    
    #turnRight(1,3.9)
    #forward(2,3.5)
    
    #turnLeft(1,3.9)
    #forward(10,2.2)
    
    #turnRight(1,3.9)
    #forward(2,2)

def g3():
    motors(6,2.6)
    wait(4)
    stop()
    
    turnLeft(1,.8)
    motors(4.5,7)
    wait(2)
    stop()

def g6():
    forward(5,1.7)
    turnRight(2,0.9)
    forward(4,2)
    turnRight(2,0.9)
    forward(5,0.9)
    turnRight(2,0.9)
    forward(4,1.5)
    turnRight(2,0.9)
    forward(5,1.5)
    turnLeft(2,0.5)
    forward(5,2)
   
   
def turn90(direction=1):
    s=10
    t=0.25
    turnRight(direction*s,t)
    
def topPath():
    turn90(-1)
    forward(4,1.8)
    turn90()
    forward(4,1.6)
    turn90(-1)
    forward(4,1)
def botPath():
    forward(4,1.7)
    turn90(-1)
    forward(4,2.5)

def topC1():
    penDown("red")
    forward(4,1)
    turnRight(-2,1.5)
    penDown("blue")
    forward(4,1)
    turnLeft(-1,1)
    penDown("green")
    forward(2,2.5)
    turnLeft(-1,1)
    penDown("orange")
    forward(4,1)
    turnLeft(-1,1.4)
    penDown("purple")
    forward(4,1.5)
    turnLeft(-1,1.5)
    penDown("yellow")
    forward(2,2.5)
    turnLeft(-1,1.5)
    
    forward(2,2.5)

def botC1():
    penDown("red")
    forward(4,1)
    turnRight(2,1.5)
    penDown("blue")
    forward(4,1)
    turnLeft(1,1)
    penDown("green")
    forward(4,1.5)
    turnLeft(1,1.5)
    penDown("orange")
    forward(4,1.5)
    turnLeft(1,1.5)
    penDown("purple")
    forward(4,1.5)
    turnLeft(1,1.5)
    penDown("yellow")
    forward(2,2.5)
    turnLeft(1,1.7)
    
    forward(2,2.5)
    
def topC():
    penDown("red")
    forward(4,0.9)
    turnRight(-4,0.9)
    penDown("blue")
    motors(3,2)

def botC():
    penDown("red")
    forward(4,0.9)
    turnRight(4,0.9)
    penDown("blue")
    motors(2,3) 
'''
def goLeft():
    penDown("red")
    forward(2,2) 
    turnLeft(4,0.9)
    penDown("green")
    forward(4,2.5)
    turnRight(4,0.9)
    penDown("blue")
    forward(2,3.5)
    
def goRight():
    penDown("red")
    forward(2,2) 
    turnRight(4,0.9)
    penDown("green")
    forward(4,2.5)
    turnLeft(4,0.9)
    penDown("blue")
    forward(2,3.5)
'''

def go(value=1):
    penDown("red")
    forward(2,2)
    turnLeft(value*4,0.9)
    penDown("blue")
    forward(4,2.3)
    turnRight(value*4,0.9)
    penDown("green")
    forward(2,3.7)
    
    
def cond2_alt():
    forward(4,2)
    left,right=getIR()
    if left==0 and right==0:
        turnLeft(4,0.9)
        forward(4,3)
    else:
        forward(4,2.7)
        turnRight(4,0.9)
        forward(4,3)

def cond2():
    forward(4,3.2)
    stall=getStall()
    print("stall",stall)
    if stall==1:
        backward(1,0.5)
        turnLeft(4,0.9)
        forward(4,3)
    else:
        forward(4,1.5)
        turnRight(4,0.9)
        forward(4,3)        
        
def cond3():
    print(getStall())
    forward(4,4)
    print(getStall())
    turnRight(4,1)
    left,right=getIR()
    if left==1 and right==1:
        backward(4,4)
    else:
        forward(4,4)
        
def cond4():
    forward(4,2.5)
    turnRight(4,0.9)
    pic=takePicture()
    show(pic)
    r,g,b=pic.getRGB(110,120)
    print(r,g,b)
    if r<100 and g>200 and b<100:
        forward(4,3)
    else:
        backward(4,3)
def cond5():
    forward(4,4)
    left,center,right=getObstacle()
    if left>right:
        turnRight(2,1)
        forward(4,3)
    else:
        turnLeft(2,1)
        forward(4,3)

def cond6():
    forward(4,3)
    left,right=getLine()

    if left==1 and right==1:
        forward(4,3)
        turnRight(4,0.9)
        forward(4,2)
    elif left==1 and right==0:
        turnLeft(4,0.9)
        forward(8,3)
    else:# left==0 and right==1:
        turnRight(14,0.9)
        forward(8,3)
        
def cond7():
    forward(4,3)
    left,right=getLine()
    
    if left==1 and right==1:
        forward(4,2)
        turnRight(4,0.9)
    else:
        turnLeft(4,0.9)        
    
    forward(4,2)
    turnRight(4,0.9)
    
    left,right=getIR()
    if left==1 and right==1:
        backward(4,2)
    else:
        forward(4,2)
        
def cond8():
    forward(4,2)
    left,right=getLine()
    if left==1 and right==0:
        turnLeft(4,0.9)
    else:
        turnRight(4,0.9)
    forward(4,1.5)
    left,center,right=getObstacle()
    print(left,center,right)
    if left<right:
        turnLeft(2,1)
        forward(4,3)
    else:
        turnRight(2,1)
        forward(4,3)