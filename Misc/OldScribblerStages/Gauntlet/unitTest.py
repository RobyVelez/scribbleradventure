import sys
import os
import importlib
import warnings

if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    sys.path.append("../Gauntlet")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Gauntlet")

import solutions

def runTest(level, solution):
    success = -2
    try:
        labyrinth = importlib.import_module(level)
        labyrinth.startLevel()
        solution()
        success = labyrinth.getLevelObject().gameOver
        labyrinth.getLevelObject().quitGame()
    except:
        warnings.warn(traceback.format_exc())
        success = -3
    print()
    print("Unit test report:", level)
    print("- successfull:", success)
    print()


def testAll():
    for i in range(1, 2):
        levelName = "gauntlet" + str(i)
        solutionName = "gaunt" + str(i)
        runTest(levelName, getattr(solutions, solutionName))
