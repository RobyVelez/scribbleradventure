import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet5Window(levelWindow):
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        self.thwomp=[]
        self.thwompAvatarUp=[]
        self.thwompAvatarDown=[]
        self.upVelocity=-13
        self.downVelocity=-2*self.upVelocity#-2*upVelocit  
        self.lowBound=675
        self.upBound=50   
        
        if random()>0.5:
            rEndX=290
            rEndY=75
        else:
            rEndX=410
            rEndY=75
        

        levelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)
    
        self.briefingUp=True
        self.briefScreen.visible=True
    
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet5BriefScreen.png"
        
    def setStatusText(self):
        self.statusText.append("I'm just going to wait() here until this thwomp passes.")
        self.statusPoints.append((550,450))
        self.statusText.append("Got to be patient")
        self.statusPoints.append((250,150))    
        self.statusText.append("Turn and move! Turn and move!")
        self.statusPoints.append((150,150))
    
    
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(100,0),(100,700),(0,700)))
        wall.append(Polygon((600,0),(700,0),(700,700),(600,700)))
        wall.append(Polygon((200,600),(500,600),(500,700),(200,700)))
        
        wall.append(Polygon((500,200),(500,500),(200,500),(200,200),(325,100),
        (325,0),(375,0),(375,100)))
        
        wall.append(Polygon((200,0),(200,100),(325,0)))
        wall.append(Polygon((375,0),(500,0),(500,100)))
        
        for w in wall:
            w.bodyType="static"            
            self.sim.window.draw(w)
            w.body.IgnoreGravity=True

    def createHazards(self):  
        thwompX=[150,550]
        thwompY=[55,55]

        if len(self.thwomp)==0:
            for i in range(len(thwompX)):
                self.thwomp.append(Circle((thwompX[i],thwompY[i]),28))
         
            for i in range(len(self.thwomp)):
                self.sim.addShape(self.thwomp[i])            
                self.thwomp[i].body.OnCollision+=self.thwompCollide
                
                self.thwompAvatarUp.append(Picture(LEVEL_PATH+"thwompUp.png"))
                self.thwompAvatarUp[i].outline.alpha=0
                self.thwompAvatarUp[i].scale(0.65)
            
                self.thwompAvatarDown.append(Picture(LEVEL_PATH+"thwompDown.png"))
                self.thwompAvatarDown[i].outline.alpha=0
                self.thwompAvatarDown[i].scale(0.65)
            
            
                self.thwompAvatarUp[i].draw(self.thwomp[i],(0,0))
                self.thwompAvatarDown[i].draw(self.thwomp[i],(0,0))
            
                self.thwompAvatarDown[i].setAlpha(0)
                self.thwompAvatarUp[i].setAlpha(255)

        else: #used in restart. simply moves thwomp back into position
            for i in range(len(self.thwomp)):
                self.thwomp[i].moveTo(thwompX[i],thwompY[i])
                self.thwomp[i].body.LinearVelocity=Vector(0,0)
                self.thwompAvatarDown[i].setAlpha(0)
                self.thwompAvatarUp[i].setAlpha(255)
    
    def thwompCollide(self, myfixture, otherfixture, contact): 
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    
    def setStartActions(self):       
                
        for i in range(len(self.thwomp)):
            self.thwomp[i].body.LinearVelocity=Vector(0,self.downVelocity)
            self.thwompAvatarDown[i].setAlpha(255)
            self.thwompAvatarUp[i].setAlpha(0)
            
        
    def levelThread(self):
           
        if not self.gameOver:
            if self.runTimer:
                for i in range(len(self.thwomp)):
                    t=self.thwomp[i]                   
                    if t.getY()>self.lowBound and t.body.LinearVelocity.Y>0:
                        t.body.LinearVelocity=Vector(0,self.upVelocity)
                        self.thwompAvatarDown[i].setAlpha(0)
                        self.thwompAvatarUp[i].setAlpha(255)
                    elif t.getY()<self.upBound and t.body.LinearVelocity.Y<0:
                        t.body.LinearVelocity=Vector(0,self.downVelocity)
                        self.thwompAvatarDown[i].setAlpha(255)
                        self.thwompAvatarUp[i].setAlpha(0)                
                
        else:
            for t in self.thwomp:
                t.body.LinearVelocity=Vector(0,0)

def startLevel():
    gauntlet5Window("Gauntlet5",700,700,350,550,-180,25,450,None,None,False)
if __name__ == "__main__":
    startLevel()
    