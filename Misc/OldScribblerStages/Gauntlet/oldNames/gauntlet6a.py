import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    import setCalico
    setCalico.setCalicoGlobal(calico)
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet8Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.thwomp=[]
        NewLevelWindow.__init__(self,"Gauntlet8",rStartX=275,rStartY=550,rStartT=-180,
        rEndX=425,rEndY=100,gameData=gameData,parent=parent)
        self.setup()
        self.briefScreen.visible=False      

    
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet7BriefScreen.png"
        
    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, looks like the goal randomly moves between the left and right. Plus there's thwomps... I need two functions to handle both paths. Check out the functions1 Help Page to see how to build them.")
        #self.statusPoints.append((350,550))

    
    def teleport(self, portal, items):
        #Rather than teleporting here, add it to the events, such that the actual teleport
        #Is executed in the level thread. Should prevent deadlocks.        
        self.addStepEvent(lambda: self.teleportStep(portal.userData["angle"],portal.userData["destination"]))
        
        
    def teleportStep(self, angle,destination):
        robot = getRobot()
        robot.stop()
        
        #The contacts do not like the robot suddenly teleporting. Clear all of them (they will be recreated).
        del self.interactContactList[:]
        del self.pickupContactList[:]
        del self.talkContactList[:]
        robot.setPose(destination[0], destination[1], angle)
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
        return False
    
    def createObstacles(self):
        self.addObstacle(Polygon((0,0),(100,0),(100,700),(0,700)))
        self.addObstacle(Polygon((600,0),(700,0),(700,700),(600,700)))
        self.addObstacle(Polygon((200,600),(500,600),(500,700),(200,700)))
        self.addObstacle(Polygon((500,225),(500,500),(375,500),(375,600),(325,600),(325,500),(200,500),(200,225),(325,125),
        (325,0),(375,0),(375,125)))
        
        self.addObstacle(Polygon((200,0),(200,100),(325,0)))
        self.addObstacle(Polygon((375,0),(500,0),(500,100)))
        
        #Create portals
        self.portal1 = self.addActor(PictureActor((275, 100), LEVEL_PATH+"portalBlue.png", interact=self.teleport, description="portal 1"))
        self.portal1.userData["angle"] = int(0)
        self.portal1.userData["destination"] = [400,75]
        
        self.portal1 = self.addActor(PictureActor((425, 550), LEVEL_PATH+"portalOrange.png", interact=self.teleport, description="portal 1"))
        self.portal1.userData["angle"] = int(-180)
        self.portal1.userData["destination"]=[290,75] 

    def startThwomp(self):
        for t in self.thwomp:
            t.run=True
            
    def createHazards(self):  
        self.thwomp.append(Thwomp((150,50),(150,650),1,self.deathCollide,run=False))
        self.thwomp.append(Thwomp((550,50),(550,650),1,self.deathCollide,run=False))

        for t in self.thwomp:
            self.addActor(t)
            self.addStepEvent(t.step)
        self.addStartEvent(self.startThwomp) 
        
    '''
    def createHazards(self):  
        thwompX=[150,550]
        thwompY=[55,55]

        if len(self.thwomp)==0:
            for i in range(len(thwompX)):
                self.thwomp.append(Circle((thwompX[i],thwompY[i]),28))
         
            for i in range(len(self.thwomp)):
                self.sim.addShape(self.thwomp[i])            
                self.thwomp[i].body.OnCollision+=self.thwompCollide
                
                self.thwompAvatarUp.append(Picture(LEVEL_PATH+"thwompUp.png"))
                self.thwompAvatarUp[i].outline.alpha=0
                self.thwompAvatarUp[i].scale(0.65)
            
                self.thwompAvatarDown.append(Picture(LEVEL_PATH+"thwompDown.png"))
                self.thwompAvatarDown[i].outline.alpha=0
                self.thwompAvatarDown[i].scale(0.65)
            
            
                self.thwompAvatarUp[i].draw(self.thwomp[i],(0,0))
                self.thwompAvatarDown[i].draw(self.thwomp[i],(0,0))
            
                self.thwompAvatarDown[i].setAlpha(0)
                self.thwompAvatarUp[i].setAlpha(255)

        else: #used in restart. simply moves thwomp back into position
            for i in range(len(self.thwomp)):
                self.thwomp[i].moveTo(thwompX[i],thwompY[i])
                self.thwomp[i].body.LinearVelocity=Vector(0,0)
                self.thwompAvatarDown[i].setAlpha(0)
                self.thwompAvatarUp[i].setAlpha(255)
    
    def thwompCollide(self, myfixture, otherfixture, contact): 
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    
    def setStartActions(self):       
                
        for i in range(len(self.thwomp)):
            self.thwomp[i].body.LinearVelocity=Vector(0,self.downVelocity)
            self.thwompAvatarDown[i].setAlpha(255)
            self.thwompAvatarUp[i].setAlpha(0)
            
        
    def levelThread(self):
           
        if not self.gameOver:
            if self.runTimer:
                for i in range(len(self.thwomp)):
                    t=self.thwomp[i]                   
                    if t.getY()>self.lowBound and t.body.LinearVelocity.Y>0:
                        t.body.LinearVelocity=Vector(0,self.upVelocity)
                        self.thwompAvatarDown[i].setAlpha(0)
                        self.thwompAvatarUp[i].setAlpha(255)
                    elif t.getY()<self.upBound and t.body.LinearVelocity.Y<0:
                        t.body.LinearVelocity=Vector(0,self.downVelocity)
                        self.thwompAvatarDown[i].setAlpha(255)
                        self.thwompAvatarUp[i].setAlpha(0)                
                
        else:
            for t in self.thwomp:
                t.body.LinearVelocity=Vector(0,0)

    '''
level=None
def startLevel(gameData=None, parent=None):
    global level
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=gauntlet8Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()
    

    
