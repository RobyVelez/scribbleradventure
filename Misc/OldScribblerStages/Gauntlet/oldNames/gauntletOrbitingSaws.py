import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    import setCalico
    setCalico.setCalicoGlobal(calico)
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class gauntlet8Window(levelWindow):
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):

        self.saw=[]
        self.sawAvatar=[]
        
        self.sawAngle=(6/4)*pi
        self.sawAngleStep=0.05
        
        self.gX=400
        self.gY=300
        self.sawR=250

        if random()>0.5:
            self.direction=1
        else:
            self.direction=-1
        
        levelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

        self.briefingUp=True
        self.briefScreen.visible=True
    
    def getBriefImage(self):
        return LEVEL_PATH+"gauntlet8BriefScreen.png"
        
    def setStatusText(self):
        self.statusText.append("These just keep getting weirder...")
        self.statusPoints.append((50,300))
        self.statusText.append("Maybe a motors() command will make it easier to get around these curves.")
        self.statusPoints.append((150,300))

    def createObstacles(self):
        obs=[]        

        gR=self.sawR-50
        g1R=self.sawR+50
                
        obs.append(Polygon())#(550,300),(550,200),(150,200),(150,300)))
        cX=obs[0].center[0]
        cY=obs[0].center[1]
        
        thetaStep=-0.2
        angle=2*pi
        while angle > 0:
            obs[0].append( (cos(angle)*gR+self.gX-cX,sin(angle)*gR+self.gY-cY) )
            angle=angle+thetaStep
                
        obs.append(Polygon((100,250),(0,250),(0,0),(700,0),(700,350),(700,700),(0,700),
        (0,350),(100,350)))        
        c1X=obs[1].center[0]
        c1Y=obs[1].center[1]
                
        thetaStep=-0.27
        angle=pi+thetaStep        
        while angle > (-pi - thetaStep):
            obs[1].append( (cos(angle)*g1R+self.gX-c1X,sin(angle)*g1R+self.gY-c1Y) )
            angle=angle+thetaStep            

        for o in obs:
            o.bodyType="static"
            self.sim.addShape(o)
            o.body.IgnoreGravity=True
        
        

        
    def createHazards(self):
        hub=[]
        s=50 #tooth width
        #global thwomp,temp
        th=40#botRad*0.25 #tooth height
        turnSpeed=-10
        hubRad=15

        sawX=    [400,400]
        sawY=    [50,550]
        sawRad=  [25,25]#250,200,200, 150,175,150]
        sawScale=[0.2,0.2]
  
        #sawScale=[0.53,0.32]
        if len(self.saw)==0:
            for i in range(len(sawX)):
                self.saw.append(Circle((sawX[i],sawY[i]),sawRad[i]))
                
                hub.append(Circle((sawX[i],sawY[i]),hubRad))
                
                self.sawAvatar.append(Picture(LEVEL_PATH+"grinder.png"))
                self.sawAvatar[i].scale(sawScale[i])
                self.sawAvatar[i].outline.alpha=0
                #sawAvatar[i].setAlpha(100)       
        
            for i in range(len(self.saw)):
                self.sim.addShape(self.saw[i]) 
                
                self.sawAvatar[i].draw(self.saw[i],(0,0))
                self.saw[i].fill.alpha=0
                self.saw[i].setWidth(0)

                #saw[i].bodyType="static"
                #saw[i].body.IgnoreGravity=True
                self.saw[i].body.OnCollision+=self.sawCollide
                self.saw[i].body.LinearVelocity=Vector(0,0)
 
                self.saw[i].body.AngularVelocity=-turnSpeed
            
        
        else:
            for i in range(len(self.saw)):
                self.saw[i].moveTo(sawX[i],sawY[i])
                
            self.sawAngle=(6/4)*pi
            
            if random()>0.5:
                self.direction=1
            else:
                self.direction=-1
            
    #game over man
    def sawCollide(self,myfixture, otherfixture, contact):

        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
          
    def levelThread(self):   
        if not self.gameOver:
            self.saw[0].moveTo( cos(self.sawAngle)*self.sawR+self.gX , sin(self.sawAngle)*self.sawR+self.gY )
            self.saw[1].moveTo( cos(self.sawAngle-pi)*self.sawR+self.gX , sin(self.sawAngle-pi)*self.sawR+self.gY )
            
            if self.direction==1:
                self.sawAngle+=self.sawAngleStep
            else:
                self.sawAngle-=self.sawAngleStep
            #self.sawAngle = self.sawAngle % (2*pi)
            self.sawAngle %= (2*pi)
     
                                
def startLevel():
    gauntlet8Window("Gauntlet8",700,700,45,300,0,650,300,None,None,False)#"woodFloor3.png","diamondStruts.png")
    #gauntlet6=gauntlet6Window(700,700,"Gauntlet6",200,574,0,150,650)#"woodFloor3.png","diamondStruts.png")

if __name__ == "__main__":
    startLevel()
