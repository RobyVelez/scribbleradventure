from levelWindow import*# levelWindow

class gauntlet6Window(levelWindow):
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        self.saw=[]
        self.sawAvatar=[]
        self.boulder=None
        levelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)
        
    def createObstacles(self):
        #(100,0)
        leftWall=Polygon((0,0),(100,100),(200,200),(450,200),(550,300),(550,400),
            (450,500),(200,500),(100,600),(100,700),(0,700))
        
        rightWall=Polygon((150,0),(200,50),(250,100),(500,100),(650,250),
            (650,450),(500,600),(250,600),(200,650),(200,700),(700,700),
            (700,0))
        
        self.gate=Polygon((0,95),(190,95),(190,100),(0,100))
        
        self.gate.bodyType="static"
        leftWall.bodyType="static"  
        rightWall.bodyType="static"  
        
        self.sim.addShape(leftWall)
        self.sim.addShape(rightWall)
        self.sim.addShape(self.gate)     
        leftWall.body.IgnoreGravity=True
        rightWall.body.IgnoreGravity=True
        self.gate.body.IgnoreGravity=True
        
        leftWall.bounce=0
        leftWall.friction=0
        rightWall.bounce=0
        rightWall.friction=0
           
    def createHazards(self):
        debug=False
        bX=120
        bY=50
        bR=48
        pad=2
        if self.boulder==None and len(self.saw)==0:   
            if debug:     
                self.boulder=Circle((bX,bY),bR,color=makeColor(255,0,0,100))            
            else:
                self.boulder=Circle((bX,bY),bR,color=makeColor(255,0,0,0))            
            self.boulder.bounce=0
            self.boulder.friction=0
            
            
            self.sim.addShape(self.boulder)
            self.boulder.mass=0
            if debug:
                self.saw.append(Circle((bX,bY),bR+pad,color=makeColor(0,0,0,100)))
            else:
                self.saw.append(Circle((bX,bY),bR+pad,color=makeColor(0,0,0,0)))

            self.sim.addShape(self.saw[0])
            self.saw[0].body.OnCollision+=self.sawCollide        
            self.saw[0].makeJointTo(self.boulder,"angle")
            
            self.sawAvatar.append(Picture("grinder.png"))
            self.sawAvatar[0].scale(0.23)
            self.sawAvatar[0].outline.alpha=0
            self.sawAvatar[0].draw(self.saw[0],(0,0))
            if debug:
                self.sawAvatar[0].setAlpha(100)

            self.saw[0].body.AngularVelocity=-25
            
   #game over man
    def sawCollide(self,myfixture, otherfixture, contact):

        if self.runTimer:
            if contact.Manifold.LocalNormal.Y>0:
                self.saw[0].body.AngularVelocity=-25
            else:
                self.saw[0].body.AngularVelocity=25
                
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    
    def setStartActions(self):
        self.gate.rotateTo(45)
    
    def levelThread(self):
           
        if not self.gameOver and self.runTimer:
            if self.saw[0].getY()>350:
                self.boulder.body.LinearVelocity=Vector(-15,15)
            else:
                self.boulder.body.LinearVelocity=Vector(15,15)
        else:
            self.boulder.body.LinearVelocity=Vector(0,0)
            self.boulder.body.AngularVelocity=0
            self.saw[0].body.AngularVelocity=0
            

    

        
      

def startLevel():
    gauntlet6Window("Gauntlet6",700,700,225,145,0,150,650,None,None,False)#"woodFloor3.png","diamondStruts.png")
    #gauntlet6Window("Gauntlet6",700,700,225,625,0,150,650,None,None,False)#"woodFloor3.png","diamondStruts.png")

if __name__ == "__main__":
    startLevel()
