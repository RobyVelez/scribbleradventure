from levelWindow import*# levelWindow

class cond3Window(levelWindow):
    def __init__(self, panel1W=300, panel1H=700, levelName="game",rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None):                

        self.gate=[]
        self.boo=None
        self.booAvatar=None
        
        self.debug=True
        
        temp=random()
        
        if temp<0.25:
            endX=200
            endY=50
            self.pattern=0
        elif temp<0.50:
            endX=500
            endY=50
            self.pattern=1
        elif temp<0.75:
            endX=200
            endY=650
            self.pattern=2
        else:
            endX=500
            endY=650
            self.pattern=3
                
        fog=not self.debug
            
        levelWindow.__init__(self, panel1W, panel1H, levelName,rStartX,rStartY,rStartT,endX,endY,backPic,forePic,fog)
        

    def createObstacles(self):
        wall=[]
        
        wall.append(Polygon((100,0),(0,0),(0,700),(100,700),(300,500),(300,200)))
        
        wall.append(Polygon((700,700),(600,700),(400,500),(400,400),(600,400),(600,300),(400,300),(400,200),(600,0),(700,0)))
        
        
        wall.append(Polygon((225,0),(475,0),(350,125)))
        
        wall.append(Polygon((225,700),(475,700),(350,575)))

        #wall.append(Polygon((0,0),(400,0),(400,20),(300,20),(300,450),(50,700),(0,700)))
        #wall.append(Polygon((400,0),(700,0),(700,700),(650,700),(400,450),(400,300),(500,300),(500,200),(400,200)))
        #wall.append(Polygon((350,525),(175,700),(525,700)))
        for w in wall:  
                 
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
            w.stackOnBottom()

        line=[]
        w=50
        h=50
        
        #top left corner of tiles    
        cX=325
        cY=325    
        
        line.append(Line((cX,cY),(cX+w,cY)))
        line.append(Line((cX,cY+h),(cX+w,cY+h)))
        
        line.append(Line((cX,cY),(cX,cY+h)))
        line.append(Line((cX+w,cY),(cX+w,cY+h)))
        
        for l in line:
            l.draw(self.sim.window)
            l.bodyType="static"
            l.stackOnBottom()
                    
        tile1=Picture("tileRot.png")
        tile2=Picture("tileRot.png")
        tile1.tag="Tile"
        tile2.tag="Tile"
        
 

        self.gate.append(Rectangle((348,23),(352,223)))
        self.gate.append(Rectangle((348,477),(352,677)))
        
        for g in self.gate:
            g.bodyType="static"
            self.sim.addShape(g)
            g.body.IgnoreGravity=True
            g.stackOnBottom()
        
        if self.pattern==0 or self.pattern==1:
            self.sim.window.drawAt(tile1,(cX+w/2,cY+tile2.height/2))
        else:
            self.sim.window.drawAt(tile2,(cX+w/2,cY+h/2+tile2.height/2))            
                
        if self.pattern==0:
            self.gate[0].rotateTo(-45)
            
        elif self.pattern==1:
            self.gate[0].rotateTo(45) 
            
        elif self.pattern==2:
            self.gate[1].rotateTo(45)            
            
        else:
            self.gate[1].rotateTo(-45)
            
            
    def createHazards(self):
        if self.boo==None:
            self.boo=Circle((250+10*random(),800),25)
            self.sim.addShape(self.boo)
            self.boo.stackOnTop()
            
            self.booAvatar=Picture("booRight.png")
            self.booAvatar.outline.alpha=0
            self.booAvatar.scale(0.11)
            self.booAvatar.tag="right"
            #self.booAvatar.setAlpha(0)
            #self.booAvatar.fill.alpha=0
            self.booAvatar.draw(self.boo,(0,0))
            
                    
            self.boo.body.OnCollision+= self.booCollide
        else:
            self.boo.moveTo(250,650)
            for g in self.gate:
                g.rotateTo(0)    
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        s=15
        
        if self.runTimer:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"
            
    def setStartActions(self):
        
        self.boo.stackOnTop()
        self.panel2.stackOnTop()
        self.panel3.stackOnTop()




                        


cond3=None        
def main():
    global cond3
    cond3=cond3Window(700,700,"Conditional3",550,350,-180)#"woodFloor3.png","diamondStruts.png")
    #gauntlet6=gauntlet6Window(700,700,"Gauntlet6",200,574,0,150,650)#"woodFloor3.png","diamondStruts.png")


if __name__ == "__main__":
    main() 
