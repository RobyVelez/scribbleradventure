import sys
import os
import warnings

#level=getLevelObject()
def gaunt1():
    forward(4,2)
    forward(4,2)
    forward(4,2)
    
def gaunt2():
    #level.restartGame()
    #wait(1)
    forward(4,0.5)
    wait(4)
    saveWindow("test.png",getWindow())
    forward(4,1.25)
    wait(1.1)
    forward(4,1.25)
    wait(2.1)
    forward(4,1.5)
    turnRight(4,0.9)
    forward(4,2)
    turnRight(4,0.9)
    forward(4,1.75)
    wait(1)
    forward(4,1.75)
    turnLeft(4,0.9)
    forward(4,4)
    
    
def gaunt3():
    motors(5,2.2)
    wait(4.7)
    stop()
    motors(2.2,5)
    
def gaunt4():
    forward(4,0.5)
    wait(2)
    forward(4,0.5)
    turnRight(4,0.9)
    forward(3,3.5)
    turnLeft(4,0.9)
    forward(4,2)
    wait(4)
    forward(4,1)
    turnLeft(4,0.9)
    forward(4,2.5)
    turnRight(4,0.9)
    forward(4,2)

def gaunt5():
    forward(4,1)
    wait(5.5)
    forward(4,1)
    turnRight(4,0.9)
    forward(4,3)
    turnLeft(4,0.9)
    forward(4,2.25)
    wait(4)
    forward(4,0.5)
    turnLeft(4,0.9)
    forward(4,3)
    turnRight(4,0.9)
    forward(4,2)
    
def gaunt6():
    forward(4,2)
    turnRight(4,0.4)
    forward(4,2)
    turnRight(4,0.5)
    forward(4,1.5)
    turnRight(4,0.4)
    forward(4,1.5)
    turnRight(4,0.4)
    forward(4,2.3)
    turnLeft(4,0.5)
    forward(4,2)
    
#gaunt7 solution
def left():
    forward(4,1)
    wait(6)
    forward(4,1)
    turnRight(4,0.9)
    forward(4,3.5)
    turnRight(2,1.2)
    forward(4,2)
    
def right():
    backward(4,1)
    wait(6)
    backward(4,1)
    turnRight(4,0.9)
    forward(4,3.5)
    turnLeft(2,1.2)
    forward(4,2)
    
    
def top():
    forward(4,1)
    turnLeft(4,0.9)
    motors(4.2,2.5)
def bot():
    forward(4,1)
    turnRight(4,0.9)
    motors(2.5,4.2)

def left2():
    forward(4,1.25)
    turnLeft(4,0.9)
    forward(4,2.5)
    turnRight(4,0.9)
    forward(4,1.5)
    
def right2():
    forward(4,1.25)
    turnRight(4,0.9)
    forward(4,2.5)
    turnLeft(4,0.9)
    forward(4,1.5)
    
#def spin():
#    motors(1,-1)
    #while True:
    #    motors(1,-1)
    
    
    

    