import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        #rEndX=350    
        #rEndY=50
        
        self.phaser=None
        self.fragileWalls=[]
        
        self.chomps=[]
        
        self.links1=[]
        self.links2=[]
        
        self.pad=[]
        
        self.anchorsX=[600,100]
        self.anchorsY=[150,250]
        self.anchors=[]
        
        self.chompVelocity=30
        
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True

        
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(300,0),(300,100),(100,100),
        (100,300),(350,300),(350,700),(0,700)))
        wall.append(Polygon((400,0),(700,0),(700,700),(350,700),(350,300),(600,300),
        (600,100),(400,100),(400,0)))
        for w in wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        PHASER_DESC="Phaser welcome. Set to stun."        
        self.phaser=Item(self, 350, 125, PHASER_DESC, Picture(LEVEL_PATH+"phaser.png"), vis=True)
                
        debug=False
        
        wX=300
        wY=70
        h=5
        w=100
        
        edge=10
        for i in range(5,0,-1):
            newY=wY+i*h
        
            self.fragileWalls.append(FragileWall(self, wX, newY, w, h))
            MoveableLock(self, 350,150,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
            #Interactable(self, wX+w/2,newY-DEFAULT_RADIUS+2*h,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
  
        
        for i in range(len(self.anchorsX)):
            self.anchors.append(Circle( (self.anchorsX[i],self.anchorsY[i]),10,color=makeColor(0,0,0,0) ))
            temp=Picture(LEVEL_PATH+"anchor.png")
            temp.outline.alpha=0
            temp.scale(0.25)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.anchors[-1])

            
        
        for a in self.anchors:
            a.draw(self.sim.window)
            a.bodyType="static"
            a.body.IgnoreGravity=True
            a.body.OnCollision+=self.dummyCollide
        
              
        #touch pads
        self.pad.append(Circle((350,150),15,color=makeColor(0,0,0,0)))
        self.pad.append(Circle((350,250),15,color=makeColor(0,0,0,0)))
        
        for p in self.pad:
            p.draw(self.sim.window)
            p.bodyType="static"
            p.body.IgnoreGravity=True
        self.pad[0].body.OnCollision+=self.pad1Collide
        self.pad[0].tag="NotActive"
        self.pad[1].body.OnCollision+=self.pad2Collide    
        self.pad[1].tag="Active"

    def createHazards(self):
        
        rad=25
        xVal=[600,100]
        yVal=[150,250]
        
        for i in range(len(self.anchorsX)):
            
            self.chomps.append(Circle((self.anchorsX[i],self.anchorsY[i]),rad))
            self.chomps[-1].draw(self.sim.window)
            self.chomps[-1].body.OnCollision+=self.chompCollide
            self.chomps[-1].fill.alpha=0
            self.chomps[-1].outline.alpha=0
            temp=Picture(LEVEL_PATH+"chompClose.png")
            if i==1:
                temp.flipHorizontal()
            temp.outline.alpha=0
            #temp.fill.alpha=125
            temp.scale(0.35)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.chomps[-1])
            temp=Picture(LEVEL_PATH+"chompOpen.png")
            if i==1:
                temp.flipHorizontal()
            temp.outline.alpha=0
            temp.visible=False
            temp.scale(0.35)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.chomps[-1])
            
        
        step=15
        numLinks=5
        for i in range(numLinks):  
            if i < (numLinks/2):
                self.links1.append(Circle((self.anchorsX[0],step+self.anchorsY[0]+step*i),2)) 
                self.links2.append(Circle((self.anchorsX[1],step+self.anchorsY[1]+step*i),2))
            else:

                self.links1.append(Circle((self.anchorsX[0],step+self.anchorsY[0]+step*(numLinks-i-1)),2)) 
                self.links2.append(Circle((self.anchorsX[1],step+self.anchorsY[1]+step*(numLinks-i-1)),2)) 
            
            temp=Picture(LEVEL_PATH+"link.png")
            temp.outline.alpha=0
            temp.scale(0.25)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.links1[-1])
            temp.draw(self.links2[-1])
                    
        
        
        for l in self.links1:
            l.draw(self.sim.window)
            l.body.IgnoreGravity=True
            l.body.OnCollision+=self.dummyCollide
        for l in self.links2:
            l.draw(self.sim.window)
            l.body.IgnoreGravity=True
            l.body.OnCollision+=self.dummyCollide
            #l.mass=1
        
        
        self.chomps[0].makeJointTo(self.links1[0],"distance")
        for i in range(1,len(self.links1)):
            self.links1[i].makeJointTo(self.links1[i-1],"distance")    
        self.anchors[0].makeJointTo(self.links1[4],"distance")    
        
        self.chomps[1].makeJointTo(self.links2[0],"distance")
        for i in range(1,len(self.links2)):
            self.links2[i].makeJointTo(self.links2[i-1],"distance")    
        self.anchors[1].makeJointTo(self.links2[4],"distance")

    def pad1Collide(self,myfixture,otherfixture,contact):
        if self.runTimer and self.gameOver==0:
            if otherfixture.Body.UserData==self.robot.frame.body.UserData:
                #self.canBall[0].tag="Active"
                #self.canBall[1].tag="NotActive"
                         
                if self.pad[0].tag=="NotActive":
                #self.canBall[1].moveTo(100,self.canBall[1].getY())
                #self.canBall[1].body.LinearVelocity=Vector(0,0)
                
                    self.chomps[0].moveTo(600,self.chomps[0].getY())
                    self.chomps[0].body.LinearVelocity=Vector(-self.chompVelocity,0)
                    self.pad[0].tag="Active"
                    
                    self.chomps[1].moveTo(100,self.chomps[1].getY())
                    self.chomps[1].body.LinearVelocity=Vector(0,0)                    
                    self.pad[1].tag="NotActive"
                    
    def pad2Collide(self,myfixture,otherfixture,contact):
        if self.runTimer and self.gameOver==0:                               
            if otherfixture.Body.UserData==self.robot.frame.body.UserData:
                #self.canBall[1].tag="Active"
                #self.canBall[0].tag="NotActive"
                
                if self.pad[1].tag=="NotActive":
                #self.canBall[0].moveTo(600,self.canBall[0].getY())
                #self.canBall[0].body.LinearVelocity=Vector(0,0)
                
                    self.chomps[1].moveTo(100,self.chomps[1].getY())
                    self.chomps[1].body.LinearVelocity=Vector(self.chompVelocity,0)
                    self.pad[1].tag="Active"
                    
                    self.chomps[0].moveTo(600,self.chomps[0].getY())
                    self.chomps[0].body.LinearVelocity=Vector(0,0)                    
                    self.pad[0].tag="NotActive"

    #   game over man
    def chompCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    


    def levelThread(self):   
        bSpeed=5
        if self.gameOver==0:
            if self.runTimer:
                if self.pad[0].tag=="Active":
                    if self.chomps[0].shapes[0].visible:
                        self.chomps[0].shapes[0].visible=False
                        self.chomps[0].shapes[1].visible=True
                    else:
                        self.chomps[0].shapes[1].visible=False
                        self.chomps[0].shapes[0].visible=True
                else:
                    self.chomps[0].shapes[0].visible=True
                    self.chomps[0].shapes[1].visible=False
                
                if self.pad[1].tag=="Active":
                    if self.chomps[1].shapes[0].visible:
                        self.chomps[1].shapes[0].visible=False
                        self.chomps[1].shapes[1].visible=True
                    else:
                        self.chomps[1].shapes[1].visible=False
                        self.chomps[1].shapes[0].visible=True
                else:
                    self.chomps[1].shapes[0].visible=True
                    self.chomps[1].shapes[1].visible=False    
        else:
            self.chomps[0].body.LinearVelocity=Vector(0,0)
            self.chomps[1].body.LinearVelocity=Vector(0,0)
                        
              
    
def startLevel():
    gauntlet1Window("Dungeon1",700,700,350,250,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 