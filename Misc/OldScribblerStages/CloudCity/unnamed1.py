import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class cloudCity1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):

        
        self.fragileWalls=[]
        self.initialApproach=False
        self.runAway=False
        self.stopDist=150
        self.lineAnchor=None
        self.bossGroup=None
        
        self.lineJoint=None
        
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    
        
    def setStatusText(self):
        
        self.statusText.append("Oh wow. This looks like it's going to be easy.")
        self.statusPoints.append((650,650))
        self.statusText.append("Who is that?! And where's he going. I have to catch him.")
        self.statusPoints.append((350,650))        
        self.statusText.append("Oh no! I'm trapped...")
        self.statusPoints.append((350,50))
        
        self.statusDebug=True
        
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(300,0),(300,700),(0,700)))
        wall.append(Polygon((400,0),(700,0),(700,600),(400,600)))        
        for w in wall:
            
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True

        debug=False
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=Item(self, 550, 650, PHASER_DESC, Picture(LEVEL_PATH+"phaser.png"), vis=True)
                
        #self.fragileWalls.append(FragileWall(self, 500, 600, 5, 100))
        self.fragileWalls.append(Animatable(self, 500, 650, Rectangle((500,600),(505,700)), [self.startApproach],[None]))
        #self.fragileWalls[-1].animation=[lambda:self.fragileWalls[-1].shape.move(0,-100)]
        #self.fragileWalls[-1].animation=self.startApproach
        
        
        Interactable(self, 550,650,self.fragileWalls[-1].animate, self.phaser,None,"",debug)

        
    def createHazards(self):
        #-100 -50        
        #self.lakitu=Npc(self, -100, -50, Picture(LEVEL_PATH+"lakitu2.png"), True, Circle((-100,-50),100))
        self.lakitu=Npc(self, 0, 0, Picture(LEVEL_PATH+"lakitu2.png"), True, Circle((0,0),100,color=makeColor(0,0,0,0)))
        
        self.lineAnchor=Circle((self.lakitu.getX()+self.lakitu.shape.width/2,self.lakitu.getY()-self.lakitu.shape.height/2),10,color=makeColor(0,0,0,0))
        
        self.lineAnchor.draw(self.sim.window)
        self.lineAnchor.body.OnCollision+=self.dummyCollide
        
        self.lineAnchor.makeJointTo(self.lakitu.hitshape,"weld",(0,0,1.35,-1.69))
        #self.lakitu.hitshape.makeJointTo(self.lineAnchor,"weld",(0,-1,0.1,-1.65))
        
        self.bossGroup=Group(self.lakitu.shape,self.lakitu.hitshape,self.lineAnchor)
        self.lakitu.shape.stackOnTop()       
        
        #self.line.draw(self.lakitu.shape)
        self.lakitu.talkbox=Rectangle((0,0),(700,700))
        #self.lakitu.shape.flipHorizontal()
        self.lakitu.shape.tag="right"
    


    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            
    def moveTowards(self,eX,eY,sX,sY,src,minDist=10,speed=10):
        #print(eX,eY,sX,sY,src,minDist,speed)
        #speed of move
        s=speed
        minDist=self.stopDist
        
        #eX=tgt.getX()+offX
        #eY=tgt.getY()+offY
        
        #sX=src.getX()
        #sY=src.getY()
            
        if eX-sX==0:
            src.move(copysign(s,eX-sX),copysign(s,eY-sY))
        else:
            #slope of line between robot and ghost
            m=(eY-sY)/(eX-sX)
            #distance between tgt and src
            D=sqrt(pow(eX-sX,2) + pow(eY-sY,2))
            if D<minDist:
                return 0
            #distance from robot that ghost will be placed at
            d=D-s
            
            #solve the equation
            #d^2=y^2 + x^2 where y and x are the distance to the 
            #point the ghost will be placed at
            
            #ans should produce two values which will be
            #the x (run) value
            ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
            
            if ans[0]==2:
                nX1=eX+ans[1][0]
                nX2=eX+ans[1][1]
                
                nY1=eY+ans[1][0]*m
                nY2=eY+ans[1][1]*m
                
                #there are two possible solutions to the equation
                #we want the one closer to the ghost
                pD1=sqrt(pow(sX-nX1,2)+pow(sY-nY1,2))
                pD2=sqrt(pow(sX-nX2,2)+pow(sY-nY2,2))
                
                if pD1<pD2:
                
                    #src.moveTo(nX1,nY1)
                    src.move(nX1-sX,nY1-sY)
                    #print(nX1,nY1)
                else:
                    #src.moveTo(nX2,nY2)
                    src.move(nX2-sX,nY2-sY)
                    #print(nX2,nY2)

        return 1    
    
    def startApproach(self):
        self.initialApproach=True
        
    def launchRun(self):
        
        self.lineJoint=self.lineAnchor.makeJointTo(self.endPlatform,"distance")
        wait(1)
        self.runAway=True  
        self.fragileWalls[-1].shape.move(0,-100)
          
    def levelThread(self):
        #speed of lakitu
        s=10
        
        if self.gameOver==0:

            
            #lakitu's initial approach and dialogue
            if self.initialApproach:

                result=self.moveTowards(285,600,self.lakitu.getX(),self.lakitu.getY(),self.bossGroup,10,25)
                
                if result==0:
                    self.initialApproach=False
                    self.lakitu.updateSpeechBubble()
                    
                    self.lakitu.addResponse("I've been watching you for a bit now.")
                    self.lakitu.addResponse("You've come very far.")
                    self.lakitu.addResponse("If you want to make it out of here you will have to deal with me.")
                    self.lakitu.addResponse("Catch me if you can. Or stay here for eternity.",self.launchRun)
            
                    self.lakitu.speak()
                    self.lakitu.nextResponse()

                    
            if self.runAway:
                if len(self.waypoints)>0:
                    speed=7
                    w=self.waypoints[0]
                    self.moveTowards(350,self.lakitu.getY(),self.endPlatform.getX(),self.endPlatform.getY(),self.endPlatform,10,speed)
                    
                    result=self.moveTowards(w[0],w[1],self.lakitu.getX(),self.lakitu.getY(),self.bossGroup,10,speed)
  
                    if self.lakitu.speechBubble:
                        self.lakitu.silence()
                    if result==0:
                        self.waypoints.remove(w)
  
            
               
                
        
        
        
        
def startLevel():
    gauntlet1Window("Dungeon1",700,700,650,650,-180,350,650,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 