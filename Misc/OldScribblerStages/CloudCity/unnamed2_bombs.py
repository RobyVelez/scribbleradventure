import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        rEndX=350    
        rEndY=50
        
        self.bombs=[]
        self.fragileWalls=[]
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True
     
    def unlitLamp(self):
        return False
    def litLamp(self):
        return True
        
    def createObstacles(self):
        wall=[]
        wall.append(Rectangle((0,0),(300,700)))
        wall.append(Rectangle((400,0),(700,700)))
        
        for w in wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        BOMB_DESC="Big boom. Use to destroy walls."
        
        self.bombs.append(Item(self, 350, 650, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
        self.bombs.append(Item(self, 350, 550, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
        self.bombs.append(Item(self, 350, 450, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
        self.bombs.append(Item(self, 350, 350, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
        self.bombs.append(Item(self, 350, 250, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
        self.bombs.append(Item(self, 350, 150, BOMB_DESC, Picture(LEVEL_PATH+"bomb.png"), vis=True))
                
        debug=False
        
        self.fragileWalls.append(FragileWall(self, 300, 100, 100, 5))
        Interactable(self, 350,150,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)
        
        self.fragileWalls.append(FragileWall(self, 300, 200, 100, 5))
        Interactable(self, 350,250,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)
       
        self.fragileWalls.append(FragileWall(self, 300, 300, 100, 5))
        Interactable(self, 350,350,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)
        
        self.fragileWalls.append(FragileWall(self, 300, 400, 100, 5))
        Interactable(self, 350,450,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)
        
        self.fragileWalls.append(FragileWall(self, 300, 500, 100, 5))
        Interactable(self, 350,550,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)
        
        self.fragileWalls.append(FragileWall(self, 300, 600, 100, 5))
        Interactable(self, 350,650,self.fragileWalls[-1].destroyWall, self.bombs,None,"",debug)


def startLevel():
    gauntlet1Window("Dungeon1",700,700,350,675,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 