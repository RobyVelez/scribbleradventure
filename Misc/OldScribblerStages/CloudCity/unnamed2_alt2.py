import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):

        
        self.phaser=None
        self.fragileWalls=[]
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True

        
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(200,0),(200,100),(300,100),
        (300,200),(200,200),(200,300),(300,300),(300,400),
        (200,400),(200,500),(300,500),(300,600),(200,600),
        (200,700),(0,700)))
        wall.append(Polygon((400,0),(700,0),(700,600),(500,600),        
        (500,500),(400,500),(400,400),(500,400),(500,300),
        (400,300),(400,200),(500,200),(500,100),(400,100)))
        #wall.append(Rectangle((400,0),(700,700)))
        
        for w in wall:
            
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=Item(self, 600, 650, PHASER_DESC, Picture(LEVEL_PATH+"phaser.png"), vis=True)
                
        debug=True
        
        spacing=100
        h=5
        w=100
        for i in range(5):
            self.fragileWalls.append(FragileWall(self, 300, 500-i*spacing, 100, 5))
            Interactable(self, 350,500-i*spacing+DEFAULT_RADIUS,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 200, 100, 5))
        #Interactable(self, 350,200,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
       
        #self.fragileWalls.append(FragileWall(self, 300, 300, 100, 5))
        #Interactable(self, 350,300,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 400, 100, 5))
        #Interactable(self, 350,400,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 500, 100, 5))
        #Interactable(self, 350,500,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 600, 100, 5))
        #Interactable(self, 350,600,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)


def startLevel():
    gauntlet1Window("Dungeon1",700,700,650,650,-180,450,650,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 