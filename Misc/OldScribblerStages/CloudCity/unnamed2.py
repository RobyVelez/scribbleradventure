import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        rEndX=350    
        rEndY=50
        
        self.phaser=None
        self.fragileWalls=[]
        
        print("Initial approach True")
        self.initialApproach=True

        
        
        self.spiny=[]
        self.spinySpeed=8
        
        #here I need to be able to access the walls in the spiny collision
        self.wall=[]
        
        self.turnY=[600,550,500,450,400,350,300,250,200,150,100,50]#,0,-50,-100]
        self.turnDir=["right","left","right","left","right","left","right","left","right","left","right","left","right","left","right","left","right","left","right","left","right","left"]
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)
        print("Finished building level")

                                        
    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True

        
    def createObstacles(self):
        
        self.wall.append(Polygon((0,0),(300,0),
        (300,150),(100,150),(100,200),(300,200),
        (300,250),(100,250),(100,300),(300,300),
        (300,350),(100,350),(100,400),(300,400),
        (300,450),(100,450),(100,500),(300,500),
        (300,550),(100,550),(100,600),(300,600),
        (300,700),(0,700)))
        self.wall.append(Polygon((700,0),(400,0),
        (400,100),(600,100),(600,150),(400,150),
        (400,200),(600,200),(600,250),(400,250),
        (400,300),(600,300),(600,350),(400,350),
        (400,400),(600,400),(600,450),(400,450),
        (400,500),(600,500),(600,550),(400,550),
        (400,700),(700,700)))
        
        for w in self.wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=Item(self, 350, 600, PHASER_DESC, Picture(LEVEL_PATH+"phaser.png"), vis=True)
        self.phaser.hide()
        debug=False
        
        spacing=50
        h=5
        w=100
        #for i in range(10):
        #    self.fragileWalls.append(FragileWall(self, 300, 550-i*spacing, 100, 5))
        #    MoveableLock(self, 350,550-i*spacing+DEFAULT_RADIUS,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        for i in range(10):
            self.fragileWalls.append(FragileWall(self, 300, 100+i*spacing, 100, 5))
            MoveableLock(self, 350,100+i*spacing+DEFAULT_RADIUS,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
         
                
    def createHazards(self):
        self.lakitu=Npc(self, 350, 600, Picture(LEVEL_PATH+"lakitu3.png"), True, Circle((0,0),100,color=makeColor(0,0,0,0)))
        self.lakitu.shape.tag="right"
        self.lakitu.hitshape.body.OnCollision+=self.dummyCollide
        self.lakitu.updateSpeechBubble()
                    
        self.lakitu.addResponse("Ah so you made it out the last level.")
        self.lakitu.addResponse("Well let's see if you can get past this obstacles and dodge spiney!",action=self.phaser.show())

        
        for i in range(10):
            if i%2==0:
                self.spiny.append(Circle((self.lakitu.shape.getX()-self.lakitu.shape.width/2,self.lakitu.shape.getY()),
                    15,color=makeColor(0,0,0,0)))
            else:
                self.spiny.append(Circle((self.lakitu.shape.getX()+self.lakitu.shape.width/2,self.lakitu.shape.getY()),
                    15,color=makeColor(0,0,0,0)))
            
            self.spiny[-1].draw(self.sim.window)
            #self.spiny[-1].body.IgnoreGravity=True
            self.spiny[-1].body.OnCollision+=self.spinyCollide
            self.spiny[-1].tag="on"
            
            Picture(LEVEL_PATH+"spinyBall.png").draw(self.spiny[-1])
            Picture(LEVEL_PATH+"spinyDown.png").draw(self.spiny[-1])
            Picture(LEVEL_PATH+"spinyUp.png").draw(self.spiny[-1])
            for p in self.spiny[-1].shapes:
                p.outline.alpha=0
                p.move(-p.width/2,-p.height/2)
                p.visible=False
                p.scale(0.75)
                p.tag="left"
                #p.fill.alpha=125
          
        
    
    
    def spinyCollide(self,myfixture,otherfixture,contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
        
        elif self.runTimer:
            
            if otherfixture.Body.UserData==self.wall[0].body.UserData or otherfixture.Body.UserData==self.wall[1].body.UserData:
                
                for s in self.spiny:
                    if s.body.UserData==myfixture.Body.UserData and s.tag=="off":
                        if contact.Manifold.LocalNormal.Y==-1:
                            s.shapes[0].visible=False
                            s.shapes[1].visible=True
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)                           
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"
                                
                                
                            s.moveTo(s.getX(),s.getY()-7)
                        else:                      
                            
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)
                                if s.shapes[1].tag=="right" and s.shapes[2].tag=="right":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="left"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="left"
                            
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"

        
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            
    def moveTowards(self,eX,eY,sX,sY,src,minDist=10,speed=10):
        #print(eX,eY,sX,sY,src,minDist,speed)
        #speed of move
        s=speed
        minDist=minDist
        
        #eX=tgt.getX()+offX
        #eY=tgt.getY()+offY
        
        #sX=src.getX()
        #sY=src.getY()
            
        if eX-sX==0:
            src.move(copysign(s,eX-sX),copysign(s,eY-sY))
        else:
            #slope of line between robot and ghost
            m=(eY-sY)/(eX-sX)
            #distance between tgt and src
            D=sqrt(pow(eX-sX,2) + pow(eY-sY,2))
            if D<minDist:
                return 0
            #distance from robot that ghost will be placed at
            d=D-s
            
            #solve the equation
            #d^2=y^2 + x^2 where y and x are the distance to the 
            #point the ghost will be placed at
            
            #ans should produce two values which will be
            #the x (run) value
            ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
            
            if ans[0]==2:
                nX1=eX+ans[1][0]
                nX2=eX+ans[1][1]
                
                nY1=eY+ans[1][0]*m
                nY2=eY+ans[1][1]*m
                
                #there are two possible solutions to the equation
                #we want the one closer to the ghost
                pD1=sqrt(pow(sX-nX1,2)+pow(sY-nY1,2))
                pD2=sqrt(pow(sX-nX2,2)+pow(sY-nY2,2))
                
                if pD1<pD2:
                
                    #src.moveTo(nX1,nY1)
                    src.move(nX1-sX,nY1-sY)
                    #print(nX1,nY1)
                else:
                    #src.moveTo(nX2,nY2)
                    src.move(nX2-sX,nY2-sY)
                    #print(nX2,nY2)

        return 1    

    def levelThread(self):
        ySpeed=-8
        if self.gameOver==0:
        
            if self.initialApproach:
                self.initialApproach=False
                    #print("Initial approach")
                    #self.initialApproach=False
                    #self.lakitu.speak()
                    #self.lakitu.nextResponse()

            elif self.runTimer:
                if self.lakitu.shape.getY()>-100:
                    self.lakitu.move(0,ySpeed)
                    for s in self.spiny:
                        if s.tag=="on":
                            s.move(0,ySpeed)
                    if self.lakitu.shape.getY()>=self.turnY[-2]:
                        for i in range(len(self.turnY)-1):
                            if self.lakitu.shape.getY()<self.turnY[i] and self.lakitu.shape.getY()>self.turnY[i+1] and self.lakitu.shape.tag==self.turnDir[i]:
                                
                                self.lakitu.shape.flipHorizontal()
                                self.lakitu.shape.tag=self.turnDir[i+1]
                                if i < len(self.spiny):
                                    self.spiny[i].moveTo(self.spiny[i].getX(),self.spiny[i].getY()-20)
                                    self.spiny[i].body.LinearVelocity=Vector(0,-ySpeed)
                                    self.spiny[i].tag="off"
                                    self.spiny[i].shapes[0].visible=True
                    
            #animate spiny
            for s in self.spiny:
                if s.tag=="off":
                    if s.shapes[1].visible==True:# and s.shapes[2].visible==False:
                        s.shapes[1].visible=False
                        s.shapes[2].visible=True
                    elif s.shapes[2].visible==True:# and s.shapes[1].visible==False:
                        s.shapes[2].visible=False
                        s.shapes[1].visible=True
                    #else:
                    #    s.shapes[2].visible=False
                    #    s.shapes[1].visible=False
                  
        #make everything stop
        else:
            for s in self.spiny:
                s.body.LinearVelocity=Vector(0,0)
         
                    
def startLevel():
    gauntlet1Window("Dungeon1",700,700,350,675,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 