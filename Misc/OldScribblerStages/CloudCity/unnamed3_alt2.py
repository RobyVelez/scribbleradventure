import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        rEndX=350    
        rEndY=50
        self.phaser=None
        self.fragileWalls=[]
        
        self.lakitu=None
        self.spiny=[]
        self.initialApproach=True
        
        self.
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True

        
    def createObstacles(self):
        
        self.wall.append(Rectangle((0,0),(300,700)))
        self.wall.append(Rectangle((400,0),(700,700)))
        
        for w in wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=Item(self, 350, 600, PHASER_DESC, Picture(LEVEL_PATH+"phaser.png"), vis=True)
        self.phaser.hide()        
        debug=False
        
        spacing=100
        h=5
        w=100
        for i in range(5):
            for j in range(i+1):        
                self.fragileWalls.append(FragileWall(self, 300, 550-i*spacing-j*h, 100, 5))
                MoveableLock(self, 350,550-i*spacing-j*h+DEFAULT_RADIUS,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 200, 100, 5))
        #Interactable(self, 350,200,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
       
        #self.fragileWalls.append(FragileWall(self, 300, 300, 100, 5))
        #Interactable(self, 350,300,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 400, 100, 5))
        #Interactable(self, 350,400,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 500, 100, 5))
        #Interactable(self, 350,500,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        #self.fragileWalls.append(FragileWall(self, 300, 600, 100, 5))
        #Interactable(self, 350,600,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)


    def createHazards(self):
    
        self.lakitu=Npc(self, 350, 600, Picture(LEVEL_PATH+"lakitu3.png"), True, Circle((0,0),100,color=makeColor(0,0,0,0)))
        self.lakitu.shape.tag="right"
        self.lakitu.hitshape.body.OnCollision+=self.dummyCollide
        
        self.lakitu.addResponse("You're kind of stubborn aren't you.")            
        self.lakitu.addResponse("Alright, no more Mr. Nice Flying Koopa.",action=self.phaser.show())

        
        for i in range(10):
            if i%2==0:
                self.spiny.append(Circle((self.lakitu.shape.getX()-self.lakitu.shape.width/2,self.lakitu.shape.getY()),
                    15,color=makeColor(0,0,0,0)))
            else:
                self.spiny.append(Circle((self.lakitu.shape.getX()+self.lakitu.shape.width/2,self.lakitu.shape.getY()),
                    15,color=makeColor(0,0,0,0)))
            
            self.spiny[-1].draw(self.sim.window)
            #self.spikey[-1].body.IgnoreGravity=True
            self.spiny[-1].body.OnCollision+=self.spinyCollide
            self.spiny[-1].tag="on"
            
            Picture(LEVEL_PATH+"spinyBall.png").draw(self.spiny[-1])
            Picture(LEVEL_PATH+"spinyDown.png").draw(self.spiny[-1])
            Picture(LEVEL_PATH+"spinyUp.png").draw(self.spiny[-1])
            for p in self.spiny[-1].shapes:
                p.outline.alpha=0
                p.move(-p.width/2,-p.height/2)
                p.visible=False
                p.scale(0.75)
                p.tag="left"
                #p.fill.alpha=125
                
    def spinyCollide(self,myfixture,otherfixture,contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
        
        elif self.runTimer:
            
            if otherfixture.Body.UserData==self.wall[0].body.UserData or otherfixture.Body.UserData==self.wall[1].body.UserData:
                
                for s in self.spiny:
                    if s.body.UserData==myfixture.Body.UserData and s.tag=="off":
                        if contact.Manifold.LocalNormal.Y==-1:
                            s.shapes[0].visible=False
                            s.shapes[1].visible=True
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)                           
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"
                                
                                
                            s.moveTo(s.getX(),s.getY()-7)
                        else:                      
                            
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)
                                if s.shapes[1].tag=="right" and s.shapes[2].tag=="right":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="left"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="left"
                            
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"
           

    def test(self):        
        
        ySpeed=-8
        if self.gameOver==0:
        
            if self.initialApproach:
                    self.initialApproach=False
                    self.lakitu.speak()
                    self.lakitu.nextResponse()
                

            elif self.runTimer:
                if self.lakitu.shape.getY()>-100:
                    self.lakitu.move(0,ySpeed)
                    for s in self.spiny:
                        if s.tag=="on":
                            s.move(0,ySpeed)
                    if self.lakitu.shape.getY()>=self.turnY[-2]:
                        for i in range(len(self.turnY)-1):
                            if self.lakitu.shape.getY()<self.turnY[i] and self.lakitu.shape.getY()>self.turnY[i+1] and self.lakitu.shape.tag==self.turnDir[i]:
                                
                                self.lakitu.shape.flipHorizontal()
                                self.lakitu.shape.tag=self.turnDir[i+1]
                                if i < len(self.spiny):
                                    self.spiny[i].moveTo(self.spiny[i].getX(),self.spiny[i].getY()-20)
                                    self.spiny[i].body.LinearVelocity=Vector(0,-ySpeed)
                                    self.spiny[i].tag="off"
                                    self.spiny[i].shapes[0].visible=True
                    
            #animate spiny
            for s in self.spiny:
                if s.tag=="off":
                    if s.shapes[1].visible==True:# and s.shapes[2].visible==False:
                        s.shapes[1].visible=False
                        s.shapes[2].visible=True
                    elif s.shapes[2].visible==True:# and s.shapes[1].visible==False:
                        s.shapes[2].visible=False
                        s.shapes[1].visible=True
                    #else:
                    #    s.shapes[2].visible=False
                    #    s.shapes[1].visible=False
        #make everything stop
        else:
            for s in self.spiny:
                s.body.LinearVelocity=Vector(0,0)
         

def startLevel():
    gauntlet1Window("Dungeon1",700,700,350,675,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 