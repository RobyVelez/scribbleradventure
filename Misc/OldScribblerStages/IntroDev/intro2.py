import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(NewLevelWindow):    
    def __init__(self,gameData,parent):      
        
         #hazard objects
        self.fire=[] 
        self.blockade=[]
        self.sojourner=None
        self.scribby=None    
        self.talkBrief=True
            
        
        NewLevelWindow.__init__(self,levelName="Gauntlet1",rStartX=650,rStartY=450,rStartT=-180,
        rEndX=50,rEndY=150,gameData=gameData,parent=parent)                    
        self.setup()
        
        self.briefScreen.visible=False
        self.oppyCue()
        #self.sojourner.speak("Sojourner: (From the kitchen) Scribby! Come eat!.",self.launchBrief)
        
    #Calico will crash if there is no image!!!!
    def getBriefImage(self):      
        return LEVEL_PATH+"introBrief1a.png"
        
    def launchBrief(self):
        self.briefScreen.visible=True
        
    def oppyCue(self):
        self.oppy.shape.visible=True
        #self.conversation.addResponse("Sojourner: (From the kitchen) Scribby! Come eat!.", self.launchBrief, portrait=sojournerPort)
        
        self.oppy.speak("Ha ha, looks like Mr. Tough guy got yelled at by Nana again.")
        self.scribby.addResponse("Shut up.", self.spiritCue)        

    def spiritCue(self):
        wait(1)
        #move oppy to the door
        for i in range(50):
            self.oppy.move(-5,0)
            wait(0.01)
        for i in range(60):
            self.oppy.move(0,-5)
            wait(0.01)
        for i in range(50):
            self.oppy.move(-5,0)
            wait(0.01)
        self.oppy.shape.visible=False
            
        self.spirit.shape.visible=True
        
        self.spirit.speak("Oh don't be like that bro. It's not our fault we're so cool and completed so many awesome quests, unlike yourself.")
        self.scribby.addResponse("Grrrrrr!!!!", self.curiosityCue)
        
    def toggleTalk1(self):
        self.curiosity.talkRadius=1000
        self.scribby.talkRadius=0
        print("toggle talk 1")
        wait(0.05)
    def toggleTalk2(self):
        self.curiosity.talkRadius=0
        self.scribby.talkRadius=1000
        print("toggle talk 2")
        wait(0.05)
    def curiosityCue(self):     
        print("in curiosity cue")   
        wait(1)
        #move spirit to the door
        for i in range(30):
            self.spirit.move(-5,0)
            wait(0.01)
        for i in range(60):
            self.spirit.move(0,-5)
            wait(0.01)
        for i in range(50):
            self.spirit.move(-5,0)
            wait(0.01)
        self.spirit.shape.visible=False    
    
        self.curiosity.shape.visible=True
        
        self.curiosity.speak("Don't worry about those two, they're knuckleheads the both of them.")
        
        self.scribby.addResponse("I know. I just wish you all would stop treating me like a kid. Like I can't handle quests.",self.toggleTalk1)
        self.curiosity.addResponse("You have to be patient. You'll get you're turn soon enough.",self.toggleTalk2)
        self.scribby.addResponse("Not if Nana gets her way.",self.toggleTalk1)
        self.curiosity.addResponse("Hey now. She's done a lot for us. She... we've all lost a lot. She is just looking out for you, for all of us.",self.toggleTalk2)
        self.scribby.addResponse("I know. I guess I just want to show you all what I can do.",self.doneTalking)
        
        #self.curiosity.addResponse("All in good time little brother. All in good time.",self.doneTalking())
          
        
    def doneTalking(self):
        self.curiosity.talkRadius=0
        self.scribby.talkRadius=0
        wait(1)
        #move curiosity to the door
        for i in range(40):
            self.curiosity.move(-5,0)
            wait(0.01)
        for i in range(60):
            self.curiosity.move(0,-5)
            wait(0.01)
        for i in range(50):
            self.curiosity.move(-5,0)
            wait(0.01)
        self.curiosity.shape.visible=False         
    
        self.moveBlockade()
          
        self.launchBrief()
        

                          
    def stopGame(self):
        self.setGameOver(1)
        
    def blockInRobot(self):
        #hardcodig the height and width of the robot
        height=45
        width=45
        #Upper right, Lower right, Lower left, and Upper left corners of the robot
        ur=(self.robotX+width/2,self.robotY-height/2)
        lr=(self.robotX+width/2,self.robotY+height/2)
        ll=(self.robotX-width/2,self.robotY+height/2)
        ul=(self.robotX-width/2,self.robotY-height/2)

        #adds walls all around the robot so it can't turn or move
        self.blockade.append(self.addObstacle(Rectangle(ur,(lr[0]+1,lr[1]))))
        self.blockade.append(self.addObstacle(Rectangle((ul[0]-1,ul[1]),ll)))
        
        self.blockade.append(self.addObstacle(Rectangle((ul[0],ul[1]-4),(ur[0],ur[1]-1))))        
        self.blockade.append(self.addObstacle(Rectangle((ll[0],ll[1]+2),(lr[0],lr[1]+4))))
        
        #for b in self.blockade:
        #    b.visible=False
            
    def moveBlockade(self):
        #crude function. just moves the blockade away
        for i in range(len(self.blockade)):
            if i==0 or i==1:
                self.blockade[i].move(0,100)
            elif i==2 or i==3:
                self.blockade[i].move(1000,0)
            
    def createObstacles(self):
        wall1=self.addObstacle(Polygon((0,0),(700,0),(700,400),
            (350,400),(350,100),(0,100)))
        wall2=self.addObstacle(Polygon((0,200),(250,200),(250,500),
            (700,500),(700,700),(0,700)))
        
        
        #add doors and names to the hallway
        #wall1.draw(Text((50,50),"Sprirt",fill=Color("black")))
        self.quickActor(400, 325, LEVEL_PATH+"door1.png",scale=0.4,obstructs=False,debug=False)
        self.quickActor(500, 325, LEVEL_PATH+"door1.png",scale=0.4,obstructs=False,debug=False)
        self.quickActor(400, 575, LEVEL_PATH+"door1.png",scale=0.4,obstructs=False,debug=False)
        self.quickActor(500, 575, LEVEL_PATH+"door1.png",scale=0.4,obstructs=False,debug=False)

                                        
        self.blockInRobot()  
            
    def createHazards(self):
        self.oppy=self.quickActor(550, 450, LEVEL_PATH+"oppyPortrait.png", visible=False, obstructs=False,debug=False)
        self.oppy.talkRadius=0
        self.spirit=self.quickActor(450, 450, LEVEL_PATH+"spiritPortrait.png", visible=False, obstructs=False,debug=False)
        self.spirit.talkRadius=0
        self.curiosity=self.quickActor(500, 450, LEVEL_PATH+"curiosityPortrait.png", visible=False, obstructs=False,debug=False)
        self.curiosity.talkRadius=0
        self.scribby = self.quickActor(550, 650, LEVEL_PATH+"scribbyPortrait.png", visible=False, obstructs=False,debug=False)
        self.scribby.talkRadius=10000
        
        
        
        '''
        self.sojourner = self.quickActor(600, 650, LEVEL_PATH+"sojournerPortrait.png", speech=True, visible=False, obstructs=False,debug=False)
        self.scribby = self.quickActor(550, 650, LEVEL_PATH+"scribbyPortrait.png", speech=True, visible=False, obstructs=False,debug=False)
        
        #turns sojourner and scribby talk on and off
        self.scribby.talkRadius=10000
        self.sojourner.talkRadius=0            
        
        
        #line of dialogue for the sojourner and scribby
        self.scribby.addResponse("Scribby: I'm not hungry.",action=self.toggleTalk1)        
        self.sojourner.addResponse("Sojourner: (From the kitchen) Come eat now! Don't make me come over there!",action=self.toggleTalk2)
        self.scribby.addResponse("Scribby: Yes ma'am.",action=self.doneTalking)
        '''
def startLevel(gameData=None,parent=None):
    #gauntlet1Window("Gauntlet1",700,700,350,650,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()