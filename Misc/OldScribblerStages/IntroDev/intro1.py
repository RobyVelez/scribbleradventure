import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):      
        
         #hazard objects
        self.fire=[] 
        self.blockade=[]
        self.sojourner=None
        self.scribby=None    
        self.talkBrief=True
            
        
<<<<<<< HEAD:IntroDev/intro1.py
        LevelWindow.__init__(self,levelName="Gauntlet1",rStartX=350,rStartY=650,rStartT=-90,
        rEndX=350,rEndY=0,gameData=gameData,parent=parent)                    
=======
        LevelWindow.__init__(self,levelName="Gauntlet1",gameData=gameData,parent=parent)                    
>>>>>>> da2b230f0b6c504c2c08cad8bb437504de691777:Misc/IntroDev/intro1.py
        self.setup()
        #print(self.sim.window.AppPaintable, self.sim.window.canvas.AppPaintable)
        #self.briefScreen.visible=False        

        
    def onLevelStart(self):
        self.talk()
        self.robots[0].pauseRobot=True
    
    def createLevel(self):
        self.setBackground(LEVEL_PATH+"wooFloor2.png")   
    
        #Create static objects
    
        #glorified event pads
        self.createEndPad(350,0)
        self.createStartPad(350,650)
        
        #create the robot
        self.createRobot(350,650,-90)
        
    
        sojournerPort = LEVEL_PATH+"sojournerPortrait.png"
        scribbyPort = LEVEL_PATH+"scribbyPortrait.png"
        
        self.addResponse(sojournerPort, "Sojourner: (From the kitchen) Scribby! Come eat!.")
        self.addResponse(scribbyPort, "Scribby: I'm not hungry.")        
        self.addResponse(sojournerPort, "Sojourner: (From the kitchen) Come eat now! Don't make me come over there! But what if there was may more text that did not fit on a single instance of the textbox will it properly resize? Currenlty it does not, and it will overwrite the talk() box, but maybe in the future?")
        self.addResponse(scribbyPort, "Scribby: Yes ma'am.", action=self.doneTalking)

    
    #Calico will crash if there is no image!!!!
    def getBriefImage(self):
        if self.talkBrief:        
            return LEVEL_PATH+"introBrief1a.png"
        else:
            return LEVEL_PATH+"introBrief1b.png"
        
    #def launchBrief(self):
    #    self.briefScreen.visible=True
        
    def doneTalking(self):
        self.robot.pauseRobot=False
        #Changes the brief from the talk() instruction to the forward() instruction
        self.talkBrief=False
        #self.createBriefScreen()  #<- This line can crash Calico!
        #self.launchBrief()

    def stopGame(self):
        self.setGameOver(1)
        
            
            
    
def startLevel(gameData=None,parent=None):
    #gauntlet1Window("Gauntlet1",700,700,350,650,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()