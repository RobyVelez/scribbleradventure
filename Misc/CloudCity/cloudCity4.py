import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"

class EventPad(ShapeActor):

    """
    Pad to exclusively launch interactable events.    """
    
    def __init__(self,(x,y),radius,callback,vis=False,deb=False):
        self.pad=Circle((x,y),radius)        
        ShapeActor.__init__(self,(x,y),self.pad,bodyType="static", visible=vis, debug=deb,interact=callback)
        
class BreakableWall():
    """
    Breakable Wall composed of 5 bricks that scatter when hit with something. Pass the top left corner and the
    gapWidth that the wall must cover. 
    """
    
    def __init__(self, x,y,gap,window,horiz=False):   
        self.numBricks=10
        self.brickThickness=10
        self.brickHeight=gap/self.numBricks
        self.bricks=[]
        
        for i in range(self.numBricks):
            if horiz:
                self.bricks.append(Rectangle((x+i*self.brickHeight,y),(x+(i+1)*self.brickHeight,y+self.brickThickness),color=Color("firebrick")))
            else: #vertical
                self.bricks.append(Rectangle((x,y+i*self.brickHeight),(x+self.brickThickness,y+(i+1)*self.brickHeight),color=Color("firebrick")))
            
            self.bricks[i].outline=Color("black")
            self.bricks[i].draw(window)
            self.bricks[i].bodyType="static"
    def destroyWall(self):
        if self.numBricks>2:
            for i in range(1,self.numBricks-1):
                self.bricks[i].fill.alpha=50    
                self.bricks[i].body.IsSensor=True
                    
class Spiny(SpriteActor):
    """
    Class for creating Spiny hazrads
    """
    def __init__(self,sX,sY,eX,eY,vis,deb):
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        
        self.stopDist=15
        self.speed=10
        self.atEnd=False
        
        self.walkDirection=1
        self.leftBoundary=100
        self.rightBoundary=500
        
        self.costumes=[LEVEL_PATH+"spinyBall.png",LEVEL_PATH+"spinyDown.png",LEVEL_PATH+"spinyUp.png"]
        self.mainBody=Circle((self.startX,self.startY),15,color=makeColor(0,0,0,0))
        SpriteActor.__init__(self,position=(self.startX,self.startY),fileNames=self.costumes,collisionShape=self.mainBody,scale=0.75,collision=self.spinyCollide)
        
        
        
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
        
    def spinyCollide(self,myfixture,otherfixture,contact):
        if getLevelObject().robot is not None:
            if otherfixture.Body.UserData==getLevelObject().robot.frame.body.UserData:
                getLevelObject().setGameOver(-1)
            if isinstance(otherfixture.Body.UserData,Polygon):
                v=myfixture.Body.LinearVelocity.X
                if abs(v)>0:
                    myfixture.Body.LinearVelocity=Vector(-1*v,0)
                    self.flipCostumeHoriz(1)
                    self.flipCostumeHoriz(2)
                
        return True
    def thrown(self):
        result=self.moveToward() 
        
        #spiny has reached it's destination
        if not result:       
            self.shape.body.LinearVelocity=Vector(self.walkDirection*7,0)
            getLevelObject().addStepEvent(self.walking)
            self.changeCostume(1)
        return result

    def walking(self):
        if self.currentCostumeIndex==1:
            self.changeCostume(2)
        elif self.currentCostumeIndex==2:
            self.changeCostume(1)    

        return True
        
        
class Lakitu(PictureActor):
    """
    Class to make it easier to make Lakitu. 
    -Lakitu can make an initial approach based on some event.
    """
    def __init__(self, sX,sY,eX,eY,direction):
        
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        self.atEnd=False
        
        self.stopDist=25
        self.speed=25
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        self.speechBubble=LRC_SpeechBubble(talkRadius=450)
        PictureActor.__init__(self,(self.startX,self.startY),LEVEL_PATH+"lakitu2.png",speech=self.speechBubble)
        self.shape.tag="right"
        
        #holds the spinys
        self.spiny=[]
        self.spinyIndex=0
        
        #y values when spinys will be tossed
        self.tossPoints=[]
    
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
    
    def finalEscape(self):
        oldY=self.getY()
        result=self.moveToward()
        newY=self.getY()
        level=getLevelObject()
        level.endPlatform.moveTo(level.endPlatform.getX(),level.endPlatform.getY()-abs(oldY-newY))
        
        return result
        
    def initialApproach(self):
        result=self.moveToward()
        if not result:
            self.speak("I've been watching you for a bit now.")
        return result
    
    def throwingSpinys(self):
        result=self.moveToward()
        level=getLevelObject()
        if self.spinyIndex<len(self.spiny) and self.getY()<self.tossPoints[self.spinyIndex]:
            self.spiny[self.spinyIndex].show()
            level.addStepEvent(self.spiny[self.spinyIndex].thrown)
            self.spinyIndex+=1
            
        return result                                                    



class cloudCity4Window(NewLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        #rEndX=350    
        #rEndY=50
        
        self.phaser=None
        self.breakWalls=[]
        
        self.chomps=[]
        
        self.links1=[]
        self.links2=[]
        
        self.pad=[]
        
        self.anchorsX=[600,100]
        self.anchorsY=[150,250]
        self.anchors=[]
        
        self.chompVelocity=30
        
        self.phaserRechargeTime=0
        
        
        NewLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        

    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        #self.statusPoints.append((350,675))
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((350,535))
        
        #self.statusDebug=True

        
    #lakitu is done talking, time to catch him
    def catchEndGoal(self): 
        self.doneTalking=True
        self.phaser.show()
        wait(1)
        self.lakitu.silence()
        
    def phaserRecharge(self):
        if self.phaserRechargeTime>0:
            self.phaserRechargeTime=self.phaserRechargeTime-1
            return True
        else:
            return False
    #robot can only destroy the wall once lakitu is done talking        
    def destroyWall(self,lock,items): 
        if self.doneTalking and items[0]==self.phaser and self.phaserRechargeTime==0:
            lock.move(200, 0)
            lock.userData["wall"].destroyWall()            
            lock.canInteract=False
            
            self.phaserRechargeTime=1
            self.addStepEvent(self.phaserRecharge)
        
        if self.phaserRechargeTime>0:
            self.printStatusText("Phaser needs to recharge.")
            
            
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(300,0),(300,100),(100,100),
        (100,300),(350,300),(350,700),(0,700)))
        wall.append(Polygon((400,0),(700,0),(700,700),(350,700),(350,300),(600,300),
        (600,100),(400,100),(400,0)))
        for w in wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        #debug=False

        wX=300
        wY=70
        h=5
        w=100
        
        edge=10
        for i in range(5,0,-1):
            newY=wY+i*h
            self.breakWalls.append(BreakableWall(wX, newY, 100, self.sim.window,horiz=True))
            pad=self.addActor(EventPad((wX+w/2, 100+DEFAULT_RADIUS),25,self.destroyWall,vis=True,deb=True))
            pad.userData["wall"] = self.breakWalls[-1]
            
            
            #self.fragileWalls.append(FragileWall(self, wX, newY, w, h))
            #MoveableLock(self, 350,150,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
            #Interactable(self, wX+w/2,newY-DEFAULT_RADIUS+2*h,self.fragileWalls[-1].destroyWall, self.phaser,None,"",debug)
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=self.addActor(PictureActor((350, 135), Picture(LEVEL_PATH+"phaser.png"), description=PHASER_DESC, pickup="self"))
        
        self.phaser.hide()
      
        
        for i in range(len(self.anchorsX)):
            self.anchors.append(Circle( (self.anchorsX[i],self.anchorsY[i]),10,color=makeColor(0,0,0,0) ))
            temp=Picture(LEVEL_PATH+"anchor.png")
            temp.outline.alpha=0
            temp.scale(0.25)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.anchors[-1])

            
        
        for a in self.anchors:
            a.draw(self.sim.window)
            a.bodyType="static"
            a.body.IgnoreGravity=True
            a.body.OnCollision+=self.dummyCollide
        
              
        #touch pads
        self.pad.append(Circle((350,150),15,color=makeColor(0,0,0,0)))
        self.pad.append(Circle((350,250),15,color=makeColor(0,0,0,0)))
        
        for p in self.pad:
            p.draw(self.sim.window)
            p.bodyType="static"
            p.body.IgnoreGravity=True
        self.pad[0].body.OnCollision+=self.pad1Collide
        self.pad[0].tag="NotActive"
        self.pad[1].body.OnCollision+=self.pad2Collide    
        self.pad[1].tag="Active"

    def createHazards(self):
        
        self.lakitu=self.addActor(Lakitu(250,600,250,-200,"right"))    
        self.lakitu.speechBubble.addResponse("So you got past my spinys.")
        self.lakitu.addResponse("Let's see if you can dodge the chomps.",self.catchEndGoal)
        
        rad=25
        xVal=[600,100]
        yVal=[150,250]
        
        for i in range(len(self.anchorsX)):
            
            self.chomps.append(Circle((self.anchorsX[i],self.anchorsY[i]),rad))
            self.chomps[-1].draw(self.sim.window)
            self.chomps[-1].body.OnCollision+=self.chompCollide
            self.chomps[-1].fill.alpha=0
            self.chomps[-1].outline.alpha=0
            temp=Picture(LEVEL_PATH+"chompClose.png")
            if i==1:
                temp.flipHorizontal()
            temp.outline.alpha=0
            #temp.fill.alpha=125
            temp.scale(0.35)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.chomps[-1])
            temp=Picture(LEVEL_PATH+"chompOpen.png")
            if i==1:
                temp.flipHorizontal()
            temp.outline.alpha=0
            temp.visible=False
            temp.scale(0.35)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.chomps[-1])
            
        
        step=15
        numLinks=5
        for i in range(numLinks):  
            if i < (numLinks/2):
                self.links1.append(Circle((self.anchorsX[0],step+self.anchorsY[0]+step*i),2)) 
                self.links2.append(Circle((self.anchorsX[1],step+self.anchorsY[1]+step*i),2))
            else:

                self.links1.append(Circle((self.anchorsX[0],step+self.anchorsY[0]+step*(numLinks-i-1)),2)) 
                self.links2.append(Circle((self.anchorsX[1],step+self.anchorsY[1]+step*(numLinks-i-1)),2)) 
            
            temp=Picture(LEVEL_PATH+"link.png")
            temp.outline.alpha=0
            temp.scale(0.25)
            temp.move(-temp.width/2,-temp.height/2)
            temp.draw(self.links1[-1])
            temp.draw(self.links2[-1])
                    
        
        
        for l in self.links1:
            l.draw(self.sim.window)
            l.body.IgnoreGravity=True
            l.body.OnCollision+=self.dummyCollide
        for l in self.links2:
            l.draw(self.sim.window)
            l.body.IgnoreGravity=True
            l.body.OnCollision+=self.dummyCollide
            #l.mass=1
        
        
        self.chomps[0].makeJointTo(self.links1[0],"distance")
        for i in range(1,len(self.links1)):
            self.links1[i].makeJointTo(self.links1[i-1],"distance")    
        self.anchors[0].makeJointTo(self.links1[4],"distance")    
        
        self.chomps[1].makeJointTo(self.links2[0],"distance")
        for i in range(1,len(self.links2)):
            self.links2[i].makeJointTo(self.links2[i-1],"distance")    
        self.anchors[1].makeJointTo(self.links2[4],"distance")

        
    def pad1Collide(self,myfixture,otherfixture,contact):
        if self.runTimer and self.gameOver==0:
            if otherfixture.Body.UserData==self.robot.frame.body.UserData:
                #self.canBall[0].tag="Active"
                #self.canBall[1].tag="NotActive"
                         
                if self.pad[0].tag=="NotActive":
                #self.canBall[1].moveTo(100,self.canBall[1].getY())
                #self.canBall[1].body.LinearVelocity=Vector(0,0)
                
                    self.chomps[0].moveTo(600,self.chomps[0].getY())
                    self.chomps[0].body.LinearVelocity=Vector(-self.chompVelocity,0)
                    self.pad[0].tag="Active"
                    
                    self.chomps[1].moveTo(100,self.chomps[1].getY())
                    self.chomps[1].body.LinearVelocity=Vector(0,0)                    
                    self.pad[1].tag="NotActive"
                    
    def pad2Collide(self,myfixture,otherfixture,contact):
        if self.runTimer and self.gameOver==0:                               
            if otherfixture.Body.UserData==self.robot.frame.body.UserData:
                #self.canBall[1].tag="Active"
                #self.canBall[0].tag="NotActive"
                
                if self.pad[1].tag=="NotActive":
                #self.canBall[0].moveTo(600,self.canBall[0].getY())
                #self.canBall[0].body.LinearVelocity=Vector(0,0)
                
                    self.chomps[1].moveTo(100,self.chomps[1].getY())
                    self.chomps[1].body.LinearVelocity=Vector(self.chompVelocity,0)
                    self.pad[1].tag="Active"
                    
                    self.chomps[0].moveTo(600,self.chomps[0].getY())
                    self.chomps[0].body.LinearVelocity=Vector(0,0)                    
                    self.pad[0].tag="NotActive"

    #   game over man
    def chompCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    


    def levelThread(self):   
        bSpeed=5
        if self.gameOver==0:
            if self.phaserRechargeTime>0:
                self.phaserRechargeTime=self.phaserRechargeTime-1
                
            if self.runTimer:
                if self.pad[0].tag=="Active":
                    if self.chomps[0].shapes[0].visible:
                        self.chomps[0].shapes[0].visible=False
                        self.chomps[0].shapes[1].visible=True
                    else:
                        self.chomps[0].shapes[1].visible=False
                        self.chomps[0].shapes[0].visible=True
                else:
                    self.chomps[0].shapes[0].visible=True
                    self.chomps[0].shapes[1].visible=False
                
                if self.pad[1].tag=="Active":
                    if self.chomps[1].shapes[0].visible:
                        self.chomps[1].shapes[0].visible=False
                        self.chomps[1].shapes[1].visible=True
                    else:
                        self.chomps[1].shapes[1].visible=False
                        self.chomps[1].shapes[0].visible=True
                else:
                    self.chomps[1].shapes[0].visible=True
                    self.chomps[1].shapes[1].visible=False    
        else:
            self.chomps[0].body.LinearVelocity=Vector(0,0)
            self.chomps[1].body.LinearVelocity=Vector(0,0)
                        
              
    
def startLevel():
    cloudCity4Window("Dungeon1",700,700,350,250,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 