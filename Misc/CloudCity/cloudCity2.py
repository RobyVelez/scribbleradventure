import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"

class EventPad(ShapeActor):

    """
    Pad to exclusively launch interactable events.    """
    
    def __init__(self,(x,y),radius,callback,vis=False,deb=False):
        self.pad=Circle((x,y),radius)        
        ShapeActor.__init__(self,(x,y),self.pad,bodyType="static", visible=vis, debug=deb,interact=callback)
        
class BreakableWall():
    """
    Breakable Wall composed of 5 bricks that scatter when hit with something. Pass the top left corner and the
    gapWidth that the wall must cover. 
    """
    
    def __init__(self, x,y,gap,window,horiz=False):   
        self.numBricks=10
        self.brickThickness=10
        self.brickHeight=gap/self.numBricks
        self.bricks=[]
        
        for i in range(self.numBricks):
            if horiz:
                self.bricks.append(Rectangle((x+i*self.brickHeight,y),(x+(i+1)*self.brickHeight,y+self.brickThickness),color=Color("firebrick")))
            else: #vertical
                self.bricks.append(Rectangle((x,y+i*self.brickHeight),(x+self.brickThickness,y+(i+1)*self.brickHeight),color=Color("firebrick")))
            
            self.bricks[i].outline=Color("black")
            self.bricks[i].draw(window)
            self.bricks[i].bodyType="static"
    def destroyWall(self):
        if self.numBricks>2:
            for i in range(1,self.numBricks-1):
                self.bricks[i].fill.alpha=50    
                self.bricks[i].body.IsSensor=True
                    
class Spiny(SpriteActor):
    """
    Class for creating Spiny hazrads
    """
    def __init__(self,sX,sY,eX,eY,vis,deb):
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        
        self.stopDist=15
        self.speed=10
        self.atEnd=False
        
        self.walkDirection=1
        self.leftBoundary=100
        self.rightBoundary=500
        
        self.costumes=[LEVEL_PATH+"spinyBall.png",LEVEL_PATH+"spinyDown.png",LEVEL_PATH+"spinyUp.png"]
        self.mainBody=Circle((self.startX,self.startY),15,color=makeColor(0,0,0,0))
        SpriteActor.__init__(self,position=(self.startX,self.startY),fileNames=self.costumes,collisionShape=self.mainBody,scale=0.75,collision=self.spinyCollide)
        
        
        
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
        
    def spinyCollide(self,myfixture,otherfixture,contact):
        if getLevelObject().robot is not None:
            if otherfixture.Body.UserData==getLevelObject().robot.frame.body.UserData:
                getLevelObject().setGameOver(-1)
            if isinstance(otherfixture.Body.UserData,Polygon):
                v=myfixture.Body.LinearVelocity.X
                if abs(v)>0:
                    myfixture.Body.LinearVelocity=Vector(-1*v,0)
                    self.flipCostumeHoriz(1)
                    self.flipCostumeHoriz(2)
                
        return True
    def thrown(self):
        result=self.moveToward() 
        
        #spiny has reached it's destination
        if not result:       
            self.shape.body.LinearVelocity=Vector(self.walkDirection*5,0)
            getLevelObject().addStepEvent(self.walking)
            self.changeCostume(1)
        return result

    def walking(self):
        if self.currentCostumeIndex==1:
            self.changeCostume(2)
        elif self.currentCostumeIndex==2:
            self.changeCostume(1)    

        return True
        
        
class Lakitu(PictureActor):
    """
    Class to make it easier to make Lakitu. 
    -Lakitu can make an initial approach based on some event.
    """
    def __init__(self, sX,sY,eX,eY,direction):
        
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        self.atEnd=False
        
        self.stopDist=25
        self.speed=25
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        self.speechBubble=LRC_SpeechBubble(talkRadius=450)
        PictureActor.__init__(self,(self.startX,self.startY),LEVEL_PATH+"lakitu2.png",speech=self.speechBubble)
        self.shape.tag="right"
        
        #holds the spinys
        self.spiny=[]
        self.spinyIndex=0
        
        #y values when spinys will be tossed
        self.tossPoints=[]
    
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
    
    def finalEscape(self):
        oldY=self.getY()
        result=self.moveToward()
        newY=self.getY()
        level=getLevelObject()
        level.endPlatform.moveTo(level.endPlatform.getX(),level.endPlatform.getY()-abs(oldY-newY))
        
        return result
        
    def initialApproach(self):
        result=self.moveToward()
        if not result:
            self.speak("I've been watching you for a bit now.")
        return result
    
    def throwingSpinys(self):
        result=self.moveToward()
        level=getLevelObject()
        if self.spinyIndex<len(self.spiny) and self.getY()<self.tossPoints[self.spinyIndex]:
            self.spiny[self.spinyIndex].show()
            level.addStepEvent(self.spiny[self.spinyIndex].thrown)
            self.spinyIndex+=1
            
        return result                                                    
                                                                                                
class cloudCity2Window(NewLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        rEndX=350    
        rEndY=50
        
        self.doneTalking=False
        
        self.phaser=None
        self.breakWalls=[]

        self.initialApproach=True
        
        #here I need to be able to access the walls in the spikey collision
        self.wall=[]

        NewLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

                                        
    def setStatusText(self):
        pass
    
    #lakitu is done talking, time to catch him
    def catchEndGoal(self):
        self.lakitu.speed=7
        self.addStepEvent(self.lakitu.throwingSpinys) 
        self.doneTalking=True
        self.phaser.show()
        wait(1)
        self.lakitu.silence()
        
    #robot can only destroy the wall once lakitu is done talking        
    def destroyWall(self,lock,items): 
        if self.doneTalking and items[0]==self.phaser:
            lock.move(200, 0)
            lock.userData["wall"].destroyWall()
        
    def createObstacles(self):
        
        self.wall.append(Polygon((0,0),(300,0),
        (300,150),(100,150),(100,200),(300,200),
        (300,250),(100,250),(100,300),(300,300),
        (300,350),(100,350),(100,400),(300,400),
        (300,450),(100,450),(100,500),(300,500),
        (300,550),(100,550),(100,600),(300,600),
        (300,700),(0,700)))
        self.wall.append(Polygon((700,0),(400,0),
        (400,100),(600,100),(600,150),(400,150),
        (400,200),(600,200),(600,250),(400,250),
        (400,300),(600,300),(600,350),(400,350),
        (400,400),(600,400),(600,450),(400,450),
        (400,500),(600,500),(600,550),(400,550),
        (400,700),(700,700)))
        
        for w in self.wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        PHASER_DESC="Phaser welcome. Set to stun."
        
        self.phaser=self.addActor(PictureActor((350, 600), Picture(LEVEL_PATH+"phaser.png"), description=PHASER_DESC, pickup="self"))
        self.phaser.hide()
        debug=False
        
        spacing=50
        h=5
        w=100
        for i in range(10):
            self.breakWalls.append(BreakableWall(300, 100+i*spacing, 100, self.sim.window,horiz=True))
            pad=self.addActor(EventPad((350,100+i*spacing+DEFAULT_RADIUS),25,self.destroyWall,vis=True,deb=True))
            pad.userData["wall"] = self.breakWalls[-1]
         
                
    def createHazards(self):
        speechBubble = LRC_SpeechBubble()
        self.lakitu=self.addActor(Lakitu(250,600,250,-200,"right"))    
        self.lakitu.speechBubble.addResponse("Ah so you made it out the last level.")
        self.lakitu.speechBubble.addResponse("Well let's see if you can get past this \nobstacles and dodge spiney!",self.catchEndGoal)
        
        startY=575
        stepY=-50
        
        endHeightOff=5
        
        
        for i in range(10):
            if i%2==0:
                spiny=self.addActor(Spiny(175,startY+i*stepY,150,startY+i*stepY+endHeightOff,True,True))
                spiny.speed=5
                spiny.walkDirection=1
                spiny.flipCostumeHoriz(1)
                spiny.flipCostumeHoriz(2)
            else:
                spiny=self.addActor(Spiny(175,startY+i*stepY-25,550,startY+i*stepY+endHeightOff,True,True))
                spiny.speed=20                               
                spiny.walkDirection=-1
                
            self.lakitu.spiny.append(spiny)    
            self.lakitu.spiny[-1].hide()
            self.lakitu.tossPoints.append(startY+i*stepY)
            
            
        self.lakitu.spinyIndex=0
        
                   
def startLevel():
    cloudCity2Window("Dungeon1",700,700,350,675,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 