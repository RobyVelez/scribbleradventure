import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class EventPad(ShapeActor):

    """
    Pad to exclusively launch interactable events.    """
    
    def __init__(self,(x,y),radius,callback,vis=False,deb=False):
        self.pad=Circle((x,y),radius)        
        ShapeActor.__init__(self,(x,y),self.pad,bodyType="static", visible=vis, debug=deb,interact=callback)
        
class BreakableWall():
    """
    Breakable Wall composed of 5 bricks that scatter when hit with something. Pass the top left corner and the
    gapWidth that the wall must cover. 
    """
    
    def __init__(self, x,y,gap,window,horiz=False):   
        self.numBricks=10
        self.brickThickness=10
        self.brickHeight=gap/self.numBricks
        self.bricks=[]
        
        for i in range(self.numBricks):
            if horiz:
                self.bricks.append(Rectangle((x+i*self.brickHeight,y),(x+(i+1)*self.brickHeight,y+self.brickThickness),color=Color("firebrick")))
            else: #vertical
                self.bricks.append(Rectangle((x,y+i*self.brickHeight),(x+self.brickThickness,y+(i+1)*self.brickHeight),color=Color("firebrick")))
            
            self.bricks[i].outline=Color("black")
            self.bricks[i].draw(window)
            self.bricks[i].bodyType="static"
    def destroyWall(self):
        if self.numBricks>2:
            for i in range(1,self.numBricks-1):
                self.bricks[i].fill.alpha=50    
                self.bricks[i].body.IsSensor=True
                    
class Lakitu(PictureActor):
    """
    Class to make it easier to make Lakitu. 
    -Lakitu can make an initial approach based on some event.
    """
    def __init__(self, sX,sY,eX,eY,direction):
        
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        self.atEnd=False
        
        self.stopDist=25
        self.speed=25
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        self.speechBubble=LRC_SpeechBubble(talkRadius=450)
        PictureActor.__init__(self,(self.startX,self.startY),LEVEL_PATH+"lakitu2.png",speech=self.speechBubble)
        self.shape.tag="right"
    
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:q
                self.move(delta[0],delta[1])
        return not self.atEnd
    
    def finalEscape(self):
        oldY=self.getY()
        result=self.moveToward()
        newY=self.getY()
        level=getLevelObject()
        level.endPlatform.moveTo(level.endPlatform.getX(),level.endPlatform.getY()-abs(oldY-newY))
        
        return result
        
    def initialApproach(self):
        result=self.moveToward()
        if not result:
            self.speak("I've been watching you for a bit now.")
        return result

class cloudCity1Window(NewLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):

        self.phaser=None
        self.eventPad=None
        self.breakableWall=None
        self.doneTalking=False
        NewLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)

    def setStatusText(self):
        
        self.statusText.append("Oh wow. This looks like it's going to be easy.")
        self.statusPoints.append((650,650))
        self.statusText.append("Who is that?! And where's he going. I have to catch him.")
        self.statusPoints.append((350,650))        
        self.statusText.append("Oh no! I'm trapped...")
        self.statusPoints.append((350,50))
        
        self.statusDebug=True
    
    #robot can only destroy the wall once lakitu is done talking        
    def destroyWall(self,lock,items): 
        if self.doneTalking and items[0]==self.phaser:
            self.breakableWall.destroyWall()
    
    #add lakitu's approach routine to the level thread queue
    def lakituApproach(self):
        self.addStepEvent(self.lakitu.initialApproach)    
            
    #lakitu is done talking, time to catch him
    def catchEndGoal(self):
        self.addStepEvent(self.lakitu.finalEscape) 
        self.doneTalking=True
        self.lakitu.endX=self.lakitu.getX()
        self.lakitu.endY=-200
        self.lakitu.speed=7
        self.lakitu.atEnd=False
    
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(300,0),(300,700),(0,700)))
        wall.append(Polygon((400,0),(700,0),(700,600),(400,600)))        
        for w in wall:
            
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True

        
        self.phaser = self.addActor(PictureActor((550, 650), LEVEL_PATH+"phaser.png", pickup=True, 
            description="Set to stun.",visible=True))                                 
        self.breakableWall=BreakableWall(500,600,100,self.sim.window)        
        self.eventPad=self.addActor(EventPad((550,650),25,self.destroyWall,vis=False,deb=False))

    def createHazards(self):
    
        #-100 -50        
        #self.lakitu=Npc(self, -100, -50, Picture(LEVEL_PATH+"lakitu2.png"), True, Circle((-100,-50),100))
        self.lakitu=self.addActor(Lakitu(0,0,275,600,"right"))
        
        self.lakitu.addResponse("You've come very far.")
        self.lakitu.addResponse("If you want to make it out of here you \nwill have to deal with me.")
        self.lakitu.addResponse("Catch me if you can. Or stay here for eternity.",self.catchEndGoal)
        
                
        self.phaser.onPickup=self.lakituApproach
        #self.addActor(PictureActor((0,0),LEVEL_PATH+"lakitu2.png"))
        #self.lakitu.shape.tag="right"
      
def startLevel():
    cloudCity1Window("Dungeon1",700,700,650,650,-180,350,650,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 