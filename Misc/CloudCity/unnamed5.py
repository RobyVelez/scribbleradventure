import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"


class Lakitu(PictureActor):
    """
    Class to make it easier to make Lakitu. 
    -Lakitu can make an initial approach based on some event.
    """
    def __init__(self, sX,sY,eX,eY,direction):
        
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        self.atEnd=False
        
        self.stopDist=25
        self.speed=25
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        self.speechBubble=LRC_SpeechBubble(talkRadius=450)
        PictureActor.__init__(self,(self.startX,self.startY),LEVEL_PATH+"lakitu2.png",speech=self.speechBubble)
        self.shape.tag="right"
        
        #holds the spinys
        self.spiny=[]
        self.spinyIndex=0
        
        #y values when spinys will be tossed
        self.tossPoints=[]
    
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
    
    def finalEscape(self):
        oldY=self.getY()
        result=self.moveToward()
        newY=self.getY()
        level=getLevelObject()
        level.endPlatform.moveTo(level.endPlatform.getX(),level.endPlatform.getY()-abs(oldY-newY))
        
        return result
        
    def initialApproach(self):
        result=self.moveToward()
        if not result:
            self.speak("I've been watching you for a bit now.")
        return result
    
    def throwingSpinys(self):
        result=self.moveToward()
        level=getLevelObject()
        if self.spinyIndex<len(self.spiny) and self.getY()<self.tossPoints[self.spinyIndex]:
            self.spiny[self.spinyIndex].show()
            level.addStepEvent(self.spiny[self.spinyIndex].thrown)
            self.spinyIndex+=1
            
        return result                                                    

                                                                
class cloudCity5Window(NewLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        #rEndX=350    
        #rEndY=50
        
        self.phaser=None
        self.fragileWalls=[]
        
        self.chomps=[]
        
        self.links1=[]
        self.links2=[]
        
        self.pad=[]
        
        self.anchorsX=[600,100]
        self.anchorsY=[150,250]
        self.anchors=[]
        
        self.chompVelocity=30
        
        self.setup=0
        temp=random()
        #print(temp)
        temp=0
        if temp<0.33:
            self.setup=0
            picFile=LEVEL_PATH+"lineBack4.png"
            rEndX=700
            rEndY=100
            
            rStartX=140
            rStartY=675
        
        elif temp<0.66:
            self.setup=1
            picFile=LEVEL_PATH+"lineBack5.png"
            rEndX=40
            rEndY=0
            
            rStartX=185
            rStartY=675
        
        else:
            self.setup=2
            picFile=LEVEL_PATH+"lineBack6.png" 
            rStartX=375
            rStartY=675      
        
            rEndX=175
            rEndY=0      
        
        self.mines=[]
        
        
        NewLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,picFile,forePic,fogOfWar)
        self.backImage.tag="Tile"
                                        
    def createObstacles(self):

        
        pass
    #   game over man
    def mineCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
        return True            
    
    def createHazards(self):
    
        if self.setup==0:
            points=[(100.0, 538.0),(34.0, 401.0),(95.0, 446.0),(176.0, 463.0),(257.0, 435.0),
                (243.0, 653.0),(668.0, 246.0),(619.0, 60.0),(369.0, 413.0),(428.0, 500.0),(478.0, 424.0),
                (300.0, 562.0),(436.0, 580.0),(581.0, 551.0),(651.0, 503.0),(541.0, 389.0),(591.0, 274.0),
                (516.0, 265.0),(435.0, 290.0),(359.0, 241.0),(270.0, 228.0),(191.0, 284.0),(65.0, 227.0),
                (575.0, 156.0),(143.0, 112.0),(197.0, 157.0),(314.0, 85.0),(371.0, 37.0),(470.0, 130.0),(437.0, 193.0),
                (593.0, 103.0),(290.0, 363.0),(139.0, 340.0),(155.0, 606.0)]
            
            s=0.75
            rad=10
            
                    
            for i in range(len(points)):  
                temp=Picture(LEVEL_PATH+"spinyBall.png")
                temp.scale(s)
                self.mines.append(Hazard(self, points[i][0], points[i][1],temp , vis = True, hitshape=Circle((points[i][0],points[i][1]),rad), collision=self.mineCollide))

  
def startLevel():
    cloudCity5Window("Dungeon1",700,700,215,675,-90,350,0,LEVEL_PATH+"lineBack1.png",None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 