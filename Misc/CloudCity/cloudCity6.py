import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class Lakitu(PictureActor):
    """
    Class to make it easier to make Lakitu. 
    -Lakitu can make an initial approach based on some event.
    """
    def __init__(self, sX,sY,eX,eY,direction):
        
        self.startX=sX
        self.startY=sY
        self.endX=eX
        self.endY=eY
        self.atEnd=False
        
        self.stopDist=25
        self.speed=25
        self.waypoints=[(175,450),(300,350),(175,250),(300,150),(175,50),(300,-50),(175,-150),(300,-250),(175,-350)]
        
        self.speechBubble=LRC_SpeechBubble(talkRadius=700)
        PictureActor.__init__(self,(self.startX,self.startY),LEVEL_PATH+"lakitu2.png",speech=self.speechBubble)
        self.shape.tag="right"
        
        #holds the spinys
        self.spiny=[]
        self.spinyIndex=0
        
        #y values when spinys will be tossed
        self.tossPoints=[]
    
    #this move toward function requires you to set self.end and self.atEnd    
    def moveToward(self):
        if not self.atEnd:
            delta=util.moveDelta(self.endX,self.endY,self.getX(),self.getY(),self.stopDist,self.speed)
            
            if delta[0]==None and delta[1]==None:
                self.atEnd=True
            else:
                self.move(delta[0],delta[1])
        return not self.atEnd
    
    def finalEscape(self):
        oldY=self.getY()
        result=self.moveToward()
        newY=self.getY()
        level=getLevelObject()
        level.endPlatform.moveTo(level.endPlatform.getX(),level.endPlatform.getY()-abs(oldY-newY))
        
        return result
        
    def initialApproach(self):
        result=self.moveToward()
        if not result:
            self.speak("I've been watching you for a bit now.")
        return result
    
    def throwingSpinys(self):
        result=self.moveToward()
        level=getLevelObject()
        if self.spinyIndex<len(self.spiny) and self.getY()<self.tossPoints[self.spinyIndex]:
            self.spiny[self.spinyIndex].show()
            level.addStepEvent(self.spiny[self.spinyIndex].thrown)
            self.spinyIndex+=1
            
        return result                                                    


class cloudCity6Window(NewLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
        #rEndX=350    
        #rEndY=50
        

        
        self.setup=0
        self.wayPoints=[]
        self.rotation=[]
        temp=random()
        #print(temp)
        temp=0.8
        
        off=20
        if temp<0.25:
            self.setup=0
            rEndX=650
            rEndY=50
            
            rStartX=50
            rStartY=650
            rStartT=0
        
        elif temp<0.5:
            self.setup=1
            rEndX=450
            rEndY=50
            
            rStartX=650
            rStartY=650
            rStartT=-90
        
        elif temp<0.75:
            self.setup=2
            rEndX=450
            rEndY=50
            
            rStartX=450
            rStartY=650    
            rStartT=-90  
        else:
            self.setup=3
            rStartX=50
            rStartY=650  
            rStartT=-90   
        
            rEndX=350
            rEndY=50 
            
            self.wayPoints=[[0+off, 700.0-off],[0+off, 400.0+off],[210.0+off, 400.0+off],
            [200.0+off, 200.0+off],[500.0-off, 200.0+off],[500.0-off, 510.0+off],
            [610.0+off, 500.0+off],[610.0+off, 90.0-off],[300.0+off, 90.0-off]]
            
            self.rotation=[0,90,-180,90,-180,270,-180,90,0,-90]
        #self.setup=1   
        self.walls=[]
        self.spiny=[]
        self.lakitu=None
        
        
        NewLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)
        
                                        
    #lakitu is done talking, time to catch him
    def catchEndGoal(self): 
        wait(3)
        self.addStepEvent(self.lakitu.moveToward)
        
        self.lakitu.silence()
        
    def createObstacles(self):
        if self.setup==0:
            self.walls.append(Polygon((0,0),(200,0),(200,300),(500,300),
            (500,400),(200,400),(200,600),(0,600)))
            self.walls.append(Polygon((700,100),(300,100),(300,200),(600,200),
            (600,500),(300,500),(300,700),(700,700)))    
        
        elif self.setup==1:
            self.walls.append(Rectangle((0,0),(400,200)))
            self.walls.append(Rectangle((400,500),(600,700)))
            self.walls.append(Polygon((500,0),(700,0),(700,400),(300,400),
            (300,600),(100,600),(100,300),(500,300)))
        
        elif self.setup==2:
            self.walls.append(Polygon((0,0),(400,0),(400,100),(100,100),
            (100,200),(0,200)))
            self.walls.append(Polygon((700,0),(600,0),(600,200),(200,200),
            (200,300),(100,300),(100,600),(200,600),(200,400),(500,400),
            (500,700),(700,700)))
            self.walls.append(Rectangle((300,500),(400,700)))
        else:
            self.walls.append(Polygon((0,0),(300,0),(300,100),(600,100),
            (600,500),(500,500),(500,200),(200,200),(200,400),(0,400)))
            
            self.walls.append(Polygon((200,700),(200,600),(100,600),(100,500),(300,500),
            (300,300),(400,300),(400,600),(700,600),(700,700)))
            
        for w in self.walls:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
    
    #   game over man
    def mineCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)            
    
    def startStage(self):
        
        wait(1)
        self.lakitu.silence()
        
    
    def setStartActions(self):
        self.runTimer=True
        self.spiny[0].tag="off"
        self.spiny[0].shapes[0].visible=True
        self.spiny[0].moveTo(self.spiny[0].getX(),self.spiny[0].getY()+10)
        self.spiny[0].body.LinearVelocity=Vector(0,10)
    
    def createHazards(self):
        self.lakitu=self.addActor(Lakitu(250,625,1000,1000,"left"))   
        self.lakitu.avatar.flipHorizontal() 
        self.lakitu.speechBubble.addResponse("Impressive.")
        self.lakitu.addResponse("I can see now how you've managed to get this far.")
        self.lakitu.addResponse("Well let's see if you can outrun my pet spike.",self.startStage)
        self.lakitu.speechBubble.setTextXOffset(-200)
    
        #self.lakitu=Npc(self, 250, 625, Picture(LEVEL_PATH+"lakitu2.png"), True, Circle((0,0),100,color=makeColor(0,0,0,0)))
        #self.lakitu.shape.flipHorizontal()
        #self.lakitu.shape.tag="left"
        #self.lakitu.hitshape.body.OnCollision+=self.dummyCollide
        
                    
        #self.lakitu.addResponse("Alright try to run if you can.")
        
        
        self.spiny.append(Circle((self.lakitu.getX()-self.lakitu.avatar.width/2,self.lakitu.getY()),
            35,color=makeColor(0,0,0,0)))

        
        self.spiny[0].draw(self.sim.window)
        self.spiny[0].body.OnCollision+=self.spinyCollide
        self.spiny[0].tag="on"
        
        Picture(LEVEL_PATH+"spinyBall.png").draw(self.spiny[0])
        Picture(LEVEL_PATH+"spinyDown.png").draw(self.spiny[0])
        Picture(LEVEL_PATH+"spinyUp.png").draw(self.spiny[0])
        for p in self.spiny[-1].shapes:
            p.outline.alpha=0
            p.move(-p.width/2,-p.height/2)
            p.visible=False
            p.scale(1.50)
            p.tag="left"
            #p.fill.alpha=125
          
    def spinyCollide(self,myfixture,otherfixture,contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
            
        elif self.runTimer:
            if (type(otherfixture.Body.UserData)==Polygon or type(otherfixture.Body.UserData)==Rectangle) and myfixture.Body.UserData.tag=="off":
                
                myfixture.Body.LinearVelocity=Vector(0,0)
                myfixture.Body.UserData.tag="walking"
                myfixture.Body.UserData.shapes[0].visible=False
                myfixture.Body.UserData.shapes[1].visible=True
                #myfixture.Body.UserData.move(0,-250)
                
            
            #print(type(otherfixture.Body.UserData))
            '''
            if otherfixture.Body.UserData==self.wall[0].body.UserData or otherfixture.Body.UserData==self.wall[1].body.UserData:
                
                for s in self.spiny:
                    if s.body.UserData==myfixture.Body.UserData and s.tag=="off":
                        if contact.Manifold.LocalNormal.Y==-1:
                            s.shapes[0].visible=False
                            s.shapes[1].visible=True
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)                           
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"
                                
                                
                            s.moveTo(s.getX(),s.getY()-7)
                        else:                      
                            
                            if s.getX()>350:
                                s.body.LinearVelocity=Vector(-self.spinySpeed,0)
                                if s.shapes[1].tag=="right" and s.shapes[2].tag=="right":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="left"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="left"
                            
                            else:
                                s.body.LinearVelocity=Vector(self.spinySpeed,0)
                                if s.shapes[1].tag=="left" and s.shapes[2].tag=="left":
                                    s.shapes[1].flipHorizontal()
                                    s.shapes[1].tag="right"
                                    s.shapes[2].flipHorizontal()
                                    s.shapes[2].tag="right"        
    
            '''    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            
    def moveTowards(self,eX,eY,sX,sY,src,minDist=10,speed=10):
        #print(eX,eY,sX,sY,src,minDist,speed)
        #speed of move
        s=speed
        minDist=minDist
        
        #eX=tgt.getX()+offX
        #eY=tgt.getY()+offY
        
        #sX=src.getX()
        #sY=src.getY()
        D=sqrt(pow(eX-sX,2) + pow(eY-sY,2))
        if D<minDist:
            return 0            
        if eX-sX==0:
            src.move(copysign(s,eX-sX),copysign(s,eY-sY))
        else:
            #slope of line between robot and ghost
            m=(eY-sY)/(eX-sX)
            #distance between tgt and src

            #distance from robot that ghost will be placed at
            d=D-s
            
            #solve the equation
            #d^2=y^2 + x^2 where y and x are the distance to the 
            #point the ghost will be placed at
            
            #ans should produce two values which will be
            #the x (run) value
            ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
            
            if ans[0]==2:
                nX1=eX+ans[1][0]
                nX2=eX+ans[1][1]
                
                nY1=eY+ans[1][0]*m
                nY2=eY+ans[1][1]*m
                
                #there are two possible solutions to the equation
                #we want the one closer to the ghost
                pD1=sqrt(pow(sX-nX1,2)+pow(sY-nY1,2))
                pD2=sqrt(pow(sX-nX2,2)+pow(sY-nY2,2))
                
                if pD1<pD2:
                
                    #src.moveTo(nX1,nY1)
                    src.move(nX1-sX,nY1-sY)
                    #print(nX1,nY1)
                else:
                    #src.moveTo(nX2,nY2)
                    src.move(nX2-sX,nY2-sY)
                    #print(nX2,nY2)

        return 1    
    
    
        
    def levelThread(self):
        if self.gameOver==0:        
            if self.runTimer:
                if self.spiny[0].tag=="walking":
                    for s in self.spiny:
                        if s.shapes[1].visible==True:# and s.shapes[2].visible==False:
                            s.shapes[1].visible=False
                            s.shapes[2].visible=True
                        elif s.shapes[2].visible==True:# and s.shapes[1].visible==False:
                            s.shapes[2].visible=False
                            s.shapes[1].visible=True
                        
                        if len(self.wayPoints)>0:
                            wp=self.wayPoints[0]
                            r=self.rotation[0]
                            result=self.moveTowards(wp[0],wp[1],self.spiny[0].getX(),self.spiny[0].getY(),self.spiny[0],15,10)
                            self.spiny[0].rotateTo(r)
                            
                            if result==0:
                                self.wayPoints.remove(wp)
                                self.rotation.remove(r)



def startLevel():   
    cloudCity6Window("Dungeon1",700,700,50,650,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 