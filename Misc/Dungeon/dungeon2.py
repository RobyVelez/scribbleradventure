import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    import setCalico
    setCalico.setCalicoGlobal(calico)
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class gauntlet1Window(varLevelWindow):
    
    def __init__(self, levelName="gauntlet1",panel1W=700, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):
      
        
        self.yPos=[45,185,325,465]
        temp=random()
        if temp<0.25:
            self.goalLoc=0
        elif temp<0.5:
            self.goalLoc=1
        elif temp<0.75:
            self.goalLoc=2
        else:
            self.goalLoc=3
        rEndX=130    
        rEndY=self.yPos[self.goalLoc]
        
        varLevelWindow.__init__(self,levelName,panel1W, panel1H,rStartX,rStartY,rStartT,rEndX,rEndY,backPic,forePic,fogOfWar)
        
        #self.foreImage.stackOnTop()
        #self.promptScreen.stackOnTop()
        #self.briefScreen.stackOnTop()
        self.briefingUp=True
        self.briefScreen.visible=True
                                        
    #def getBriefImage(self):
    #    return LEVEL_PATH+"gauntlet1BriefScreen.png"
    def getBriefImage(self):
        return LEVEL_PATH+"dungeon1BriefScreen.png"
    def setStatusText(self):
        self.statusText.append("Hmm, when I pick up this lamp and use it, it returns False. So I guess it doesn't work. Look up the conditional page so that I can automatically pickup all the lamps and check for myself if they are good. I'm guessing the correct goal is next to the working lamp")
        self.statusPoints.append((330,605))
        self.statusText.append("Ah!!! A ghost!")
        self.statusPoints.append((330,535))
        
        self.statusDebug=True
     
    def unlitLamp(self):
        return False
    def litLamp(self):
        return True
        
    def createObstacles(self):
        wall=[]
        wall.append(Polygon((0,0),(100,0,),(100,90),(300,90),(300,140),
        (100,140),(100,230),(300,230),(300,280),(100,280),(100,370),
        (300,370),(300,420),(100,420),(100,510),(300,510),(300,560),
        (100,560),(100,650),(300,650),(300,700),(0,700)))
        wall.append(Polygon((370,0),(450,0),(450,700),(370,700)))
        #wall.append(Polygon((400,0),(700,0),(700,700),(400,700)))

        for w in wall:
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
        
        LAMP_DESC="Lamp to light the way."
        #First lamp doesn't work
        Item(self, 330, 610, LAMP_DESC, Picture(LEVEL_PATH+"lampUnlit.png"), vis=True,effect=self.unlitLamp)
        
        for i in range(4):
            if i==self.goalLoc:
                Item(self, 330, self.yPos[i], LAMP_DESC, Picture(LEVEL_PATH+"lampUnlit.png"), vis=True,effect=self.litLamp)
            else:
                Item(self, 330, self.yPos[i], LAMP_DESC, Picture(LEVEL_PATH+"lampUnlit.png"), vis=True,effect=self.unlitLamp)
    

                       
    def createHazards(self):
        self.boo=Circle((750,750),25)
        self.sim.addShape(self.boo)
        self.boo.stackOnTop()
        
        self.booAvatar=Picture(LEVEL_PATH+"booRight.png")
        self.booAvatar.outline.alpha=0
        self.booAvatar.scale(0.11)
        self.booAvatar.tag="right"
        #self.booAvatar.setAlpha(0)
        #self.booAvatar.fill.alpha=0
        self.booAvatar.draw(self.boo,(0,0))
        
                
        self.boo.body.OnCollision+= self.booCollide
   
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)

    
    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        s=15
        #s=5
        
        if self.statusPads[1].tag==None and self.gameOver==0:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"



def startLevel():
    gauntlet1Window("Dungeon1",700,700,330,675,-90,350,0,None,LEVEL_PATH+"dungeon1Fore.png",False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        

if __name__ == "__main__":
    startLevel()


 