import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    import setCalico
    setCalico.setCalicoGlobal(calico)
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
    
class dungeon2Window(levelWindow):
    def __init__(self, levelName="game", panel1W=300, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):                

        self.boo=None
        self.booAvatar=None
        
        self.gate=[]
        self.pad=[]

        
        self.debug=True
        
        
        if random()>0.5:
            endX=50
            endY=250
            self.botGoal=True
        else:
            endX=450
            endY=50
            self.botGoal=False
        #fogOfWar=False    
        levelWindow.__init__(self, levelName,panel1W, panel1H,rStartX,rStartY,rStartT,endX,endY,backPic,forePic,fogOfWar)
        self.briefingUp=True
        self.briefScreen.visible=True
        
    def getBriefImage(self):
        return LEVEL_PATH+"dungeon2BriefScreen.png"
        
    def setStatusText(self):
        pass
        #self.statusText.append("Hmm, do I go left or straight?")
        #self.statusPoints.append((250,250))
        #self.statusText.append("I hope going straight was the right decision.")
        #self.statusPoints.append((250,75))
        
    def createObstacles(self):
        
        wall=Polygon((0,0),(200,0),(200,15),
        (200,100),(200,200),(15,200),(15,300),(200,300),
        (200,585),(300,585),(300,400),        
        (300,300),(300,100),(485,100),(485,15),(200,15),
        (200,0),(500,0),(500,700),(0,700))        
        wall.draw(self.sim.window)
        wall.bodyType="static"
        wall.body.IgnoreGravity=True
        wall.stackOnBottom()
        wall.outline=Color("white")

        
        #leftmost gates, associated with pad1
        self.gate.append(Rectangle((195,95),(200,300)))
        #self.gate.append(Rectangle((300,300),(305,500)))
        
        #rightmost gates, associated with pad2
        #self.gate.append(Rectangle((195,0),(200,200)))
        self.gate.append(Rectangle((300,-100),(305,100)))
        
        
        for g in self.gate:
            g.outline.alpha=0
            
            g.fill=Color("black")
            g.fill.alpha=200
            g.bodyType="static"
            self.sim.addShape(g)
            g.body.IgnoreGravity=True
            g.stackOnBottom()
        
        if self.botGoal:
            self.gate[0].rotateTo(90)
        else:
            self.gate[1].rotateTo(90)
        
        line=[]
        w=50
        h=50
        
        #top left corner of tiles    
        cX=225
        cY=225    
        
        line.append(Line((cX,cY),(cX+w,cY)))
        line.append(Line((cX,cY+h),(cX+w,cY+h)))
        
        line.append(Line((cX,cY),(cX,cY+h)))
        line.append(Line((cX+w,cY),(cX+w,cY+h)))
        
        
        tile1=Picture(LEVEL_PATH+"tile.png")
        tile2=Picture(LEVEL_PATH+"tile.png")
        tile1.tag="Tile"
        tile2.tag="Tile"
        
        if self.botGoal:
            self.sim.window.drawAt(tile1,(cX+tile1.width/2,cY+h/2))
        else:
            self.sim.window.drawAt(tile1,(cX+tile1.width/2,cY+h/2))
            self.sim.window.drawAt(tile2,(cX+w-tile2.width/2,cY+h/2))
        
        
        cX=225
        cY=50    
        
        line.append(Line((cX,cY),(cX+w,cY)))
        line.append(Line((cX,cY+h),(cX+w,cY+h)))
        
        line.append(Line((cX,cY),(cX,cY+h)))
        line.append(Line((cX+w,cY),(cX+w,cY+h)))
        
        
        tile3=Picture(LEVEL_PATH+"tile.png")
        tile3.tag="Tile"
        
        if not self.botGoal:
            self.sim.window.drawAt(tile3,(cX+w-tile2.width/2,cY+h/2))
        for l in line:
            l.draw(self.sim.window)
            l.bodyType="static"
            l.stackOnBottom()
        
    def createHazards(self):
        if self.boo==None:
            self.boo=Circle((250+10*random(),800),25)
            self.sim.addShape(self.boo)
            self.boo.stackOnTop()
            
            self.booAvatar=Picture(LEVEL_PATH+"booRight.png")
            self.booAvatar.outline.alpha=0
            self.booAvatar.scale(0.11)
            self.booAvatar.tag="right"
            #self.booAvatar.setAlpha(0)
            #self.booAvatar.fill.alpha=0
            self.booAvatar.draw(self.boo,(0,0))
            
                    
            self.boo.body.OnCollision+= self.booCollide
        else:
            self.boo.moveTo(250,650)
            for g in self.gate:
                g.rotateTo(0)    
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)

    
    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        s=17
        #s=5
        
        if self.runTimer:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"
            
    #def setStartActions(self):
    #    
    #    self.boo.stackOnTop()
    #    self.panel2.stackOnTop()
    #    self.panel3.stackOnTop()
        
            
def startLevel():

    dungeon2Window("Dungeon2",700,700,250,550,-90,0,0,None,None,True)#woodFloor3.png","diamondStruts.png")
    
if __name__ == "__main__":
    startLevel() 
