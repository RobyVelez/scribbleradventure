import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
    import setCalico
    setCalico.setCalicoGlobal(calico)
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
class dungeon7Window(levelWindow):
    def __init__(self, levelName="game", panel1W=300, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):                

        self.gate=[]
        self.boo=None
        self.booAvatar=None
        
        self.debug=False
        temp=random()
        if temp<0.25:
            self.pattern=0
            endX=50
            endY=500            
        elif temp<0.5:
            self.pattern=1
            endX=50
            endY=200
        elif temp<0.75:
            self.pattern=2
            endX=450
            endY=0
        else:
            self.pattern=3
            endX=450
            endY=300
        
        
        fog=not self.debug
            
        levelWindow.__init__(self, levelName,panel1W, panel1H,rStartX,rStartY,rStartT,endX,endY,backPic,forePic,fogOfWar)
        

    def createObstacles(self):
        wall=[]
        
        wall.append(Polygon((0,0),(400,0),(400,100),(200,100),(200,300),(100,300),(100,200),(0,200)))
        
        wall.append(Polygon((0,500),(100,500),(100,400),(200,400),(200,700),(0,700)))
        
        wall.append(Polygon((300,200),(400,200),(400,300),(500,300),(500,700),(300,700)))
        
        
        #wall.append(Polygon((0,0),(400,0),(400,20),(300,20),(300,450),(50,700),(0,700)))
        #wall.append(Polygon((400,0),(700,0),(700,700),(650,700),(400,450),(400,300),(500,300),(500,200),(400,200)))
        #wall.append(Polygon((350,525),(175,700),(525,700)))
        
        for w in wall:  
                 
            w.draw(self.sim.window)
            w.bodyType="static"
            w.body.IgnoreGravity=True
            w.stackOnBottom()
        
        line=[]
        w=50
        h=50
        
        #top left corner of tiles    
        cX=225
        cY=325    
        
        line.append(Line((cX,cY),(cX+w,cY)))
        line.append(Line((cX,cY+h),(cX+w,cY+h)))
        
        line.append(Line((cX,cY),(cX,cY+h)))
        line.append(Line((cX+w,cY),(cX+w,cY+h)))
        
        
        tile1=Picture(LEVEL_PATH+"tile.png")
        tile2=Picture(LEVEL_PATH+"tile.png")
        tile1.tag="Tile"
        tile2.tag="Tile"
        
        if self.pattern==0 or self.pattern==1:
            self.sim.window.drawAt(tile1,(cX+tile1.width/2,cY+h/2))
        else:
            self.sim.window.drawAt(tile1,(cX+tile1.width/2,cY+h/2))
            self.sim.window.drawAt(tile2,(cX+w-tile2.width/2,cY+h/2))
        
        
        cX=225
        cY=125    
        
        line.append(Line((cX,cY),(cX+w,cY)))
        line.append(Line((cX,cY+h),(cX+w,cY+h)))
        
        line.append(Line((cX,cY),(cX,cY+h)))
        line.append(Line((cX+w,cY),(cX+w,cY+h)))
        
        
        tile3=Picture(LEVEL_PATH+"tile.png")
        tile3.tag="Tile"
        
        if self.pattern==2 or self.pattern==3:
            self.sim.window.drawAt(tile3,(cX+w-tile2.width/2,cY+h/2))

    
        for l in line:
            l.draw(self.sim.window)
            l.bodyType="static"
            l.stackOnBottom()
        
        self.gate.append(Rectangle((-150,400),(150,405)))
        self.gate.append(Rectangle((-150,295),(150,300)))
        
        self.gate.append(Rectangle((400,200),(600,205)))
        self.gate.append(Rectangle((400,95),(600,100)))
        
        for g in self.gate:
            g.bodyType="static"
            self.sim.addShape(g)
            g.body.IgnoreGravity=True
            g.stackOnBottom()
        
        if self.pattern==0:
            self.gate[0].rotateTo(90)
        elif self.pattern==1:
            self.gate[1].rotateTo(90)
        elif self.pattern==2:            
            self.gate[3].rotateTo(90) 
        else:
            self.gate[2].rotateTo(90)
            
    def createHazards(self):
        if self.boo==None:
            self.boo=Circle((250+10*random(),800),25)
            self.sim.addShape(self.boo)
            self.boo.stackOnTop()
            
            self.booAvatar=Picture(LEVEL_PATH+"booRight.png")
            self.booAvatar.outline.alpha=0
            self.booAvatar.scale(0.11)
            self.booAvatar.tag="right"
            #self.booAvatar.setAlpha(0)
            #self.booAvatar.fill.alpha=0
            self.booAvatar.draw(self.boo,(0,0))
            
                    
            self.boo.body.OnCollision+= self.booCollide
  
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)
    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        #s=19
        s=12
        
        if self.runTimer:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"
            
    def setStartActions(self):
        
        self.boo.stackOnTop()
        self.panel2.stackOnTop()
        self.panel3.stackOnTop()


       
def startLevel():
    dungeon7Window("Dungeon7",700,700,250,660,-90,0,0,None,None,True)#"woodFloor3.png","diamondStruts.png")
    

if __name__ == "__main__":
    startLevel() 
