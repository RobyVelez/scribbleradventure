import sys
import os

#need to import levelWindow, so first must append the location
#of CommonFunctions
if __name__ == "__main__":
    sys.path.append("../CommonFunctions")
else:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions")
from levelWindow import*      

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class dungeon4Window(levelWindow):
    def __init__(self, levelName="game", panel1W=300, panel1H=700,rStartX=150,rStartY=650,rStartT=-90,rEndX=150,rEndY=0,backPic=None,forePic=None,fogOfWar=False):                

        self.boo=None
        self.booAvatar=None
        
        
        self.debug=False
        
        if random()>0.5:
            endX=350
            endY=50
            self.topGoal=True
        else:
            endX=350
            endY=650
            self.topGoal=False
        
        levelWindow.__init__(self, levelName,panel1W, panel1H,rStartX,rStartY,rStartT,endX,endY,backPic,forePic,fogOfWar)
        

    def createObstacles(self):
        
        wall=Polygon((300,0),(0,0),(0,700),(300,700),(300,400),
        (20,400),(20,300),(300,300))   
        wall.fill=makeColor(0,255,255,255)
        wall.draw(self.sim.window)
        wall.bodyType="static"
        wall.body.IgnoreGravity=True
        wall.stackOnBottom()
        

            
        


    def createHazards(self):
        if self.boo==None:
            if self.topGoal:            
                self.boo=Circle((350,750),25,color=makeColor(255,255,255,255))
            else:
                self.boo=Circle((350,-50),25,color=makeColor(255,255,255,255))
                
            
            self.sim.addShape(self.boo)
            self.boo.zHeight=1
            #self.boo.stackOnTop()
            
            self.booAvatar=Picture(LEVEL_PATH+"booRight.png")
            self.booAvatar.outline.alpha=0
            self.booAvatar.scale(0.11)
            self.booAvatar.tag="right"
            #self.booAvatar.setAlpha(0)
            #self.booAvatar.fill.alpha=0
            self.booAvatar.draw(self.boo,(0,0))
            
                    
            self.boo.body.OnCollision+= self.booCollide
        else:
            self.boo.moveTo(250,650)
            for g in self.gate:
                g.rotateTo(0)    
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)


    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        s=14
        #s=0
        
        if self.runTimer:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"
            
    def setStartActions(self):
        
        self.boo.stackOnTop()
        self.panel2.stackOnTop()
        self.panel3.stackOnTop()
        
                
def startLevel():
    
    dungeon4Window("Dungeon4",700,700,70,350,0,0,0,None,None,True)#"woodFloor3.png","diamondStruts.png")
    #gauntlet6=gauntlet6Window(700,700,"Gauntlet6",200,574,0,150,650)#"woodFloor3.png","diamondStruts.png")

if __name__ == "__main__":
    startLevel() 
    
    
