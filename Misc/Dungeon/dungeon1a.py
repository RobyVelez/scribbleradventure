import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                                
class dungeon1Window(NewLevelWindow):
    def __init__(self,gameData,parent):
        self.goalLoc=randrange(0,4)
        if self.goalLoc==1:
            x=50
            y=50
        elif self.goalLoc==2:
            x=50
            y=200
        elif self.goalLoc==3:
            x=50
            y=350
        else:
            x=50
            y=500
        NewLevelWindow.__init__(self,"Dungeon1",rStartX=650,rStartY=650,rStartT=-180,
        rEndX=x,rEndY=y,gameData=gameData,parent=parent,forePic=LEVEL_PATH+"dungeon1Fore_a.png")
        self.boss=None
        self.fakeRobot=None
        
        self.lamp=[]
        
        self.setup()
        self.briefScreen.visible=False

        self.boss.speak("Who do we have here?")
    

    def getBriefImage(self):
        return LEVEL_PATH+"dungeon1BriefScreen.png"
    
    def setStatusText(self):
        #self.addStatusPad("Spooky...",Circle((650,650),25),True)
        #self.addStatusPad("What is this? I'll try to pick it up lamp=pickup()",Circle((350,650),15),True)
        #p=self.addStatusPad("Ghost!!!",Rectangle((300,550),(400,575)),True)
        
        #p.action=self.testAction2
        
        #self.statusText.append("Spooky...")
        #self.statusPoints.append((650,650))
        
        #self.statusText.append("Hmm, what is this. I can try lamp=pickup() and then use(lamp) to see what it is.")
        #self.statusPoints.append((350,650))
        
        #self.statusText.append("Ah!!! A ghost!")
        #self.statusPoints.append((330,535))
        
        self.statusDebug=True
     
    def pickUpFirstLamp(self,item):
        self.printStatusText("Hmm, a lamp. I should try to use the lamp, use(lamp)")        
    def useFirstLamp(self,item):
        self.printStatusText("False. bad lamp. I need 'if' statements to check the lamp and then make movement decisions. The 'if' Help Page should be informative.")
        return False
        
        
    def unlitLamp(self):
        return False
    def litLamp(self):
        return True
    def testAction(self,myfixture, otherfixture, contact):
        print("TEST!") 
    def testAction2(self):
        print("TEST2!")        
    def createObstacles(self):
        self.addObstacle(Rectangle((0,100),(300,150)))
        self.addObstacle(Rectangle((0,250),(300,300)))
        self.addObstacle(Rectangle((0,400),(300,450)))
        self.addObstacle(Rectangle((0,550),(300,600)))
        self.addObstacle(Polygon((400,0),(400,600),(700,600),
            (700,545),(461,545),(461,0)))
        #self.addActor(EventPad((350,575),100,10,action=self.testAction,debug=True))    
        '''
        self.addObstacle(Rectangle((0,0),(300,100)))
        self.addObstacle(Rectangle((0,450),(300,500)))
        self.addObstacle(Rectangle((0,450),(300,500)))
        
        self.addObstacle(Rectangle((0,450),(300,500)))
        self.addObstacle(Rectangle((0,650),(300,700)))
        
        self.addObstacle(Polygon((0,0),(100,0,),(100,90),(300,90),(300,140),
        (100,140),(100,230),(300,230),(300,280),(100,280),(100,370),
        (300,370),(300,420),(100,420),(100,510),(300,510),(300,560),
        (100,560),(100,650),(300,650),(300,700),(0,700)))
        self.addObstacle(Polygon((370,0),(450,0),(450,700),(370,700)))
        #wall.append(Polygon((400,0),(700,0),(700,700),(400,700)))
        '''
        #first lamp gauranteed not to work
        self.lamp.append(Lantern((350,650),False,scale=1,debug=True,onPickup=self.pickUpFirstLamp,use=self.useFirstLamp))

                
        startY=50
        for i in range(4):
            if i==self.goalLoc:
                self.lamp.append(Lantern((350,50+i*150),True,scale=1,debug=True))
            else:
                self.lamp.append(Lantern((350,50+i*150),False,scale=1,debug=True))
            
        for l in self.lamp:
            self.addActor(l)
            
               
    def toggleBossTalk(self):
        self.boss.talkRadius=1000
        self.fakeRobot.talkRadius=0
    def toggleRobotTalk(self):
        self.boss.talkRadius=0
        self.fakeRobot.talkRadius=1000
                          
    def stopGame(self):
        self.setGameOver(1)
        
    def createHazards(self):
        self.boss=Boo((650,100),0,self.deathCollide, scale=0.1,run=False)                
        self.addActor(self.boss)
        
        self.fakeRobot = self.quickActor(550, 650, LEVEL_PATH+"scribblerPortrait.png", speech=True, visible=False, obstructs=False,debug=False)
        #self.fakeRobot.portrait=Picture(LEVEL_PATH+"scribblerPortrait.png")
        
        #turns boss talk off and fakeRobot talk on
        self.fakeRobot.talkRadius=10000
        self.boss.talkRadius=0            
        
        
        #Firt line of dialogue for the fakerobot
        self.fakeRobot.addResponse("Are you a ghost",action=self.toggleBossTalk)        
        self.boss.addResponse("Yes, yes I am a ghost",action=self.toggleRobotTalk)
        
        self.fakeRobot.addResponse("What do you want.",action=self.toggleBossTalk)
        self.boss.addResponse("Oh, nothing much, just you're brain.",action=self.toggleRobotTalk)
        self.fakeRobot.addResponse("Well I'm sorry, but I'm currently using it.",action=self.toggleBossTalk)
        self.boss.addResponse("Maybe we can do somehitng about that.",action=self.toggleRobotTalk)
        self.fakeRobot.addResponse("Catch me if you can then.",action=self.toggleBossTalk)
        #self.boss.addResponse("Ha,ha,ha. Let's see how far you get in the dark.",action=self.stopGame)
       
        #self.boss.talkRadius=10000
        
        
        #self.addStepEvent(self.boss.step)
        #self.boss = self.quickActor(650, 500, LEVEL_PATH+"booLeft.png", speech=True, obstructs=False,scale=0.1)
        #self.boss.addResponse("mean taunt 1")
        #self.boss.addResponse("mean taunt 2")
        '''
        self.witch.addResponse(WITCH_3, self.showItems)
        self.witch.addResponse(WITCH_4)
        
        
        self.boss=self.quickActor(
        
        Boo( (550,650), ), end, costume, collision, scale=1, upSpeed=10,downSpeed=20, run=True, debug=False, startLoc=None, bounce=True):# scale, collision):lowerBound, upperBound, leftBound, rightBound, collision, costume=0):

        self.boo=Circle((750,750),25)
        self.sim.addShape(self.boo)
        self.boo.stackOnTop()
        
        self.booAvatar=Picture(LEVEL_PATH+"booRight.png")
        self.booAvatar.outline.alpha=0
        self.booAvatar.scale(0.11)
        self.booAvatar.tag="right"
        #self.booAvatar.setAlpha(0)
        #self.booAvatar.fill.alpha=0
        self.booAvatar.draw(self.boo,(0,0))
        
                
        self.boo.body.OnCollision+= self.booCollide
        '''
   
    '''
    #game over man
    def booCollide(self,myfixture, otherfixture, contact):
        if otherfixture.Body.UserData==self.robot.frame.body.UserData:
            self.setGameOver(-1)

    
    
    def quadFormula(self,a,b,c):
        
        d=sqrt(pow(b,2)-4*a*c)
        
        if d<0:
            #print("No solution")
            return [0,[]]
        elif d==0:
            #print("One solution")
            return [1,[-b/(2*a)]]
        else:
            #print("Two solutions")
            return [2,[(-b+d)/(2*a),(-b-d)/(2*a)]]
            

    
    
    def levelThread(self):
        
        #speed of ghost
        s=15
        #s=5
        
        if self.statusPads[1].tag==None and self.gameOver==0:
        
            rX=self.robot.frame.getX()
            rY=self.robot.frame.getY()
            bX=self.boo.getX()
            bY=self.boo.getY()
            
            if rX-bX==0:
                self.boo.move(copysign(s,rX-bX),copysign(s,rY-bY))
            
            else:
                #slope of line between robot and ghost
                m=(rY-bY)/(rX-bX)
                #distance between robot and ghost
                D=sqrt(pow(rX-bX,2) + pow(rY-bY,2))
                #distance from robot that ghost will be placed at
                d=D-s
                
                #solve the equation
                #d^2=y^2 + x^2 where y and x are the distance to the 
                #point the ghost will be placed at
                
                #ans should produce two values which will be
                #the x (run) value
                ans=self.quadFormula(pow(m,2)+1,0,-pow(d,2))
                
                if ans[0]==2:
                    nX1=rX+ans[1][0]
                    nX2=rX+ans[1][1]
                    
                    nY1=rY+ans[1][0]*m
                    nY2=rY+ans[1][1]*m
                    
                    #there are two possible solutions to the equation
                    #we want the one closer to the ghost
                    pD1=sqrt(pow(bX-nX1,2)+pow(bY-nY1,2))
                    pD2=sqrt(pow(bX-nX2,2)+pow(bY-nY2,2))
                    
                    if pD1<pD2:
                        self.boo.moveTo(nX1,nY1)
                    else:
                        self.boo.moveTo(nX2,nY2)
                        
        if self.boo.getX()<self.robot.frame.getX() and self.booAvatar.tag=="left":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="right"
        elif self.boo.getX()>=self.robot.frame.getX() and self.booAvatar.tag=="right":
            self.booAvatar.flipHorizontal()
            self.booAvatar.tag="left"


        '''
level=None
def startLevel(gameData=None, parent=None):
    global level
    #gauntlet2Window("Gauntlet2",700,700,670,550,-180,550,50,None,None,False)#"gauntlet2_ForeBack.png gauntlet2_Back.png, "gauntlet2_Fore.png"
    level=dungeon1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel() 
    


 