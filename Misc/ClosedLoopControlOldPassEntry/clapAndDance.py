import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class ClapAndDanceWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="Clap and Dance",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a ",self.getI("functions1")," to make the Scribbler robot dance and ",self.getI("beep()")," when you clap or tap it's microphone."])
        text.append(["Use    ",self.getI("getMicrophone()"),"   to read the loudness received by Scribbler's built in microphone. See Sample Code for example."])
        text.append(["When your robot can dance and beep in response to sound, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getMicrophone()"),self.getR("beep()")])                    
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        temp+="def danceAndBeep():"        
        temp+="\n    thres=1200000 #Volume threshold"
        temp+="\n    while True:"
        temp+="\n        vol=getMicrophone()"
        temp+="\n        print('Volume is',vol)"        
        temp+="\n        if vol<thres:"
        temp+="\n              print('It is pretty quiet.')"
        temp+="\n        else:"
        temp+="\n              print('I hear something')"
        temp+="\n              print('I should dance and sing.')"
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getMicrophone()"),self.getR("beep()")])  
        self.addBriefShape(b,"Sample Code")
        
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Have Scribbler dance and beep in response to sounds.")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()","getObstacle()","setIRPower()"])
        self.helpWidgets[5].setButtonsVisible(["getMicrophone()","beep()"])
        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)    
                        
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"dancingRobot.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        #s=self.addDecor(Text((325,375),"Start",color=Color("black"),fontSize=36))
        #e=self.addDecor(Text((650,450),"End",color=Color("black"),fontSize=36))
        
        self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        self.addActor(EventPad((500,600),50,50,collisionAction=self.enterPassword,debug=False))
        
def startLevel(gameData=None,parent=None):
    level=ClapAndDanceWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    