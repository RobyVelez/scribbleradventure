import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class LoopOfDoomWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="Loop Of Doom",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor and ask them for the 'Loop of Doom' challenge (see behind this brief screen) and a ",Bold("real")," Scribbler robot."])
        text.append(["Connect to the real Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected to a ",Bold("real")," Scribbler robot create a ",self.getI("Script")," that includes ",
        self.getI("forward()")," ",self.getI("turnRight()")," and ",self.getI("turnLeft()"),
        " commands to get from the Start position to the End position on the 'Loop of Doom'."])
        text.append(["When you're done, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])
        
        b=simpleBrief(text,[self.getR("Script"),self.getR("forward()"),self.getR("turnLeft()"),self.getR("turnRight()")])                    
        self.addBriefShape(b,"Getting Started")
    
        
        
    def onLevelStart(self):
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Get around the Loop of Doom.")       
        self.launchBriefScreen(0)
        
        
                        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)
            
    
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"LoopOfDoom.png")   
        
        #glorified event pads
        self.createEndPad(0,340,locked=True)
        self.createStartPad(25,550)
        
        #create the robot
        self.createRobot(25,550,-50)  
        
        s=self.addObstacle(Text((25,550),"Start",color=Color("black"),fontSize=36))
        s.removeFromPhysics()
        
        e=self.addObstacle(Text((25,340),"End",color=Color("black"),fontSize=36))
        e.removeFromPhysics()
        
        self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        self.addActor(EventPad((0,340),50,50,collisionAction=self.enterPassword,debug=False))
        
        
def startLevel(gameData=None,parent=None):
    level=LoopOfDoomWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    