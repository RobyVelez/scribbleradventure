import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class LineFollowWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="LineFollow",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor and ask them for one of the line follow challenges (see behind this brief screen) and a ",Bold("real")," Scribbler robot."])
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a ",self.getI("functions1")," to make the Scribbler robot perform line following. The next Brief Screen has sample code to get you started."])
        text.append(["Try your lineFollow() function on all line follow challenges. When you're done, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLine()"),self.getR("if")])                    
        self.addBriefShape(b,"Getting Started")
    
        temp=""
        temp+="def lineFollow():"        
        temp+="\n    while True:"
        temp+="\n        left,right=getLine()"
        temp+="\n        print('Sensor values are.',left,right)"            
        temp+="\n        if left==0 and right==0:"
        temp+="\n              print('See line going forward')"
        temp+="\n        elif left==0 and right==1:"
        temp+="\n              print('Line on the left, going left')"
        temp+="\n        elif left==1 and right==0:"
        temp+="\n              print('Line on the right, going right')"
        temp+="\n        elif left==1 and right==1:"
        temp+="\n              print('Can't see line. Have to find it.')"
        temp+="\n        else:"
        temp+="\n              print('Not sure how I got here.')"
        text=[]
        text.append([SetSpacing(3),temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLine()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
    
        
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()
        self.createBriefings()
        self.setObjectiveText("-Follow the drawn lines.")       
        self.launchBriefScreen(0)
        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)    
                        
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"LineFollow.png")   
        
        #glorified event pads
        self.createEndPad(450,350,locked=True)
        self.createStartPad(450,450)
        
        #create the robot
        self.createRobot(450,450,-90)  
        
        #s=self.addDecor(Text((325,375),"Start",color=Color("black"),fontSize=36))
        #e=self.addDecor(Text((650,450),"End",color=Color("black"),fontSize=36))
        
        self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        self.addActor(EventPad((450,350),50,50,collisionAction=self.enterPassword,debug=False))
        
def startLevel(gameData=None,parent=None):
    level=LineFollowWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    