import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class ShyPartyRobotWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="Shy Party Robot",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor & ask for the Scribbler cardboard box (see behind brief screen) & a ",Bold("real")," Scribbler robot."])         
        text.append(["Connect to the ",Bold("real")," Scribbler robot. Go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a ",self.getI("functions1")," to make the Scribbler robot dance and ",self.getI("beep()")," when you cover it with a box."])
        text.append(["Use    ",self.getI("getLight()"),"   to read the Scribbler's light sensors. See Sample Code for example."])
        text.append(["When your robot can dance and beep in response to darkness, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLight()"),self.getR("beep()")])                    
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        temp+="def danceAndBeepDark():"        
        temp+="\n    thres=1000 #getLight() value in darkness?"
        temp+="\n    while True:"
        temp+="\n        left,center,right=getLight()"
        temp+="\n        print('Light levels are ',left,center,right)"
        temp+="\n        if left<thres and center<thres and right<thres:"
        temp+="\n            print('It's dark. No one around. I should dance.')"
        temp+="\n        else:"
        temp+="\n            print('Still light. Staying still.')"
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLight()"),self.getR("beep()")])  
        self.addBriefShape(b,"Sample Code")
        
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Have Scribbler dance and beep in response to sounds.")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()","getObstacle()","setIRPower()"])
        self.helpWidgets[5].setButtonsVisible(["getMicrophone()","beep()",])
        self.helpWidgets[5].setButtonsVisible(["getLight()"],8)
        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)    
                        
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"scribblerBox.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        #s=self.addDecor(Text((325,375),"Start",color=Color("black"),fontSize=36))
        #e=self.addDecor(Text((650,450),"End",color=Color("black"),fontSize=36))
        
        self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        self.addActor(EventPad((500,600),50,50,collisionAction=self.enterPassword,debug=False))
        
def startLevel(gameData=None,parent=None):
    level=ShyPartyRobotWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    