import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class StopWallObstacleWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="Stop At Wall Obstacle",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor & ask for the cardboard bricks (see behind brief screen) & a ",Bold("real")," Scribbler robot."])         
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a  ",self.getI("functions1")," that uses ",self.getI("getObstacle()")," and ",self.getI("setIRPower()")," to create a wall following robot."])
        text.append(["When your robot can make at least a single loop around the wall, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getObstacle()"),self.getR("setIRPower()")])                    
        self.addBriefShape(b,"Getting Started")
        
        '''
        temp=""
        
        temp+="def stopAtWallObs1():"        
        temp+="\n    #Thresold that determines at what distance to stop"
        temp+="\n    thres=3000"
        temp+="\n    #Allows robot to sense objects about 12 inches ahead"        
        temp+="\n    setIRPower(150)"        
        temp+="\n    while True:"
        temp+="\n        left,center,right=getObstacle()"
        temp+="\n        if center>thres:"
        temp+="\n              print('At threshold,probably at wall.Stopping.)"
        temp+="\n              stop()"
        temp+="\n        else:"
        temp+="\n              print('Not close enough to wall. Still going.')"
        temp+="\n              forward(1,1)"
        
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getDistance()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
        '''
        
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Stop set distance from blocks w/ getDistance().")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()"])
        self.helpWidgets[5].setButtonsVisible(["getObstacle()","setIRPower()"],8)
        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)    
                        
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"stopAtWallObs.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        #s=self.addDecor(Text((325,375),"Start",color=Color("black"),fontSize=36))
        #e=self.addDecor(Text((650,450),"End",color=Color("black"),fontSize=36))
        
        #self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        #self.addActor(EventPad((650,450),50,50,collisionAction=self.enterPassword,debug=False))
        
def startLevel(gameData=None,parent=None):
    level=StopWallObstacleWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    