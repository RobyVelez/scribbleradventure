import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class FollowWall1ObstacleWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
  
        LevelWindow.__init__(self,levelName="Follow Wall 2",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def createBriefings(self):
        text=[]
        text.append(["Similiar to previous Challenge, but now the wall is more complicated."])
        text.append(["First try to follow one side of the wall. Once you are confident try to have the robot do a loop."])
        text.append([Bold("Warning, if you make the wall on a table don't let the robot fall off.")])
        text.append([Bold("Repeating the warning. If you make the wall on a table don't let the robot fall off.")])
        text.append(["When your robot can at least make it down one side, confirm with a mentor and they will do a 'use(password)' to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getObstacle()"),self.getR("setIRPower()")])                    
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        
        temp+="def followWall2():"        
        temp+="\n    while True:"
        temp+="\n        left,center,right=getObstacle()"
        temp+="\n        print('Distances are.',left,center,right)"
        temp+="\n        if right>0:"
        temp+="\n              print('Sense wall on my right.')"
        temp+="\n        elif left>0:"
        temp+="\n              print('Sense wall on my left.')"
        temp+="\n        elif center>0:"
        temp+="\n              print('Sense wall straight ahead.')"
        temp+="\n        else:"
        temp+="\n              print('Lost the wall. ')"
        temp+="\n              print('Trying to find it. ')"
        
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getObstacle()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
        
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Follow wall. Careful about robot falling off table.")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()","getObstacle()","setIRPower()"])
        
    def enterPassword(self):
        self.pauseRobot(True)
        self.addResponse(IMAGE_PATH+"lockClosed.png","When done ask mentor to enter password with 'use(password)'")
        talk()  
        self.pauseRobot(False)    
                        
    def confirmCompletion(self,o,item):
        self.checkPassword(item[0])
        '''
        if item[0].ToLower()==self.levelPassword:
            self.burnEvidence()
            self.setGameOver(1)
            
        else:
            #self.setResponse("Incorrect Password.")
            self.addResponse("foo","Incorrect Password.")
            talk()
        '''
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"followWall2.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        #s=self.addDecor(Text((325,375),"Start",color=Color("black"),fontSize=36))
        #e=self.addDecor(Text((650,450),"End",color=Color("black"),fontSize=36))
        
        self.addActor(EventPad((350,350),700,700,interact=self.confirmCompletion,debug=False))
        self.addActor(EventPad((500,600),50,50,collisionAction=self.enterPassword,debug=False))
        
def startLevel(gameData=None,parent=None):
    level=FollowWall1ObstacleWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    