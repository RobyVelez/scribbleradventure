from gifTemplate import *

win=Window("Gif Window",256,256)
win.setBackground(Color("steelblue"))

def mouseDown(obj,event):
    print(event.x,event.y)
win.onMouseDown(mouseDown)

numFrames=100

first=Text((-10,135),"Roby",color=Color("yellow"),fontSize=30)
first.draw(win)

last=Text((265,135),"Velez",color=Color("yellow"),fontSize=30)
last.draw(win)

l1=Line((120,128),(136,128))
l1.draw(win)
l1.visible=False


for i in range(numFrames):
    
    lineCopy=copy(l1)
    lineCopy.draw(win)    
    lineCopy.rotate(i*10)    
    #lineCopy.move(2*i,0)
    lineCopy.scale(1+i/10)
    lineCopy.visible=False 
   
    
    c1=Circle(lineCopy.getP1(),10)
    c2=Circle(lineCopy.getP2(),10)
    c1.draw(win)
    c2.draw(win)

    first
    first.move(1,0)
    last.move(-1,0)
        
    addFrame()

first.fill=Color("red")
last.fill=Color("red")
    
            
saveGif("groovy.gif")
