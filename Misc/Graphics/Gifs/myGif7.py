from gifTemplate import *

win=Window("Gif Window",256,256)
win.setBackground(Color("steelblue"))

def mouseDown(obj,event):
    print(event.x,event.y)
win.onMouseDown(mouseDown)

first=Text((-10,25),"Roby",color=Color("black"),fontSize=30)
first.draw(win)

last=Text((265,25),"Velez",color=Color("black"),fontSize=30)
last.draw(win)

l1=Line((100,256),(156,256))
#l1.draw(win)

r1=Rectangle((25,50),(75,100))
r1.outline=Color("black")
r1.draw(win)

numFrames=100

for i in range(numFrames):
    first.move(1,0)
    last.move(-1,0)
    
    tempLine=copy(l1)
    tempLine.draw(win)
    tempLine.move(0,-2*i)
    tempLine.rotate(10*i)
    
    c1=Circle(tempLine.getP1(),2,color=Color("black"))
    c2=Circle(tempLine.getP2(),2,color=Color("black"))
    c1.draw(win)
    c2.draw(win)
 
    tempR=copy(r1)
    tempR.draw(win)
    tempR.move(0,2*i)
    tempR.rotate(2*i)
    tempR.fill=Color(pickOne(getColorNames()))
    
    addFrame()


saveGif("name.gif")
