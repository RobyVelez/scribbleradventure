from gifTemplate import *

win=Window("Gif Window",256,256)
def mouseDown(obj,event):
    print(event.x,event.y)
    
win.onMouseDown(mouseDown)

c1=Circle((128,128),10)
c1.draw(win)

r1=Rectangle2((15,200),25,10)
r1.draw(win)

for i in range(15):
    c1.scale(1.1)
    c1.fill = Color(pickOne(getColorNames()))
    
    r1.rotate(-5)
    r1.move(5,0)
    
    if even(i):
        r1.fill=Color("red")
    else:
        r1.fill=Color("blue")
    
    addFrame()

for i in range(15):
    c1.scale(0.9)
    c1.fill = Color(pickOne(getColorNames()))
    
    r1.rotate(5)
    r1.move(-5,0)
    
    if even(i):
        r1.fill=Color("red")
    else:
        r1.fill=Color("blue")
        
    addFrame()

                

saveGif("test1.gif")
