from gifTemplate import *

win=Window("Gif Window",256,256)
win.setBackground(Color("steelblue"))

def mouseDown(obj,event):
    print(event.x,event.y)
win.onMouseDown(mouseDown)

def drawRectangle(x,y,w,h,colorName="red"):
    r=Rectangle((x,y),(x+w,y+h),color=Color(colorName))
    r.draw(win)
    return r
    
r1=drawRectangle(50,50,50,50,"green")    

for i in range(10):
    r1.scale(1-i/100)
    r1.rotate(i)
    addFrame()

saveGif("fractal.gif")
