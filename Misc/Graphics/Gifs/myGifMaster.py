from gifTemplate import *

win=Window("Gif Window",250,250)
win.setBackground(Color("steelblue"))

def mouseDown(obj,event):
    print(event.x,event.y)
win.onMouseDown(mouseDown)

text1=Text((75,25),"Roby")
text1.fill=Color("black")
text1.draw(win)
text1.fontSize=30

text2=Text((175,225),"Velez")
text2.fill=Color("black")
text2.draw(win)
text2.fontSize=30

r1=Rectangle((250,120),(450,130))
r1.draw(win)

saveFrame(win)

dx1=0
dy1=10

dx2=32
dy2=0

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

text1.move(dx1,dy1)
text2.move(dx1,-dy1)
r1.fill=randomColor()
r1.move(-dx2,dy2)
saveFrame(win)

r1.fill=Color("black")
saveFrame(win)

numFrames=85
for c in range(1,numFrames):
    
    l1=Line((-25,215),(25,215))
    l1.draw(win)
    l1.rotate(20*c)
    l1.outline=Color("black")
    l1.move(3*c,0)
    
    l2=Line((225,45),(275,45))
    l2.draw(win)
    l2.rotate(20*c)
    l2.outline=Color("black")
    l2.move(-3*c,0)
    
    #l1.scale(1+c/20)
    #l1.visible=False
    
    #c1=Circle(l1.getP1(),5,color=Color("green"))
    #c1.draw(win)
    
    #c2=Circle(l1.getP2(),5,color=Color("yellow"))
    #c2.draw(win)
    
    saveFrame(win)
'''    
numFrames=80
for c in range(1,numFrames):
    l1=Line((150,50),(170,50))
    l1.draw(win)
    l1.rotate(20*c)
    l1.scale(1+c/20)
    l1.visible=False
    
    c1=Circle(l1.getP1(),5,color=Color("red"))
    c1.draw(win)
    
    c2=Circle(l1.getP2(),5,color=Color("blue"))
    c2.draw(win)
    
    saveFrame(win)
'''


saveFrame(win)
saveFrame(win)

saveGif("final.gif")
