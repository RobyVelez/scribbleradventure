from Graphics import *
from Myro import * #pickOne, randomNumber
from math import *

#win=Window("Gif Window",256,256)
gif=AnimatedGifEncoder()
gif.Start()
gif.SetRepeat(0)

def saveFrame(win):
    global gif
    gif.AddFrame(Picture(win))
    
    #try:
    #    win=getWindow()
    #    gif.AddFrame(Picture(win))
    #except:
    #    print("No open window")

#def saveFrame(win):
#    addFrame(win)
    
def saveGif(filename):
    global gif
    try:
        win=getWindow()
        gif.Finish()
        if filename[-4:]==".gif":
            gif.Output(filename)
        else:
            print("Invalid filename. Filename must end with '.gif'")
    except:
        print("No open window")

def copy(shape):
    if isinstance(shape,Circle):
        circleCopy=Circle((shape.x,shape.y),shape.radius,color=shape.color)
        circleCopy.outline=shape.outline
        circleCopy.scaleFactor=shape.scaleFactor
        return circleCopy
        
    elif isinstance(shape,Rectangle):
        rectCopy=Rectangle(shape.getP1(),shape.getP2())
        rectCopy.outline=rectCopy.outline
        rectCopy.fill=rectCopy.fill
        rectCopy.scaleFactor=shape.scaleFactor
        return rectCopy
   
    elif isinstance(shape,Text):
        textCopy=Text((shape.x,shape.y),shape.text,color=shape.color,fontSize=shape.fontSize)
        textCopy.scaleFactor=shape.scaleFactor
        return textCopy
    elif isinstance(shape,Line):
        lineCopy=Line(shape.getP1(),shape.getP2(),color=shape.outline)
        lineCopy.scaleFactor=shape.scaleFactor
        return lineCopy    
    else:
        print("Copy not implemented for this shape.")
def remove(shape):
    try:
        if getWindow() != None:
            win=getWindow()
            win.canvas.shapes.Remove(shape)
    except:
        print("No current window")        

def Rectangle2(p1,w,h,color=Color("purple")):
    return Rectangle(p1,(p1[0]+w,p1[1]+h),color=color)

def randomColor():
    return Color(pickOne(getColorNames()))

'''    
c=Circle((10,50),10)
c.draw(win)

c.move(5,5)
addFrame()
c.move(5,5)
addFrame()
c.move(5,5)
addFrame()
c.move(5,5)
addFrame()
c.move(5,5)

saveGif("test4.gif")

slide.append(Picture(win))
c.move(5,0)

slide.append(Picture(win))
c.move(5,0)
slide.append(Picture(win))
c.move(5,0)
slide.append(Picture(win))
c.move(5,0)
slide.append(Picture(win))
c.move(0,5)
slide.append(Picture(win))
c.move(0,5)
slide.append(Picture(win))
c.move(0,5)

savePicture(slide,"testGif.gif",True,Color("purple"))
'''