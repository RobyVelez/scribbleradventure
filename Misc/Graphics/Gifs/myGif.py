from gifTemplate import *

win=Window("Gif Window",250,250)
win.setBackground(Color("steelblue"))

def mouseDown(obj,event):
    print(event.x,event.y)
win.onMouseDown(mouseDown)

text1=Text((75,25),"Hello")
text1.fill=Color("black")
text1.draw(win)
text1.fontSize=30

text2=Text((175,225),"World")
text2.fill=Color("black")
text2.draw(win)
text2.fontSize=30

r1=Rectangle((25,120),(225,130))
r1.draw(win)

saveFrame(win)

saveGif("myGif.gif")
