

#Two variables that are numbers
#an int and a float (decimal)
a=42 
b=3.14

#Two variables that are strings 
#Double (") or Single (') quotes
#used to indicate strings
c="42"
d='3.14'

#Normal addition of numbers
print(a + b)
#Concatenation of strings
print(c + d)

#Prints a empty string to 
#make a space
print(" ")

#Cast numbers as strings and
#concatenates them
print(str(a) + str(b))
#Cast strings are number and
#adds them
print(int(c) + float(d))
