from Graphics import *
from Myro import *

win=Window("Space Ship Simulator",800,800)
win.setBackground(Color("black"))



'''
#win.mode="manual"

z=1
dx=0
dy=0
sp=10

def keyPressed(obj,event):
    global dx,dy,z
    
    if event.key == "a":
        dx=dx-sp
    elif event.key=="d":
        dx=dx+sp
    elif event.key=="w":
        dy=dy-sp
    elif event.key=="s":
        dy=dy+sp
    elif event.key=="c":
        z-=sp/1000
    elif event.key=="v":
        z+=sp/1000
        
win.onKeyPress(keyPressed)



spaceObjects=[]

def createSpaceObjects():
    #earth
    earth=Picture("earth.png")
    earth.scale(0.25)
    earth.moveTo(400,400)
    #earth.rotateTo(-90)
    earth.setWidth(0)
    earth.draw(win)

    shadow=Circle((350,425),100,color=Color("black"))
    shadow.fill.alpha=125
    shadow.tag="shadow"
    shadow.setWidth(0)
    shadow.draw(win)
    #shadow.draw(earth)

    #moon
    moon=Picture("moon.png")
    moon.scale(0.5)
    moon.moveTo(150,550)
    moon.tag="moon"
    #moon.rotateTo(-90)
    moon.setWidth(0)
    moon.draw(win)

    spaceObjects.append(moon)
    spaceObjects.append(earth)
    spaceObjects.append(shadow)



def createPanel():
    panel=[]
    panel.append(Rectangle((0,0),(800,150)))
    #panel.append(Polygon((0,100),(200,100),(50,700),(0,700)))
    #panel.append(Polygon((600,100),(800,100),(800,700),(750,700)))
    panel.append(Rectangle((0,600),(800,800)))
    panel.append(Rectangle((0,0),(100,800)))
    panel.append(Rectangle((700,0),(800,800)))



    for p in panel:
        p.draw(win)
        p.fill=Color("gray")
        p.setWidth(0)

    structs=[]
    structs.append(Rectangle((200,300),(600,500),color=makeColor(0,0,0,0)))
    structs.append(Line((100,200),(200,300)))
    structs.append(Line((700,200),(600,300)))
    structs.append(Line((200,500),(100,600)))
    structs.append(Line((600,500),(700,600)))
    
    for s in structs:
        s.setWidth(15)
        s.outline=Color("gray")
        s.draw(win)

createSpaceObjects()

createPanel()

direction=1
counter=0
factor=0.01
while False:
    wait(0.1)
    for s in spaceObjects:
        
        #sizing
        if direction==1:
            s.scale(1+factor)
        else:
            s.scale(1-factor)
        
        #moving for moon and shadow
        if s.tag=="moon":# or s.tag=="shadow":
            if direction==1:
                s.move(-5,5)
            else:
                s.move(5,-5)
                            
    counter+=1
    if counter>100:
        counter=0
        
        direction*=-1
    #print(direction)
while True:
    wait(0.045)
    win.update()
    
    #print(z,dx,dy)
    #f.scale(z)
    #f.move(dx,dy)
    
    for s in spaceObjects:
        s.scale(z)
        s.move(dx,dy)
'''
    
            
'''
f=Frame(Point(400,400))
f.draw(win)
earth.move(-400,-400)
moon.move(-400,-400)
shadow.move(-400,-400)
earth.draw(f)
moon.draw(f)
shadow.draw(f)
win.mode="manual"
win.update()
'''
