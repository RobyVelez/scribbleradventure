from Graphics import *
from Myro import *

win=Window("Space Ship Simulator",800,800)
win.setBackground(Color("black"))

earth=Picture("earth.png")
earth.draw(win)
earth.scale(0.25)
earth.moveTo(400,400)
earth.setWidth(0)

moon=Picture("moon.png")
moon.scale(0.5)
moon.moveTo(150,550)
moon.draw(win)
moon.setWidth(0)

shadow=Circle((350,425),100,color=Color("black"))
shadow.fill.alpha=128
shadow.draw(win)

p1=Rectangle((0,0),(800,200),color=Color("gray"))
p2=Rectangle((0,600),(800,800),color=Color("gray"))
p1.draw(win)
p2.draw(win)

#Set Speed Display
h1=Text((50,625),"Set Speed:",color=Color("black"))
l1=Text((75,650),"x",color=Color("black"))
l2=Text((150,650),"y",color=Color("black"))
l3=Text((225,650),"z",color=Color("black"))

h1.draw(win)
l1.draw(win)
l2.draw(win)
l3.draw(win)

#A and D buttons
bA=TextBox((50,675),(100,725),"a",fill=Color("white"))
bD=TextBox((50,725),(100,775),"d",fill=Color("white"))
bA.draw(win)
bD.draw(win)

#W and S buttons
bW=TextBox((125,675),(175,725),"w",fill=Color("white"))
bS=TextBox((125,725),(175,775),"s",fill=Color("white"))
bW.draw(win)
bS.draw(win)

#F and G buttons
bF=TextBox((200,675),(250,725),"f",fill=Color("white"))
bG=TextBox((200,725),(250,775),"g",fill=Color("white"))
bF.draw(win)
bG.draw(win)

thrusterIndicator=TextBox((375,750),(525,800),"Engage Thrusters",fill=Color("white"))
thrusterIndicator.draw(win)

#Current Speed Display
h2=Text((350,625),"Current Speed:",color=Color("black"))
l4=Text((400,650),"x",color=Color("black"))
l5=Text((475,650),"y",color=Color("black"))
l6=Text((550,650),"z",color=Color("black"))

h2.draw(win)
l4.draw(win)
l5.draw(win)
l6.draw(win)

sX=TextBox((375,675),(425,725),"0",fill=Color("white"))
sX.draw(win)

sY=TextBox((450,675),(500,725),"0",fill=Color("white"))
sY.draw(win)

sZ=TextBox((525,675),(575,725),"0",fill=Color("white"))
sZ.draw(win)

#Will display what you have clicked on
analysisLabel=Text((15,50),"Analysis Target:",color=Color("black"),xJustification="left")
analysisText=TextBox((15,75),(100,100)," ",color=Color("white"))
analysisLabel.draw(win)
analysisText.draw(win)

#Will display info regarding object that was clicked
resultLabel=Text((15,125),"Results:",color=Color("black"),xJustification="left")
resultText=TextBox((15,150),(100,175)," ",color=Color("white"))
resultLabel.draw(win)
resultText.draw(win)

def mouseDown(obj,event):
    #print(event.x,event.y) 

    if moon.hit(event.x,event.y)==True:
        analysisText.setText("Moon")
        resultText.setText("Unhabitable")
    elif earth.hit(event.x,event.y)==True:
        analysisText.setText("Earth")
        resultText.setText("Habitable")
    else:
        analysisText.setText(" ")
        resultText.setText(" ")
        
dx=0
dy=0
dz=0

def displaySpeed():
    sX.setText(str(dx))
    sY.setText(str(dy))
    sZ.setText(str(dz))
    
def fireThruster():
    earth.move(dx,dy)
    shadow.move(dx,dy)
    moon.move(dx,dy)
    
    #convernt dz into
    #a scaling/zooming
    #factor
    s=1+dz*0.01
    earth.scale(s)
    shadow.scale(s)
    moon.scale(s)    
    
def keyPress(obj,event):
    global dx,dy,dz
    if event.key=="a":
        bA.fill=Color("yellow")
        dx=dx-1
    elif event.key=="d":
        bD.fill=Color("yellow")
        dx=dx+1
    elif event.key=="w":
        bW.fill=Color("yellow")
        dy=dy+1
    elif event.key=="s":
        bS.fill=Color("yellow")
        dy=dy-1
    elif event.key=="f":
        bF.fill=Color("yellow")        
        dz=dz+1
    elif event.key=="g":
        bG.fill=Color("yellow")
        dz=dz-1
    elif event.key=="space":
        fireThruster()
        thrusterIndicator.fill=Color("yellow")
    else:
        print("answer to universe.")         
    
    displaySpeed()
    #print("Control variables are:")
    #print(dx,dy,dz)
    
    
def keyRelease(o,e):
    bA.fill=Color("white")
    bD.fill=Color("white")
    bW.fill=Color("white")
    bS.fill=Color("white")
    bF.fill=Color("white")
    bG.fill=Color("white")
    thrusterIndicator.fill=Color("white")

win.onKeyRelease(keyRelease)
win.onKeyPress(keyPress)
win.onMouseDown(mouseDown)







#win.onKeyRelease(keyRelease)


 

'''
bF
bG

labels.append(
buttons.append(TextBox((125,650),(175,700),"w"))
buttons.append(TextBox((125,700),(175,750),"s"))
labels.append(Text((150,625),"y"))
buttons.append(TextBox((200,650),(250,700),"f"))
buttons.append(TextBox((200,700),(250,750),"g"))
labels.append(Text((225,625),"z"))





for b in buttons:
    b.draw(win)
    b.fill=Color("white")
    b.move(0,25)
    
for l in labels:
    l.draw(win)
    l.fill=Color("black")
    l.move(0,25)
'''
'''
earth.tag="Habitable"
moon.tag="Unhabitable"
def mouseDown(o,e):
    if moon.hit(e.x,e.y):
        targetText.setText("Moon")
        resultText.setText(moon.tag)
    elif earth.hit(e.x,e.y):
        targetText.setText("Earth")
        resultText.setText(earth.tag)
    else:
        targetText.setText(" ")
        resultText.setText(" ")
    print(e.x,e.y)
    
z=1
dx=0
dy=0
sp=10

def fireThrusters():
    earth.scale(z)
    moon.scale(z)
    #shadow.scale(z)
    
    earth.move(dx,dy)
    moon.move(dx,dy)
            
'''

'''
def keyPress(o,e):
    global dx,dy,z
    
    if e.key=="a":
        bA.fill=Color("yellow")
        dx+=sp
        sX.setText(str(dx))
    elif e.key=="d":    
        bD.fill=Color("yellow")
        dx-=sp
        sX.setText(str(dx))
    elif e.key=="w":    
        bW.fill=Color("yellow")
        dy+=sp
        sY.setText(str(dy))
    elif e.key=="s":    
        bS.fill=Color("yellow")
        dy-=sp
        sY.setText(str(dy))
    elif e.key=="f":    
        bF.fill=Color("yellow")
        z+=sp/1000
        sZ.setText(str(z))
    elif e.key=="g":    
        bG.fill=Color("yellow")
        z-=sp/1000
        sZ.setText(str(z))
    elif e.key=="space":        
        fireThrusters()
        
    print(e.key)

def keyRelease(o,e):
    bA.fill=Color("white")
    bD.fill=Color("white")
    bW.fill=Color("white")
    bS.fill=Color("white")
    bF.fill=Color("white")
    bG.fill=Color("white")
    
'''

#t=Text(((200,700),(300,800)),"Hello World")
#t.draw(win)






'''
pw1=Rectangle((0,0),(800,150))
pw1=Rectangle((0,0),(800,150))

    #panel.append(Polygon((0,100),(200,100),(50,700),(0,700)))
    #panel.append(Polygon((600,100),(800,100),(800,700),(750,700)))
    panel.append(Rectangle((0,600),(800,800)))
    panel.append(Rectangle((0,0),(100,800)))
    panel.append(Rectangle((700,0),(800,800)))
'''




'''
shadow.fill.alpha=125
shadow.tag="shadow"
shadow.setWidth(0)
    
#shadow.draw(earth)






moon.tag="moon"
#moon.rotateTo(-90)
moon.setWidth(0)


#earth.setWidth(0)
'''


'''
#win.mode="manual"

z=1
dx=0
dy=0
sp=10

def keyPressed(obj,event):
    global dx,dy,z
    
    if event.key == "a":
        dx=dx-sp
    elif event.key=="d":
        dx=dx+sp
    elif event.key=="w":
        dy=dy-sp
    elif event.key=="s":
        dy=dy+sp
    elif event.key=="c":
        z-=sp/1000
    elif event.key=="v":
        z+=sp/1000
        
win.onKeyPress(keyPressed)



spaceObjects=[]

def createSpaceObjects():
    #earth
    earth=Picture("earth.png")
    earth.scale(0.25)
    earth.moveTo(400,400)
    #earth.rotateTo(-90)
    earth.setWidth(0)
    earth.draw(win)

    shadow=Circle((350,425),100,color=Color("black"))
    shadow.fill.alpha=125
    shadow.tag="shadow"
    shadow.setWidth(0)
    shadow.draw(win)
    #shadow.draw(earth)

    #moon
    moon=Picture("moon.png")
    moon.scale(0.5)
    moon.moveTo(150,550)
    moon.tag="moon"
    #moon.rotateTo(-90)
    moon.setWidth(0)
    moon.draw(win)

    spaceObjects.append(moon)
    spaceObjects.append(earth)
    spaceObjects.append(shadow)



def createPanel():
    panel=[]
    panel.append(Rectangle((0,0),(800,150)))
    #panel.append(Polygon((0,100),(200,100),(50,700),(0,700)))
    #panel.append(Polygon((600,100),(800,100),(800,700),(750,700)))
    panel.append(Rectangle((0,600),(800,800)))
    panel.append(Rectangle((0,0),(100,800)))
    panel.append(Rectangle((700,0),(800,800)))



    for p in panel:
        p.draw(win)
        p.fill=Color("gray")
        p.setWidth(0)

    structs=[]
    structs.append(Rectangle((200,300),(600,500),color=makeColor(0,0,0,0)))
    structs.append(Line((100,200),(200,300)))
    structs.append(Line((700,200),(600,300)))
    structs.append(Line((200,500),(100,600)))
    structs.append(Line((600,500),(700,600)))
    
    for s in structs:
        s.setWidth(15)
        s.outline=Color("gray")
        s.draw(win)

createSpaceObjects()

createPanel()

direction=1
counter=0
factor=0.01
while False:
    wait(0.1)
    for s in spaceObjects:
        
        #sizing
        if direction==1:
            s.scale(1+factor)
        else:
            s.scale(1-factor)
        
        #moving for moon and shadow
        if s.tag=="moon":# or s.tag=="shadow":
            if direction==1:
                s.move(-5,5)
            else:
                s.move(5,-5)
                            
    counter+=1
    if counter>100:
        counter=0
        
        direction*=-1
    #print(direction)
while True:
    wait(0.045)
    win.update()
    
    #print(z,dx,dy)
    #f.scale(z)
    #f.move(dx,dy)
    
    for s in spaceObjects:
        s.scale(z)
        s.move(dx,dy)
'''
    
            
'''
f=Frame(Point(400,400))
f.draw(win)
earth.move(-400,-400)
moon.move(-400,-400)
shadow.move(-400,-400)
earth.draw(f)
moon.draw(f)
shadow.draw(f)
win.mode="manual"
win.update()
'''
