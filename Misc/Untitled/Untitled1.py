import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 

class untitled1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        self.pathfinder=None
        self.curiosity=None
        self.trapDoor=[]     
        self.mask=None  
        
        self.roundNum=0
        self.testTime=150 #number of simulation ticks
        self.coolDown=50 #number of ticks before next test
        
        self.maxRounds=3      
        self.ans=[]
        
        #self.endLocations=[0,1,2,3,4]
        self.sample=[]        
        self.endLocations=[0,4]
        self.equalValues=[0,lambda:randint(1,9)]
        self.displayValues=["if sample==0:"," "," "," ","else:"]
        self.testDisplayText=[]
        
        for i in range(self.maxRounds):
            self.ans.append(choice(self.endLocations))
            
        self.startTestEvent=None
        self.endTestEvent=None
        self.timerDisplay=None
        self.testStarted=False
        
        #test chamber decorations
        self.testNum=1
        
        LevelWindow.__init__(self,"Untitled1",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
    def updateTimerDisplay(self):
        s=None
        if self.resetEvent:            
            s=str(self.resetEvent.ticks)            
        elif self.evaluateEvent:
            s=str(self.evaluateEvent.ticks)
        
        if s:    
            if len(s)==1:
                s="0"+s
            #add a decimal to the number             
            d=s[:-1]+"."+s[-1:]
            self.timerDisplay.setText(d)
        
        return True
        
    def teleportStep(self):
        angle=-90
        destination=350,650
        robot = getRobot()
        robot.stop()
        
        #The contacts do not like the robot suddenly teleporting. Clear all of them (they will be recreated).
        del self.interactContactList[:]
        del self.pickupContactList[:]
        #del self.levelWindow.talkContactList[:]
        robot.setPose(destination[0], destination[1], angle)
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
        return False
   
    def createSample(self):
        val=None
        
        self.sample[0].show()
        ansIndex=self.endLocations.index(self.ans[self.roundNum]) 
        eVal=self.equalValues[ansIndex]
        
        #is a function        
        if hasattr(eVal, '__call__'):
            self.sample[0].description=eVal()
        else:
            self.sample[0].description=eVal
        
    def evaluateTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)        
        
        if self.roundNum<self.maxRounds:
            
            #opens trap doors.
            for i in range(len(self.trapDoor)):
                if i!=self.ans[self.roundNum]:                    
                    self.trapDoor[i].move(0,100)  
            
            #sees if Scribby in is test area.
            #if he isn't game over man
            if self.robots[0].frame.y<300 or self.robots[0].frame.y>400:
                self.setGameOver(-1)              
                
            #not dead    
            if self.gameOver==0:
                if self.roundNum>=self.maxRounds:
                    print("Error. roundNum is bigger than maxRounds for some reason.")
                elif self.roundNum==self.maxRounds-1:
                    self.setGameOver(1)
                else:
                    self.roundNum+=1
                    self.resetEvent=TimedEvent(self.coolDown,self.resetTest)
                    self.addStepEvent(self.resetEvent)
            
    
    def startTest(self):
        self.evaluateEvent=TimedEvent(self.testTime,self.evaluateTest)
        self.addStepEvent(self.evaluateEvent)
            
    def resetTest(self):
        self.evaluateEvent=None
        self.resetEvent=None
        self.pauseRobot(False)
        
        #set the display timer
        s=str(self.testTime)
        if len(s)==1:
            s="0"+s
        self.timerDisplay.setText(s[:-1]+"."+s[-1:])
            
        self.createSample()
        
        #teleports the robot back to the start location
        self.addStepEvent(self.teleportStep) 
        
        #resets the trapdoors
        for i in range(len(self.trapDoor)):
            origX=self.trapDoor[i].getX()
            self.trapDoor[i].moveTo(origX,250)
        
        #test starts as soon as robot movoes
        self.addStepEvent(OnMoveEvent(self.startTest))
    
    def revealCuriosity(self):
        self.curiosity.show()
        self.vent.visible=True
        
    def revealPathfinder(self):
        self.vent.visible=False
        self.pathfinder.show()
        self.curiosity.hide()
        
    def endConvo(self):
        self.pauseRobot(False)
        self.resetTest()
        self.addStepEvent(self.updateTimerDisplay) 
        self.launchBriefScreen(0)
        self.setObjectiveText("-Pickup the the sample and drive into correct test area.")
        
    def buildBriefings(self):
        text=[]
        text.append(["Pickup the sample with sample=pickup()."])
        
        text.append(["Check the contents of the sample with if statments."])
        text.append(["If the sample is 0 drive into the left most test area, if it is something else drive into right most test area."])
        text.append(["Here's an example: \nforward(4,1)\nsample=pickup()\nif sample==0:\n     print('foo')\nelse:\n     print('bar')"])
        text.append(["See ",self.getI("if")," page for more information on if statments."])
        
        b=simpleBrief(text,[self.getR("if")])                    
        self.addBriefShape(b,"Pickup sample & use 'if' to see what it is")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        pathfinderPort=IMAGE_PATH+"pathfinderPortrait.png"
        
        self.addResponse(scribbyPort, "Where... where am I?")
        self.addResponse(scribbyPort, "Why, why can't I move.",self.revealCuriosity)        
        self.addResponse(curiosityPort, "Scribby. Scribby. Over here.")
        self.addResponse(scribbyPort, "Curiosity! I found you. I'm here to resc-")
        self.addResponse(curiosityPort, "SSSHHHHH!!! Pathfinder is coming and will hear you.")
        self.addResponse(scribbyPort, "Pathfinder. I keep hearing th-")
        self.addResponse(curiosityPort, "Quiet. Look he's almost here. Just play dumb. Do what he asks. I'll be back and will get us out of here.")
        self.addResponse(curiosityPort, "Rust, there he is. Remember play dumb and follow along.",self.revealPathfinder)
        
        self.addResponse(pathfinderPort, "Hmmm.... I thought I heard someone in here. What's this? Another test subject. ")
        self.addResponse(pathfinderPort, "I thought I was finished with the clone testing... Was I done?...")
        self.addResponse(pathfinderPort, "Oh, well. No harm in doing another test.")
        self.addResponse(pathfinderPort, "Hello there test subject... let's see you would be test subject #1498.")
        self.addResponse(pathfinderPort, "Subject #1498 in your mission you will have to be autonomous and make quick decision on your own.")
        self.addResponse(pathfinderPort, "Infront of you is a rock sample you could find on the surface. pickup() the sample with sample=pickup().")
        self.addResponse(pathfinderPort, "Check the sample with if sample==0: .If sample==0 then drive into the left most test area. If the sample is something else drive into the right most test area.",lambda:self.helpWidgets[3].setButtonsVisible(["if"],8))
        self.addResponse(pathfinderPort, "If you fail to drive into the right test area or time runs out then you will be scrapped.")
        
        self.addResponse(pathfinderPort, "Once I'm done explaining I'll release you're clamps. Once you move the test will begin.",self.endConvo)
        
        
    def onLevelStart(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        self.vent.removeFromPhysics()
        for t in self.testDisplayText:
            t.removeFromPhysics()
        talk()
        
    def createLevel(self):
        
        #glorified event pads
        #self.createEndPad(250,50,locked=True)
        self.createStartPad(350,650)
        
        
        #main area
        self.addObstacle(Rectangle((95,95),(100,700)))
        self.addObstacle(Rectangle((595,95),(600,700)))
        self.addObstacle(Rectangle((95,95),(600,100)))        
        self.addObstacle(Rectangle((95,295),(600,300)))
        
        #pathfinder and his office/viewing room
        self.addObstacle(Rectangle((100,195),(200,200)))
        self.addObstacle(Rectangle((195,100),(200,200)))
        #door to office
        door=self.addObstacle(Rectangle((90,125),(105,175),color=Color("gray")))
        door.setWidth(0)
        
        self.pathfinder=self.quickActor(150,150,IMAGE_PATH+"pathfinderPortrait.png",scale=0.75,visible=False)
        self.curiosity=self.quickActor(625,450,IMAGE_PATH+"curiosityPortrait.png",visible=False)
        self.vent=self.addObstacle(Rectangle((575,425),(605,475),color=Color("gray")))
        self.vent.setWidth(0)
        self.vent.visible=False
        
        
        #timer display
        self.addObstacle(Rectangle((495,100),(500,150)))
        self.addObstacle(Rectangle((495,145),(595,150)))        
        self.timerDisplay=self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right"))
        
        #sign with test number on it
        self.addObstacle(Text((200,110),"Test "+str(self.testNum),color=Color("black"),fontSize=18,xJustification="left"))
        
        #test spaces
        self.addObstacle(Rectangle((195,295),(200,400)))
        self.addObstacle(Rectangle((295,295),(300,400)))
        self.addObstacle(Rectangle((395,295),(400,400)))
        self.addObstacle(Rectangle((495,295),(500,400)))
        
        
        #trapdoors and mask
        for i in range(5):
            self.trapDoor.append(self.quickActor(150+i*100,250,Rectangle((155+i*100,255),(245+i*100,345),color=Color("black")),obstructs=False,collision=self.deathCollide))
        self.mask=self.quickActor(349,248,Rectangle((145,202),(635,295),color=Color("gray")))
        
        #sample object(s)
        self.sample.append(self.quickActor(350,550,IMAGE_PATH+"unknownSample0.png",pickup=True,scale=0.25,description=-1))
        
        #text above the test spaces/boxes
        for i in range(5):
            if i in self.endLocations:
                #self.testDisplayText.append(self.addObstacle(Text((550,125),"0",color=Color("black"),fontSize=24,xJustification="right")))
                self.testDisplayText.append(self.addObstacle(Text((105+i*100,305),self.displayValues[i],color=Color("black"),xJustification="left",fontSize=12)))
                self.testDisplayText[-1].rotation=50
                
        #create the robot
        self.createRobot(350,650,-90)
        
        
def startLevel(gameData=None, parent=None):
    level=untitled1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()
                                         

