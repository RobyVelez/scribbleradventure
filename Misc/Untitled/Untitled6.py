import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
 
    
class untitled1Window(LevelWindow):
    def __init__(self,gameData,parent):        
        
        #simple initEvent that starts the actuall tests after the user enters
        #the testing area/hallway
        self.initEvent=None
        #After starting take some time until you evaluate whether Scribby did good
        self.evaluateEvent=None
        
        self.initTime=20 #time between entering test area and start of first test
        self.initTimeFirst=40
        self.testTime=40 #time between start of test and evaluations
        self.coolDown=50 #time between end of evaluation and start of new test
        
        self.keypad=None
        self.door=None
        self.enteredCode=None
        self.firstTime=True
        #
        
        self.roundNum=0
        
        self.maxRounds=6
        self.codeValues=[0,1,2,3]
        self.ans=[]
        lastChoice=None
        for i in range(self.maxRounds):
            val=choice(self.codeValues)
            #prevents two concurrent answers that are the same
            while val==lastChoice:
                val=choice(self.codeValues)
            lastChoice=val        
            self.ans.append(val)
        
        print(self.ans)
        #first wall is essentially an empty wall that doesn't have a position
        self.walls=[]
        
        #visual elements
        #text that displays the countdowns
        self.timerDisplay=None
        
        #text that tells user what phase of the test they are in
        self.testPhaseText=None
        
        #text that tells user the current code entered
        self.currentCodeText=None
        
        #self round indicators
        self.roundIndicators=[]
        
        LevelWindow.__init__(self,"Untitled6",gameData=gameData,parent=parent,debug=True)        
        self.setup()
        
    def updateTimerDisplay(self):
        s=None
        if self.initEvent:
            s=str(self.initEvent.ticks) 
        elif self.evaluateEvent:
            s=str(self.evaluateEvent.ticks) 
            
        if s:          
            if len(s)==1:
                s="0"+s
            #add a decimal to the number  
            try:           
                d=s[:-1]+"."+s[-1:]
            
                self.timerDisplay.setText(d)
            except:
                print("error")
                print(d,self.timerDisplay)
                    
        else:   
            self.timerDisplay.setText("0:00")
            
        return True

   



    def checkCode(self,items=None):
        print(items)
        if items[0]!=None:        
            self.enteredCode=items[0]
            self.currentCodeText.setText(str(self.enteredCode))  
    
        
    def evaluateTest(self):        
        self.evaluateEvent=None
        
        self.testPhaseText.setText("Evaluating Results.")  
        
        if self.roundNum<self.maxRounds:
            
            #wrong code                  
            if not self.enteredCode==self.ans[self.roundNum]:
                self.setGameOver(-1)               
            
            self.enteredCode=None
            self.currentCodeText.setText(".")
            
                                
            #not dead    
            if self.gameOver==0:
                if self.roundNum>=self.maxRounds:
                    print("Error. roundNum is bigger than maxRounds for some reason.")
                elif self.roundNum==self.maxRounds-1:
                    self.door.move(-100,0)
                    self.keypad.move(-100,0)
                    self.testPhaseText.setText("Unlocked.")  
                    self.roundIndicators[self.roundNum].fill=Color("green")
                    #self.setGameOver(1)
                else:
                    self.roundIndicators[self.roundNum].fill=Color("green")
                    self.roundNum+=1
                    self.initTest()
    
    
    #when the robot first enters the test areas give a small countdown to the beginning
    #of the test
    def initTest(self):
        if self.firstTime:
            self.testPhaseText.setText("Initializing Test.")
            self.pauseRobot(False)        
        else:
            self.testPhaseText.setText(".")
        
        
        #reset all the walls if necessary
        for w in self.walls:
            if w:
                w.runForward=False
                w.runBackward=True
        #move wall for testing
        w=self.walls[self.codeValues.index(self.ans[self.roundNum])]
        if w:
            w.runForward=True
            w.runBackward=False
        
        if self.firstTime:
            self.initEvent=TimedEvent(self.initTimeFirst,self.startTest)
        else:
            self.initEvent=TimedEvent(self.initTime,self.startTest)
        self.addStepEvent(self.initEvent)
        
        if self.firstTime:
            self.firstTime=False
        
    def startTest(self):
        self.initEvent=None 
        self.testPhaseText.setText("Lock "+str(self.roundNum+1)+". \nEnter key code.")
        self.evaluateEvent=TimedEvent(self.testTime,self.evaluateTest)
        self.addStepEvent(self.evaluateEvent)
    
    def endConvo(self):
        self.pauseRobot(False)
        self.launchBriefScreen(0)
        self.setObjectiveText("-Try using the wait() command to time your movements.")
        
    def buildBriefings(self):
        text=[]
        text.append(["A ",self.getI("Script")," will execute commands one after another without a pause."])
        text.append(["Use the ",self.getI("wait()")," command to add a pause into your script."])
        text.append(["For example:\n\n     forward(4,3)\n     turnRight(4,1)\n     wait(0.5)\n     forward(4,2)"])
        b=simpleBrief(text,[self.getR("wait()"),self.getR("Script"),self.getR("commands")])                    
        self.addBriefShape(b,"Use wait() in Script to time movements")
    
    def buildConvo(self):
        sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        
        self.addResponse(sojournerPort, "<static>... Scribb... Scribby can <static> hear me...<static>")
        self.addResponse(scribbyPort, "Yes I can hear you, but you're breaking up.")
        self.addResponse(sojournerPort, "As you get deeper in The <static>...harder for <staic>...communi-....")
        self.addResponse(scribbyPort, "Sojourner? Sojourner! Are you there? Sojourner?...",self.endConvo)
        
   
    def onLevelStart(self):
       self.addStepEvent(self.updateTimerDisplay)       
       #self.resetTest()
       
       pass
       #self.runTest()
        
    def hideText(self,items):
        self.sampleText[0].visible=False
        
    def createLevel(self):
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createStartPad(350,650)
        
        #keypad
        self.keypad=self.addActor(EventPad((350,225),50,75,collisionAction=self.initTest,avatar=LEVEL_PATH+"keypad.png",scale=0.20,use=self.checkCode,debug=False))
        
        
        #create the robot
        self.createRobot(350,650,-90)
        
        self.addObstacle(Rectangle((0,0),(300,300)))
        self.addObstacle(Rectangle((400,0),(700,300)))
        
        self.door=self.addObstacle(Rectangle((300,195),(400,200)))
        c1=self.addObstacle(Line((0,197),(300,197)))
        c2=self.addObstacle(Line((400,197),(700,197)))
        
        c1.setWidth(5)
        c2.setWidth(5)
        
        #timerDisplay
        disp=self.addObstacle(Rectangle((600,225),(675,275),color=Color("white")))
        disp.outline=Color("black")
        disp.setWidth(3)
        
        self.timerDisplay=self.addObstacle(Text((650,250),"0:00",color=Color("black"),fontSize=18,xJustification="right"))
        self.testPhaseText=self.addObstacle(Text((425,221),".",color=Color("black"),fontSize=18,xJustification="left"))
        self.currentCodeText=self.addObstacle(Text((425,250),".",color=Color("black"),fontSize=18,xJustification="right"))
        
        
        #round indicators
        for i in range(self.maxRounds):
            self.roundIndicators.append(self.addObstacle(Circle((475+i*20,250),10,color=Color("red"))))
            self.roundIndicators[-1].setWidth(0)
        
        #going to make the walls at the origin and then just  move them to their start positions
        self.walls.append(testWall((250,300),(350,300),100,5))
        self.walls.append(testWall((275,300),(375,300),50,5))
        self.walls.append(testWall((225,300),(325,300),50,5))        
        
        self.walls.append(None)
        
        for w in self.walls:
            if w:
                self.addActor(w)
                self.addStepEvent(w.step)
        # self.addObstacle(Rectangle((250,300),(300,305))))
        #self.testWalls.append(self.addObstacle(Rectangle((200,300),(250,305))))        
        #self.testWalls.append(self.addObstacle(Rectangle((200,300),(300,305))))
        
        #self.initPad=self.addActor(EventPad((350,250),50,50,collisionAction=self.initTest,debug=True))
        
        
        
              
def startLevel(gameData=None, parent=None):
    level=untitled1Window(gameData,parent)
    return level
if __name__ == "__main__":
    startLevel()       
L=getLevelObject()

'''
def diffAngle(self,target,source):
        print
        a = target - source    
        a = (a + 180) % 360 - 180
        return a
        #print("Diff between target and source is ",a)      
    def checkRotation(self):
        target=self.ans[self.roundNum]
        margin=15
        diff=self.diffAngle(target,L.robots[0].frame.rotation%360)
        print("angle diff is ",diff)
        if fabs(diff)<margin:
            
            return True
        else:
            return False
    
 '''   

