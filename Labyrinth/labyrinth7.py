from labyrinthObjects import *
    
class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 7", gameData, parent)
        self.setup()
            
            
    def onLevelStart(self):
        self.addResponse(self.beaglePort, "Okay, this is the elevator back to Lab-RINTH proper.")
        self.addResponse(self.beaglePort, "I think you can use it if you activate both terminals.")
        self.convEvent(lambda: self.setObjectiveText("-Activate both terminals"))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
                    

    def onLevelEnd(self):
        self.addResponse(self.scribbyPort, "The elevator is working, let's get out of here!")
        self.addResponse(self.beaglePort, "You go, I'll stay here.")
        self.addResponse(self.scribbyPort, "Why?")
        self.addResponse(self.beaglePort, "Listen, I've been living here for a while, and I like it here.")
        self.addResponse(self.beaglePort, "It's quite, lots of scrap to tinker with, nobody is bothering you.")
        self.addResponse(self.beaglePort, "I'll leave the world above to bots like you.")
        self.addResponse(self.beaglePort, "Go save your brother!")
        self.convStart()
        
        
    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()
        # Add background
        #self.setBackground(LEVEL_PATH+"lab7back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0,0), (700,0), (700,100), (20,100), (20,450), (300,450), (300,500), (200,500), (200,700), (0,700)), vis=True)
        self.addObstacle(Polygon((400,200), (600,300), (700,300), (700,700), (500,700), (500,500), (400,500), (400,350), (120,350), (120,200)), vis=True) 

        # Add gates
        self.gate1 = self.addActor(DoubleDoor(20, 200, 100, 50, 1))
        self.gate2 = self.addActor(DoubleDoor(300, 450, 100, 50, 1))
        
        # Add cards
        self.x1 = self.addActor(Keycard(450, 150, 1, 1403608273))
        self.x2 = self.addActor(Keycard(250, 150, 2, 9243836928))

        # Add terminals
        c1 = (self.x1.code-2)/self.x2.code
        c2 = (self.x1.code-self.x2.code)*8
        self.addActor(Terminal(70, 50, 0, 0, self.gate1, "(x1-x2)*8", c2, scale=1.2))
        self.addActor(Terminal(350, 300, 0, 0, self.gate2, "(x1-2)/x2", c1, scale=1.2))
        
        # Add junkbot
        self.quickActor(650, 250, self.beaglePort)
        
        # Create the end trigger
        self.addActor(EventPad((350,600),300,200, self.onLevelEnd, debug=False))
        
        # Create the robot
        self.createEndPad(350,650)
        self.createStartPad(650,150)
        self.createRobot(650,150,180)

        # Create the briefings
        self.createBriefings()
        
        
    def createBriefings(self):
        text=[]
        text.append("Reach the elevator")
        text.append("You can perform subtraction, multiplication, and division in the same way you can do addition.")
        b=simpleBrief(text,[self.getR("use()")])                    
        self.addBriefShape(b,"Activate both terminals.")
        
                                          
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
