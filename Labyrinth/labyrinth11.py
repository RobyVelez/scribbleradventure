import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from makeShapeFunctions import *
from labyrinthObjects import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
SCRIBBY = IMAGE_PATH + "scribbyPortrait.png"
OPPY = LEVEL_PATH+"oppyPortrait.png"
NR_OF_TERMINALS = 6
    
class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 11", gameData, parent)
        Labyrinth.__init__(self)
        self.remainingTerminals = range(NR_OF_TERMINALS)
        self.screenShakeTime = 36
        self.hit = False
        self.won = False
        self.setup()
         
         
    def onVictory(self):
        self.conversation.speechBoxYPos = 500
        self.addResponse(self.skull.getPort(10), "Now, I h@ve one last miss&le for you!", self.fire.show)
        self.addResponse(self.skull.getPort(8), "Here y&u go!")
        self.addResponse(self.skull.getPort(8), Mono("Error: unable to read disk 7 sector 0xFF23BE54FED7"))
        self.addResponse(self.skull.getPort(11), "What!?")
        self.addResponse(self.skull.getPort(12), Mono("Error: unable to read disk 4 sector 0x28A3E70FC41A"))
        self.addResponse(self.oppyPort, "Hey SKULL, while I was your 'guest' here, I couldn't help but notice that you are running on some fairly old disks.")
        self.addResponse(self.skull.getPort(13), Mono("CRITICAL **: Unrecognized encoding."))
        self.addResponse(self.oppyPort, "Do you know what happens to old disks when you shake them while they're in operation?")
        self.addResponse(self.skull.getPort(14), Mono("Segmentation fault: core dumped"))
        self.addResponse(self.oppyPort, "Such as when you have a bunch of missiles exploding right next to them?")
        self.addResponse(self.skull.getPort(15), Mono("Reloading program SKULL...\nError: unable to read disk 1 sector 0x09EFD023A23\nReloading program SKULL...\nError: file not found\n"))
        self.addResponse(self.oppyPort, "They tend to break.")
        self.addResponse(self.skull.getPort(16), Mono("Attempting repair...\nError: glibc detected.\nError: unable to allocate memory at 0x3EF5D8003DA"))
        self.addResponse(self.skull.getPort(17), Mono("Rebooting...\nBoot sector for system disk partition is corrupt (error code: 0x490).\n"))
        self.addResponse(self.skull.getPort(18), Mono("Rebooting...\nBoot sector for system disk partition is corrupt (error code: 0x635).\n"))
        self.addResponse(self.skull.getPort(19), Mono("Rebooting...\nBoot sector for system disk partition is corrupt (error code: 0x175).\n"))
        self.addResponse(self.skull.getPort(0), Mono("Reboot failed. System shutting down. All locks released."), self.gate.open)
        self.addResponse(self.scribbyPort, "Well, I guess we won't be seeing him again.")
        self.addResponse(self.scribbyPort, "Let's get out of here.")
        self.addResponse(None, "")
        self.convEvent(self.winGame)
        self.convStart()
        
                                
    def nextActiveTerminal(self):
        self.conversation.speechBoxYPos = None
        if len(self.remainingTerminals) == 0:
            self.won = True
            self.fire.hide()
            self.fire.reset()
            self.fire.stop()
            self.onVictory()
            return
        index = randint(0, len(self.remainingTerminals) - 1)
        self.activeTerminal = self.remainingTerminals[index]
        self.targetPortal = self.activeTerminal
        del self.remainingTerminals[index]
        #self.skulls[self.activeTerminal].changeCostume(2)
        self.codeStr = str(self.codes[self.activeTerminal])
        self.printToSpeechBox("Okay! The code for x" + str(self.activeTerminal+1) + " is: " + self.codeStr, makePic(OPPY), closeOnMove=False)
        self.fire.reset()
        self.fire.show()
        
        
    def onLevelStart(self):
        self.conversation.speechBoxYPos = 550
        self.addResponse(self.oppyPort, "Scribby! You're here!")
        self.addResponse(self.scribbyPort, "How do I open this gate, where's the terminal?")
        self.addResponse(self.skull.getPort(2), "There is no terminal for this gate, this one is controlled by me, personally.")
        self.addResponse(self.skull.getPort(1), "I'm afraid this is the end of the road for you.")
        self.addResponse(self.skull.getPort(5), "There are no exits, there is nowhere to go next.")
        self.addResponse(self.skull.getPort(4), "And I've prepared a very special surprise for you.", self.fire.show)
        self.addResponse(self.scribbyPort, "Is that a missle?!")
        self.addResponse(self.skull.getPort(2), "It's a target seeking explosive device, but I guess you can call it a missle.")
        self.addResponse(self.skull.getPort(3), ["So why don't you just power down. Then this whole thing will just ", Bold("blow")," over."])
        self.addResponse(self.skull.getPort(4), "Hahahahahaha!")
        self.addResponse(self.oppyPort, "Scribby, drive!")
        self.addResponse(self.scribbyPort, "But where to?")
        self.addResponse(self.oppyPort, "Try to get to those portals!")
        self.addResponse(self.scribbyPort, "Those gates are locked, and I don't have the right keycards!")
        self.addResponse(self.oppyPort, "You don't need keycards to activate terminals, all you need is a code...")
        self.addResponse(self.oppyPort, "...and it just so happens that someone downloaded all terminal codes to my hard drive.")
        self.addResponse(self.skull.getPort(1), "Gah, I should have stripped your speakers while I had the chance.")
        self.addResponse(self.skull.getPort(5), "It doesn't matter though, your red little brother can't type that fast. I'll blow him to pieces regardless.")
        self.addResponse(self.oppyPort, "...SKULL's right, isn't he?")
        self.addResponse(self.scribbyPort, "No he's not. I'll create a function!")
        self.addResponse(self.scribbyPort, "I'll create a function that takes two arguments: a terminal and a code.")
        self.addResponse(self.scribbyPort, ["Oppy, if you tell me the terminal and the code, I can call my function with: ", Mono("go(terminalNumber, code)")])
        self.addResponse(self.scribbyPort, "And if I time it right, I should be able to activate the terminal and lower the forcefield, move through the portal, and avoid the missile.")
        self.convEvent(lambda: self.setObjectiveText("-Avoid the missiles"))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.addResponse(None, "")
        self.convEvent(self.nextActiveTerminal)
        self.convEvent(self.hidePanel1)
        self.convStart()


    def skullSpeak(self):
        if self.hit or self.won:
            return
        if len(self.remainingTerminals) == 5:
            self.printToSpeechBox("Turn around stupid missile! He's behind you!", self.skull.getPortNow(5), closeOnMove=False)
        elif len(self.remainingTerminals) == 4:
            self.printToSpeechBox("You can't keep dodging forever.", self.skull.getPortNow(4), closeOnMove=False) 
        elif len(self.remainingTerminals) == 3:
            self.printToSpeechBox("I'm not sure what you are trying to do, none of the portals lead out, you know.", self.skull.getPortNow(3), closeOnMove=False)
        elif len(self.remainingTerminals) == 3:
            self.printToSpeechBox("You're resiliant, but you'll run out of portals soon enough.", self.skull.getPortNow(2), closeOnMove=False)
        elif len(self.remainingTerminals) == 2:
            self.printToSpeechBox("The worst thing is that I'll have \@lean up this me$s.", self.skull.getPortNow(7), closeOnMove=False)
        elif len(self.remainingTerminals) == 1:
            self.printToSpeechBox("I really need to repr%^gram these missiles.", self.skull.getPortNow(9), closeOnMove=False)
        elif len(self.remainingTerminals) == 0:
            self.printToSpeechBox("And y&o're out, s\@ems I've won!", self.skull.getPortNow(8), closeOnMove=False)


    def checkKey(self, pad, key):
        if len(key) != 1: return 
        #print("Code provided:", key[0], "code required:", pad.userData["code"])
        if pad.userData["index"] != self.activeTerminal:
            self.printToSpeechBox("I don't know the code for this terminal.", makePic(SCRIBBY))
        elif fuzzyCompare(key[0], pad.userData["code"]):
            self.gates[self.activeTerminal].moveTo(-1000,-1000)
        else:
            self.printToSpeechBox("Darn it, that code didn't work!", self.scribbyPort)
        
        
    def onTeleport(self):
        if self.fire.getY() < 550:
            self.addStepEvent(TimedEvent(5, self.skullSpeak))
        
        
    def screenShake(self):
        self.screenShakeTime -= 1
        if self.screenShakeTime <= 0:
            self.worldFrame.moveTo(0,0)
            return False
        force = int(self.screenShakeTime/6)
        self.worldFrame.moveTo(randint(-force, force), randint(-force, force))
        return True
        
        
    def onHit(self, myfixture, otherfixture, contact):
        if self.hit: return True
        robot = getRobot()
        if robot is None: return True
        if otherfixture.Body.UserData==robot.frame.body.UserData:
            self.printToSpeechBox("Scribby!", makePic(OPPY), closeOnMove=False)
            self.hit = True
            self.pauseRobot(True)
            self.wreck.visible = True
            self.explode()
            self.addStepEvent(TimedEvent(40, lambda: self.setGameOver(-1)))
        return True
        
        
    def explode(self):
        self.explosion.moveTo(self.fire.getX(), self.fire.getY())
        self.explosion.changeCostume(0)
        self.explosion.animationIndex=0
        self.explosion.show()
        self.explosion.onEndOfAnimation = self.explosion.hide
        self.addStepEvent(self.explosion.animate)
        self.screenShakeTime = 36
        self.addStepEvent(self.screenShake)
        self.fire.hide()

        
    def trackRobot(self):
        robot=getRobot()
        if robot is None: return True
        if self.hit: return False
        if self.won: return False
        x=robot.frame.getX()
        y=robot.frame.getY()
        portal = self.portals[self.targetPortal]
        if (x > self.fire.getX()) or (self.fire.getX() < portal.getX()):
            self.fire.rotateTo(90)
            self.fire.move(10,0)
        elif self.fire.getY() < portal.getY():
            self.fire.rotateTo(190)
            self.fire.move(0,10)
        else:
            self.explode()
            self.addStepEvent(TimedEvent(40, self.nextActiveTerminal))
            portal.changeCostume(2)
            portal.disabled = True
            return False
        return True
            
    def adjustSpeed(self, missile):
        if missile.currentWaypoint == 1:
            missile.speed=3
            missile.changeCostume(1)
        elif missile.currentWaypoint == 2:
            missile.speed=10
            missile.changeCostume(2)
            
    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()
        # Add background
        self.setBackground(LEVEL_PATH+"lab11back.png")  
        # Create start pad
        rStart = (50,550)
        rAngle = -90
        self.addActor(SpriteActor(rStart, LEVEL_PATH+"portal-end.png"))
    
        # Add walls
        self.addObstacle(Polygon((0,0),(0,100),(100,100),(100,300),(700,300),(700,0)), vis=False)
        #self.addObstacle(Polygon((100,400),(100,600),(700,600),(700,400)), vis=True)
##         tempPoints = []
##         for i in range(NR_OF_TERMINALS):
##             x=95+i*100
##             tempPoints+=[(x,400),(x+10,400),(x+10,530),(x+100,530),(x+100,400),(x+110,400)]
##         del tempPoints[-1]
##         del tempPoints[-1]
##         tempPoints+=[(695, 400), (700, 400), (700, 600), (95, 600)]
##         print(tempPoints)
        p=Polygon((95, 400), (105, 400), (105, 530), (195, 530), (195, 400), (205, 400), 
                  (195, 400), (205, 400), (205, 530), (295, 530), (295, 400), (305, 400), 
                  (295, 400), (305, 400), (305, 530), (395, 530), (395, 400), (405, 400), 
                  (395, 400), (405, 400), (405, 530), (495, 530), (495, 400), (505, 400), (495, 400), 
                  (505, 400), (505, 530), (595, 530), (595, 400), (605, 400), (595, 400), (605, 400), 
                  (605, 530), (695, 530), (695, 400), (700, 400), (700, 600), (95, 600))
        self.addObstacle(p, vis=False)
        
        # Add screen
        self.screen = self.addDecor((365, 100), makePic(LEVEL_PATH+"screen-small.png"))
    
        # Add SKULL
        self.skull = self.addActor(Skull(365, 100, corrupt=True))
        
        # Add terminals
        self.codes = []
        self.pads = []
        self.screens = []
        self.gates = []
        self.text = []
        self.portals = []
        for i in range(NR_OF_TERMINALS):
            code = randint(0, 100)
            x = 150 + 100*i
            pad = self.quickActor(x, 350, makeColRec(50, 100), interact=self.checkKey, debug=False)
            pad.userData["code"] = code
            pad.userData["index"] = i
            self.codes.append(code)
            self.pads.append(pad)
            self.screens.append(self.addDecor((x, 254), makePic(LEVEL_PATH + "black-screen-black.png")))
            self.text.append(self.addDecor(Text((x, 254), "(x" + str(i+1) + ")", color=Color(230,230,230))))
            self.portals.append(self.addActor(Portal((x, 480), rStart, rAngle, 0, additionalAction=self.onTeleport)))
            self.gates.append(self.addActor(SpriteActor((x, 410), 
                LEVEL_PATH + "ForceField.png", makeColRec(90,10),
                obstructs=True, debug=False)))
            
        # Add gate
        self.gate = self.addActor(DoubleDoor(0, 250, 100, 50))
        
        # Add Oppy
        self.oppy = self.quickActor(50, 150, OPPY)
        
        # Create the robot
        self.createRobot(rStart[0],rStart[1],rAngle)
        
        # Initialize wrecked sprite for Scribby
        self.wreck = initScribbyWreck()
        
        # Add explosion
        self.explosion = self.addActor(Explosion())
        
        # Add missle
        self.fire = self.addActor(Missile([(650, 650), (50, 650, self.adjustSpeed), (50, 350, self.adjustSpeed)], run=False, collision=self.onHit, visible=False, rotation=-90, speed=2))
        self.fire.atEnd="custom"
        self.fire.customTrackingEvent = self.trackRobot
        
        # Create the briefings
        self.createBriefings()
        
        
    def createBriefings(self):
        text=[]
        text.append(["Create a function that takes two arguments, a terminal number and a code."])
        text.append(["To create a function that takes two arguments, type as the first line: ", Mono("def  go(terminal, code):")])
        text.append(["The function should drive to the indicated terminal, use the code, and then move through the portal."])
        text.append(["For example, if your function is called with: ", Mono("go(3, 72)"), " Scribby should drive to terminal x3, and call ", Mono("use(73)"), "."])
        text.append(["Use Oppy's instructions as arguments for your function. For example, if Oppy says: 'The code for x2 is: 23' you should type: ", Mono("go(2, 23)")])
        text.append(["Prepare your function! The level will start on ", Mono("talk()")])
        b=simpleBrief(text,[self.getR("functions2"), self.getR("variables"), self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Avoid the missiles")

                                          
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
