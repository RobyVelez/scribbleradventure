from labyrinthObjects import *

class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 9", gameData, parent)
        self.setup()
        
    def onLevelStart(self):
        self.addResponse(self.skull.costumes[2], "Trying to save your brother, huh. How touching.")
        self.addResponse(self.scribbyPort, "Just ignore him...")
        self.addResponse(self.scribbyPort, "I need to get to the other side of the room, but those thwomps are in the way.")
        self.addResponse(self.scribbyPort, "It will be hard to pass both of them with a single script, because their timings are random.")
        self.addResponse(self.scribbyPort, "I may be able to create a function and call it twice, but that means I can not pickup the keycard in the same function.")
        self.addResponse(self.scribbyPort, "Maybe I can pass the keycard to the function with an argument.")
        self.convEvent(lambda: self.setObjectiveText("-Get to the exit."))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
    
        
    def checkKey(self, lock, items):
        if items == [self.code] and lock == self.lock1:
            self.addStepEvent(MoveEvent(self.gate1, 0, -100, 100))
        elif items == [self.code] and lock == self.lock2:
            self.addStepEvent(MoveEvent(self.gate2, 0, -100, 100))
        else:
            self.printStatusText("That code did not work.")
       

    def onPickup(self, item):
        self.printToSpeechBox("However, I can not let you do that. Don't worry though, your sudden disassembly will be quick and painless.", portrait=self.skull.costumes[4])
        self.skull.changeCostume(4)
       
       
    def startThwomps(self):
        self.thwomp1.run = True
        self.thwomp2.run = True
            
            
    def thwompStep(self, thwomp):
        if thwomp.pastEnd:
            thwomp.upSpeed = 0
        elif random() < 0.2:
            thwomp.upSpeed = 10


    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()
        # Add background
        self.setBackground(LEVEL_PATH+"lab9back.png")  
        
        # Add walls
        self.addObstacle(Polygon((0, 0), (0, 300), (100, 300), (100, 400), (200, 400), (200, 200), (300, 200), (300, 500), (400, 500), (400, 300), (500, 300), (500, 600), (700, 600), (700, 0)), vis=False)
        self.addObstacle(Polygon((0, 500), (200, 500), (200, 600), (400, 600), (400, 700), (0, 700)), vis=False)

        # Add gate
        self.gate1 = self.addActor(SpriteActor((310, 550), Rectangle((0,0), (20, 100), bodyType='static', fill=Color("silver")), obstructs=True))
        self.gate2 = self.addActor(SpriteActor((510, 650), Rectangle((0,0), (20, 100), bodyType='static', fill=Color("silver")), obstructs=True))
       
        # Add code
        self.code = 18364038422
          
        # Add items
        self.key = self.addActor(SpriteActor((150, 450), LEVEL_PATH+"keycard_1.png", pickup=self.code, onPickup=self.onPickup))
        
        # Add terminals
        self.screen1 = self.quickActor(170, 550, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen2 = self.quickActor(370, 650, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen1.rotate(90)
        self.screen2.rotate(90)
        self.term1text = self.addDecor(Text((170, 550), "(x1)", color=Color(200,200,200)))
        self.term2text = self.addDecor(Text((370, 650), "(x1)", color=Color(200,200,200)))
        self.term1text.rotate(90)
        self.term2text.rotate(90)
        self.lock1 = self.addActor(SpriteActor((250, 550), makeColCircle(45), interact=self.checkKey, debug=False))
        self.lock2 = self.addActor(SpriteActor((450, 650), makeColCircle(45), interact=self.checkKey, debug=False))

        # Add screen
        self.screen = makePic(LEVEL_PATH+"screen-small.png")
        self.screen.moveTo(350, 100)
        self.addDecor(self.screen)
        
        # Add SKULL
        self.frames = [LEVEL_PATH+"skull_blank.png"]
        self.frames += [LEVEL_PATH+"skull_neutral.png"]
        self.frames += [LEVEL_PATH+"skull_talking.png"]
        self.frames += [LEVEL_PATH+"skull_smug.png"]
        self.frames += [LEVEL_PATH+"skull_surprised.png"]
        self.frames += [LEVEL_PATH+"skull_wink.png"]
        self.frames += [LEVEL_PATH+"skull_afraid.png"]
        self.skull = self.quickActor(350, 100, self.frames, scale=0.5)
        self.skull.changeCostume(2)

        # Add thwomps
        self.thwomp1 = Thwomp((250,250),(250,550),1,self.deathCollide,run=False, onStep=self.thwompStep)
        self.thwomp2 = Thwomp((450,350),(450,650),1,self.deathCollide,run=False, onStep=self.thwompStep)
        self.addActor(self.thwomp1)
        self.addActor(self.thwomp2)
        self.addStepEvent(self.thwomp1.step)
        self.addStepEvent(self.thwomp2.step)
        self.addStartEvent(self.startThwomps)
        
        # Add robot
        self.createEndPad(650,650)
        self.createStartPad(50,450)
        self.createRobot(50,450,0)
   
        # Create the briefings
        self.createBriefings()
        
        
    def createBriefings(self):
        text=[]
        text.append([self.getI("pickup()"), " the key."])
        text.append(["Create a function that takes a keycard as an argument, ", self.getI("use()"), " the key on the lock and escapes the thwomp."])
        text.append("Reuse your function to get to the exit.")
        b=simpleBrief(text,[self.getR("functions2"), self.getR("variables"), self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Reach the exit.")
   
        
##     def buildBriefing(self):
##         briefing = Briefing(self)
##         briefing.enum(Inline("Pickup"), "the key.")
##         briefing.enum("Create a", Inline("Function", "functions2") ,"that takes a key as an argument,")
##         briefing.line(Inline("Uses", "Use"), "the key on the lock and escapes the thwomp.")
##         briefing.enum("Reuse your function to get to the exit.")
##         briefing.relatedPages("Pickup", "Use", "variables", "functions2")
##         briefing.write()
                                             
def startLevel(gameData=None,parent=None):            
    return CurrentLevel(gameData, parent)
    
if __name__ == "__main__":
    startLevel()
