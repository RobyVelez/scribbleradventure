import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from makeShapeFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
            
class labyrinth1Window(LevelWindow):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 2", gameData, parent)
        self.setup()
            
                    
    def onLevelStart(self):
        pass
        r=getRobot()
        self.pauseRobot(True)
        self.addStepEvent(MoveToEvent(r.frame, 350, 350, 10, talk))
##         forward(2,6)
##         talk()
        
    def startConvo(self):
        self.skull.changeCostume(2)
        r=getRobot()
        self.addStepEvent(RotateToEvent(r.frame, 90, 20))
##         turnLeft(4,1)
        
    def endConvo(self):
        self.trapDoorOutline.visible = True
        self.trapDoor.visible = True
        self.addStepEvent(self.openTrapDoor)
        self.skull.changeCostume(3)
        
    def openTrapDoor(self):
        r=getRobot()
        if r: r.frame.scale(0.9)
        self.distanceOpen += 5
        self.obscure.fill = Color(0,0,0, min(self.distanceOpen*2, 255))
        self.trapDoor.set_points(Point(350-self.distanceOpen, 300), Point(350+self.distanceOpen, 300), Point(350+self.distanceOpen, 500), Point(350-self.distanceOpen, 500))
        if self.distanceOpen > 150:
            self.setGameOver(1)
            return False
        return True
        
    def switchToFeed(self):
        self.skull.hide()
        self.feed.visible = True
        
    def switchFromFeed(self):
        self.skull.show()
        self.feed.visible = False
        
    def createLevel(self):
        # Set buttons
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.helpWidgets[2].setButtonsVisible(["pickup()", "variables"])
        
        # Add background
        self.setBackground(LEVEL_PATH+"lab2back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0,0), (0,250), (200,250), (250,200), 
                                 (450,200), (500,250), (600,250), (600,350), (700,350), (700,0)),vis=False)
        self.addObstacle(Polygon((0,450), (100, 450), (100, 600), (250, 600), (250, 700), (0, 700)),vis=False)
        self.addObstacle(Polygon((450,600), (600,600), (600,550), (700,550), (700,700), (450,700)),vis=False) 

        # Add trap door
        self.distanceOpen = 5
        self.trapDoorOutline = Rectangle((200, 300), (500, 500))
        self.trapDoorOutline.fill = Color(0,0,0,0)
        self.trapDoorOutline.visible = False
        self.trapDoorOutline.draw(self.worldFrame)
        self.trapDoor = Rectangle((350-self.distanceOpen, 300), (350+self.distanceOpen, 500))
        self.trapDoor.fill = Color(0,0,0, 255)
        self.trapDoor.visible = False
        self.trapDoor.draw(self.worldFrame)

        # Add screen
        self.screen = makePic(LEVEL_PATH+"screen-small.png")
        self.screen.moveTo(350, 100)
        self.screen.draw(self.worldFrame)
        
        # Add video feed
        self.feed = makePic(LEVEL_PATH+"oppyVideoFeed.png")
        self.feed.moveTo(350, 100)
        self.feed.visible = False
        self.feed.draw(self.worldFrame)
        
        # Add SKULL
        frames = [LEVEL_PATH+"skull_blank.png"]
        frames += [LEVEL_PATH+"skull_neutral.png"]
        frames += [LEVEL_PATH+"skull_talking.png"]
        frames += [LEVEL_PATH+"skull_smug.png"]
        frames += [LEVEL_PATH+"skull_surprised.png"]
        frames += [LEVEL_PATH+"skull_wink.png"]
        frames += [LEVEL_PATH+"skull_afraid.png"]
        self.skull = self.quickActor(350, 100, frames, scale=0.5)

        # Add gate
        self.gate = self.addActor(DoubleDoor(625, 350, 50, 200))
        self.gate = self.addActor(DoubleDoor(250, 625, 200, 50))
        
        # Create the robot
        self.createStartPad(50,350)
        self.createRobot(50,350,0)
        
        # Add the obscuring filter
        self.obscure = Rectangle((0,0), (700, 700))
        self.obscure.fill = Color(0,0,0, 0)
        self.obscure.border = 0
        self.obscure.draw(self.worldFrame)
        
        # Create Conversation
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        oppyPort = IMAGE_PATH+"oppyPortrait.png"
        self.addResponse(frames[2], "Well hello there! To what do I owe this pleasure?", self.startConvo)     
        self.addResponse(scribbyPort, "Ahh! Who are you?")
        self.addResponse(frames[3], "I am SKULL, the euh... caretaker of Lab RINTH. And you are...")
        self.addResponse(scribbyPort, "I'm Scribby and I am looking for my brother. His name is Oppy. Blue robot, similar model. Have you seen him?")
        self.addResponse(frames[2], "Ah, yes, he is here.")
        self.addResponse(scribbyPort, "Really? Show me where he his!")
        self.addResponse(frames[5], "As you wish...", lambda: self.skull.changeCostume(5))
        self.addResponse(oppyPort, "This is awesome! I can control the entire facility from here!", self.switchToFeed)
        self.addResponse(scribbyPort, "Oppy! Oppy!")
        self.addResponse(frames[2], "I am sorry, but he can not hear you.", self.switchFromFeed)
        self.addResponse(scribbyPort, "Then let me speak to him!")
        self.addResponse(frames[3], "I am afraid I can not let you do that. We have a 'project' that needs to be finished first. I can not risk him getting distracted by irrelevant family matters.", lambda: self.skull.changeCostume(3))
        self.addResponse(scribbyPort, "I won't leave until I talked to him!")
        self.addResponse(frames[2], "As you wish...", lambda: self.skull.changeCostume(2))
        self.addResponse(frames[4], "Then you won't leave at all.", lambda: self.skull.changeCostume(4))
        self.addResponse(frames[3], "Off to the scrap heap with you!", self.endConvo)
        

                                          
def startLevel(gameData=None,parent=None):          
    return labyrinth1Window(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
