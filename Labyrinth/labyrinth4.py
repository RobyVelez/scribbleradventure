from labyrinthObjects import *
                    
class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 4", gameData, parent)
        self.setup()
            
            
    def onLevelStart(self):
        self.addResponse(self.beaglePort, "Throughout Lab-RINTH you'll find these terminals, which generally control gates.")
        self.addResponse(self.beaglePort, "Most of them are busted, but you can often get them to work if you know the correct code.")
        self.addResponse(self.beaglePort, "The terminals themselves generally show a hint as to what the code might be.")
        self.addResponse(self.beaglePort, "This one, for example, can be opened by the number 16.")
        self.addResponse(self.beaglePort, "You can pass a code to a terminal by being near them and calling the use function.")
        self.addResponse(self.beaglePort, ["Thus, this terminal should open with: ", Mono("use(16)")])
        self.addResponse(self.beaglePort, "Try it!")
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.convEvent(lambda: self.setObjectiveText("-Pass 16 to the first terminal."))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def checkKey1(self, terminal, key):
        if terminal.check(key):
            self.gate1.open()
            self.addResponse(self.beaglePort, "However, most terminals aren't that easy to operate.")
            self.addResponse(self.beaglePort, "They'll require codes that are stored on keycards, which you can find all over the place.")
            self.addResponse(self.beaglePort, "Which keycard you'll need depends on the terminal.")
            self.addResponse(self.beaglePort, ["But look closely at the keycard, and you'll see it has an identifier, like ", Mono("x1")])
            self.addResponse(self.beaglePort, "If that identifier corresponds to the one shown on the terminal, you know you have the right one.")
            self.addResponse(self.beaglePort, "Make sure to name your variables appropriately, so you don't forget which variable holds which keycard.")
            self.addResponse(self.beaglePort, "Try using that card there to open the next gate.")
            #self.addResponse(self.scribbyPort, "Don't worry, I've done this before.")
            self.convEvent(lambda: self.launchBriefScreen(1))
            self.convEvent(lambda: self.setObjectiveText("-Pass a sum to the third terminal."))
            self.addResponse(None, "")
            self.convEvent(self.hidePanel1)
            self.convStart()
        else:
            self.printToSpeechBox("That was not the correct code.", makePic(self.beaglePort))    
        
        
    def checkKey2(self, terminal, key):
        if terminal.check(key):
            self.gate2.open()
            self.addResponse(self.beaglePort, "To make matters worse, many terminals won't directly respond to a keycard code.")
            self.addResponse(self.beaglePort, "Instead, they'll require the code to be modified first...")
            self.addResponse(self.beaglePort, "...through basic arithmetic!")
            self.addResponse(self.scribbyPort, "That doesn't sound too bad.")
            self.addResponse(self.beaglePort, "I hate arithmetic.")
            self.addResponse(self.beaglePort, "Anyway, you can perform arithmetic directly with variables.")
            self.addResponse(self.beaglePort, ["For example, to add 15 to a variable called ", Mono("var"), " simply type: ", Mono("var + 15")])
            self.addResponse(self.beaglePort, ["Note that this does not change the value of ", Mono("var")])
            self.addResponse(self.beaglePort, ["Use this method in combination with functions to pass a modified code to a terminal."])
            self.addResponse(self.beaglePort, ["For this terminal, you should be able to type: ", Mono("use(x2 + 15)"), " to open the gate. Provided that your variable is called x2, that is."])
            self.convEvent(lambda: self.launchBriefScreen(2))
            self.convEvent(lambda: self.setObjectiveText("-Escape the room!"))
            self.addResponse(None, "")
            self.convEvent(self.hidePanel1)
            self.convStart()
        else:
            self.printToSpeechBox("That was not the correct code.", makePic(self.beaglePort))  
            
            
    def checkKey3(self, terminal, key):
        if terminal.check(key):
            self.gate3.open()
            self.addResponse(self.beaglePort, "Lastly, some terminals may need a combination of codes to work.")
            self.addResponse(self.beaglePort, "This one needs the sum of two different keycards to activate.")
            self.addResponse(self.beaglePort, "Adding two variables works the same way as adding two numbers, or a variable and a number.")
            self.addResponse(self.beaglePort, "I think that is enough information for you to figure this one out.")
            self.convEvent(lambda: self.launchBriefScreen(3))
            self.convEvent(lambda: self.setObjectiveText("-Pass code to the fourth terminal."))
            self.addResponse(None, "")
            self.convEvent(self.hidePanel1)
            self.convStart()
        else:
            self.printToSpeechBox("That was not the correct code.", makePic(self.beaglePort))  
            
            
    def checkKey4(self, terminal, key):
        if terminal.check(key):
            self.gate4.open()
            self.addResponse(self.beaglePort, "You've done it! Great job!")
            self.convStart()
        else:
            self.printToSpeechBox("That was not the correct code.", makePic(self.beaglePort))  
        
        
    def createLevel(self):
        # Set buttons
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.helpWidgets[2].setButtonsVisible(["pickup()", "variables"])
        
        # Add background
        self.setBackground(LEVEL_PATH+"lab4back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0, 300), (300, 300), (300, 200), 
                                 (500, 200), (500, 500), (400, 500), 
                                 (400, 400), (200, 400), (200, 500), 
                                 (100, 500), (100, 400), (0, 400)), vis=False)
        self.addObstacle(Rectangle((300,0),(400, 100)), vis=False)
        self.addObstacle(Rectangle((600,200),(700, 300)), vis=False)
        self.addObstacle(Rectangle((100,600),(200, 700)), vis=False)
        self.addObstacle(Rectangle((400,600),(500, 700)), vis=False)
        
        # Add gates
        self.gate1 = self.addActor(DoubleDoor(325, 100, 50, 100, 1))
        self.gate2 = self.addActor(DoubleDoor(500, 225, 100, 50, 1))
        self.gate3 = self.addActor(DoubleDoor(425, 500, 50, 100, 1))
        self.gate4 = self.addActor(DoubleDoor(125, 500, 50, 100, 1))
        
##         images=[]
##         for i in range(1,23):
##             images.append(LEVEL_PATH+"gate_" + str(i) + ".png")
##         gate = self.quickActor(350, 150, images)
##         gate.animationCostumes=range(len(images))
##         self.addStepEvent(gate.animate)
        
        # Add keycards
        x1 = self.addActor(Keycard(450, 150, 1, 37))
        x2 = self.addActor(Keycard(550, 350, 2, 42))
        
        # Add terminals
        self.addActor(Terminal(250, 50, 0, 0, self.checkKey1, "(16)", 16, debug=False))
        self.addActor(Terminal(650, 150, 1, 0, self.checkKey2, "(x1)", x1.code, debug=False))
        self.addActor(Terminal(450, 650, 2, 0, self.checkKey3, "(x2+15)", x2.code+15, padWidth=300, debug=False))
        self.addActor(Terminal(250, 450, 0, 0, self.checkKey4, "(x1+x2)", x1.code+x2.code, debug=False))
        
        # Add junkbot
        self.quickActor(50, 250, self.beaglePort)
        
        # Create the robot
        self.createEndPad(50,550)
        self.createStartPad(50,150)
        self.createRobot(50,150,0)
        
        # Create the briefings
        self.createBriefings()
        
    def createBriefings(self):
        text=[]
        text.append(["Drive to the first terminal.\n"])
        text.append(["Pass the code 16 to the terminal with: ", Mono("use(16)"), "\n"])       
        b=simpleBrief(text,[self.getR("use()")])                    
        self.addBriefShape(b,"Pass 16 to the first terminal.")
        
        text=[]    
        text.append(["Pickup keycard x1 and store it in a variable.\n"]) 
        text.append(["Drive to the second terminal.\n"])
        text.append(["Pass keycard x1 to the terminal with: ", Mono("use(x1)"), " where x1 is the name of the variable.\n"])     
        b=simpleBrief(text,[self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Pass keycard to the second terminal.")
        
        text=[]    
        text.append(["Pickup keycard x2 and store it in a variable.\n"]) 
        text.append(["Drive to the third terminal.\n"])
        text.append(["Pass the sum of keycard x2 and 15 to the terminal with: ", Mono("use(x2 + 15)"), " where x2 is the name of the variable.\n"])     
        b=simpleBrief(text,[self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Pass a sum to the third terminal.")
        
        text=[]    
        text.append(["Drive to the fourth terminal.\n"])
        text.append(["Activate the fourth terminal by passing the correct code.\n"])     
        b=simpleBrief(text,[self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Pass code to the fourth terminal.")
                                          
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
if __name__ == "__main__":
    startLevel()
