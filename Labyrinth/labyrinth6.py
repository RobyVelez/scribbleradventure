from labyrinthObjects import *
from threading import Thread
import System
#import time as t
    
class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 6", gameData, parent)
        self.debug = False
        self.conversation.setDebug(False)
        self.setup()
        self.key_retrieved = False
        self.testActive = False


    def startTest(self, frameMove=0):
        self.testActive = True
        self.conversation.getActiveThread().blockSkip()
        self.speechBox.hide()
        self.testingScreen.visible = True
        self.testingScreenText.text = "| BEAGLE Testing Mode\n"
        for i in range(40 - frameMove):
            self.testingScreen.move(10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        for i in range(frameMove):
            self.worldFrame.move(10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()

    def writeTest(self, text):
        self.testingScreenText.text += "| " + text + "\n"
        wait(1)


    def endTest(self, frameMove=0):
        wait(1)
        for i in range(frameMove):
            self.worldFrame.move(-10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        for i in range(40 - frameMove):
            self.testingScreen.move(-10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        self.testingScreen.visible = False
        self.testActive = False


    def onLevelStart(self):
        self.functionName = "grabKey"
        self.variableName = "x7"
        self.phase = 1
        self.targetValue = self.key1.code
        self.addResponse(self.beaglePort, "Another way variables interact with functions is through return values.")
        self.addResponse(self.beaglePort, "When you create a variable inside a function, that variable will only be available within that function.")
        self.addResponse(self.beaglePort, "This means that, if you try to use it outside the function, the variable will not be found.")
        self.addResponse(self.beaglePort, "This is convenient, because it means that variables inside a function will not interfere with variables outside of the function.")
        self.addResponse(self.beaglePort, "However, sometimes we want to use the value of a variable created inside a function, outside of that function.")
        self.addResponse(self.beaglePort, "And that is where return values come in.")
        self.addResponse(self.beaglePort, ["To return a value from a function, add the following code ", Italic("as the last line of your funtion:\n"), Mono("return  variable")])
        self.addResponse(self.beaglePort, ["A simple example is:\n", Mono("def  myReturn():\n    x=1\n    return  x")])
        self.addResponse(self.beaglePort, ["To use such a function, type:\n", Mono("x = myReturn()")])
        self.addResponse(self.beaglePort, "Now, create a function named " + self.functionName + " that drives forward, picks up that keycard, and returns the keycard.")
        self.addResponse(self.beaglePort, "Then, use that function to pickup the keycard and store it in a variable called " + self.variableName + ".")
        self.addResponse(self.beaglePort, "And make sure to use a function, rather than picking it up manually, because I will test your function.")
        self.addResponse(self.beaglePort, ["You should call your function with:\n", Mono(self.variableName + " = " + self.functionName +"()")])
        self.convEvent(lambda: self.setObjectiveText("-Pickup keycard x7 with your function"))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        

    def startPhase2(self):
        if self.phase >= 2: return
        self.functionName = "grabKey"
        self.variableName = "x4"
        self.targetValue = self.key5.code
        self.phase = 2
        self.addResponse(self.beaglePort, "Nice work!")
        self.addResponse(self.beaglePort, "Move to the next hallway for one last test.")
        #self.convEvent(lambda: self.launchBriefScreen(1))
        self.convEvent(self.gate2.open)
        self.convEvent(RotateToEvent(self.scribby, -90, 20))
        self.convEvent(MoveToEvent(self.scribby, self.rx, self.ry, 20))
        self.convEvent(RotateToEvent(self.scribby, -180, 20))
        self.rx=300
        self.ry=650
        self.ra=-90
        self.convEvent(MoveToEvent(self.scribby, self.rx, self.ry, 20))
        self.convEvent(RotateToEvent(self.scribby, -270, 20))
        self.addResponse(self.beaglePort, "Okay, now let's combine arguments with return values.")
        self.addResponse(self.beaglePort, "Modify your " + self.functionName + " function such that it takes the number of a keycard, drives to that keycard, picks up that keycard, and returns that keycard.")
        self.addResponse(self.beaglePort, ["For example, if you type ",  Mono(self.functionName +"(2)"), ", your function should drive to keycard x2, pick it up, and return its value."])
        self.addResponse(self.beaglePort, ["How do you drive to a specific keycard?"])
        self.addResponse(self.beaglePort, ["You pass the argument of your function to your movement command."])
        self.addResponse(self.beaglePort, ["For example, if you write the following function:\n", Mono("def  moveToKey(time):\n    forward(4, time)")])
        self.addResponse(self.beaglePort, ["The distance you move forward depends on the argument."])
        self.addResponse(self.beaglePort, ["If you'd type: ", Mono("moveToKey(1)"), " you will move forward with a speed of 4 for 1 second."])
        self.addResponse(self.beaglePort, ["But if you'd type: ", Mono("moveToKey(4)"), " you will move forward with a speed of 4 for 4 seconds."])
        self.addResponse(self.beaglePort, ["As it turns out, moving forward with a speed of 4 for 1 second will get you to keycard x1."])
        self.addResponse(self.beaglePort, ["And, moving forward with a speed of 4 for 4 seconds will get you to keycard x4."])
        self.addResponse(self.beaglePort, ["Use this information to create a function that drives to a keycard depending on the argument, picks it up, and returns it."])
        self.addResponse(self.beaglePort, ["Then test your function by picking up keycard x4 and storing it in a variable called x4. You can do this by typing: ", Mono("x4=grabKey(4)")])
        self.convEvent(lambda: self.setObjectiveText("-Pickup keycard x4 with your function"))
        self.convEvent(lambda: self.launchBriefScreen(1))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
        
    def endLevel(self):
        self.addResponse(self.beaglePort, "Great job!")
        self.addResponse(self.beaglePort, "Now let me show you to the exit.")
        self.convEvent(self.gate1.open)
        self.addResponse(self.beaglePort, "Follow me.")
        self.convEvent(lambda: self.setGameOver(1))
        self.convStart()
        
        
    def testPhase1(self):
        if self.debug: print("Phase 1: Starting phase 1 test")
        self.startTest()
        if self.debug: print("Teleporting player")
        self.safeMoveBlock(self.rx, self.ry, self.ra)
        if self.debug: print("Teleporting done")
        self.pauseRobot(False)
        # Check that the variable in which they were supposed to store 
        # the keycard actually exists
        self.writeTest("Testing variable: x7")
        try:
            x=calico.manager.scope.GetVariable(self.variableName)
        except Exception as e:
            if self.debug: print("Phase 1 variable exception")
            self.handleVariableException(e, self.variableName)
            self.endTest()
            return
        if not x==self.key7.code:
            self.addResponse(self.beaglePort, "I'm sorry, but the variable called x7 does not hold keycard x7.")
            self.addResponse(self.beaglePort, "Make sure you did not forget to store the keycard, and that your variable name was x4.")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            self.endTest()
            return
        self.writeTest("Success")
            
        self.writeTest("Calling function: x7 = grabKey()")
        # Check that the function they created actualy retrieves the key
        try:
            x=calico.manager.scope.GetVariable(self.functionName)()
        except Exception as e:
            self.handleFunctionException(e, self.functionName, 0)
            self.endTest()
            return
        if not x==self.key7.code:
            self.addResponse(self.beaglePort, "I'm sorry, but while you have retrieved the key, your function doesn't work.")
            self.addResponse(self.beaglePort, "Make sure you pickup the key with a function called " + self.functionName + " and that that function works from the start of the hallway.")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            self.endTest()
            return
        self.writeTest("Success")
        self.endTest()
        self.startPhase2()
        
        
    def testPhase2(self):
        if self.debug: print("Phase 2: Starting phase 2 test")
        
        
        self.startTest(20)
        
        if self.debug: print("Teleporting player")
        self.safeMoveBlock(self.rx, self.ry, self.ra)
        self.pauseRobot(False)
        # Check that the variable in which they were supposed to store 
        # the keycard actually exists
        x=None
        self.writeTest("Testing variable: x4")
        try:
            x=calico.manager.scope.GetVariable(self.variableName)
        except Exception as e:
            if self.debug: print("Phase 2 variable exception")
            self.handleVariableException(e, self.variableName)
            if self.debug: print("Phase 2: Returned after variable exception.")
            self.endTest(20)
            return
        if not x==self.key4.code:
            self.addResponse(self.beaglePort, "I'm sorry, but the variable called x4 does not hold keycard x4.")
            self.addResponse(self.beaglePort, "Make sure you did not forget to store the keycard, and that your variable name was x4.")
            if self.debug: print("Teleporting player after wrong code")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            if self.debug: print("Phase 2: Returned after variable mismatch.")
            self.endTest(20)
            return
        self.writeTest("Success")
            
        # Check that the function they created actualy retrieves the key
        x=None
        self.writeTest("Calling function: x4 = grabKey(4)")
        try:
            x=calico.manager.scope.GetVariable(self.functionName)(4)
        except Exception as e:
            self.handleFunctionException(e, self.functionName, 1)
            if self.debug: print("Phase 2: Returned after function exception 1.")
            self.endTest(20)
            return
        if not x==self.key4.code:
            self.addResponse(self.beaglePort, "I'm sorry, but while you have retrieved the key, your function didn't work properly. I tried to retrieve key x4, but your function did not return key x4.")
            self.addResponse(self.beaglePort, "Make sure you pickup the key with a function called " + self.functionName + " and that that function works from the start of the hallway.")
            if self.debug: print("Teleporting player after wrong code, part 2")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            if self.debug: print("Phase 2: Returned after function mismatch 1.")
            self.endTest(20)
            return
        self.writeTest("Success")
            
        # Check that the function can also retrieve a different key
        if self.debug: print("Teleporting player for second test")
        self.safeMoveBlock(self.rx, self.ry, self.ra)
        x=None
        self.writeTest("Calling function: x2 = grabKey(2)")
        try:
            x=calico.manager.scope.GetVariable(self.functionName)(2)
        except Exception as e:
            self.handleFunctionException(e, self.functionName, 1)
            if self.debug: print("Phase 2: Returned after function exception 2.")
            self.endTest(20)
            return
        if not x==self.key2.code:
            self.addResponse(self.beaglePort, "I'm sorry, but while you have retrieved the key, your function doesn't work properly. I tried to retrieve key x2, but your function did not return key x2.")
            self.addResponse(self.beaglePort, "Make sure your function can pickup any of the keys, based on the argument, and not just key x4.")
            if self.debug: print("Teleporting player wrong code second test")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            if self.debug: print("Phase 2: Returned after function mismatch 2.")
            self.endTest(20)
            return
        self.writeTest("Success")
        if self.debug: print("Phase 2: Initiating phase 3")
        self.endTest(20)
        self.endLevel()
        
        
    def handleFunctionException(self, e, funcName, nrArg):
        if "unknown member" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but I could not find a function named: " + funcName + ".")
            self.addResponse(self.beaglePort, "Make sure you typed it correctly, and that you have loaded the function by pressing the green button.")
        elif "takes" in e.message and nrArg == 0:
            self.addResponse(self.beaglePort, "I'm sorry, but your function should not take any arguments.")
            self.addResponse(self.beaglePort, "Make sure the first line of your function looks like this: def " + funcName + "():")
        elif "takes" in e.message and nrArg == 1:
            self.addResponse(self.beaglePort, "I'm sorry, but your function should take exactly one argument. No more, no less.")
            self.addResponse(self.beaglePort, "Make sure the first line of your function looks like this: def " + funcName + "(argument):")
        elif "not callable" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but the thing named: " + funcName + " is not a function.")
            self.addResponse(self.beaglePort, "Make sure you typed it correctly, have loaded the function by pressing the green button, and that you don't have any lines that look like this: " + funcName +"=something")
        else:
            self.addResponse(self.beaglePort, "I'm sorry, your function didn't work, and I have no clue why.")
            self.addResponse(self.beaglePort, "The error message was: " + e.message)
            self.addResponse(self.beaglePort, "See the Output for details.")
            if self.debug: print("Teleporting player after crash")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            raise
        if self.debug: print("Teleporting player after handeling exception")
        self.safeMoveBlock(self.rx, self.ry, self.ra)
        self.convStart()
            
            
    def handleVariableException(self, e, varName, rightHandSide="value"):
        if "unknown member" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but I could not find a variable named: " + varName + ".")
            self.addResponse(self.beaglePort, "Make sure that you have typed it correctly and have assigned it a value with: " + varName + "=" + rightHandSide)
        else:
            self.addResponse(self.beaglePort, "Something is wrong with your variable, and I don't know what.")
            self.addResponse(self.beaglePort, "The error message was: " + e.message)
            self.addResponse(self.beaglePort, "See the Output for details.")
            if self.debug: print("Teleporting player after fatal variable exception")
            self.safeMoveBlock(self.rx, self.ry, self.ra)
            self.convStart()
            raise
        if self.debug: print("Teleporting player after variable exception")
        self.safeMoveBlock(self.rx, self.ry, self.ra)    
        self.convStart()


    def onPickup(self, item):
        item.show()
        item.getAvatar().setAlpha(50)
        if self.testActive or self.phase != 1: return
        self.testActive = True
        self.addResponse(self.beaglePort, "Great job! Now let me test your function.")
        self.addResponse(self.beaglePort, "", self.testPhase1)
        self.convStart()
        self.waitForStep()
##         self.addTestStartResponse()

        
        
    def onPickup2(self, item):
        item.show()
        item.getAvatar().setAlpha(50)
        if self.testActive or self.phase != 2: return
        self.testActive = True
        self.key_retrieved = True
        self.addResponse(self.beaglePort, "Great job! Now let me test your function.")
        self.addResponse(self.beaglePort, "", self.testPhase2)
        self.convStart()
        self.waitForStep()

          
        
    def onUse1(self, term, key):
        if len(key) == 1:
            if key[0] == self.key1.code:
                self.addResponse(self.scribbyPort, "Hey, this key didn't work.")
                self.addResponse(self.beaglePort, "That's because it is not the key to this door.")
                self.addResponse(self.beaglePort, "I will open the door once your function works correctly.")
                self.addTestStartResponse()
                self.talk()
            else:
                self.printToSpeechBox("This key didn't work", makePic(self.scribbyPort))
        else:
            self.printToSpeechBox("I should pass exactly one argument to this terminal.", makePic(self.scribbyPort))
        
        
    def onUse2(self, term, key):
        if len(key) == 1:
            if key[0] == self.key5.code:
                self.addResponse(self.scribbyPort, "Hey, this key didn't work.")
                self.addResponse(self.beaglePort, "That's because it is not the key to this door.")
                self.addResponse(self.beaglePort, "I will open the door once your function works correctly.")
                self.convStart()
            else:
                self.printToSpeechBox("This key didn't work", makePic(self.scribbyPort))
        else:
            self.printToSpeechBox("I should pass exactly one argument to this terminal.", makePic(self.scribbyPort))
        
        
    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()
        self.thread = None
    
        # Add background
        self.setBackground(LEVEL_PATH+"lab6back.png")  
       
        
        # Add walls
        self.addObstacle(Polygon((0,0), (0,600), (250,600), (250,0)), vis=False)
        self.addObstacle(Polygon((350,0),(350,600),(500,600),(500,500),(600,500),(600,600),(700,600),(700,0)), vis=False)

        # Add gates
        self.gate1 = self.addActor(DoubleDoor(100, 600, 50, 100, 1))
        self.gate2 = self.addActor(DoubleDoor(350, 600, 50, 100, 1))

        # Add keycards
        self.key7 = self.addActor(Keycard(550,550, 7, 124, onPickup=self.onPickup))
        self.key1 = self.addActor(Keycard(300,550, 1, 432, onPickup=self.onPickup2))
        self.key2 = self.addActor(Keycard(300,450, 2, 356, onPickup=self.onPickup2))
        self.key3 = self.addActor(Keycard(300,350, 3, 456, onPickup=self.onPickup2))
        self.key4 = self.addActor(Keycard(300,250, 4, 577, onPickup=self.onPickup2))
        self.key5 = self.addActor(Keycard(300,150, 5, 689, onPickup=self.onPickup2))
        self.key6 = self.addActor(Keycard(300,50, 6, 198, onPickup=self.onPickup2))
                        
        # Add terminals
        self.term1 = self.addActor(Terminal(450, 550, 0, 0, self.onUse1, "(?)", 0, debug=False))
        self.term2 = self.addActor(Terminal(200, 550, 0, 0, self.onUse2, "(?)", 0, debug=False))

        # Add pads
        self.createEndPad(50,650)
        
        # Add junkbot
        self.quickActor(650, 650, LEVEL_PATH + "junk-bot_raw.png")
        
        # Create the robot
        self.rx=550
        self.ry=650
        self.ra=-90
        self.createRobotStart(self.rx, self.ry, self.ra)
        self.scribby = getRobot().frame
        
        # Create testing screen
        self.testingScreen = Rectangle((-400,0), (0,700), color=Color(0,0,0, 255), border=0)
        self.testingScreen.visible = True
        self.testingScreenText = makeText("| LAB-RINTH Testing Mode\n")
        self.testingScreenText.fontSize = 24
        self.testingScreenText.yJustification = "top"
        self.testingScreenText.xJustification = "left"
        self.testingScreenText.color = Color(50,200,50)
        self.testingScreenText.moveTo(-350/2, -320)
        self.testingScreenText.draw(self.testingScreen)
        self.addDecor(self.testingScreen)
        
        
        # Create briefings
        self.createBriefings()

        
    def createBriefings(self):
        text=[]
        text.append(["Create a function named ", Mono("grabKey"), " that drives forward, picks up that keycard, and returns the keycard."])
        text.append(["A simple example of a function that returns a variable is:\n", Mono("def  myReturn():\n    x=1\n    return  x")])
        text.append(["Call your function to pickup the keycard and store it in a variable called ", Mono("x7"), "."]) 
        text.append(["To call your function and store its return value, type: ", Mono("x7=grabKey()")]) 
        b=simpleBrief(text,[self.getR("functions2"), self.getR("pickup()")])                    
        self.addBriefShape(b, "Create grabKey function with return")
        
        text=[]    
        text.append(["Modify your " , Mono("grabKey"), " function such that takes the number of a keycard, drives to that keycard, picks up that keycard, and returns that keycard."])
        text.append(["A function that drives to a keycard based on the argument is:\n", Mono("def  moveToKey(time):\n    forward(4, time)")])
        text.append(["Test your function by picking up keycard x4 and storing it in a variable called x4. You can do this by typing: ", Mono("x4=grabKey(4)")])     
        b=simpleBrief(text,[self.getR("functions2"), self.getR("pickup()")])                    
        self.addBriefShape(b, "Extend grabKey function with argument")
        
                                                                                                              
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
