from labyrinthObjects import *
from threading import Thread
    
class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 5", gameData, parent)
        self.setup()
        self.testActive = False

    def startTest(self, frameMove=0):
        self.testActive = True
        self.conversation.getActiveThread().blockSkip()
        self.speechBox.hide()
        self.testingScreen.visible = True
        self.testingScreenText.text = "| BEAGLE Testing Mode\n"
        for i in range(40 - frameMove):
            self.testingScreen.move(10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        for i in range(frameMove):
            self.worldFrame.move(10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()

    def writeTest(self, text):
        self.testingScreenText.text += "| " + text + "\n"
        wait(1)


    def endTest(self, frameMove=0):
        wait(1)
        for i in range(frameMove):
            self.worldFrame.move(-10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        for i in range(40 - frameMove):
            self.testingScreen.move(-10, 0)
            self.sim.window.refresh()
            self.sim.window.updateNow()
        self.testingScreen.visible = False
        self.testActive = False
             
                       
    def onLevelStart(self):
        self.addResponse(self.beaglePort, "To navigate Lab-RINTH, you'll also need to be able to create your own functions.")
        self.addResponse(self.scribbyPort, "I've done that!")
        self.addResponse(self.beaglePort, "You have? So you are familiar with arguments and return values?")
        self.addResponse(self.scribbyPort, "Arguments and return what?")
        self.addResponse(self.beaglePort, "Okay, so you know that functions execute some fixed lines of code.")
        self.addResponse(self.beaglePort, "That's handy when you need to do the same task over-and-over, but it is not very flexible.")
        self.addResponse(self.beaglePort, "Arguments can make a function more flexible by allowing you to pass values from outside the function.")
        self.addResponse(self.beaglePort, ["Your ", Mono("forward()"), " function wouldn't be very usefull if it only allowed you to move a fixed distance forward, would it?"])
        self.addResponse(self.beaglePort, ["However, by passing a speed and time to the forward function, you can move exactly as far as you want."])
        self.addResponse(self.beaglePort, self.phase1howTo)
        self.addResponse(self.beaglePort, ["Let's practice."])
        self.addResponse(self.beaglePort, self.phase1createFunction)
        self.addResponse(self.beaglePort, self.phase1useFunction)
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.convEvent(lambda: self.setObjectiveText("-Create a function that takes an argument"))
        self.convEvent(lambda: self.helpWidgets[2].setButtonsVisible(["functions2"], 4))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def startPhase2(self):
        if self.phase >= 2: return
        self.functionName = "myFunction"
        self.phase = 2
        self.term1.canInteract = True
        self.catchUsePad.canInteract = False
        self.addResponse(self.beaglePort, "Well done!")
        self.addResponse(self.beaglePort, "Our current function isn't very useful, it just takes an argument and immediately passes it to the use function.")
        self.addResponse(self.beaglePort, "But we can add extra code to the function, such as movement commands.")
        self.addResponse(self.beaglePort, self.phase2line1)
        self.addResponse(self.beaglePort, self.phase2line2)
        self.addResponse(self.beaglePort, self.phase2line3)
        self.convEvent(lambda: self.launchBriefScreen(1))
        self.convEvent(lambda: self.setObjectiveText("-Extent you function by adding a forward command before use"))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def startPhase3(self):
        if self.phase >= 3: return
        self.phase = 3
        self.safeMoveBlock(self.rx-200, self.ry, self.ra)
        self.addResponse(self.beaglePort, "Great job! You're getting the hang of it.")
        self.addResponse(self.beaglePort, ["You should extent your function one more time, so it also drives forward " , Italic("after"), " calling use."])
        self.addResponse(self.beaglePort, "Once your are done, you should be able to reach the end of the hallway by calling your function twice, each time with a different code.")
        self.addResponse(self.beaglePort, ["Remember, to call your function with a specific code, type: ", Mono(self.functionName + "(code)"), " where ", Mono("code"), " should be replaced by the number you see on the terminal."])
        self.addResponse(self.beaglePort, "Good luck!")
        self.convEvent(lambda: self.launchBriefScreen(2))
        self.convEvent(lambda: self.setObjectiveText("-Extent you function again by adding forward command after use"))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def testPhase1(self):
        self.startTest()
        self.writeTest("Calling function: " + self.functionName + "(1)")
        try:
            calico.manager.scope.GetVariable(self.functionName)(1)
        except Exception as e:
            self.handleException(e)
            self.convStart()
            self.endTest()
        else:
            self.startPhase2()
        self.writeTest("Success")
        self.endTest()
            
            
    def testPhase2(self):
        self.startTest()
        self.safeMoveBlock(self.rx, self.ry, self.ra)
        self.gate1.close()
        self.writeTest("Calling function: " + self.functionName + "(" + str(self.term1.code) + ")")
        try:
            calico.manager.scope.GetVariable(self.functionName)(self.term1.code)
        except Exception as e:
            self.handleException(e)
            self.convStart()
            self.endTest()
        else:
            if self.gate1.isOpen():
                self.startPhase3()
            else:
                self.addResponse(self.beaglePort, "Your function did not drive towards the terminal, or did not call the use function with the correct argument.")
                self.addResponse(self.beaglePort, "Make sure your function works from the starting location, and passes its argument to the use function.")
                self.convStart()
        self.writeTest("Success")
        self.endTest()
            

    def handleException(self, e):
        if "unknown member" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but I could not find a function named: " + self.functionName + ".")
            self.addResponse(self.beaglePort, "Make sure you typed it correctly, and that you have loaded the function by pressing the green button.")
        elif "takes" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but your function should take exactly one argument. No more, no less.")
            self.addResponse(self.beaglePort, "Make sure the first line of your function looks like this: def " + self.functionName + "(argument):")
        elif "not callable" in e.message:
            self.addResponse(self.beaglePort, "I'm sorry, but the thing named: " + self.functionName + " is not a function.")
            self.addResponse(self.beaglePort, "Make sure you typed it correctly, have loaded the function by pressing the green button, and that you don't have any lines that look like this: " + self.functionName +"=something")
        else:
            # This special case should not happen, so it works a little differently.
            # It actually reraises the error, meaning this case will not return normally
            # hence the convStart call
            self.addResponse(self.beaglePort, "I'm sorry, your function didn't work, and I have no clue why.")
            self.addResponse(self.beaglePort, "The error message was: " + e.message)
            self.addResponse(self.beaglePort, "See the Output for details.")
            self.convStart()
            raise
        
        
    def onUsePad(self, pad, item):
        if len(item) == 1:
##             if self.thread and self.thread.isAlive(): return
##             self.thread = Thread(target = self.testPhase1)
##             self.addStepEvent(self.thread.start)
            if self.testActive or self.phase != 1: return
            self.addResponse(self.beaglePort, "Great job! Now let me test your function.")
            self.addResponse(self.beaglePort, "", self.testPhase1)
            self.convStart()
            self.waitForStep()
        else:
            self.addResponse(self.beaglePort, "You need to pass exactly one argument to the use function.")
            self.convStart()
            
            
    def onUseTerm(self, term, item):
        if term.check(item):
            self.gate1.open()
##             if self.thread and self.thread.isAlive(): return
##             self.thread = Thread(target = self.testPhase2)
##             self.addStepEvent(self.thread.start)
            if self.testActive or self.phase != 2: return
            self.addResponse(self.beaglePort, "Great job! Now let me test your function.")
            self.addResponse(self.beaglePort, "", self.testPhase2)
            self.convStart()
            self.waitForStep()
        else:
            self.addResponse(self.beaglePort, "You need to pass the correct code to your function, and your function needs to pass that code to the use function.")
            self.convStart()
        
        
    def createLevel(self):
        # Set buttons
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.helpWidgets[2].setButtonsVisible(["pickup()", "variables"])
    
        # Add background
        self.setBackground(LEVEL_PATH+"lab5back.png")  
        
        # Add state variables
        self.phase = 1
        self.functionName = "myFunction"
        
        # Add walls
        self.addObstacle(Polygon((0,0), (0,300), (600,300), (600, 200), (700, 200), (700, 0)), vis=False)
        self.addObstacle(Polygon((0, 400), (100, 400), (100, 600), (200,600), (200,400), 
                                 (300,400), (300,600), (400,600), (400,400), (500,400), 
                                 (500, 600), (600,600), (600, 400), (700, 400), (700, 700), (0, 700)), vis=False)
        
        # Add gates
        self.gate1 = self.addActor(Forcefield(490, 350, 1, rotation=90))
        self.gate2 = self.addActor(Forcefield(290, 350, 1, rotation=90))
        self.gate3 = self.addActor(Forcefield(90, 350, 1, rotation=90))
        
        # Add terminals
        self.term1 = self.addActor(Terminal(550, 250, 0, 0, self.onUseTerm, "(374)", 374, debug=False))
        self.term1.canInteract = False
        self.term2 = self.addActor(Terminal(350, 250, 0, 0, self.gate2, "(263)", 263, debug=False))
        self.term3 = self.addActor(Terminal(150, 250, 0, 0, self.gate3, "(184)", 184, debug=False))
        
        # Add fireballs
        self.addActor(FireBall2([(150, 550), (150, 350)]))
        self.addActor(FireBall2([(350, 550), (350, 350)]))
        
        # Add junkbot
        self.quickActor(650, 250, LEVEL_PATH + "junk-bot_raw.png", rotation=180)
        
        # Add end pad
        self.createEndPad(50,350)
        
        # Create the robot
        self.rx=650
        self.ry=350
        self.ra=180
        self.createRobotStart(self.rx, self.ry, self.ra)
        
        # Add pad
        self.catchUsePad = self.quickActor(600, 400, makeColRec(200,400), interact=self.onUsePad, debug=False)
        self.catchUsePad.canInteract = True

        # These variables allow me to explain the core of the explanation in one place.
        # This text is used in both the conversations, as well as the brief screens.
        self.phase1funName = "myFunction"
        self.phase1createFunction = ["Create a function called ",  Mono(self.phase1funName), " that takes one argument and uses it."]
        self.phase1howTo = ["To create a function that takes an argument and uses it, you can type the following in the scripting area:\n", Mono("def  " +self.phase1funName +"(argument):\n    use(argument)")]
        self.phase1useFunction = ["Once you're done, press the green button to load your function. Then call your function with: ", Mono(self.phase1funName + "(1)")]
        self.phase2funName = "myFunction"
        self.phase2line1 = ["Modify the function called ", Mono(self.phase2funName), " such that it moves forward before it calls use."]
        self.phase2line2 = ["Your move command has to be able to reach the terminal from the staring position, so a ", Mono("forward(4,1)"), " should do the trick."]
        self.phase2line3 = ["When you are done, call your function and activate the terminal by typing: ", Mono(self.phase2funName + "(" + str(self.term1.code) + ")")]
        self.phase3line1 = ["You should extent your function one more time, so it also drives forward " , Italic("after"), " calling use."]
        self.phase3line2 = ["Once your are done, you should be able to reach the end of the hallway by calling your function twice, each time with a different code."]
        self.phase3line3 = ["Remember, to call your function with a specific code, type: ", Mono(self.phase2funName + "(code)"), " where ", Mono("code"), " should be replaced by the number you see on the terminal."]
        self.thread = None

        # Create testing screen
        self.testingScreen = Rectangle((-400,0), (0,700), color=Color(0,0,0, 255), border=0)
        self.testingScreen.visible = True
        self.testingScreenText = makeText("| LAB-RINTH Testing Mode\n")
        self.testingScreenText.fontSize = 24
        self.testingScreenText.yJustification = "top"
        self.testingScreenText.xJustification = "left"
        self.testingScreenText.color = Color(50,200,50)
        self.testingScreenText.moveTo(-350/2, -320)
        self.testingScreenText.draw(self.testingScreen)
        self.addDecor(self.testingScreen)

        # Create the briefings
        self.createBriefings()
        
        
    def createBriefings(self):
        text=[]
        text.append(self.phase1createFunction)
        text.append(self.phase1howTo)
        text.append(self.phase1useFunction) 
        b=simpleBrief(text,[self.getR("functions2"), self.getR("use()")])                    
        self.addBriefShape(b,"Create function with one argument")
        
        text=[]    
        text.append(self.phase2line1)
        text.append(self.phase2line2)
        text.append(self.phase2line3)     
        b=simpleBrief(text,[self.getR("functions2"), self.getR("use()")])                    
        self.addBriefShape(b,"Add forward command before use")
        
        text=[]    
        text.append(self.phase3line1)
        text.append(self.phase3line2)
        text.append(self.phase3line3)     
        b=simpleBrief(text,[self.getR("functions2"), self.getR("use()")])                    
        self.addBriefShape(b,"Add forward command after use")
                  
                                                                  
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
