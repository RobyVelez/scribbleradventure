import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from makeShapeFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
JUNKBOT = LEVEL_PATH + "junk-bot_raw.png"
SCRIBBY = IMAGE_PATH + "scribbyPortrait.png"

class labyrinth1Window(LevelWindow):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 3", gameData, parent)
        self.tapeGathered = False
        self.drillGathered = False
        self.escaped = False
        self.conversation.setDebug(False)
        #self.debug=True
        self.setup()
            
            
    def onLevelStart(self):
        self.addResponse(SCRIBBY, "Where am I?")
        self.addResponse(JUNKBOT, "You're in the scrapheap.")
        self.addResponse(SCRIBBY, "Who are you? And where are you?")
        self.addResponse(JUNKBOT, "I'm Beagle, and I'm at the other side of these doors.")
        self.addResponse(SCRIBBY, "Can you open them?")
        self.addResponse(JUNKBOT, "Nope, and neither can you. These doors have been rusted shut for ages.")
        self.addResponse(JUNKBOT, "You're best hope would be to bust them down, before the crusher activates.")
        self.addResponse(SCRIBBY, Bold("Crusher?!"))
        self.addResponse(JUNKBOT, "Yeah. The thing activates every evening and turns whatever is in that room into nice little packages.")
        self.addResponse(SCRIBBY, "I'm going to get crushed?!")
        self.addResponse(JUNKBOT, "Sorry kid, but there is nothing I can do for you.")
        self.addResponse(JUNKBOT, "But maybe you can find some parts that look useful, and bust open that door.")
        self.addResponse(JUNKBOT, "Should at least take your mind of things.")
        self.addResponse(SCRIBBY, "There are too many items here that I want to pickup. I can only store one item in a variable.")
        self.addResponse(JUNKBOT, "Well, then use multiple different variables.")
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.convEvent(lambda: self.setObjectiveText("-Pickup two items and store them."))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def onPickup(self, item):
        if item == self.tape:
            self.tapeGathered = True
        elif item == self.drill:
            self.drillGathered = True
        if self.tapeGathered and self.drillGathered:
            self.skip()
            self.addResponse(SCRIBBY, "Okay, I found some tape and an old drill. What should I do with them?")
            self.addResponse(JUNKBOT, "Try using them together.")
            self.addResponse(SCRIBBY, "How?")
            self.addResponse(JUNKBOT, ["Type: ", Mono("use(item1, item2)\n"), "Here, item1 and item2 refer to the names of the variables in which you stored the two items."])
            self.addResponse(JUNKBOT, "You did store both items in separate variables, didn't you?")
            self.convEvent(lambda: self.launchBriefScreen(1))
            self.convEvent(lambda: self.setObjectiveText("-Use the two items."))
            self.addResponse(None, "")
            self.convEvent(self.hidePanel1)
            self.convStart()
            
    def onUse(self, item, items):
        if(len(items) == 2):
            if(self.tape in items and self.drill in items):
                self.gate.gate1.body.LinearDamping = 10.0
                self.gate.gate2.body.LinearDamping = 10.0
                self.gate.gate1.body.AngularDamping = 10.0
                self.gate.gate2.body.AngularDamping = 10.0
                self.gate.gate1.bodyType = "dynamic"
                self.gate.gate2.bodyType = "dynamic"
                self.scribbyDrill.visible=True
                self.skip()
                self.addResponse(SCRIBBY, "I've combined both items, and taped a drill to my chassis, now what?")
                self.addResponse(JUNKBOT, "You did what?!")
                self.addResponse(SCRIBBY, "I've taped a drill to my chassis. So what do I do now?")
                self.addResponse(JUNKBOT, "I don't know. Ram the door, maybe?")
                self.convEvent(lambda: self.launchBriefScreen(2))
                self.convEvent(lambda: self.setObjectiveText("-Escape the room!"))
                self.addResponse(None, "")
                self.convEvent(self.hidePanel1)
                self.convStart()
        
        
    def onEscape(self):
        print("On escape")
        if self.escaped: return
        self.escaped = True
        print("Before skip")
        self.skip()
        print("After skip")
        self.addResponse(JUNKBOT, "Wow, you escaped?")
        self.addResponse(SCRIBBY, "Yes. You... You look surprised.")
        self.addResponse(JUNKBOT, "Well, don't get me wrong, but it's sortof a first time that someone escaped the crusher room. I was sortof already thinking about how I might use your parts.")
        self.addResponse(SCRIBBY, "...")
        self.addResponse(JUNKBOT, "But nevermind that, heh. You're still in one piece, and that's what counts. Welcome to the scrapheap!")
        self.addResponse(SCRIBBY, "Okay, nevermind, I need to get out. I need to save my brother.")
        self.addResponse(JUNKBOT, "Get out, huh? Been here for barely a minute, and already talks about leaving.")
        self.addResponse(SCRIBBY, "I NEED TO SAVE MY BROTHER!")
        self.addResponse(JUNKBOT, "Alright, alright, don't short your circuits, I'll help you get out.")
        self.addResponse(JUNKBOT, "But first, I'll need to teach you how to operate Lab-RINTH.")
        self.addResponse(JUNKBOT, "Follow me.")
        self.addResponse(None, "")
        print("Before end")
        self.convEvent(self.winGame)
        print("After end")
        #print("Responses:", self.conversation.threads[0].responses)
        #print("Response index:", self.conversation.threads[0].currentResponseIndex)
        self.convStart()
        
        
    def createLevel(self):
        # Set buttons
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        self.helpWidgets[2].setButtonsVisible(["pickup()", "variables"])
        
        # Add background
        self.setBackground(LEVEL_PATH+"lab3back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0,0), (0,400), (300,400), (300,300), (200,300), (200,0)), vis=False)
        self.addObstacle(Polygon((500,0), (500,300), (400,300), (400,400), (700,400), (700,0)), vis=False)
        self.addObstacle(Polygon((0,600), (700,600), (700,700), (0,700)), vis=False) 

        # Add gate
        self.gate = DoubleDoor(300, 350, 100, 50, 1)
        self.gate.frame.xOff=0
        self.gate.frame.yOff=0
        self.gate.frame.width=700
        self.gate.frame.height=700
        self.gate.gate1.density = 1.0
        self.gate.gate2.density = 1.0
        self.addActor(self.gate)
        
        # Add items
        desc = "tape"
        pic = LEVEL_PATH+"tape-small.png"
        
        self.tape = self.quickActor(350, 150, pic, pickup=True, description=desc, use=self.onUse, onPickup=self.onPickup)
        desc = "drill"
        pic = LEVEL_PATH+"drill-small.png"
        self.drill = self.quickActor(350, 250, pic, pickup=True, description=desc, use=self.onUse, onPickup=self.onPickup)
        self.scribbyDrill = makePic(pic)
        self.scribbyDrill.visible=False
        self.scribbyDrill.scale(0.5)
        self.scribbyDrill.moveTo(37,0)
        
        # Add junkbot
        self.quickActor(250, 450, LEVEL_PATH + "junk-bot_raw.png")
        
        # Add event pad
        self.addActor(EventPad((350,450),100,50, self.onEscape, debug=False))
        
        # Create the robot
        self.createStartPad(350,50)
        self.createRobot(350,50,90)
        self.createEndPad(650,550)
        
        # Add drill
        r=getRobot()
        self.scribbyDrill.draw(r.frame)
        
        # Add briefing
        self.createBriefings()


    def createBriefings(self):
        text=[]
        text.append(["Pickup the tape and store it in a variable. Then pickup the drill and store it in a ", Italic("different")," variable.\n"])
        text.append(["Remember, ", Mono("item1=pickup()"), " will store your item in a variable named ", Mono("item1"),"\n"])
        text.append(["If you want to pickup a second item, simply change the name of the variable. For example, for your second item you could type: ", Mono("item2=pickup()"), "\n"])        
        b=simpleBrief(text,[self.getR("pickup()")])                    
        self.addBriefShape(b,"Pickup two items and store them.")
        
        text=[]     
        text.append(["Use the two items together with: ", Mono("use(item1, item2)"),"\n"])    
        text.append(["Note, ", Mono("item1"), " and ", Mono("item2")," should be the names of the variables you chose.\n"])    
        b=simpleBrief(text,[self.getR("use()")])                    
        self.addBriefShape(b,"Use the two items.")
        
        text=[]     
        text.append(["Try ramming the door to escape the room.\n"])    
        b=simpleBrief(text,[])                    
        self.addBriefShape(b,"Escape the room!")
        
                                          
def startLevel(gameData=None,parent=None):                  
    return labyrinth1Window(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
