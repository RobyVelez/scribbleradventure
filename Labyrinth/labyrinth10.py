from labyrinthObjects import *


class Fire(SpriteActor):
    def __init__(self, start, lowerBound, upperBound, leftBound, rightBound, collision, costume=0):
        files = [LEVEL_PATH+"fireballUp.png", LEVEL_PATH+"fireballDown.png", LEVEL_PATH+"fireballLeft.png", LEVEL_PATH+"fireballRight.png"]
        SpriteActor.__init__(self, start, files=files , shape=makeColCircle(35, "dynamic"), collision=collision, debug=False)
        self.description = "fire " + str(start)
        self.lowerBound = lowerBound
        self.upperBound = upperBound
        self.leftBound = leftBound
        self.rightBound = rightBound
        self.speed = 10
        
        if self.lowerBound == self.upperBound:
            self.lowerBound = start[1]+10
            self.upperBound = start[1]-10
            
        if self.leftBound == self.rightBound:
            self.leftBound = start[0]-10
            self.rightBound = start[0]+10
   
        self.changeCostume(costume)

    def up(self):
        self.changeCostume(0)
        self.shape.body.LinearVelocity = Vector(0, -self.speed)
            
    def down(self):
        self.changeCostume(1)
        self.shape.body.LinearVelocity = Vector(0, self.speed)
        
    def left(self):
        self.changeCostume(2)
        self.shape.body.LinearVelocity = Vector(-self.speed, 0)
        
    def right(self):
        self.changeCostume(3)
        self.shape.body.LinearVelocity = Vector(self.speed, 0)
        
    def stop(self):
        self.shape.body.LinearVelocity = Vector(0, 0)
            
    def step(self):
        if self.shape.y > self.lowerBound:
            self.up()
        elif self.shape.y < self.upperBound:
            self.down()
        elif self.shape.x < self.leftBound:
            self.right()
        elif self.shape.x > self.rightBound:
            self.left()
        return True    

class CurrentLevel(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 10", gameData, parent)
        self.fires=[]
        self.trapsMoving=False
        self.setup()
        
        
    def onLevelStart(self):
        self.addResponse(self.frames[2], "You know, this would be a whole lot easier for both of us if you'd just give up.")
        self.addResponse(self.frames[2], "Turn back. Leave. I'll promise to spare you.")
        self.addResponse(self.scribbyPort, "I will not turn back!")
        self.addResponse(self.scribbyPort, "This looks way too complex...")
        self.addResponse(self.scribbyPort, "I should take it one step at a time.")
        self.addResponse(self.scribbyPort, "If I create a function that can navigate one hallway, I should be able to get through all hallways.")
        self.addResponse(self.scribbyPort, "I should create a function that takes a keycard as an argument, and that uses that keycard on the door.")
        self.addResponse(self.scribbyPort, "The function should then pickup the new keycard, and return it right after using the portal.")
        self.addResponse(self.scribbyPort, ["I should be able to repeatedly call the function as: " , Mono("x=myFunction(x)"), " to clear all the hallways."])
        self.addResponse(self.scribbyPort, "Well, here goes nothing!")
        self.convEvent(lambda: self.setObjectiveText("-Get to the exit."))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
    def fireCollide(self, myfixture, otherfixture, contact): 
        robot = getRobot()
        if robot is None: return True
        if otherfixture.Body.UserData==robot.frame.body.UserData:
            self.setGameOver(-1)
        return True

    def realStart(self):
        self.fire5.up()
        self.fire6.right()
        self.trapsMoving = True

    def plateCollide(self, myfixture, otherfixture, contact):
        robot = getRobot()
        if robot is None: return True
        if not self.trapsMoving: 
            if otherfixture.Body.UserData==robot.frame.body.UserData:
                self.skull1.changeCostume(3)
                self.skull2.changeCostume(3)
                self.addResponse(self.frames[3], "Well, I gave you a chance. Time to be toasted!")
                self.addResponse(None, "", self.realStart)
                self.convStart()
                self.addStepEvent(lambda: self.teleportStep(270))
        return True
            

    def teleport(self, portal, items):
        #Rather than teleporting here, add it to the events, such that the actual teleport
        #Is executed in the level thread. Should prevent deadlocks.        
        self.addStepEvent(lambda: self.teleportStep(portal.userData["angle"]))
        
        
    def teleportStep(self, angle):
        robot = getRobot()
        if robot is None: return False
        robot.stop()
        #self.stopScript()
        
        #The contacts do not like the robot suddenly teleporting. Clear all of them (they will be recreated).
        del self.interactContactList[:]
        del self.pickupContactList[:]
        robot.setPose(350, 350, angle)
        robot.frame.body.ResetDynamics()
        disableContactList(robot.frame.body.ContactList)
        return False


    def checkKey(self, lock, items):
##         print("Provided:", items)
##         if lock == self.pad1:
##             print("Required:", self.x1)
##         elif lock == self.pad2:
##             print("Required:", self.x2)
##         elif lock == self.pad3:
##             print("Required:", self.x3)
##         elif lock == self.pad4:
##             print("Required:", self.x4)
        if items == [self.x1] and lock == self.pad1:
            self.pair1gate1.move(0, 100)
            self.pair1gate2.move(0, 100)
            self.addStepEvent(TimedEvent(10,self.fire1.right))
        elif items == [self.x2] and lock == self.pad2:
            self.pair2gate1.move(100, 0)
            self.pair2gate2.move(100, 0)
            self.addStepEvent(TimedEvent(10,self.fire2.up))
        elif items == [self.x3] and lock == self.pad3:
            self.pair3gate1.move(-100, 0)
            self.pair3gate2.move(-100, 0)
            self.addStepEvent(TimedEvent(10,self.fire3.down))
        elif items == [self.x4] and lock == self.pad4:
            self.pair4gate1.move(0, -100)
            self.pair4gate2.move(0, -100)
            self.addStepEvent(TimedEvent(10,self.fire1.left))
        else:
            self.printToSpeechBox("The key does not fit.")


    def addLock(self, x, y, key, action=None):
        lock = self.addActor(SpriteActor((x, y), Circle((0,0), 50, color=Color("gold")), interact=self.checkKey, description="lock "+str(key)))
        #lock.shape.stackOnBottom()
        return lock


    def addGate(self, x, y, w, h, color):
        shape = Rectangle((0,0), (w, h))
        shape.fill = color
        shape.bodyType = 'static'
        return self.addActor(SpriteActor((x, y), shape, obstructs=True, description="gate " + str(color)))


    def addFire(self, start, lowerBound, upperBound, leftBound, rightBound, costume=0):
        fire = self.addActor(Fire(start, lowerBound, upperBound, leftBound, rightBound, self.fireCollide, costume))
        self.addStepEvent(fire.step)
        self.addEndEvent(fire.stop)
        return fire
        

    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()

        # Add background
        self.setBackground(LEVEL_PATH+"lab10back.png")  
        
        # Add pads
        self.createEndPad(50,650)
        self.createStartPad(350,550)
    
        # Create walls
        wall1 = self.addObstacle(Polygon((100, 0), (100, 300), (300, 300), (300, 100), (180, 100), (180, 0)), vis=False)
        wall2 = self.addObstacle(Polygon((400, 100), (400, 300), (600, 300), (600, 180), (700, 180), (700, 100)), vis=False)
        wall3 = self.addObstacle(Polygon((0, 520), (0, 600), (300, 600), (300, 400), (100, 400), (100, 520)), vis=False)
        wall4 = self.addObstacle(Polygon((400, 400), (400, 600), (520, 600), (520, 700), (600, 700), (600, 400)), vis=False)
        
        # Create portals
        self.portal1 = self.addActor(SpriteActor((650, 50), LEVEL_PATH+"portal-small.png", interact=self.teleport, description="portal 1"))
        self.portal1.userData["angle"] = int(180)
        self.portal2 = self.addActor(SpriteActor((50, 50), LEVEL_PATH+"portal-small.png", interact=self.teleport, description="portal 2"))
        self.portal2.userData["angle"] = int(0)
        self.portal3 = self.addActor(SpriteActor((650, 650), LEVEL_PATH+"portal-small.png", interact=self.teleport, description="portal 3"))
        self.portal3.userData["angle"] = int(90)
        
        # Codes
        self.x1 = 140360827
        self.x2 = 924383692
        self.x3 = 485103850
        self.x4 = 538204852
        
        # Create keys
        self.key1 = self.addActor(SpriteActor((350, 450), LEVEL_PATH+"keycard_1.png", pickup=self.x1))
        self.key2 = self.addActor(SpriteActor((550, 50), LEVEL_PATH+"keycard_2.png", pickup=self.x2))
        self.key3 = self.addActor(SpriteActor((50, 150), LEVEL_PATH+"keycard_3.png", pickup=self.x3))
        self.key4 = self.addActor(SpriteActor((650, 550), LEVEL_PATH+"keycard_4.png", pickup=self.x4))

        # Create gates
        self.pair1gate1 = self.addGate(290, 50, 20, 100, makeColor(200, 0, 0, 255))
        self.pair1gate2 = self.addGate(410, 50,20, 100, makeColor(200, 0, 0, 255))
        self.pair2gate1 = self.addGate(50, 290,100, 20, makeColor(0, 0, 200, 255))
        self.pair2gate2 = self.addGate(50, 410,100, 20, makeColor(0, 0, 200, 255))
        self.pair3gate1 = self.addGate(650, 290,100, 20, makeColor(200, 200, 0, 255))
        self.pair3gate2 = self.addGate(650, 410,100, 20, makeColor(200, 200, 0, 255))
        self.pair4gate1 = self.addGate(290, 650,20, 100, makeColor(0, 200, 0, 255))
        self.pair4gate2 = self.addGate(410, 650,20, 100, makeColor(0, 200, 0, 255))

        # Add plate
        self.plate = self.addActor(SpriteActor((350, 350), LEVEL_PATH+"portal-end.png", collision=self.plateCollide))
        
        # Add fire
        self.fire1 = self.addFire((230, 50), 0, 0, 230, 650, 3)
        self.fire2 = self.addFire((50, 470), 470, 50, 0, 0, 0)
        self.fire3 = self.addFire((650, 230), 650, 230, 0, 0, 1)
        self.fire4 = self.addFire((470, 650), 0, 0, 50, 470, 2)
        self.fire5 = self.addFire((350, 650), 650, 50, 0, 0, 0)
        self.fire6 = self.addFire((50, 350), 0, 0, 50, 650, 3)

        # Add terminals
        self.pad1 = self.quickActor(350, 150, makeColRec(100, 300), interact=self.checkKey, debug=False)
        self.pad2 = self.quickActor(150, 350, makeColRec(300, 100), interact=self.checkKey, debug=False)
        self.pad3 = self.quickActor(550, 350, makeColRec(300, 100), interact=self.checkKey, debug=False)
        self.pad4 = self.quickActor(350, 550, makeColRec(100, 300), interact=self.checkKey, debug=False)
        self.screen1 = self.quickActor(270, 150, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen2 = self.quickActor(150, 270, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen3 = self.quickActor(550, 270, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen4 = self.quickActor(270, 550, LEVEL_PATH + "black-screen-with-buttons-skewed.png")
        self.screen1.rotate(90)
        self.screen4.rotate(90)
        self.term1text = self.addDecor(Text((270, 150), "(x1)", color=Color(200,200,200)))
        self.term2text = self.addDecor(Text((150, 270), "(x2)", color=Color(200,200,200)))
        self.term3text = self.addDecor(Text((550, 270), "(x3)", color=Color(200,200,200)))
        self.term4text = self.addDecor(Text((270, 550), "(x4)", color=Color(200,200,200)))
        self.term1text.rotate(90)
        self.term4text.rotate(90)

        # Add screen
        self.screen1 = self.quickActor(500, 455, LEVEL_PATH+"screen-smaller.png")
        self.screen2 = self.quickActor(200, 455, LEVEL_PATH+"screen-smaller.png")
        
        # Add SKULL
        self.frames = [LEVEL_PATH+"skull_blank.png"]
        self.frames += [LEVEL_PATH+"skull_neutral.png"]
        self.frames += [LEVEL_PATH+"skull_talking.png"]
        self.frames += [LEVEL_PATH+"skull_smug.png"]
        self.frames += [LEVEL_PATH+"skull_surprised.png"]
        self.frames += [LEVEL_PATH+"skull_wink.png"]
        self.frames += [LEVEL_PATH+"skull_afraid.png"]
        self.skull1 = self.quickActor(500, 455, self.frames, scale=0.3)
        self.skull1.changeCostume(2)
        self.skull2 = self.quickActor(200, 455, self.frames, scale=0.3)
        self.skull2.changeCostume(2)
        
        # Add robot
        self.createRobot(350,550,-90)
   
        # Create the briefings
        self.createBriefings()
        
        
    def createBriefings(self):
        text=[]
        text.append(["Create a function that takes a key as an argument, navigates the corridor, uses the keycard to open the door, picks up the new keycard, uses the portal, and returns."])
        text.append(["Move to the center and use your function with the first keycard:\n", Mono("x = yourFunction(x)")])
        text.append(["Reuse your function to get to the exit."])
        text.append([Bold("Hint:"), " add ", Mono("talk()"), " as the first line of your function, so you can skip the dialog and immediately start moving."])
        text.append([Bold("Hint:"), " to return a variable, add, as the last line of the function: ", Mono("return variable")])
        text.append([Bold("Hint:"), " use a portal with ", Mono("use()")])
        b=simpleBrief(text,[self.getR("functions2"), self.getR("variables"), self.getR("pickup()"), self.getR("use()")])                    
        self.addBriefShape(b,"Reach the exit.")
         
##     def buildBriefing(self):
##         briefing = Briefing(self)
##         briefing.enum("Create a", Inline("Function", "functions2") ,"that takes a key as an argument,")
##         briefing.line("navigates the corridor,", Inline("Uses", "Use"), "the key to open the door,")
##         briefing.line(Inline("picks up", "Pickup"), "up the new key,", Inline("Uses", "Use"), "the portal, and returns")
##         briefing.line("the new key.")
##         briefing.enum("Move to the center and use your function with the red key.")
##         briefing.line(Code("blueKey = yourFunction(redKey)"))
##         briefing.enum("Reuse your function untill you reach the end.")
##         briefing.par(Bold("Hint:"), "to return a variable, add, as the last line of the function:")
##         briefing.line(Code("return variable"))
##         briefing.par(Bold("Hint:"), "to use the portals at the end of each hallway, call")
##         briefing.line(Code("use()"), "without any arguments.")
##         briefing.relatedPages("Use", "Pickup", "variables", "functions2")
##         briefing.write()


def startLevel(gameData=None, parent=None):
    return CurrentLevel(gameData, parent)
    
if __name__ == "__main__":
    startLevel()       
