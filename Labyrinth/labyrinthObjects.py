# Imports
import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setImports import *
from makeShapeFunctions import *
import warnings

# JH: There is a default DeprecationWarning that
# "object.__new__() takes no parameters"
# when doing multiple inhertance on the level window,
# probably due to some c# ironpython interactions
# Reading more about it, it felt safe to ignore this warning
warnings.filterwarnings("ignore",category=DeprecationWarning)

# Constants
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
SCRIBBY = IMAGE_PATH + "scribbyPortrait.png"
OPPY = LEVEL_PATH+"oppyPortrait.png"

# Know error message filter
KNOW_ERRORS = ["unknown member",
               "takes no arguments",
               "takes exactly",
               "takes at least",
               "not callable"]

# Functions
def initScribbyWreck():
    robot = getRobot()
    if not robot: return
    wreck = Picture(LEVEL_PATH+"damagedScribbler.png")
    wreck.border=0
    wreck.moveTo(0,0)
    wreck.rotate(-90)
    wreck.scale(1.5)
    wreck.visible = False
    wreck.draw(robot.frame)
    return wreck

def filterErrors(error):
    for knownError in KNOW_ERRORS:
        #print("Error:", error.message, "filter:", knownError)
        if knownError in error.message:
            return
    raise


# Classes
class Skull(SpriteActor):
    def __init__(self, x, y, costume=0, scale=0.5, corrupt=False):
        frames = [LEVEL_PATH+"skull_blank.png"]
        frames += [LEVEL_PATH+"skull_neutral.png"]
        frames += [LEVEL_PATH+"skull_talking.png"]
        frames += [LEVEL_PATH+"skull_smug.png"]
        frames += [LEVEL_PATH+"skull_surprised.png"]
        frames += [LEVEL_PATH+"skull_wink.png"]
        frames += [LEVEL_PATH+"skull_afraid.png"]
        if corrupt:
            for i in range(1,3):
                frames.append(LEVEL_PATH+"skull_talking_corrupt_" + str(i) +".png")
            for i in range(1,3):
                frames.append(LEVEL_PATH+"skull_smug_corrupt_" + str(i) +".png")
            for i in range(1,10):
                frames.append(LEVEL_PATH+"skull_afraid_corrupt_" + str(i) +".png")
        SpriteActor.__init__(self, (x,y), frames, scale=scale)
        # JH: Because the conversation box will resize the pictures it gets
        # We'll create copies for the portraits
        self.ports = []
        for frame in frames:
            self.ports.append(makePic(frame, scale=0.3))
        
    def getPort(self, i=0):
        return (self.ports[i], lambda: self.changeCostume(i))
        
    def getPortNow(self, i=0):
        self.changeCostume(i)
        return self.ports[i]   
        
        
class Explosion(SpriteActor):
    def __init__(self):
        sp = SpriteSheet(LEVEL_PATH+"explosion.png", 100, 100, 100, 100)
        allSprites = sp.getAll(6,6)
        SpriteActor.__init__(self, (0,0), allSprites, visible=False, scale=2)
        self.animationCostumes=range(len(allSprites))
        self.cycle=False


class Keycard(SpriteActor):
    def __init__(self, x, y, nr=1, code=0, **kwargs):
        SpriteActor.__init__(self, (x,y), LEVEL_PATH+"keycard_" + str(nr) + ".png", pickup=code, **kwargs)
        self.code=code
        
                
class Terminal(SpriteActor):
    def __init__(self, x, y, orientation, costume, check, text="", code=None, padWidth=150, scale=1, **kwargs):
        if costume == 0:
            cost = LEVEL_PATH + "green-screen-with-buttons.png"
            textColor = Color(100,230,100)
        elif costume == 1:
            cost = LEVEL_PATH + "black-screen-with-buttons.png"
            textColor = Color(200,200,200)
        else:
            cost = LEVEL_PATH + "black-screen-with-buttons-skewed.png"
            textColor = Color(200,200,200)
        if orientation == 0:
            angle=0
            pw=padWidth
            ph=200
            padX=x
            padY=y+50
        elif orientation == 1:
            angle=90
            pw=200
            ph=padWidth
            padX=x-50
            padY=y
        else:
            angle=-90
            pw=200
            ph=padWidth
            padX=x+50
            padY=y
        self.gate=None
        if hasattr(check, "open"):
            self.gate=check
            inter = self.openGate
        else:
            inter=check
        SpriteActor.__init__(self, (padX,padY), makeColRec(pw, ph), interact=inter, **kwargs)
        #self.pad = SpriteActor((padX,padY), makeColRec(padWidth, padHeigth), interact=check, **kwargs)
        self.screen = makePic(cost)
        self.screen.moveTo(x, y)
        self.screen.rotateTo(angle)
        self.screen.scale(scale)
        self.textStr = text
        self.text = Text((x, y), text, color=textColor)
        self.text.rotateTo(angle)
        if isinstance(code, Keycard): code = code.code
        self.code = code
        self.debug = False
        if "debug" in kwargs: self.debug = kwargs["debug"]
                
                
    def draw(self, levelWindow):
        SpriteActor.draw(self, levelWindow)
        levelWindow.addDecor(self.screen)
        levelWindow.addDecor(self.text)
        
        
    def check(self, key):
        if self.debug:
            print("Expected:", self.code, "received:", key)
        if len(key) != 1: return False
        return fuzzyCompare(self.code, key[0])
        
    def restore(self):
        self.text.text = self.textStr
        

    def openGate(self, term, key):
        if self.check(key):
            self.gate.open()
        else:
            self.text.text = "ERROR!"
            self.levelWindow.addStepEvent(TimedEvent(20, self.restore))
            


class Forcefield(SpriteActor):
    def __init__(self, x, y, size=0, **kwargs):
        if size == 0:
            pic = LEVEL_PATH + "ForceField.png"
            width = 90
        elif size == 1:
            pic = LEVEL_PATH + "ForceField-100.png"
            width = 100
        else:
            print("Warning: size", size, "not available")
        SpriteActor.__init__(self, (x,y), pic, makeColRec(width,10), obstructs=True, **kwargs)
        
    def isOpen(self):
        return not self.visible()
        
    def open(self):
        self.hide()
        self.shape.body.IsSensor=True
        
    def close(self):
        self.show()
        self.shape.body.IsSensor=False
             
                     
class Labyrinth():
    scribbyPort = Picture(IMAGE_PATH + "scribbyPortrait.png")
    oppyPort = Picture(LEVEL_PATH + "oppyPortrait.png")
    sojournerPort = Picture(LEVEL_PATH+"sojournerPortrait.png")
    spiritPort = Picture(LEVEL_PATH+"spiritPortrait.png")
    beaglePort = Picture(LEVEL_PATH + "junk-bot_raw.png")
