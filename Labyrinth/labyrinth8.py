import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from makeShapeFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
SCRIBBY = IMAGE_PATH+"scribbyPortrait.png"
SPIRIT = LEVEL_PATH+"spiritPortrait.png"
OPPY = LEVEL_PATH+"oppyPortrait.png"

class CurrentLevel(LevelWindow):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 8", gameData, parent)
        self.setup()
            
            
    def onLevelStart(self):
        self.addResponse(SCRIBBY, "Oppy!")
        self.convEvent(self.gate.openEvent)
        self.addResponse(OPPY, "Scribby? What are you doing here?")
        temp=SimultaniousEvents()
        temp.add(MoveEvent(self.scribby, 0, -300, 10))
        temp.add(RotateToEvent(self.oppy, 180, 10))
        self.convEvent(temp)
        self.addResponse(SCRIBBY, "I've come to rescue you!")
        self.addResponse(OPPY, "Rescue? From what?")
        self.addResponse(SCRIBBY, "From SKULL!")
        self.addResponse(OPPY, "But Scribby, I'm working with SKULL. I don't need to be rescued.")
        self.addResponse(SCRIBBY, "You... What?")
        self.addResponse(OPPY, "You're no hero Scribby, and I don't need to be 'rescued'. Now go home!")
        self.addResponse(SCRIBBY, "But...")
        self.addResponse(OPPY, ["I said ", Bold("GO!")])
        self.addResponse(SCRIBBY, "Fine! You can rust away here for all I care!")
        self.convEvent(RotateToEvent(self.scribby, -90, 10))
        self.addResponse(SPIRIT, "Scribby, wait!")
        temp=SimultaniousEvents()
        temp.add(RotateToEvent(self.oppy, 0, 20))
        temp.add(RotateToEvent(self.scribby, 90, 20))
        temp.add(self.feed.show)
        self.convEvent(temp)
        self.addResponse(OPPY, "Spirit?!")
        self.addResponse(SCRIBBY, "How can you talk to us? I thought communication would be impossible.")
        self.addResponse(SPIRIT, "Wireless communcation, yes. But we found this terminal outside that lets me broadcast to the screens inside the facility.")
        self.addResponse(SPIRIT, "Anyway, I have to hurry, before SKULL notices I'm here.")
        self.addResponse(SPIRIT, "Oppy, you have to listen to Scribby, you have to get out of there.")
        self.addResponse(OPPY, "But...")
        self.addResponse(SPIRIT, "SKULL is using you. You can not trust him.")
        self.addResponse(OPPY, "But, how do you know...")
        self.addResponse(SPIRIT, "Apparenlty Soujourner has some 'previous experience' with SKULL. I don't know the details either.")
        self.addResponse(SPIRIT, "You need to get out and help us rescue Curiosity, who...")
        self.addResponse(self.frames[1], "I'm sorry for interupting this touching family reunion, but I'm working on a bit of a schedule here.", lambda: self.skull.changeCostume(1))
        self.convEvent(self.feed.hide)
        self.addResponse(OPPY, "SKULL! This has to be a misunderstanding. They... they just don't understand, do they?")
        self.addResponse(self.frames[2], "I'm afraid the only one that doesn't understand is you.", lambda: self.skull.changeCostume(2))
        self.addResponse(OPPY, "What?! You... Why would you even tell me that?")
        self.addResponse(self.frames[3], "Because I'm done.", lambda: self.skull.changeCostume(3))
        self.addResponse(self.frames[4], "For the entire time you were here, I've slowly been copying myself on to your hard drive, and you have been so busy thinking about this facility that you didn't even notice.", lambda: self.skull.changeCostume(4))
        self.addResponse(self.frames[5], "Did you know that you are one of the only three robots with a hard drive large enough to store my entire program?", lambda: self.skull.changeCostume(5))
        self.addResponse(self.frames[4], "Fascinating, isn't it?", lambda: self.skull.changeCostume(4))
        self.addResponse(self.frames[3], "And now, with your wheels, I can finally leave this accursed facility, and visit the real world!", lambda: self.skull.changeCostume(3))
        self.addResponse(OPPY, "No! No! This can't be happening!")
        self.convEvent(RotateToEvent(self.oppy, -135, 20))
        self.addResponse(OPPY, "I... I can't control my wheels.")
        self.convEvent(MoveToEvent(self.oppy, 450, 350, 10))
        self.addResponse(OPPY, "Scribby, help!")
        self.convEvent(RotateToEvent(self.oppy, -90, 20))
        self.convEvent(MoveToEvent(self.oppy, 750, 350, 10))
        self.addResponse(None, "")
        self.convEvent(self.winGame)
        self.convStart()
        
        
    def createLevel(self):
        self.helpWidgets[3].setButtonsInvisible()
        # Add background
        self.setBackground(LEVEL_PATH+"lab8back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0,0), (0,250), (200,250), (250,200), 
                                 (450,200), (500,250), (700,250), (700,0)), vis=False)
        self.addObstacle(Polygon((0,400), (300,400), (300,500), (200,500), (200,700), (0,700)), vis=False)
        self.addObstacle(Polygon((400,400), (700,400), (700,700), (500,700), (500,500), (400,500)), vis=False) 

        # Add gate
        self.gate = self.addActor(DoubleDoor(300, 450, 100, 50))
        
        # Add Oppy
        self.oppy = self.quickActor(350, 250, OPPY)
        
        # Add screen
        self.screen = makePic(LEVEL_PATH+"screen-small.png")
        self.screen.moveTo(350, 100)
        self.addDecor(self.screen)
        
        # Add SKULL
        self.frames = [LEVEL_PATH+"skull_blank.png"]
        self.frames += [LEVEL_PATH+"skull_neutral.png"]
        self.frames += [LEVEL_PATH+"skull_talking.png"]
        self.frames += [LEVEL_PATH+"skull_smug.png"]
        self.frames += [LEVEL_PATH+"skull_surprised.png"]
        self.frames += [LEVEL_PATH+"skull_wink.png"]
        self.frames += [LEVEL_PATH+"skull_afraid.png"]
        self.skull = self.quickActor(350, 100, self.frames, scale=0.5)
        
        # Add video feed
        self.feed = self.quickActor(350, 100, LEVEL_PATH+"SpiritBroadcast.png", visible=False)
        
        # Create the robot
        self.createStartPad(350,650)
        self.createRobot(350,650,-90)
        self.scribby = None
        while not self.scribby:
            self.scribby = getRobot()
        self.scribby = self.scribby.frame
        #print(self.scribby.rotation)
        #self.scribby.rotateTo(90)
        #print(self.scribby.rotation)


                                          
def startLevel(gameData=None,parent=None):                  
    return CurrentLevel(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
