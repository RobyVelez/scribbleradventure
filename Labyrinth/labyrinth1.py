from labyrinthObjects import *
            
class labyrinth1Window(LevelWindow, Labyrinth):
    def __init__(self, gameData, parent):
        LevelWindow.__init__(self,"Labyrinth 1", gameData, parent)
        self.setup()

             
    def onLevelStart(self):
        self.addResponse(self.sojournerPort, "Here it is: Lab-RINTH.")
        self.addResponse(self.scribbyPort, "What is it?")
        self.addResponse(self.sojournerPort, "It used to be an artificial intelligence lab, but now...")
        self.addResponse(self.spiritPort, "Why would Oppy be taken here?")
        self.addResponse(self.sojournerPort, "I don't know, but I don't like it.")
        self.addResponse(self.scribbyPort, "Well, if Oppy is inside, we need to get him out. Let's go in!")
        self.addResponse(self.sojournerPort, "To do that, you'll need this keycard.", self.key.show)
        self.addResponse(self.scribbyPort, "Where did you get that?")
        self.addResponse(self.sojournerPort, "It doesn't matter.")
        self.addResponse(self.sojournerPort, "What matters is that you'll need to learn how to use it.")
        self.addResponse(self.scribbyPort, "So how do I use it?")
        self.addResponse(self.sojournerPort, "First, drive onto the key and store it in a variable with: key=pickup()")
        self.convEvent(lambda: self.helpWidgets[2].setButtonsVisible(["pickup()", "variables"], 4))
        self.convEvent(lambda: self.launchBriefScreen(0))
        self.convEvent(lambda: self.setObjectiveText("-Drive onto the keycard."))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()
        
        
    def checkKey(self, terminal, key):
        if terminal.check(key):
            self.addResponse(self.scribbyPort, "That worked! Let's save Oppy! Follow me!")
            self.convEvent(self.gate.openEvent)
            self.convEvent(MoveToEvent(getRobot().frame, 650, 350, 10))
            self.addResponse(self.sojournerPort, "I am sorry Scribby, but we can not follow you.")
            self.addResponse(self.sojournerPort, "Spirit has no wheels. I need to bring Spirit somewhere where he can be repaired.")
            self.addResponse(self.scribbyPort, "Well, no problem. Use the radio to keep in touch.")
            self.addResponse(self.sojournerPort, "That won't be possible either.")
            self.addResponse(self.scribbyPort, "Why not?")
            self.addResponse(self.sojournerPort, "Lab-RINTH is entirely build within a Faraday cage. Once you're inside, no communication will be possible.")
            self.addResponse(self.scribbyPort, "Why would they do that? Why would they want to keep signals out?")
            self.addResponse(self.sojournerPort,"They didn't. They wanted to keep something in.")
            self.addResponse(self.scribbyPort, [Size(30), Bold("What?!")])
            self.addResponse(self.sojournerPort, "Hurry up, the door is closing! We'll meet each other outside!")
            self.convEvent(self.gate.closeEvent)
            self.addResponse(self.scribbyPort, "")
            self.convEvent(self.winGame)
            self.convStart()
        else:
            if len(key) == 0:
                self.printToSpeechBox("You need to pass the keycard to the use command with: use(key)", self.sojourner.getAvatar())
            elif len(key) > 1:
                self.printToSpeechBox("You need to make sure that you pass only the keycard to the use command, otherwise the keycard won't work.", self.sojourner.getAvatar())
            else:
                self.printToSpeechBox("That was not the keycard I gave you.", self.sojourner.getAvatar())
               
        
    def onPickup(self, item):
        self.addResponse(self.scribbyPort, "Okay, got it!")
        self.addResponse(self.sojournerPort, "Now drive to door and use the key with the use() command.")
        self.addResponse(self.sojournerPort, "Once you are near the door, type use(key) to use the key and open the door.")
        self.convEvent(lambda: self.helpWidgets[1].setButtonsVisible(["use()"], 4))
        self.convEvent(lambda: self.launchBriefScreen(1))
        self.convEvent(lambda: self.setObjectiveText("-Use the keycard on the door."))
        self.addResponse(None, "")
        self.convEvent(self.hidePanel1)
        self.convStart()

        
    def createLevel(self):
        # Add background
        self.setBackground(LEVEL_PATH+"lab1Back.png")  
    
        # Add walls
        self.addObstacle(Polygon((0,0), (0,100), (400, 100), (400, 250), 
                                 (700, 250), (700, 0)), vis=False)
        self.addObstacle(Polygon((400,700), (400,450), (700, 450), (700, 700)), vis=False) 

        
        # Add key
        self.key = self.addActor(Keycard(150, 350, 1, 1337, vis=False, onPickup=self.onPickup))

        # Add gate
        self.gate = self.addActor(DoubleDoor(500, 251, 100, 200))

        # Add NPCs
        self.sojourner = self.quickActor(150, 250, self.sojournerPort, rotation=180)
        self.spirit = self.quickActor(70, 270, self.spiritPort, rotation=135)
        
        # Add terminal
        self.addActor(Terminal(450, 220, 0, 1, self.checkKey, "(x1)", self.key, debug=False))
        
        # Create the robot
        self.createRobotStart(50,350,0)
        
        # Enable help page
        #self.helpWidgets[0].setButtonsVisible("pickup()")
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        # Create briefings
        self.createBriefings()
        
        self.conversation.speechBoxYPos = 550
        
        
    def createBriefings(self):
        text=[]
        text.append(["Drive onto the keycard.\n"])
        text.append(["Pickup the keycard and store it with: ", Mono("key=pickup()"), "\n"])
        text.append(["Here, ", Mono("key"), " is the name of your variable.\n"])
        
        b=simpleBrief(text,[self.getR("pickup()")])                    
        self.addBriefShape(b,"Pickup the keycard and store it.")
        
        text=[]
        text.append(["Drive towards the door.\n"])        
        text.append(["Near the door, use the keycard with: ", Mono("use(key)"),"\n"])    
        text.append(["Note, ", Mono("key"), " is the name of your variable. If you named your variable something else, you will have replace the word key with the word you have chosen.\n"])    
        b=simpleBrief(text,[self.getR("use()")])                    
        self.addBriefShape(b,"Use the keycard on the door.")
           
                                                                                         
def startLevel(gameData=None,parent=None):                  
    return labyrinth1Window(gameData, parent)
    
    
if __name__ == "__main__":
    startLevel()
