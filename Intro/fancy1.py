import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):   
            
        LevelWindow.__init__(self,levelName="Intro5",gameData=gameData,parent=parent,debug=True)                    
        self.setup()

    def buildTestText(self):
        b2 = Briefing(self)
        b2.line("Hello, this is a line.")
        b2.line("Hello, this is another line.")
        b2.line("Hello, this is a line.")
        b2.relatedPages([
            ["forward()","helpPage:forwardPage",Color("red")],
            ["backward()","helpPage:backwardPage",Color("red")],            
            ["talk()","helpPage:talkPage",Color("red")]
            ])
        self.addBriefShape(b2.write(), "Testing related pages.")
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1a.png"),"Respond to Sojourner with talk()")
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1b.png"), "Exit room with forward()")        
        
        self.launchBriefScreen(0)
        #self.showPanel1()
                    
    def onLevelStart(self):    
        #pass
        self.buildTestText()    
        
        #self.buildConvo(0)       
        #no parameters means set all them all to be invisible
        #self.helpWidgets[0].setButtonsInvisible()
        
        
    def createLevel(self):
        pass
        

        
def startLevel(gameData=None,parent=None):
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()