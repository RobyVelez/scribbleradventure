import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(NewLevelWindow):    
    def __init__(self,gameData,parent):      
        
         #hazard objects
        self.fire=[] 
        self.blockade=[]
        self.sojourner=None
        self.scribby=None    
        self.talkBrief=True
            
        
        NewLevelWindow.__init__(self,levelName="Gauntlet1",rStartX=350,rStartY=650,rStartT=-90,
        rEndX=350,rEndY=0,gameData=gameData,parent=parent)                    
        self.setup()
        
        self.briefScreen.visible=False
        
        self.sojourner.speak("Sojourner: (From the kitchen) Scribby! Come eat!.",self.launchBrief)
        
    #Calico will crash if there is no image!!!!
    def getBriefImage(self):
        if self.talkBrief:        
            return LEVEL_PATH+"introBrief1a.png"
        else:
            return LEVEL_PATH+"introBrief1b.png"
        
    def launchBrief(self):
        self.briefScreen.visible=True
        
    def doneTalking(self):
        self.moveBlockade()
        
        #Changes the brief from the talk() instruction to the forward() instruction
        self.talkBrief=False
        self.createBriefScreen()  
        self.launchBrief()
        
    def toggleTalk1(self):
        self.sojourner.talkRadius=1000
        self.scribby.talkRadius=0
        self.briefScreen.visible=False
    def toggleTalk2(self):
        self.sojourner.talkRadius=0
        self.scribby.talkRadius=1000
        self.briefScreen.visible=False
                          
    def stopGame(self):
        self.setGameOver(1)
        
    def blockInRobot(self):
        #hardcodig the height and width of the robot
        height=45
        width=45
        #Upper right, Lower right, Lower left, and Upper left corners of the robot
        ur=(self.robotX+width/2,self.robotY-height/2)
        lr=(self.robotX+width/2,self.robotY+height/2)
        ll=(self.robotX-width/2,self.robotY+height/2)
        ul=(self.robotX-width/2,self.robotY-height/2)

        #adds walls all around the robot so it can't turn or move
        self.blockade.append(self.addObstacle(Rectangle(ur,(lr[0]+1,lr[1]))))
        self.blockade.append(self.addObstacle(Rectangle((ul[0]-1,ul[1]),ll)))
        
        self.blockade.append(self.addObstacle(Rectangle((ul[0],ul[1]-4),(ur[0],ur[1]-1))))        
        self.blockade.append(self.addObstacle(Rectangle((ll[0],ll[1]+2),(lr[0],lr[1]+4))))
        
        #for b in self.blockade:
        #    b.visible=False
            
    def moveBlockade(self):
        #crude function. just moves the blockade away
        for i in range(len(self.blockade)):
            if i==0 or i==1:
                self.blockade[i].move(0,100)
            elif i==2 or i==3:
                self.blockade[i].move(1000,0)
            
    def createObstacles(self):
        #pass
        self.blockInRobot()  
            
    def createHazards(self):
        
        self.sojourner = self.quickActor(600, 650, LEVEL_PATH+"sojournerPortrait.png", speech=True, visible=False, obstructs=False,debug=False)
        self.scribby = self.quickActor(550, 650, LEVEL_PATH+"scribbyPortrait.png", speech=True, visible=False, obstructs=False,debug=False)
        
        #turns sojourner and scribby talk on and off
        self.scribby.talkRadius=10000
        self.sojourner.talkRadius=0            
        
        
        #line of dialogue for the sojourner and scribby
        self.scribby.addResponse("Scribby: I'm not hungry.",action=self.toggleTalk1)        
        self.sojourner.addResponse("Sojourner: (From the kitchen) Come eat now! Don't make me come over there!",action=self.toggleTalk2)
        self.scribby.addResponse("Scribby: Yes ma'am.",action=self.doneTalking)

def startLevel(gameData=None,parent=None):
    #gauntlet1Window("Gauntlet1",700,700,350,650,-90,350,0,None,None,False)#"gauntlet1Back.png","gauntlet1Fore.png",False)                        
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()