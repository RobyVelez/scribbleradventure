import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        self.sojourner=None        
        self.sojournerPort = IMAGE_PATH+"sojournerPortrait.png"
            
        LevelWindow.__init__(self,levelName="Intro6",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    
    def endConvo(self):
        self.pauseRobot(False)
        self.setObjectiveText("-Move forward and enter The Gauntlet.",6)
        
    def revealSojourner(self):
        self.sojourner.show()
        
    def buildConvo(self):
        
        self.addResponse(self.scribbyPort,"This is where the trail stops... right into... into the Gauntlet...")
        self.addResponse(self.scribbyPort,"What am I going to do. The Gauntlet... I can't go in there.")
        self.addResponse(self.sojournerPort,"You have to.",action=self.revealSojourner)
        self.addResponse(self.scribbyPort,"Who...who are you? Where did you come from?")
        self.addResponse(self.sojournerPort,"That's not important right now. All you need to know is that my name is Sojourner.")
        self.addResponse(self.sojournerPort,"You're family was taken and you're the only one left. It's up to you to save them.")
        self.addResponse(self.scribbyPort,"I... I can't I-")
        self.addResponse(self.sojournerPort,"You have to. If you don't no one will. I can guide you along the way. I can help you.")
        self.addResponse(self.scribbyPort,"But I'm not strong enough...")
        self.addResponse(self.sojournerPort,"You are stronger than you know. You just have to believe.")
        self.addResponse(self.scribbyPort,"...ok...",action=self.endConvo)
        
    
    def onLevelStart(self):        
        self.buildConvo()          
        talk()
        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH +"intro6Back.png")

        #Create static objects
        self.addObstacle(Polygon((0,0),(700,0),(700,350),
            (400,350),(400,250),(300,250),(300,350),(0,350)), vis=False)
        #glorified event pads
        self.createEndPad(350,280)
        self.createStartPad(350,675)
        
        self.sojourner=self.quickActor(600, 400, self.sojournerPort, visible=False, obstructs=True,debug=False)
        
        #create the robot
        self.createRobot(350,675,-90)            

        
def startLevel(gameData=None,parent=None):
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
