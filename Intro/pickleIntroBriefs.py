import sys,os,pickle
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"


win=Window("testBrief",500,300)
    
def intro1Briefa():
    b2=Picture(LEVEL_PATH+"introBrief1a.png")
    text=[Wrap(475), SetV(255)]
    text+=[Related("forward()", "helpPage:forwardPage", Color("red")), " "]
    text+=[Related("backward()", "helpPage:backwardPage", Color("red")), " "]        
    text+=[Related("shell", "helpPage:shellPage", Color("purple"))] 
    
    writeText(b2, text)
    
    b2.draw(win)
    output = open('testBrief.pkl', 'wb')
    pickle.dump(b2, output)
    output.close()

intro1Briefa()    
    
