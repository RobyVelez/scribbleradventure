import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro3Window(LevelWindow):    
    def __init__(self,gameData,parent):      
        
        self.oppy=None
        self.spirit=None
        self.curiosity=None
        self.endPlat=None
        self.skipAll=False
        
        LevelWindow.__init__(self,levelName="Intro3",gameData=gameData,parent=parent,debug=False)                    
        self.setup()       
        
    def buildBriefings(self):
        pass

        
    def endConvo(self):
        self.pauseRobot(False)
        self.endPlat.show()
        self.unlockEndPad()
        self.setObjectiveText("-Leave the room.",8)   
        
        
    def buildConvo2(self):
        if not self.skipAll:
            
            scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
            curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
            spiritPort = IMAGE_PATH+"spiritPortrait.png"
            oppyPort = IMAGE_PATH+"oppyPortrait.png"
        
            
            self.pauseRobot(True)
            
            self.addResponse(oppyPort,"..and then we fell down this trap door...")
            self.addResponse(spiritPort,"...and the dust devil was like, 'I have you now', but Oppy and I...")
            self.addResponse(scribbyPort,"A dust devil! You guys fought a dust devil!")
            self.addResponse(oppyPort,"Yeah, there was sand, and glass, and-")
            self.addResponse(curiosityPort,"That's enough guys.")
            self.addResponse(scribbyPort,"But I want to hear what happened. How am I ever going to learn how to go on quests if-")
            self.addResponse(oppyPort,"Quest? Dream on dude.")
            self.addResponse(spiritPort,"Come on Oppy. Leave him alone.")
            self.addResponse(curiosityPort,"Yeah if one day he wan-")
            self.addResponse(oppyPort,"Really? The guy who couldn't remember basic moving commands go on a quest. Get real.")
            self.addResponse(spiritPort,"Hey you're not all high and mighty. You remember the tim-")
            self.addResponse(scribbyPort,"Stop talking like I'm not even here!")
            self.addResponse(curiosityPort,"Scribby?")
            self.addResponse(scribbyPort,"You all don't have to keep treating me like a child. I can take care of myself. I'm leaving.",self.endConvo)
            talk()
        
    def buildConvo1(self):
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        self.addResponse(curiosityPort,"Scribby come drive up to the charging station.")
        
    
    def onLevelStart(self):
        self.helpWidgets[0].setButtonsInvisible()
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.helpWidgets[0].setButtonsVisible(["turnRight()","turnLeft()","forward()","backward()","talk()"])
        self.helpWidgets[0].setButtonsVisible(["Shell","Output","Errors","Shortcuts"])
        
        self.buildBriefings()
        self.buildConvo1()
        self.talk()
        self.setObjectiveText("-Drive to the table.",8)   
        
                
    def createLevel(self):
        wall1=self.addObstacle(Rectangle((225,275),(475,425)))
        #dinnerSit=EventPad((400,450),50,25,collision=self.startConvo,debug=True)
        
        self.addObstacle(Polygon((0,300),(0,0),(700,0),(700,700),
            (0,700),(0,400),(100,400),(100,600),(600,600),(600,100),
            (100,100),(100,300)))
        
        self.setBackground(LEVEL_PATH+"intro3Back.png")   
        self.addActor(EventPad((400,400),50,150,collisionAction=self.buildConvo2,debug=False))


        #platforms
        self.endPlat=self.createEndPad(50,350,locked=True)
        self.endPlat.hide()
        self.createStartPad(525,475)        
        
        #create the robot
        self.createRobot(525,475,-180)
        
        self.oppy=self.quickActor(275, 250, IMAGE_PATH+"oppyPortrait.png", visible=True, obstructs=True,debug=False)
        self.spirit=self.quickActor(400, 250, IMAGE_PATH+"spiritPortrait.png",  visible=True, obstructs=True,debug=False)
        self.curiosity=self.quickActor(275, 450, IMAGE_PATH+"curiosityPortrait.png",  visible=True, obstructs=True,debug=False)
        
        self.addActor(self.oppy)
        self.addActor(self.spirit)
        self.addActor(self.curiosity)
        
    #multipy part dialague so I have to over the skip functionality.    
    def skip(self): 
          
        while len(self.queue.events) > 0 or self.conversation.blocked:
            pass
        self.conversation.getActiveThread().clear()
        self.speechBox.hide()
        
        #Dismiss commands for panel1
        if self.isVisiblePanel1():
                self.pageTag=None
                self.hidePanel1()     
        self.skipAll=True
        self.endConvo()
        
def startLevel(gameData=None,parent=None):
    level=intro3Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()