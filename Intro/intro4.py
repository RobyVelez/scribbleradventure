import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro4Window(LevelWindow):    
    def __init__(self,gameData,parent):      
        
        self.scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        self.curiosityPort=IMAGE_PATH+"curiosityPortrait.png"
                
        LevelWindow.__init__(self,levelName="Intro4",gameData=gameData,parent=parent,debug=False)                    
        self.setup()
        
    
    def buildBriefings(self):
        text=[]
        text.append(["Use your ",self.getI("motors()")," command to get around the curves."])
        text.append(["motors(0.5,1) is a good place to start."])
        text.append(["motors() allows you to set the speed of the left and right wheels independently."])
        text.append(["Note, you have to do motors(0,0) or stop() in order to stop moving."])
        
        b=simpleBrief(text,[self.getR("motors()")])                    
        self.addBriefShape(b,"Use motors() command to navigate curves.")
       
    def endConvo1(self):
        self.pauseRobot(False)
        #self.launchBriefScreen(0)
        dismiss()
    
    def endConvo3(self):
        self.pauseRobot(False)
        self.setObjectiveText("-Rush back home.",4)
        
    def buildConvo1(self):
        self.addResponse(self.scribbyPort,"Ugh! They make me so angry.")
        self.addResponse(self.scribbyPort,"I'm not just a child. I can take care of myself.")
        self.addResponse(self.scribbyPort,"I mean here. I can... I can move around these curves with my motors() command.")
        self.addResponse(self.scribbyPort,"Yes, my motors() command allows me to set the speed of my left and right wheel independently.",lambda:self.helpWidgets[0].setButtonsVisible(["motors()"]))
        self.addResponse(self.scribbyPort,"See, I can remember my commands.",lambda:self.launchBriefScreen(0))
        self.addResponse(self.scribbyPort," ",self.endConvo1)
    
    def buildConvo2(self):
        self.pauseRobot(True)
        self.addResponse(self.scribbyPort,"But I shouldn't have run out like that.")
        self.addResponse(self.scribbyPort,"I know they're all just trying to protect me.")
        self.addResponse(self.scribbyPort,"But know I could protect myself, protect them.",lambda:self.pauseRobot(False))
        talk()    
        
    def buildConvo3(self):
        self.pauseRobot(True)
         
        self.addResponse(Picture(LEVEL_PATH+"fightCloud.png") ,"(Sounds of shouting and a fight.)")
        self.addResponse(self.scribbyPort,"What's that sound. What's going on.")
        self.addResponse(self.curiosityPort,"Oppy and Spirit run! Don't let them captu-")
        self.addResponse(self.scribbyPort,"That's was Curiosity! They're being attacked. I have go help them.",action=self.endConvo3)
        talk()
        
    def onLevelStart(self):
        self.setObjectiveText("-Walk around the green house.")
        self.pauseRobot(True)
        self.buildBriefings()
        self.buildConvo1()   
        talk() 
                                                                                
    def createLevel(self):
        curve1=Polygon()
        curve1.append((250,75))
        curve1.append((600,75))
        curve1.append((600,700))
        
        angle=0
        orig=[250,700]
        step=-0.1
        r=335
        while(angle>-pi/2):
            curve1.append((r*cos(angle)+orig[0],r*sin(angle)+orig[1]))            
            angle+=step
        self.addObstacle(curve1)
        
        curve2=Polygon()
        curve2.append((250,700))
        curve2.append((0,700))
        curve2.append((0,0))
        curve2.append((25,0))
        
        angle=-pi/2
        orig=[250,225]
        step=-0.1
        r=235
        while(angle>(-3*pi)/2):
            curve2.append((r*cos(angle)+orig[0],r*sin(angle)+orig[1]))            
            angle+=step
        self.addObstacle(curve2)
        
        #self.addObstacle(Rectangle((475,625),(485,700)))
        self.addObstacle(Circle((250,700),240))                               
        self.addObstacle(Circle((250,225),140))
        
        self.setBackground(LEVEL_PATH+"intro4Back.png")   
        
        
        thoughtPad1=EventPad((200,400),100,100,collisionAction=self.buildConvo2,debug=False)
        self.addActor(thoughtPad1)
        
        thoughtPad2=EventPad((300,50),100,100,collisionAction=self.buildConvo3,debug=False)        
        self.addActor(thoughtPad2)

        self.createEndPad(675,700)
        self.createStartPad(535,665)
        
        self.createRobot(535,665,-90)
        
        
    def skip(self):
        while len(self.queue.events) > 0 or self.conversation.blocked:
            pass
        self.conversation.getActiveThread().clear()
        self.speechBox.hide()
        
        #Dismiss commands for panel1
        if self.isVisiblePanel1():
                self.pageTag=None
                self.hidePanel1()
                
        self.endConvo3()        
def startLevel(gameData=None,parent=None):
    level=intro4Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()