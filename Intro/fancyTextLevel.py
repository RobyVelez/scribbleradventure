import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):   
            
        LevelWindow.__init__(self,levelName="Intro5",gameData=gameData,parent=parent,debug=True)                    
        self.setup()

    def buildTestText(self):
        tempHelpRec = Rectangle((0,0), (500,500), color=Color("white"))
        writeText(tempHelpRec, ['This is some text.\n'])
        self.addBriefShape(tempHelpRec,"Testing help page")
        self.launchBriefScreen(0)
                    
    def onLevelStart(self):    
        #pass
        self.buildTestText()    
        
        #self.buildConvo(0)       
        #no parameters means set all them all to be invisible
        #self.helpWidgets[0].setButtonsInvisible()
        
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1a.png"),"Respond to Sojourner with talk()")
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1b.png"), "Exit room with forward()")        
        
    def createLevel(self):
        pass
        

        
def startLevel(gameData=None,parent=None):
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()