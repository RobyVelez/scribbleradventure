import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):   
    
        self.robotRadio=LEVEL_PATH+"robotRadio.png"
        self.scribbyPort = LEVEL_PATH+"scribbyPortrait.png"
        self.radioFindEventPad=None
            
        LevelWindow.__init__(self,levelName="Intro5",gameData=gameData,parent=parent,debug=True)                    
        self.setup()

    def foundRadio(self):
        self.buildConvo(3)
        if self.radioFindEventPad:
            self.radioFindEventPad.hide()
        self.pauseRobot(False)        
                
    def endThought1(self): 
        self.setObjectiveText("-Enter the Gauntlet.",10)   
        self.pauseRobot(False)
        #self.showPanel1()
    
    def buildConvo(self,i,auto=True):
        #auto means do you want this funciton to automatically
        #pause the robot and launch the convo or do you want to do it manually
        #in some other function
        
        if auto:
            self.pauseRobot(True)
        
        if i==0:                
            self.addResponse(self.scribbyPort,"This is where the trail stops...")
            self.addResponse(self.robotRadio,"......<static>.....<static>...we'll never coope-...<static>")
            self.addResponse(self.scribbyPort,"The signal has been getting stronger. They're definitely in... in there... in the Gauntlet")
            self.addResponse(self.scribbyPort,"I've heard tales of the Gauntlet and they didn't end well. Once I enter there's no turning back...")
            self.addResponse(self.scribbyPort,"But I have to go. I have to save my family. I can do this. I've been wanting to go on a quest. I can do this. I can get my family back. I can do this...",action=self.endThought1)
                          
        if auto:
            talk()   
                    
    def onLevelStart(self):        
        self.buildConvo(0)       
        #no parameters means set all them all to be invisible
        #self.helpWidgets[0].setButtonsInvisible()
        
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1a.png"),"Respond to Sojourner with talk()")
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief1b.png"), "Exit room with forward()")        
        
    def createLevel(self):
        
        #self.setBackground(LEVEL_PATH+"woodFloor2.png")   
    
        #Create static objects
        self.addObstacle(Polygon((0,0),(700,0),(700,350),
            (400,350),(400,250),(300,250),(300,350),(0,350)))
        #glorified event pads
        self.createEndPad(350,280)
        self.createStartPad(350,675)
        
        #create the robot
        self.createRobot(350,675,-90)       
        

        
def startLevel(gameData=None,parent=None):
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()