import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *
from fancyTextFunctions import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro1Window(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.timedTalkTime=50   
        LevelWindow.__init__(self,levelName="Intro1",gameData=gameData,parent=parent,debug=False)                    
        self.setup()
        
    def timedTalk(self):
        self.addStepEvent(TimedEvent(self.timedTalkTime,lambda:talk()))
        
    
    def revealTalkShell(self):
        #set the first help page visible snd have it flash 3 times
        self.helpWidgets[0].setButtonsVisible(["talk()","Shell","Shortcuts"],4)
        self.launchBriefScreen(0)
        
    def endConvo(self):    
        #set the second and third help page visible and have them flash 3 times
        self.helpWidgets[0].setButtonsVisible(["forward()","backward()","Shortcuts"],4)        
        
        self.launchBriefScreen(1)
        self.setObjectiveText("-Exit the room via the green circle.")
             
        self.robots[0].pauseRobot=False     
        dismiss() 
    
    def createBriefings(self):
        text=[]
        text.append(["Click the shell tab to bring the ",self.getI("Shell")," to front.\n"])
        text.append(["Type the ",self.getI("talk()")," command in the shell and hit Enter to response. Click on the Help Pages to the right for more info.\n"])
        #hack to get an inline button for Dismiss. Note it doesn't do anything.
        text.append(["Moving Scribby or clicking on ",Inline2("Dismiss","blank",Color("white"))," on the bottom panel to make any of these info screens go away. \n"])
        text.append(["See the ",self.getI("Shortcuts")," Help Page for useful key shortcuts and other info.\n"])
        
        b=simpleBrief(text,[self.getR("Shell"),self.getR("talk()"),self.getR("Shortcuts")])                    
        self.addBriefShape(b,"Respond to Curiosity with talk()")
        
        text=[]
        text.append(["Type forward(2,1) in the shell to get Scribby to move.\n"])        
        text.append(["The numbers '2' and '1' correspond to Scribby's speed and for how many seconds he'll move.\n"])        
        text.append(["Try different numbers for different speeds and movement durations. Check out the ",self.getI("forward()")," Help Page for more info.\n"])
        text.append(["Moving Scribby, clicking on ",Inline2("Dismiss","blank",Color("white"))," on the bottom panel, hitting the 'd' key, or typing dismiss() in the shell will make these info screens go away. \n"])
        b=simpleBrief(text,[self.getR("Shell"),self.getR("forward()")])                    
        self.addBriefShape(b,"Exit room with forward()")
    
    def createDialog(self):
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"

        self.addResponse(curiosityPort, "Scribby! It's time to recharge. Come to the kitchen.",self.timedTalk)
        self.addResponse(scribbyPort, "...",self.timedTalk)        
        self.addResponse(curiosityPort, "Scribby are you there? Do a talk() in the shell to respond.",self.revealTalkShell)
        self.addResponse(scribbyPort, "'Oh yeah that's how I talk.' Yeah Curiosity I'm coming... wait how do I move again.")
        self.addResponse(curiosityPort, "Use your forward() or backward() command to move forward and backward and leave your room.")
        self.addResponse(curiosityPort, " ",self.endConvo)
        
        
    def onLevelStart(self):
        #no parameters means set all them all to be invisible
        self.helpWidgets[0].setButtonsInvisible()
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.createDialog()
        self.createBriefings()
        
        self.setObjectiveText("-Response to Curiosity with talk() in the shell.")       
        self.pauseRobot(True)
        
        talk()
        
        
                        
    def createLevel(self):        
        
        self.addObstacle(Polygon((0,0),(300,0),(300,200),(100,200),
            (100,600),(600,600),(600,200),(400,200),(400,0),(700,0),
            (700,700),(0,700)))   
    
        self.setBackground(LEVEL_PATH+"intro1Back.png")   
        
        #glorified event pads
        self.createEndPad(350,0)
        self.createStartPad(350,500)
        
        #create the robot
        self.createRobot(350,500,-90)  
        
        
def startLevel(gameData=None,parent=None):
    level=intro1Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    