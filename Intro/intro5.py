import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class intro5Window(LevelWindow):    
    def __init__(self,gameData,parent):   
    
        self.scribbyPort = LEVEL_PATH+"scribbyPortrait.png"
        self.skipAll=False
        
        LevelWindow.__init__(self,levelName="Intro5",gameData=gameData,parent=parent,debug=True)                    
        self.setup()

    
    def endConvo1(self): 
        self.setObjectiveText("-Investigate the house.",10)   
        self.pauseRobot(False)
        
    def buildConvo(self,i,auto=True):
        #auto means do you want this funciton to automatically
        #pause the robot and launch the convo or do you want to do it manually
        #in some other function
        if not self.skipAll:
            
            if auto:
                self.pauseRobot(True)        
            if i==0:                
                self.addResponse(self.scribbyPort,"What happened here? Everything's in shambles.")
                self.addResponse(self.scribbyPort,"Where is everyone?... I need to search the house.",action=self.endConvo1)
            elif i==1:
                self.addResponse(self.scribbyPort,"The door's been ripped off the hinges. Someone forced their way in here.",action=lambda:self.pauseRobot(False))
            elif i==2:
                self.addResponse(self.scribbyPort,"This place is a wreck. What happened here. Where is everyone. Curiosity... Spirit... Oppy...",action=lambda:self.pauseRobot(False))
            elif i==3:
                self.addResponse(self.scribbyPort,"What's going on?",lambda:self.pauseRobot(False))
            elif i==4: 
                self.addResponse(self.scribbyPort,"What's this? Looks like drag marks. I think I've found a trail",lambda:self.pauseRobot(False))
            elif i==5:
                self.addResponse(self.scribbyPort,"The drag marks continue down the road.")
                self.addResponse(self.scribbyPort,"I have to follow it and find them.",lambda:self.pauseRobot(False))
                        
            if auto:
                talk()   
                        
    def onLevelStart(self):        
        pass
        self.buildConvo(0)   
        
    def createLevel(self):
        self.setBackground(LEVEL_PATH +"intro5Back.png")
        
        #Create static objects
        self.addObstacle(Polygon((100,200),(600,200),(600,500),(500,500),(500,400),(300,400),(300,300),(125,300),(125,400),(100,400)),vis=False)
        
        self.addObstacle(Polygon((100,700),(100,500),(125,500),(125,600),(300,600),(300,500),(400,500),(400,600),(700,600),(700,700)),vis=False)
    
        debris1=Rectangle((175,0),(225,200))
        debris1.rotate(45)
        self.addObstacle(debris1,vis=False)
        
        debris2=Rectangle((400,40),(600,90))
        debris2.rotate(23)
        self.addObstacle(debris2,vis=False)
        
        #thoughtPad1=EventPad((50,450),50,50,collision=self.testCollision,debug=True)
        self.addActor(EventPad((50,450),50,50,collisionAction=lambda:self.buildConvo(1),debug=False))
        self.addActor(EventPad((300,450),50,50,collisionAction=lambda:self.buildConvo(2),debug=False))
        
        self.addActor(EventPad((650,550),50,50,collisionAction=lambda:self.buildConvo(3),debug=False))        
        
        self.addActor(EventPad((650,200),50,50,collisionAction=lambda:self.buildConvo(4),debug=False))
        self.addActor(EventPad((350,150),50,50,collisionAction=lambda:self.buildConvo(5),debug=False))
        
        #glorified event pads
        self.createEndPad(350,50)
        self.createStartPad(50,50)
        
        #create the robot
        self.createRobot(50,50,90)       
        
    def skip(self):
        while len(self.queue.events) > 0 or self.conversation.blocked:
            pass
        self.conversation.getActiveThread().clear()
        self.speechBox.hide()
        
        #Dismiss commands for panel1
        if self.isVisiblePanel1():
                self.pageTag=None
                self.hidePanel1()
        
        self.skipAll=True
        self.pauseRobot(False)
def startLevel(gameData=None,parent=None):
    level=intro5Window(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()