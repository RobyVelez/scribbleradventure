import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"

class intro2Window(LevelWindow):    
    def __init__(self,gameData,parent):      
        self.oppy=None
        self.spirit=None
        self.curiosity=None
        
        #how fast the robots move after their dialog
        self.exitSpeed=40
        
        LevelWindow.__init__(self,levelName="Intro2",gameData=gameData,parent=parent,debug=False)                    
        self.setup()
        
    def addCallback(self):
        self.setObjectiveText("-Try turnleft(2,1) in the Shell.")
        self.launchBriefScreen(0)
        self.setErrorCallback(self.oppyTrick)
        
    def oppyTrick(self,e,command):
        self.dismiss()
        oppyPort = IMAGE_PATH+"oppyPortrait.png"
        self.setErrorCallback(None)
        if command.find("turnright")!=-1:
            self.addResponse(oppyPort,"Sure, you know your movement commands alright.")
        else:
            self.addResponse(oppyPort,"Man you're clueless.")        
        self.addResponse(oppyPort," ",self.spiritCue)
        
        talk()
        
            
    def buildBriefings(self):
        text=[]
        text.append(["Try the command turnright(2,1) in the ",self.getI("Shell")])
        #text.append(["You might want to keep an eye on the ",self.getI("Output")," area for any ",self.getI("Errors")])
        
        #b=simpleBrief(text,[self.getR("Output"),self.getR("Errors")])                    
        
        b=simpleBrief(text,[self.getR("Shell")])                    
        self.addBriefShape(b,"Oppy's dubious advice.")
       
        text=[]
        text.append(["Make it to the end of the corridor to get to the kitchen."])
        text.append(["Use you're ",self.getI("turnRight()")," and ",self.getI("turnLeft()")," commands to get around the corners."])
        text.append(["Remember to type commands exactly as they are presented and keep an eye on you're ",self.getI("Output")," for potential ",self.getI("Errors")])
        
        b=simpleBrief(text,[self.getR("turnRight()"),self.getR("turnLeft()"),self.getR("Output"),self.getR("Errors")])                    
        self.addBriefShape(b,"Get to end of corridor.")
       
       
        #self.addBriefShape(Picture(LEVEL_PATH+"introBrief2.png"),"Turn with turnRight() and turnLeft()")
        
    def buildConvo(self):
        
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"
        oppyPort = IMAGE_PATH+"oppyPortrait.png"
        
        #First dialogue w/ Oppy
        self.addResponse(oppyPort,"Hmm, looks like someone forgot their basic movement commands again.")        
        self.addResponse(scribbyPort,"No I haven't! I know my movement commands.")
        
        self.addResponse(oppyPort,"Oh really. Why don't you try to do a turn then.")
        self.addResponse(scribbyPort,"Uh... well if I wanted to turn right at a speed of 2 for 1 second, wouldn't the command be turnright(2,1)?")
        self.addResponse(oppyPort,"Why don't you give it a try in the Shell. <snickers>",self.addCallback)
    
    
    
    def onLevelStart(self):   
        self.conversation.setDebug(False)
        self.helpWidgets[0].setButtonsInvisible()
        self.helpWidgets[1].setButtonsInvisible()
        self.helpWidgets[2].setButtonsInvisible()
        self.helpWidgets[3].setButtonsInvisible()
        
        self.helpWidgets[0].setButtonsVisible(["talk()","forward()","backward()","Shell"])
        
        self.pauseRobot(True)
        self.buildConvo()
        self.buildBriefings()
        talk()
        self.oppy.show()
        self.addStepEvent(self.oppy.step)
        self.addStepEvent(self.spirit.step)
        self.addStepEvent(self.curiosity.step)
        
        #Starts the conversation w/ oppy
        #self.oppyCue() 
    
    def createLevel(self):    
        self.wall1=self.addObstacle(Polygon((0,0),(700,0),(700,400),
            (350,400),(350,100),(0,100)))
        wall2=self.addObstacle(Polygon((0,200),(250,200),(250,500),
            (700,500),(700,700),(0,700)))        
        self.setBackground(LEVEL_PATH+"intro2Back.png")   
        
        #platforms
        self.createEndPad(0,150)
        self.createStartPad(650,450)
        
        #create the robot
        self.createRobot(650,450,-180)
        
        self.oppy=SimpleMovingNPC([(400,450),(300,450),(300,150),(25,150)],IMAGE_PATH+"oppyPortrait.png",visible=False,endAction="hide")
        self.spirit=SimpleMovingNPC([(475,450),(300,450),(300,150),(25,150)],IMAGE_PATH+"spiritPortrait.png",visible=False,endAction="hide")
        self.curiosity=SimpleMovingNPC([(575,450),(300,450),(300,150),(25,150)],IMAGE_PATH+"curiosityPortrait.png",visible=False,endAction="hide")        
        
        self.addActor(self.curiosity)
        self.addActor(self.oppy)
        self.addActor(self.spirit)
        
    def spiritCue(self):
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        spiritPort = IMAGE_PATH+"spiritPortrait.png"
        
        
        self.oppy.run=True        
        self.spirit.show()        
               
        
        self.addResponse(scribbyPort,"Grrr!!!... What did I do wrong?")
        self.addResponse(spiritPort,"The turning right command is turnRight(2,1), not turnright(2,1).",lambda:self.helpWidgets[0].setButtonsVisible(["turnRight()","turnLeft()"],4))
        self.addResponse(scribbyPort,"Isn't that what I did?")
        self.addResponse(spiritPort,"The 'R' is capitialized. If commands aren't entered properly they won't work. ")
        self.addResponse(scribbyPort,"How will I know if I enter a command correctly?")
        self.addResponse(spiritPort,"When in doubt look at your output screen for errors.",lambda:self.helpWidgets[0].setButtonsVisible(["Errors","Output"],4))
        self.addResponse(scribbyPort,"<grumbles> ...alright",self.curiosityCue)
        talk()
        
        
        
    def curiosityCue(self):  
        self.spirit.run=True
        self.curiosity.show()
        
        scribbyPort = IMAGE_PATH+"scribbyPortrait.png"
        curiosityPort = IMAGE_PATH+"curiosityPortrait.png"
        
        self.addResponse(curiosityPort,"Hey Scribby. Awesome you made it out of your room on your own.")
        self.addResponse(scribbyPort,"Uh, yeah. You know you don't have to keep helping me like this. I can figure it out.")
        self.addResponse(curiosityPort,"Nonsense. What is a big sister for if not to nag you.")
        self.addResponse(curiosityPort,"Now hurry on. And don't let Oppy and Spirit get to you. Now follow me to the charging station.",lambda:self.launchBriefScreen(1))
        self.addResponse(curiosityPort," ",action=self.endConvo)
        
    def endConvo(self):
        self.setObjectiveText("-Make your way down the hallway.")
        self.curiosity.run=True
        self.pauseRobot(False)
        self.launchBriefScreen(1)
        self.setObjectiveText("-Use turnRight() and turnLeft() commands to get down hallway.")
        dismiss()
    
    #a lot of complicated dialogue so I have to overwrite the basic skip() functions        
    def skip(self):
        while len(self.queue.events) > 0 or self.conversation.blocked:
            pass
        self.conversation.getActiveThread().clear()
        self.speechBox.hide()
        
        #Dismiss commands for panel1
        if self.isVisiblePanel1():
                self.pageTag=None
                self.hidePanel1()
        
        self.pauseRobot(False)
        self.curiosity.hide()
        self.oppy.hide()
        self.spirit.hide()
        
        

def startLevel(gameData=None,parent=None):
    level=intro2Window(gameData,parent)
    return level
        
if __name__ == "__main__":
    startLevel()
