from Graphics import* 

#Probably best to do these side games as classes, but here is an example 
#just using functions.

#If you want to use this file as a template for your own game that we add to the LRC Hall
#of Fame then you just need:
#1. the def endGame(o,e): function which launches the score recording procedure
#2. the the def start(gameData=None,parent=None): function which initializes everything
#3. edit the diversionsWindow.py in the /WindowObjects folder and add your game
#You can ask Roby if you have any questions


#global variables that are changed by one of the functions
win=None
highScoreText=None
parent=None
score=0

#global variable, but is not changed by any of the functions
scoreStep=1
    
def updateScore():
    global highScoreText    
    
    highScoreText.setText(str(score))

#click callback
def incrementScore(o,e):
    global score
    
    score=score+scoreStep
    updateScore()   
#click callback
def decrementScore(o,e):
    global score
    
    score=score-scoreStep
    updateScore()
#click callback
def endGame(o,e): 
    #following lines of code are what record the score to the Hall of Fame
    
    #makes these variables editable
    global parent,win,score    
    
    #brings back the parent window which should be diversionsWindow.py
    if parent!=None:
        parent.showWindow()  
    
    #destroys this window so it disappears
    win.Destroy()
    
    #CRUCIAL!!! Tells the parent, which is diversionsWindow.py, to launch the score recording process
    parent.updateHallOfFame("IncrementScore",score)
    
    #resets the score so that if you run this program again is resets to 0. you don't necessarily have to do this
    score=0
   
   
def IncrementScore(gameData=None,passedParent=None):
    global parent,highScoreText,win
    parent=passedParent
    
    win=Window(600,600)

    #Create objects
    buttonUp=Rectangle((100,100),(200,200),color=Color("green"))
    buttonUpText=Text((150,150),"+"+str(scoreStep),color=Color("black"))
    
    buttonDown=Rectangle((200,100),(300,200),color=Color("red"))
    buttonDownText=Text((250,150),"-"+str(scoreStep),color=Color("black"))
    


    highScoreLabel=Text((400,100),"High Score",color=Color("black"),fontSize=24)
    highScoreText=Text((400,150),str(score),color=Color("black"),fontSize=24)

    endButton=Rectangle((100,300),(200,400),color=Color("blue"))
    endButtonLabel=Text((150,350),"End",color=Color("black"),fontSize=24)

    buttonUp.draw(win)
    buttonUpText.draw(win)
    buttonDown.draw(win)
    buttonDownText.draw(win)
    highScoreLabel.draw(win)
    highScoreText.draw(win)
    endButton.draw(win)
    endButtonLabel.draw(win)
    
    buttonUp.connect("click",incrementScore)
    buttonDown.connect("click",decrementScore)
    endButton.connect("click",endGame)

    return win


#For the most part you can change the variable name 'incrementScoreWindow' to something
#else, as well as the name of the main funciton 'IncrementScore', but aside from that
#everything else has to stay put of it won't be proplery called from diversionsWindow.py
incrementScoreWindow=None 
def start(gameData=None,parent=None):    
    global incrementScoreWindow
    incrementScoreWindow=IncrementScore(gameData,parent)
    return incrementScoreWindow
        
if __name__ == "__main__":
    start()

