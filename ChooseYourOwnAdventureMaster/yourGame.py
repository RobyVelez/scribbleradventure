from gameTemplate import *

################################
#########Game Scenes############
################################

def museumSceneProxy(o,e):
    refreshWindow()
    museumScene()
    
def museumScene():
    pic=Picture("evilRobot.jpeg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot found the Transponder, but he was an \n"+
            "evil robot so you've just doomed the world. \n"+ 
            "The End.")
    
    bText1.setText("restart")
    clickMap[button2.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button1.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def librarySceneProxy(o,e):
    refreshWindow()
    libraryScene()
    
def libraryScene():    
    pic=Picture("readingRobot.jpg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot didn't find the Transponder at the\n"+
    "Library. Instead he sat around all day reading\n"+
    "books by Issac Asimov. The End")
    
    bText1.setText("restart")
    clickMap[button2.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button1.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def mallSceneProxy(o,e):
    refreshWindow()
    mallScene()
    
def mallScene():    
    pic=Picture("dinoZords.gif")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot, which was evil, ran amook in the mall \n"+
    "but a gang of super heros showed up and \n"+
    " stopped him. The End")
            
    bText1.setText("restart")
    clickMap[button2.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button1.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def yesScene():
    pic1=Picture("museum.png")
    pic2=Picture("library.png")
    pic2.move(300,0)
    pic3=Picture("mall.png")
    pic3.move(600,0)
    pic1.draw(win)
    pic2.draw(win)
    pic3.draw(win)
    pic1.tag="temp"
    pic2.tag="temp"
    pic3.tag="temp"
    
    prompt.setText("Where is it?")    
    
    bText1.setText("museum")
    clickMap[button1.tag]=museumScene
    
    bText2.setText("library")
    clickMap[button2.tag]=libraryScene    
   
    bText3.setText("mall")
    clickMap[button3.tag]=mallScene
    
    bText4.setText("...")
    
    #Extras
    pic1.connect("click",museumSceneProxy)
    pic2.connect("click",librarySceneProxy)
    pic3.connect("click",mallSceneProxy)

def noScene():
    pic=Picture("sadRobot.jpeg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot is very sad you can't help him and \n"+
        "walks away. The End")
    
    bText1.setText("quit")  
    clickMap[button1.tag]=quitGame
    
    bText2.setText("restart")
    clickMap[button2.tag]=initialScene

    bText3.setText("...")
    
    bText4.setText("...")
    
def talkScene():
    pic=Picture("tesseract.png")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot says he's looking for an old relic called\n"+ 
    "the Cybertronic Energy Transponder. Have\n"+
    " you heard of it? ")

    bText1.setText("yes")
    clickMap[button1.tag]=yesScene
    
    bText2.setText("no")
    clickMap[button2.tag]=noScene
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def runScene():
    pass

def yellScene():
    pic1=Picture("largeImage.png")
    pic1.draw(win)
    pic1.tag="temp"
    
    pic2=Picture("yelling.jpg")
    pic2.draw(win)
    pic2.scale(0.5)
    pic2.tag="temp"
    

def initialScene():
    pic=Picture("ironGiant.png")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("You are walking down the street when a giant \n"+
            "robot walks up to you and wonders if he could \n"+
            "ask for directions. What do you do? Talk to the\n"+
            "giant robot, run away, or yell because there's a \n"+
            "giant robot?")
    
    
    bText1.setText("talk")
    clickMap[button1.tag]=talkScene
    
    bText2.setText("run")    
    clickMap[button2.tag]=runScene
    
    bText3.setText("yell")    
    clickMap[button3.tag]=yellScene
    
    bText4.setText("...")
    
    
initialScene()  

