from Graphics import *
from time import time,sleep
from math import floor

#if a window already exists get its position and
#then close it
position=None
try:
    win=getWindow()
    
    if win != None:
        position=win.GetPosition()
        win.Destroy()
except:
    pass
    
###############################
########Initial Setup##########
###############################

#set to True if you are debugging the game
debug = False

#window width and height respectively
w=800
h=700

#width and height of inventory box
bW=200
bH=100

#max width or height of icons in the inventory box
iMax=50

win=Window("Your game.",w,h)
win.setBackground(Color("gray"))

#prevents window from bouncing around on restart
if position != None:
    win.Move(position[0],position[1]+21) 

#window dressing
line1=Line((0,h-100),(w,h-100))
line1.draw(win)

#create inventory box
iBox=Rectangle((w-bW,h-bH),(w,h))
iBox.fill=Color("white")
iBox.draw(win)
iBox.tag="inventory box"


#create the main text
mText1=Text((0,h-100),"MainText1",color=Color("black"),fontSize=18)
#VERY IMPORTANT!!!, shifts the text to the right of origin
mText1.xJustification="left"
mText1.yJustification="top"
#draw main text to window
mText1.draw(win)

    
def refreshWindow():
    win.removeTagged("temp")
    win.removeTagged("item") 
    keyMap.clear()
    clickMap.clear()
    inventory=[]
    win.update()
    
#dictionary that will map button 
#clicks to a new scene
clickMap={}                             
    
#dictionary that maps the w,a,s,d keys to scenes
keyMap={}

#holds items picked up
inventory=[]

#helper function that checks to see if a picture
#with the same filename is already in the inventory
def inInventory(pic):                              
    for i in inventory:
        if i.filename==pic.filename:
            return True
    return False                          

#add item to the inventory and update
#the icon list
def updateInventory(item):
    
    #clears all icons from window
    win.removeTagged("icon")
    
    #tags item as an icon
    item.tag="icon"
    
    #reduces size of the picture so it fits the icon box
    s=1
    while item.width*s > iMax or item.height*s > iMax:
        s=s-0.01
    item.scale(s)
    
    #adds the item to the inventory and draw it
    inventory.append(item)
    for i in range(len(inventory)):
        win.draw(inventory[i])
        
        #maths to properly place the icons in the icon box
        #if you want to change how the icons look try changing
        #iMax of the bW or bH first
        dX=iBox.getP1()[0] + (i*iMax) % bW + (inventory[i].width*inventory[i].scaleFactor)/2
        dY=iBox.getP1()[1] + (floor((i*iMax)/bW)+0.5)*(inventory[i].height*inventory[i].scaleFactor)
        inventory[i].moveTo(dX,dY)
        
        
    
    
##################################
###Mouse Clicks and Key Presses###
##################################

    
def mouseDown(obj,event):
    
    foundItems=[]
    #iterate through all shapes and find any items that
    #were clicked
    for s in win.canvas.shapes:
        if s.tag=="item" and s.hit(event.x,event.y):
            foundItems.append(s)
    
    #assuming the item is represented by a Picture
    #make a copy of the Picture and add it to the invetory
    #remove the original item from the window
    for i in foundItems:
        if isinstance(i,Picture):
            updateInventory(Picture(i.filename))
        win.canvas.shapes.Remove(i)
    
    win.update()        
    
    
    print(event.x,event.y)    
    
    #the sleep prevents the user from clicking
    #rapidly and skipping between scenes
    sleep(0.2)


def keyPressed(obj,event):
    
    if event.key=="q" or event.key=="Q":
        getWindow().Destroy()
    if event.key in keyMap:
        nextScene=keyMap[event.key]
        refreshWindow()
        nextScene()

    
win.onMouseDown(mouseDown)
win.onKeyPress(keyPressed)


