from gameTemplate import *

def restartGame():
    initialScene()
def quitGame():
    getWindow().Destroy()
    

def hall1A():
    pic=Picture("hall1A.png")
    pic.draw(win)
    pic.tag="temp"
    
    mText1.setText("Front Door Boarded Up.")
    
    #forward
    keyMap["w"]=hall1A
    #right
    keyMap["d"]=hall1B
    #left
    keyMap["a"]=hall1D
    #back
    keyMap["s"]=hall1C

def hall1B():
    pic=Picture("hall1B.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=hall1B
    #right
    keyMap["d"]=hall1C
    #left
    keyMap["a"]=hall1A
    #back
    keyMap["s"]=hall1D

def hall1C():
    pic=Picture("hall1C.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=room1C
    #right
    keyMap["d"]=hall1D
    #left
    keyMap["a"]=hall1B
    #back
    keyMap["s"]=hall1D

def hall1D():
    pic=Picture("hall1D.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=hall1D
    #right
    keyMap["d"]=hall1A
    #left
    keyMap["a"]=hall1C
    #back
    keyMap["s"]=hall1B

def room1C():
    pic=Picture("room1C.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=room1C
    #right
    keyMap["d"]=room1D
    #left
    keyMap["a"]=room1B
    #back
    keyMap["s"]=room1A
    
    key=Picture("key.png")
    if not inInventory(key):
        key.scale(0.15)
        key.tag="item"
        key.draw(win)
        key.setTransparent(Color("white"))
        key.outline.alpha=0
        key.moveTo(550,475)
    
def room1B():
    pic=Picture("room1B.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=room1B
    #right
    keyMap["d"]=room1C
    #left
    keyMap["a"]=room1A
    #back
    keyMap["s"]=room1D
    
    
def room1D():
    pic=Picture("room1D.png")
    pic.draw(win)
    pic.tag="temp"
    
    #forward
    keyMap["w"]=room1D
    #right
    keyMap["d"]=room1A
    #left
    keyMap["a"]=room1C
    #back
    keyMap["s"]=room1B
    

def room1A():
    pic=Picture("room1A.png")
    pic.draw(win)
    pic.tag="temp"    
    
    key=Picture("key.png")
    if inInventory(key):
        keyMap["w"]=hall1A
        mText1.setText(" ")
    else:        
        #forward
        keyMap["w"]=room1A
        mText1.setText("Door Locked.")
        
    #right
    keyMap["d"]=room1B
    #left
    keyMap["a"]=room1D
    #back
    keyMap["s"]=room1C
        

def initialSetup(): 
    room1A()

    
            
initialSetup()

   
    