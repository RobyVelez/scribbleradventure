from Graphics import *
from Myro import ask, askQuestion
from storyMethods import *


'''
Hint if you have multiple layers of if and elif than break 
it up into functions.
'''

def myStory():
    if True:       
        storyPic(["ironGiant.png"])
        ans1 = askQuestion("You are walking down the street when a giant robot \n"+
            "walks up to you and wonders if he could ask for directions. \n"+
            "What do you do? Talk to the giant robot, run away, or yell \n"+
            "because there's a giant robot?",["talk","run","yell"])
        getWindow().close()
        if ans1 == "talk":
            talk1()
        elif ans1 == "run":
            run1()
        elif ans1 == "yell" or ans1 == "Yell":
            askQuestion("You yelled so the robot panicked \n"+
            "and ran away.",["The End"])
        else:
            response = askQuestion("You just stood there and didn't say \n"+
            "anything so the robot left.",["The End"])   
    else:
    
        print("Not sure how you got here.")

def talk1():

    storyPic(["tesseract.png"])
    ans2 = ask("The robot says he's looking for an old relic called the \n"+
    "Cybertronic Energy Transponder. Have you heard of it? (yes/no)")
    getWindow().close()
    
    if ans2 == "no" or ans2 == "No":
        askQuestion("The robot is very sad you can't help him and walks away",["The End"])
    else:
        storyPic(["museum.png","library.png","mall.png"])        
        ans2 = askQuestion("Is it at the:",["museum", "library", "mall"])
        getWindow().close()
        
        if ans2 == "museum":
            askQuestion("The robot found the Transponder, but he was an evil \n"+
            "robot so you've just doomed the world.",["The End"])
        elif ans2 == "library":
            askQuestion("The robot didn't find the Transponder at the Library. \n"+
            "Instead he sat around all day reading books by Issac Asimov",["The End"])
        elif ans2 == "mall":
            askQuestion("The robot, which was evil, ran amook in the mall but \n"+
            "a gang of good robots showed up and stopped him.", ["The End"])
        else:
            askQuestion("You didn't answer correctly so the robot just walked \n"+
            "away. It winded up at the museum were it found the device and used \n"+
            "it for evil because it was an evil robot.", ["The End"])

def run1():
    storyPic(["train.png"])
    ans3 = askNumber("You run away from the giant robot to the train station. There are \n"+
        "two train platforms 9 and 10. Which platform do you get on?")
    getWindow().close()
    
    if ans3 == 9:
        storyPic(["whiteRabbit.png"])
        ans9 = askNumber("You are about to board the train when you bump into a white \n"+
        "rabbit looking at his watch which seems broken. He looks at you and asks you \n"+
        "what hour it is. (military time)")
        getWindow().close()
        
        if ans9 <12 and ans9>22:
            ans10 = askQuestion("The rabbit breathes a sigh of relief and calmly strolls down \n"+
            "platform. Instead of getting on the train you decide to follow him.",["To Be Continue"])

        elif ans9 >=12 and ans9<15:
            ans10 = askQuestion("Oh no I'm late. The rabbit runs off down the \n"+
            "platform. Instead of getting on the train you decide to follow him.",["To Be Continue"])

        elif ans9 >=15 and ans9<22:
            ans10 = askQuestion("Oh no I'm really late! She'll have my head! The rabbit sprints off down the \n"+
            "platform. Instead of getting on the train you decide to follow him.",["To Be Continue"])
        else:
            ans10 = askQuestion("You give the rabbit a funny answer. He doesn't know what time it is so he decides \n"+
            "to sprint off down the platform. Instead of getting on the train you decide to follow him.",["To Be Continue"])

    elif ans3 == 10:
        conductor()     

    elif ans3 == 9.75:
        askQuestion("You found the hidden platform 9 3/4. Your going to Hogwarts!!!"
        ,["The End"])
    else:
        askQuestion("You couldn't make a valid decision and were too afraid of the \n"+
        "giant robot so you stayed in the train station forever.",["The End"])


def conductor():
    
    storyPic(["trainConductor.png"])
    ans4 = ask("You get on the train and run into the conductor. He asks you if \n"+
    "you have a ticket. Do you? (yes/no)")
    getWindow().close()
    
    if ans4 == "yes" or ans4 =="Yes":
        askQuestion("The conductor somehow knows you're lying and kicks you \n"+
        "off the train.", ["The End"])
    elif ans4 == "no" or ans4 == "No":
        ans5 = askNumber("The conductor says you need a ticket. If you have enough \n"+
        "money you can buy it off of him. How much money do you have?")

        if ans5>10 and ans5<1000:
            middleEarth()
        elif ans5 <0 or ans5 >1000:
            askQuestion("No one has negative dollars or carries around more 1000 dollars \n"+
            "The conductor kicks you off for trying to be funny.",["The End"])
        elif ans5 >=0 and ans5 <10:
            askQuestion("Sorry says the condutor, but that is not enough and he kicks \n"+
                "off the train.",["The End"])
        else:
            askQuestion("No proper answer so the conductor kicks you off the train.",["The End"])

    else:
        askQuestion("You don't respond properly so the conductor kicks you off \n"+
        "The train", ["The End"])

                                
def middleEarth():
    storyPic(["troll.png","goblin.png","dwarf.png"])
    ans6 = askQuestion("You have enough money so you buy a ticket and try to find \n"+
    "a seat but the only empty seats are near very strange passengers. Do you \n"+
    "sit next to the troll reading the newspaper, the goblin with a bag of coins \n"+
    "or the sleeping dwarf?",["troll","goblin","dwarf"])
    getWindow().close()
    
    if ans6 =="troll":
        askQuestion("You sit by the troll and wind up having a very pleasant \n"+
        "conversation about various subjects. When the troll's stop comes he \n"+
        "gives you his address and invites you to a barbecue he's having next \n"+
        "weekend.",["The End"])
    elif ans6 == "goblin":
        askQuestion("You sit by the goblin, but he is fairly rude and mean. \n"+
        "Half an hour into the train ride he accuses you of stealing some of \n"+
        "his gold. The conductor believes him and tosses you from the train.",["The End"])
    elif ans6 == "dwarf":
        ans7 = askQuestion("You sit by the dwarf and he doesn't stir. After an hour he \n"+
        "wakes up and looks at you. He asks you how sneaky are you?",["very","not Very"])

        if ans7 == "very":
            askQuestion("You say you are very sneaking so the dwarf leans in and says \n"+
            "have you ever fought a dragon before?", ["To Be Continued"])
        else:
            askQuestion("You say are actually not sneaky and fairly clumsy. The dwarf \n"+
            "say oh, and falls back to sleep.", ["To Be Continued"])

