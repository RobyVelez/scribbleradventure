from Graphics import *
from time import time,sleep

#if a window already exists get its position and
#then close it
position=None
try:
    win=getWindow()
    
    if win != None:
        position=win.GetPosition()
        win.Destroy()
except:
    pass
    
###############################
########Initial Setup##########
###############################

#set to True if you are debugging the game
debug = False

#window width and height respectively
w=800
h=700

#button width and height respectively
bW=100
bH=50


win=Window("Your game.",w,h)
win.setBackground(Color("gray"))

#prevents window from bouncing around on restart
if position != None:
    win.Move(position[0],position[1]+21) 

#window dressing
line1=Line((0,h-100),(w,h-100))
line1.draw(win)


#create clickable buttons
button1=RoundedRectangle((w-2*bW,h-2*bH),(w-bW,h-bH),5)
button2=RoundedRectangle((w-bW,h-2*bH),(w,h-bH),5)
button3=RoundedRectangle((w-2*bW,h-bH),(w-bW,h),5)
button4=RoundedRectangle((w-bW,h-bH),(w,h),5)
#set color of buttons
button1.fill=Color("blue")
button2.fill=Color("red")
button3.fill=Color("green")
button4.fill=Color("orange")
#set default tags to buttons.
#used in sceneMap to map buttons to funcitons that launch scenes
button1.tag="button1"
button2.tag="button2"
button3.tag="button3"
button4.tag="button4"

#draw buttons to window
button1.draw(win)
button2.draw(win)
button3.draw(win)
button4.draw(win)


#create text objects for buttons
#draws them to the center of the buttons
bText1=Text((button1.x,button1.y),"Empty text",color=Color("black"),fontSize=12)
bText2=Text((button2.x,button1.y),"Empty text",color=Color("black"),fontSize=12)
bText3=Text((button3.x,button3.y),"Empty text",color=Color("black"),fontSize=12)
bText4=Text((button4.x,button4.y),"Empty text",color=Color("black"),fontSize=12)
#draw text to window, in center of buttons
bText1.draw(win)
bText2.draw(win)
bText3.draw(win)
bText4.draw(win)

#create the main text
mText1=Text((0,h-100),"MainText1",color=Color("black"),fontSize=18)
#VERY IMPORTANT!!!, shifts the text to the right of origin
mText1.xJustification="left"
mText1.yJustification="top"
#draw main text to window
mText1.draw(win)



#create clickable buttons on the left and right for transitions
clickBoxR=Rectangle((725,150),(800,500))
clickBoxR.tag="clickBoxR"
clickBoxR.draw(win)
if not debug:
    clickBoxR.visible=False

clickBoxL=Rectangle((0,150),(75,500))
clickBoxL.tag="clickBoxL"
clickBoxL.draw(win)
if not debug:
    clickBoxL.visible=False
    
def refreshWindow():
    win.removeTagged("temp")
    win.removeTagged("item") 
    keyMap.clear()
    win.update()
    
#dictionary that will map buttons to
#a new scene
sceneMap={}

#dictionary that will hold items
itemMap={}                                
    
#dictionary that maps the w,a,s,d keys to scenes
keyMap={}
                            
################################
#########Game Scenes############
################################

## def museumSceneProxy(o,e):
##     refreshWindow()
##     museumScene()
##     
## def museumScene():
##     pic=Picture("evilRobot.jpeg")
##     pic.draw(win)
##     pic.tag="temp"
##     
##     mText1.setText("The robot found the Transponder, but he was an \n"+
##             "evil robot so you've just doomed the world. \n"+ 
##             "The End.")
##     
##     bText1.setText("quit")
##     button1.tag="quitGame"
##     
##     bText2.setText("restart")
##     button2.tag="restartGame"
##     
##     bText3.setText("...")
##     button3.tag="..."
##     
##     bText4.setText("...")
##     button4.tag="..."
##     
## def librarySceneProxy(o,e):
##     refreshWindow()
##     libraryScene()
##     
## def libraryScene():    
##     pic=Picture("readingRobot.jpg")
##     pic.draw(win)
##     pic.tag="temp"
##     
##     mText1.setText("The robot didn't find the Transponder at the\n"+
##     "Library. Instead he sat around all day reading\n"+
##     "books by Issac Asimov. The End")
##     
##     bText1.setText("quit")
##     button1.tag="quitGame"
##     
##     bText2.setText("restart")
##     button2.tag="restartGame"
##     
##     bText3.setText("...")
##     button3.tag="..."
##     
##     bText4.setText("...")
##     button4.tag="..."
##     
## def mallSceneProxy(o,e):
##     refreshWindow()
##     mallScene()
##     
## def mallScene():    
##     pic=Picture("dinoZords.gif")
##     pic.draw(win)
##     pic.tag="temp"
##     
##     mText1.setText("The robot, which was evil, ran amook in the mall \n"+
##     "but a gang of super heros showed up and \n"+
##     " stopped him. The End")
##             
##     bText1.setText("quit")
##     button1.tag="quitGame"
##     
##     bText2.setText("restart")
##     button2.tag="restartGame"
##     
##     bText3.setText("...")
##     button3.tag="..."
##     
##     bText4.setText("...")
##     button4.tag="..."
##     
## def yesScene():
##     pic1=Picture("museum.png")
##     pic2=Picture("library.png")
##     pic2.move(200,0)
##     pic3=Picture("mall.png")
##     pic3.move(400,0)
##     pic1.draw(win)
##     pic2.draw(win)
##     pic3.draw(win)
##     pic1.tag="temp"
##     pic2.tag="temp"
##     pic3.tag="temp"
##     
##     mText1.setText("Where is it?")    
##     
##     bText1.setText("museum")
##     button1.tag="museumScene"
##     sceneMap[button1.tag]=museumScene
##     
##     bText2.setText("library")
##     button2.tag="libraryScene"
##     sceneMap[button2.tag]=libraryScene    
##    
##     bText3.setText("mall")
##     button3.tag="mallScene"
##     sceneMap[button3.tag]=mallScene
##     
##     bText4.setText("...")
##     button4.tag="..."
## 
##     #Extras
##     pic1.connect("click",museumSceneProxy)
##     pic2.connect("click",librarySceneProxy)
##     pic3.connect("click",mallSceneProxy)
## 
## def noScene():
##     pic=Picture("sadRobot.jpeg")
##     pic.draw(win)
##     pic.tag="temp"
##     
##     mText1.setText("The robot is very sad you can't help him and \n"+
##         "walks away. The End")
##     
##     bText1.setText("quit")  
##     button1.tag="quitGame"
##     
##     bText2.setText("restart")
##     button2.tag="restartGame"
##     
##     bText3.setText("...")
##     button3.tag="..."
##     
##     bText4.setText("...")
##     button4.tag="..."
##     
## def talkScene():
##     pic=Picture("tesseract.png")
##     pic.draw(win)
##     pic.tag="temp"
##     
##     mText1.setText("The robot says he's looking for an old relic called\n"+ 
##     "the Cybertronic Energy Transponder. Have\n"+
##     " you heard of it? ")
## 
##     bText1.setText("yes")
##     button1.tag="yesScene"
##     sceneMap[button1.tag]=yesScene
##     
##     bText2.setText("no")
##     button2.tag="noScene"
##     sceneMap[button2.tag]=noScene
##     
##     bText3.setText("...")
##     button3.tag="..."
##     
##     bText4.setText("...")
##     button4.tag="..."
## 
## def runScene():
##     pass
## 
## def yellScene():
##     pic1=Picture("largeImage.png")
##     pic1.draw(win)
##     pic1.tag="temp"
##     
##     pic2=Picture("yelling.jpg")
##     pic2.draw(win)
##     pic2.scale(0.5)
##     pic2.set
##     pic2.tag="temp"
##     
## 
## def initialScene():
##     pic=Picture("ironGiant.png")
##     pic.draw(win)
##     pic.tag="temp"
## 
##     mText1.setText("You are walking down the street when a giant \n"+
##             "robot walks up to you and wonders if he could \n"+
##             "ask for directions. What do you do? Talk to the\n"+
##             "giant robot, run away, or yell because there's a \n"+
##             "giant robot?")
##     
##     
##     bText1.setText("talk")
##     button1.tag="talkScene"
##     sceneMap[button1.tag]=talkScene
##     
##     bText2.setText("run")    
##     button2.tag="runScene"
##     sceneMap[button2.tag]=runScene
##     
##     bText3.setText("yell")    
##     button3.tag="yellScene"
##     sceneMap[button3.tag]=yellScene
##     
##     bText4.setText("...")
##     button4.tag="..."
##     
## initialScene()  

################################
#######Mouse Clicks#############
################################

    
def mouseDown(obj,event):
    tag=None
    if button1.hit(event.x,event.y):
        tag=button1.tag
    elif button2.hit(event.x,event.y):
        tag=button2.tag
    elif button3.hit(event.x,event.y):
        tag=button3.tag
    elif button4.hit(event.x,event.y):
        tag=button4.tag
    elif clickBoxL.hit(event.x,event.y):
        tag=clickBoxL.tag    
    elif clickBoxR.hit(event.x,event.y):
        tag=clickBoxR.tag    
    
    
    #iterate through all shapes and find any that are itmes
    for s in window.canvas.shapes:
        if s.tag=="item":
            
    if tag == None:
        #if none of the buttons were clicked then
        #the tag is still None so do nothing
        pass
    elif tag not in sceneMap:
        #tag was not added to the sceneMap and mapped to
        #a function        
        print("Error!!! No matching key in sceneMap for:")
        print(tag)
        
    else: 
        print(tag)  
        refreshWindow()
        nextScene=sceneMap[tag]
        nextScene()
        '''
        if tag=="...": #do nothing
            pass
        elif tag=="restartGame":
            refreshWindow()
            initialScene()
        elif tag=="quitGame":
            win.Destroy()
        else:
            nextScene=sceneMap[tag]
            nextScene()
        '''    
    print(event.x,event.y)   
    
    #the sleep prevents the user from clicking
    #rapidly and skipping between scenes
    sleep(0.2)

def keyPressed(obj,event):
    
    if event.key in keyMap:
        nextScene=keyMap[event.key]
        refreshWindow()
        nextScene()

    
win.onMouseDown(mouseDown)
win.onKeyPress(keyPressed)


