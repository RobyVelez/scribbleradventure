from gameTemplate import *

win.title="Just Another Day"

def museumSceneProxy(o,e):
    museumScene()
    
def museumScene():
    refreshScreenPanel() 
    pic=Picture("evilRobot.jpeg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot found the Transponder, "+
            "but he was an \n"+
            "evil robot so you've just doomed the world. \n"+ 
            "The End.")
    
    bText1.setText("restart")
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button2.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def librarySceneProxy(o,e):
    libraryScene()
    
def libraryScene():    
    refreshScreenPanel() 
    pic=Picture("readingRobot.jpg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot didn't find "+
    "the Transponder at the\n"+
    "Library. Instead he sat around all day reading\n"+
    "books by Issac Asimov. The End")
    
    bText1.setText("restart")
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button2.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def mallSceneProxy(o,e):
    mallScene()
    
def mallScene(): 
    refreshScreenPanel()   
    pic=Picture("dinoZords.gif")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot, which was evil, "+
    "ran amook in the mall \n"+
    "but a gang of super heros showed up and \n"+
    " stopped him. The End")
            
    bText1.setText("restart")
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")  
    clickMap[button2.tag]=quitGame
    
    bText3.setText("...")
    
    bText4.setText("...")
    
def yesScene():
    refreshScreenPanel() 
    pic1=Picture("museum.png")
    pic2=Picture("library.png")
    pic2.move(300,0)
    pic3=Picture("mall.png")
    pic3.move(600,0)
    pic1.draw(win)
    pic2.draw(win)
    pic3.draw(win)
    pic1.tag="temp"
    pic2.tag="temp"
    pic3.tag="temp"
    
    prompt.setText("Where is it?")    
    
    bText1.setText("museum")
    clickMap[button1.tag]=museumScene
    
    bText2.setText("library")
    clickMap[button2.tag]=libraryScene    
   
    bText3.setText("mall")
    clickMap[button3.tag]=mallScene
    
    bText4.setText("...")
    
    #Extras
    pic1.connect("click",museumSceneProxy)
    pic2.connect("click",librarySceneProxy)
    pic3.connect("click",mallSceneProxy)
    
def noScene():
    refreshScreenPanel()
    pic=Picture("sadRobot.jpeg")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot is very sad you can't help him and \n"+
        "walks away. The End")
    
    bText1.setText("restart")  
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")
    clickMap[button2.tag]=quitGame

    bText3.setText("...")
    
    bText4.setText("...")
    
def talkScene():
    refreshScreenPanel() 
    pic=Picture("tesseract.png")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText("The robot says he's looking for an old relic called\n"+ 
    "the Cybertronic Energy Transponder. Have\n"+
    " you heard of it? ")

    bText1.setText("yes")
    clickMap[button1.tag]=yesScene
    
    bText2.setText("no")
    clickMap[button2.tag]=noScene
    
    bText3.setText("...")
    
    bText4.setText("...")
    
    keyMap["y"]=yesScene
    keyMap["n"]=noScene
    
def runScene():
    refreshScreenPanel()
    head=Circle((615,175),25)
    head.fill=Color("red")
    body=Line((600,200),(500,350))
    body.setWidth(10)
    body.outline=Color("blue")
    arm1=Line((600,225),(640,275),(715,250))
    arm1.setWidth(10)
    arm2=Line((575,215),(515,215),(475,250))
    arm2.setWidth(10)
    leg1=Line((550,300),(600,325),(600,400))
    leg1.setWidth(10)
    leg2=Line((500,350),(425,350))
    leg2.setWidth(10)
    
    rChest=Polygon((75,150),(275,150),(225,300),(125,300))
    rHead=Circle((175,115),35)
    rSLeft=Circle((65,175),20)
    rSRight=Circle((285,175),20)
    rArm1=Rectangle((275,200),(300,300))
    rArm2=Rectangle((50,200),(75,300))
    rLeg1=Rectangle((175,300),(225,450))
    rLeg2=Rectangle((125,300),(175,450))
    
    head.draw(win)
    body.draw(win)
    arm1.draw(win)
    arm2.draw(win)
    leg1.draw(win)
    leg2.draw(win)
    rArm1.draw(win)
    rArm2.draw(win)
    rLeg1.draw(win)
    rLeg2.draw(win)
    
    rChest.draw(win)
    rHead.draw(win)
    rSLeft.draw(win)
    rSRight.draw(win)
    
    head.tag="temp"
    body.tag="temp"
    
    #pic=Picture("underConstruction.png")
    #pic.draw(win)
    #pic.tag="temp"
    
    prompt.setText("Run away!!!")
        
    bText1.setText("restart")  
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")
    clickMap[button2.tag]=quitGame

    bText3.setText("...")
    
    bText4.setText("...")

def yellScene():
    refreshScreenPanel()
    pic=Picture("underConstruction.png")
    pic.draw(win)
    pic.tag="temp"
    
    prompt.setText(" ")    
    
    bText1.setText("restart")  
    clickMap[button1.tag]=initialScene
    
    bText2.setText("quit")
    clickMap[button2.tag]=quitGame

    bText3.setText("...")
    
    bText4.setText("...")

def initialScene():
    refreshScreenPanel()    
    pic=Picture("ironGiant.png")
    pic.draw(win)
    pic.tag="temp"    
    pic.scale(2.5)
    pic.moveTo(400,250)    
    
    prompt.setText("You are walking down the street when a giant \n"
        +"robot walks up to you. Do you talk to the giant robot, \n"
        +"run away, or yell?")
        
    
    bText1.setText("talk")
    clickMap[button1.tag]=talkScene
    
    bText2.setText("run")    
    clickMap[button2.tag]=runScene
    
    bText3.setText("yell")    
    clickMap[button3.tag]=yellScene
    
    bText4.setText("...")    
    
initialScene()

