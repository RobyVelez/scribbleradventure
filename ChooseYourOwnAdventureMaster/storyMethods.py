from Graphics import *
from Myro import ask, askQuestion

win = None

'''
filenames must be in the form of a list:
["pic1.png","pic2.png","pic3.png"]

vertBuffer is the distance between question
prompt and top of images

'''
def storyPic(filenames,vertBuffer = 200):
    pics=[]
    maxHeight=0
    totalWidth=0   
    
    for f in filenames:
        pics.append(Picture(f))
        #pics[-1] grabs the last thing appended
        if pics[-1].height>maxHeight:
            maxHeight=pics[-1].height
        totalWidth+=pics[-1].width
    
    global win    
    win = Window("Story",totalWidth,maxHeight+vertBuffer)
    win.Visible=False
    win.setBackground(Color("black"))
    last = 0
    lastX = 0
    for p in pics:
        p.draw(win)
        p.moveTo(lastX+p.width/2,p.height/2+vertBuffer)
        last+=1
        lastX+=p.width
    win.Visible=True
        
def askNumber(msg):
    ans = ask(msg)
    try:
        newValue = float(ans)
    except ValueError:
        newValue = -9999
    return newValue