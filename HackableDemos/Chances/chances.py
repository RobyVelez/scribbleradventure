from Graphics import *
from Myro import randomNumber, Random, show, wait
import os


###########################Steps to Hack###########################
#1. Substitute your own picture filename. Actual picture must live in the /Chances folder
#2. Change the number of rows or number of columns
pic = Picture("testChancesImage.png")
numRows=12
numCols=16
####################################################################

#Title of window, can be changed
winTitle="What are the chances?"          

#thickness of walls that hold falling images
#added to the total width and height of window
wallT=10

#Add space at the left, right, and bottom of the window for
#The wall that holds the falling picture segments
win = Window(winTitle, 2+pic.width+2*wallT, 1+pic.height+wallT)

#turns on physics simulator so we can simulate things like gravity and collision
win.mode = "physics"

#width and height of the pictue segments
segWidth=pic.width/numCols
segHeight=pic.height/numRows

#walls that hold falling picture segments
leftWall = Rectangle((0, -1000), (0+wallT, win.height-wallT))
leftWall.bodyType = "static"
leftWall.draw(win)
rightWall = Rectangle((win.width - wallT, -1000), (win.width, win.height - wallT))
rightWall.bodyType = "static"
rightWall.draw(win)
ground = Rectangle((0, win.height - wallT), (win.width, win.height))
ground.bodyType = "static"
ground.draw(win)

#makes the falling picture segments
for i in range(numRows*numCols):
    b = Rectangle((0, 0), (segWidth, segHeight))
    b.moveTo(win.width/2 + i, 0 - i * (2*segHeight))
    b.bounce = .98
    b.draw(win)
    if os.path.exists("seg-%s.png" % i):
        sect = Picture("seg-%s.png" % i)
        sect.draw(b)
        sect.move(-segWidth/2, -segHeight/2)


#removes all segment pictues in current directory
def clearPics():
    for f in os.listdir("."):
        #print(f)
        #looks for files that start with 'sect-' and end with '.png'
        if f[0:5]=="seg-" and f.endswith(".png"):
            os.remove(f)
    

       
def makePics():   
    count=0 
    for j in range(numRows):
        for i in range(numCols):
            seg = pic.getRegion((i*segWidth+segWidth/2,j*segHeight+segHeight/2), segWidth, segHeight, 0)
            pic.setRegion((i*segWidth+segWidth/2,j*segHeight+segHeight/2), segWidth, segHeight, 0, Color("black"))
            seg.savePicture("seg-"+str(count)+".png")
            
            count=count+1
    pic.savePicture("missing.png")        
            

getMouse()
win.run()

# run once, then run makePics()



'''

from Graphics import *
from Myro import randomNumber, Random, show, wait
import os


###########################Steps to Hack###########################
#1. Substitute your own picture filename. Actual picture must live in the /Chances folder
#2. Change the number of rows or number of columns
#3. Smaller hacks labeled as 'hackable'

pic = Picture("LogoLRC.png")
numRows=10
numCols=10

####################################################################


#Create the box the holds the falling picture segments
width=pic.width
height=pic.height

#Hackables
winTitle="What are the chances?"          
wallColor="blue"        
thickenss=25            

win = Window(winTitle, width, height)
win.mode = "physics"

leftWall = Rectangle((0, -1000), (thickness, height - thickness))
wall.bodyType = "static"
wall.draw(win)
wall = Rectangle((width - thickness, -1000), (width, height - thickness))
wall.bodyType = "static"
wall.draw(win)

ground = Rectangle((0, height - thickness), (width, height))
ground.bodyType = "static"
ground.draw(win)


#if os.path.exists(calico.relativePath("../examples/images/chances/missing.jpg")):
#    background = Picture(calico.relativePath("../examples/images/chances/missing.jpg"))
#    win.canvas.shapes.Add(background)

thickness = 25

balls = []
for i in range(250):
    ball = Rectangle((10, 10), (42, 42))
    ball.moveTo(width/2 + i, 0 - i * 40)
    ball.bounce = .98
    ball.draw(win)
    balls.append(ball)
    if os.path.exists(calico.relativePath("../examples/images/chances/" + ("sect-%s.png" % i))):
        sect = Picture(calico.relativePath("../examples/images/chances/" + ("sect-%s.png" % i)))
        sect.draw(ball)
        sect.move(-16, -16)

def makePics():
    i = 0
    for ball in balls:
        if 0 < ball.center.x < 640 and 0 < ball.center.y < 480:
            sect = pic.getRegion(ball.center, ball.width, ball.height, ball.rotation)
            pic.setRegion(ball.center, ball.width, ball.height, ball.rotation, Color("black"))
            sect.savePicture(calico.relativePath("../examples/images/chances/" + ("sect-%s.png" % i)))
        i += 1
    pic.savePicture(calico.relativePath("../examples/images/chances/missing.jpg"))

getMouse()
win.run()

# run once, then run makePics()

'''