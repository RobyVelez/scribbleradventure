# Perform imports
from Graphics import *
import random

# Constants
WINDOW_WIDTH = 700
WINDOW_HEIGHT = 700
PLAYER_JUMP = 6
PLAYER_EXTRA_JUMP = 20
PLAYER_SPEED = 6
GRAVITY = 0.3
FRAME_RATE = 0.015
PLATFORM_SPEED = 1
PLATFORM_SPEED_MODIFIER = 0.01


# Setup window
window = Window("Simple platformer", WINDOW_WIDTH, WINDOW_HEIGHT)
window.mode = "manual"
window.setBackground(Color("White"))


# Draw a circle and a platform
circle = Circle((200, 200), 10)
window.draw(circle)
platforms = []
platforms.append(Line((0, 700), (200, 700)))
platforms.append(Line((500, 600), (700, 600)))
platforms.append(Line((200, 500), (400, 500)))
platforms.append(Line((100, 400), (200, 400)))
platforms.append(Line((400, 300), (600, 300)))
platforms.append(Line((100, 200), (300, 200)))
platforms.append(Line((300, 100), (500, 100)))
for platform in platforms:
    platform.tag = "notHit"
    platform.border = 3
    window.draw(platform)
    
    
# Draw score counter
scoreCounter = Text((10,10), "Score: 0")
scoreCounter.setXJustification("left")
scoreCounter.setYJustification("top")
scoreCounter.color = Color("black")
scoreCounter.draw(window)


# Initialize variables
xVelocity = 0
yVelocity = 0
holdUpward = 0
previousY = circle.getY()
score = 0


# Define our collision function
def collision(currentX, currentY, previousY, platform):
    if currentY < platform.getP1().y:
        return False
    if previousY > platform.getP1().y:
        return False
    if currentX < platform.getP1().x:
        return False
    if currentX > platform.getP2().x:
        return False
    return True


# Start the control loop
while window.IsRealized:
    # Perform simple collision detection
    onGround = False
    for platform in platforms:
  
        # Perform collision detection
        hit = False
        if collision(circle.getX(), circle.getY(), previousY, platform):
            onGround = True
            hit = True
            if platform.tag == "notHit":
                platform.tag = "hit"
                platform.outline = Color("green")
                score += 1
                scoreCounter.setText("Score: " + str(score))
            
        # Move platforms down
        platform.move(0, PLATFORM_SPEED + score * PLATFORM_SPEED_MODIFIER)
            
        # Update player position
        if hit:
            circle.setY(platform.getY())
            
        # Reset platform at the top
        if platform.getY() >= WINDOW_HEIGHT:
            platform.moveTo(random.randint(0, WINDOW_WIDTH), 0)
            platform.outline = Color("black")
            platform.tag = "notHit"
            
    # Check if you fell of the world
    if circle.getY() >= WINDOW_HEIGHT:
        break

    # Handle gravity
    if onGround:
        yVelocity = 0
    else:
        yVelocity += GRAVITY
    
    # Handle variable jumpheight
    if holdUpward > 0:
        holdUpward -= 1
    
    # Handle user input
    xVelocity = 0
    if window.getKeyPressed('Up') and (onGround or holdUpward>0): 
        yVelocity = -PLAYER_JUMP
        if onGround:
            holdUpward = PLAYER_EXTRA_JUMP
    if window.getKeyPressed('Left'):
        xVelocity = -PLAYER_SPEED
    if window.getKeyPressed('Right'): 
        xVelocity = PLAYER_SPEED
        
    # Update position
    previousY = circle.getY()
    circle.move(xVelocity, yVelocity)
    
    # Regulate window update
    window.step(FRAME_RATE)