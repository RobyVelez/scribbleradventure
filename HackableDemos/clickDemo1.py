from Graphics import *

win=Window("Click Demo",700,300)

rect1=Rectangle((100,100),(200,200),color=Color("red"))
rect1.tag="red" #keep track of what color this rectangle is

rect2=Rectangle((300,100),(400,200),color=Color("blue"))
rect2.tag="blue" #keep track of what color this rectangle is

rect3=Rectangle((500,100),(600,200),color=Color("purple"))
rect3.tag="purple" #keep track of what color this rectangle is

rect1.draw(win)
rect2.draw(win)
rect3.draw(win)

rect1Label=Text((100,75),"Click down \nchanges color.",xJustification="left",yJustification="bottom",color=Color("black"))
rect2Label=Text((300,75),"Click up \nchanges color.",xJustification="left",yJustification="bottom",color=Color("black"))
rect3Label=Text((500,75),"Moving mouse\nover changes color.",xJustification="left",yJustification="bottom",color=Color("black"))

rect1Label.draw(win)
rect2Label.draw(win)
rect3Label.draw(win)


def mouseDown(o,e):
    print("Clicked at ",e.x,e.y)
    
    if rect1.hit(e.x,e.y):
        if rect1.tag=="red":
            rect1.fill=Color("green")
            rect1.tag="green"
        elif rect1.tag=="green":
            rect1.fill=Color("red")
            rect1.tag="red"
            
def mouseUp(o,e):
    print("Up at ",e.x,e.y)
    
    if rect2.hit(e.x,e.y):
        if rect2.tag=="blue":
            rect2.fill=Color("orange")
            rect2.tag="orange"
        elif rect2.tag=="orange":
            rect2.fill=Color("blue")
            rect2.tag="blue"
            
def mouseMove(o,e):
    #print("Move at ",e.x,e.y)
    
    if rect3.hit(e.x,e.y):
        rect3.fill=Color("yellow")
        rect3.tag="yellow"            

    else:
        rect3.fill=Color("purple")
        rect3.tag="pule"
                                


#creates a window wide mouse monitoring
win.onMouseDown(mouseDown)
win.onMouseUp(mouseUp)
win.onMouseMovement(mouseMove)