from Graphics import *
import GLib

win=Window("Click Demo",400,400)
ball1=Circle((200,200),5,color=Color("red"))
ball1.draw(win)

#Demonstrates two mehods for registering key presses and using them
#to move a shape.
#Method 1 is the simpliest, but doesn't allow multiple key presses as same time
#Method 2 is a bit more complicate but does allow multiple key presses as same time
#Note there are two methods for reseting the ball. Both are in affect in this code. 
#In a real applicaiton you would only need one if you did want to reset of key release

#########################
########Method 1#########
#########################

def keyRelease(o,e):
    ball1.moveTo(200,200)
    
def keyPress(o,e):
    #print("Move at ",e.x,e.y)
    
    if e.key=="w":
        ball1.move(0,5)
    elif e.key=="s":
        ball1.move(0,-5)
    elif e.key=="a":
        ball1.move(-5,0)
    elif e.key=="d":
        ball1.move(5,0)
    else:
        print("Key pressed was ",e.key)


#creates a window wide mouse monitoring
win.onKeyPress(keyPress)
win.onKeyRelease(keyRelease) #key releases resets the ball


#########################
########Method 2#########
#########################

#a function that executes every 100 miliseconds
#useful for animations or continously monitoring key
#presses
def backgroundLoop():
    
    if win.getKeyPressed("Shift_L") and win.getKeyPressed("Left"):
        ball1.move(5,0)
    elif win.getKeyPressed("Shift_L") and win.getKeyPressed("Right"):
        ball1.move(-5,0)
    elif win.getKeyPressed("Shift_L") and win.getKeyPressed("Up"):
        ball1.move(0,-5)
    elif win.getKeyPressed("Shift_L") and win.getKeyPressed("Down"):
        ball1.move(0,5)
    else:
        ball1.moveTo(200,200)
        
    #remains in the Timeout loop as long as it return True
    return True   
    
#way of adding a function to run in the background        
GLib.Timeout.Add(100,backgroundLoop)
    