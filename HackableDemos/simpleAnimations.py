from Graphics import *
from Myro import *

win=Window("Animations1",600,600)

ball=Circle((200,200),20)
ball.draw(win)

#draws two images to the ball
fireDownPic=Picture("fireballDown.png")
fireUpPic=Picture("fireballUp.png")

#You can center the pictures ontop of the ball
#by moving half it's width to the left and half it's hieght up
#Uncomment for yourself to see what it does
#fireDownPic.move(-fireDownPic.width/2,-fireDownPic.height/2)
#fireUpPic.move(-fireUpPic.width/2,-fireUpPic.height/2)


fireDownPic.draw(ball)
fireUpPic.draw(ball)

#make the the fireballUp, invisible
fireUpPic.visible=False

#flag that tells us which direction the ball is moving
direction="down"    


ceiling=Rectangle((0,75),(600,100),color=Color("black"))
ceiling.draw(win)

floor=Rectangle((0,550),(600,575),color=Color("black"))
floor.draw(win)

mode="mode1" #while loop for animation
#mode="mode2" #for loop for animation

if mode=="mode1":
    while True: #infinite loop. Will loop forever until you kill the program (hit the red circle)
        if ball.y>550: #ball is below the floor change direction to 'up'
            direction="up"
            
            #swaps visibility of images drawn on ball
            fireUpPic.visible=True
            fireDownPic.visible=False
            
        elif ball.y<100: #ball is above the ceiling
            direction="down"
            
            #swaps visibility of images drawn on ball
            fireUpPic.visible=False
            fireDownPic.visible=True
    
        if direction=="down":
            ball.move(0,5)
        elif direction=="up":
            ball.move(0,-5)
        wait(0.1)

elif mode=="mode2":
    
    start=100
    stop=500
    step=5
    #not an infinite loop. will move the ball to (200,100), then (200,105), then (200,110), etc    
    for i in range(start,stop,step):
        #note here we're using moveTo not move. There is a difference
        #move is relative to current position and moveTo is absolute location within the window
        ball.moveTo(200,i)
        wait(0.1)