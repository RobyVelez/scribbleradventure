from Graphics import *


# Constants
TURN_SPEED = 5
MOVE_SPEED = 3
TANK_SIZE = 50
BULLET_SIZE = 5
BULLET_SPEED = 10
BULLET_OFFSET = TANK_SIZE/2 + BULLET_SIZE + 5
BULLET_COOLDOWN = 10
WINDOW_WIDTH = 1000
WINDOW_HEIGHT = 1000
WALL_WIDTH = 10
WORLD_OBJECT_SIZE = 50
WORLD_OBJECT_NUMBER = 99


# Global variables
toRemove = []
shotCooldown = 0


# Create the window in physics mode
win = Window(WINDOW_WIDTH, WINDOW_HEIGHT)
win.mode = "physicsmanual"
win.gravity = Vector(0,0)


# Create the tank
tank = Rectangle((0, 0), (TANK_SIZE, TANK_SIZE))
tank.draw(win)
barrel = Rectangle((0, -5), (30, 5))
barrel.draw(tank)


# Create the walls
leftWall = Rectangle((-WALL_WIDTH, 0), (0, WINDOW_HEIGHT))
leftWall.bodyType = "static"
leftWall.draw(win)
rightWall = Rectangle((WINDOW_WIDTH, 0), (WINDOW_WIDTH + WALL_WIDTH, WINDOW_HEIGHT))
rightWall.bodyType = "static"
rightWall.draw(win)
topWall = Rectangle((0, -WALL_WIDTH), (WINDOW_WIDTH, 0))
topWall.bodyType = "static"
topWall.draw(win)
bottomWall = Rectangle((0, WINDOW_HEIGHT), (WINDOW_WIDTH, WINDOW_HEIGHT + WALL_WIDTH))
bottomWall.bodyType = "static"
bottomWall.draw(win)


# Create world obstacles
for i in xrange(1, WORLD_OBJECT_NUMBER+1):
    box = Rectangle((0, 0), (WORLD_OBJECT_SIZE, WORLD_OBJECT_SIZE))
    box.bodyType = "static"
    box.tag = "destructable"
    box.moveTo((i%10)*2*WORLD_OBJECT_SIZE + WORLD_OBJECT_SIZE, int(i/10)*2*WORLD_OBJECT_SIZE + WORLD_OBJECT_SIZE)
    box.draw(win)


# Create the bullet collision function
def bulletCollide(myFixture, otherFixture, contact):
    bullet = myFixture.UserData
    target = otherFixture.UserData
    toRemove.append(bullet)
    if target.tag == "destructable":
        toRemove.append(target)

        
# Start the main loop, but exit the loop if the window is closed
while win.IsRealized:

    # Basic movement commands for our tank
    if win.getKeyPressed("w"):
        tank.forward(MOVE_SPEED)
        tank.move(0,0)
    if win.getKeyPressed("a"):
        tank.rotate(TURN_SPEED)
    if win.getKeyPressed("s"):
        tank.backward(MOVE_SPEED)
        tank.move(0,0)
    if win.getKeyPressed("d"):
        tank.rotate(-TURN_SPEED)
        
    # Decrement the shot cooldown counter if necessary
    if shotCooldown > 0:
        shotCooldown -= 1
        
    # Shoot a bullet when space is pressed on the cooldown is 0
    if win.getKeyPressed("space") and shotCooldown == 0:
    
        # Set the shot cooldown
        shotCooldown = BULLET_COOLDOWN
        
        # Get a vector for the firing direction
        fireDirection = tank.body.GetWorldVector(Vector(BULLET_SPEED,0))
        
        # We don't want to spawn the bullet right in the center of our tank,
        # but instead we want to spawn it at the side of the barrel.
        # The bullet offset allows us to do that
        bulletOffset = tank.getScreenPoint(Point(BULLET_OFFSET, 0))
        
        # Now draw the bullet, give it some velocity, and install the collision function
        bullet = Circle(bulletOffset, BULLET_SIZE)
        bullet.draw(win)
        bullet.body.LinearVelocity = fireDirection
        bullet.body.OnCollision += bulletCollide
        
    # Remove all shapes that have been marked for deletion
    for shape in toRemove:
        shape.undraw()
    toRemove=[]
    
    # Update the window
    win.step(0.03)