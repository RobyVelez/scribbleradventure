from Myro import *
from Graphics import *
import os, sys
from functools import partial ### JH: partial added to prevent multi-load crash
import pickle #for saving data
import datetime 
import ConfigParser
import Myro #Adds Myro to global scope


sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/GameObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/WindowObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CommonImages")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Graphics")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/ChooseYourOwnAdventure")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Platformer")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CommonFunctions") #soon to be depreceated
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Gauntlet")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Labyrinth")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Dungeon")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CloudCity")
IMAGE_PATH=os.path.dirname(os.path.realpath(__file__))+"/CommonImages/"
HOME_DIR=os.path.dirname(os.path.realpath(__file__))

#On the windows machines in 4045 this is needed because those machines
#lose the calico global variable
import setCalico
setCalico.initCalico(calico)
#make sure the shell doesn't reset
calico.config.SetValue('python-language','reset-shell-on-run',False)
#calico.config.SetValue('python-language','reset-show-on-run',False)

from windowObjects import *
from userFunctions import *

#Install dropbox and a dropbox folder either on the desktop or
#the ScribblerAdventure folder. Enter the absolute path of the
#dropbox folder here and add the path to the Students folder
if os.name=="nt":
    DROPBOX_PATH="C:\\Users\\lrcguest01\\Dropbox\\"
else:
    DROPBOX_PATH="/Users/robyvelez/Dropbox/"

#Can be used to force the use of local directory
OFFLINE=True

#Save path will be set by the portal.
#It will either be the dropbox folder.
#Or the current directory
SAVEPATH=None
       
DEBUG=True
   
#Version number. Relates to save data and how it is structured
from version import VER
                                         
class Portal(MenuWindow):

    def __init__(self):
        #if there is a previous window shut it down and stop the Events
        try:
            existingWin=getWindow()
            existingWin.Destroy()
        except:
            pass
            
        MenuWindow.__init__(self, None, "LRC_Portal", "Activities Portal")
        
        #where the savefiles will be loaded and saved to
        if DEBUG:
            self.dropboxFolder=DROPBOX_PATH+"Roby3"
        else:
            self.dropboxFolder=self.getDropBoxFolder()
            
        #create windows and panels
##         self.mainWindow=WindowLRC("LRC_Portal",800,700) 
##         self.mainWindow.onShow = self.update
##         self.optionPanel=self.mainWindow.addPanel("Options",600,0,200,800,Color("gray"))
##         self.descriptionPanel=self.mainWindow.addPanel("Description:",0,600,600,100,Color("lightblue"))
##         self.activityPanel=self.mainWindow.addPanel("Activities Portal",0,0,600,600,Color("white"),) 
        
        scribAdAvatar = Picture(IMAGE_PATH + "/ScribLogo.png")
        scribAdAvatar.moveTo(0,0)
        scribAdAvatar.border = 0
        scribAdAvatar.scale(0.2)
        
        #add buttons to activity panel
        text=Text((-275,-70), "Activities", color=Color("black"))
        text.yJustification = "bottom" 
        text.xJustification = "left"
        text.fontFace = "Helvetica Neue Light"
        text.fontSize = 30
        text.draw(self.mainPanel)
        self.scribAdBut = self.mainPanel.addButton(25,250,250,40,self.launchActivity,"Scribbler Adventure", avatar=scribAdAvatar)
        self.scribAdBut.text.visible=False
        self.mainPanel.addButton(25,300,250,40,self.launchActivity,"Graphics")
        self.mainPanel.addButton(25,350,250,40,self.launchActivity,"Platformer")
        self.mainPanel.addButton(25,400,250,40,self.launchActivity,"Choose Your Own Adventure")

        
        text=Text((25,-70), "Documentation", color=Color("black"))
        text.yJustification = "bottom" 
        text.xJustification = "left"
        text.fontFace = "Helvetica Neue Light"
        text.fontSize = 30
        text.draw(self.mainPanel)
        self.mainPanel.addButton(325,250,250,40,self.launchActivity,"LRC Wiki")
        self.mainPanel.addButton(325,300,250,40,self.launchActivity,"Connect Scribbler Robot")
        
        # Add mouse-over description to buttons
##         self.descDict = {}
##         self.currentDescription = None
##         self.addDescription("Scribbler Adventure", "Help Scribby rescue his sibblings through programming!")
##         self.addDescription("Graphics", "Create scenes and animations through programming!")
##         self.addDescription("Platformer", "Create your own platformer game!")
##         self.addDescription("Choose Your Own Adventure", "Create your own, choose-your-own-adventure game!")
##         self.addDescription("LRC Wiki", "Contains extra activities, documentation, the hall-of-fame, and more.")
##         self.addDescription("Connect Scribbler Robot", "Detailed instructions for how to connect to a Scribbler robot.")
        
        
        #initializes the gameData
        self.mainWindow.hideWindow()
        
        #if using dropbox then points to the dropbox folder
        #if not using dropbox points to the current directory
        #also checks and creates the /UserList/ and /Students/ directories
        self.savePath=self.getSavePath()
        self.userListPath=self.savePath+"UserList/"
        self.userDataPath=self.savePath+"Students/" 
        
        #Config object that writes and reads the user data from a text file
        self.config = ConfigParser.SafeConfigParser()
        
        #Asks the user to login or create a new account. 
        #If you create a new account a new userData.p is created
        self.userDataLoc=self.prompt()
    
        loadSuccess=False
        self.gameData=None
        if self.userDataLoc!=None:
            #gets the actual game data
             try:
                self.gameData=self.loadGameData()
                loadSuccess=True
             except:
                print("Error loading game data.")
                loadSuccess=False
        else:
            print("Something went wrong with login process.")
        
        if loadSuccess:
            #Refactor gameData/userData here
            self.mainWindow.showWindow()
        
    ##########################
    ####Button Callbacks######
    ##########################

    def openPDF(self, filename):
        if sys.platform == 'linux2':
            subprocess.call(["xdg-open", "Graphics01.pdf"])
        else:
            os.startfile(filename)
            
            
    def openWebPage(self, filename):
        os.startfile(filename)
                                        
    def launchActivity(self,buttonName=None):
        print(buttonName)
        if buttonName=="Scribbler Adventure":            
            position=self.mainWindow.GetPosition()
            self.mainWindow.hideWindow()            
            launcher=__import__("ScribblerAdventureWindow", globals(), locals(), ['*'])
            self.mainWindow.child=launcher.start(self.gameData[0],self)
            self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)
        elif buttonName=="Graphics":
            position=self.mainWindow.GetPosition()
            self.mainWindow.hideWindow()            
            launcher=__import__("graphicsWindow", globals(), locals(), ['*'])
            self.mainWindow.child=launcher.start(self)
            self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)
        
            #self.openPDF(HOME_DIR+"/Graphics/Graphics01.pdf")
        elif buttonName=="Platformer":
            self.openPDF(HOME_DIR+"/Platformer/Platformer_level_1.pdf")        
        elif buttonName=="Choose Your Own Adventure":
            self.openPDF(HOME_DIR+"/ChooseYourOwnAdventure/ChooseYourOwnAdventure.pdf")
        elif buttonName=="Connect Scribbler Robot":
            self.openPDF("http://cobi.cs.uwyo.edu/lrc/index.php/Connect_to_a_Scribbler_robot")
        elif buttonName=="LRC Wiki":
            self.openPDF("http://www.evolvingai.org/lrc/index.php/Main_Page")
            
            
            
    #####################################
    ####Save and GameData Funcitons######
    #####################################
    
    #TODO JH: May be used to backpropgate this information at some poin in the future
    def update(self):
        pass
##         self.scribAdBut.metaText.text = str(0) + "/" + str(len(ScribblerAdventure.stages))
    
    def getSavePath(self):
        #checks to see if the dropbox path exists
        #if it doesn't make a local folder for UserList and Students
        saveDir=None
        
        #see if dropbox folder exists
        if os.path.exists(DROPBOX_PATH) and not OFFLINE:
            self.usingDropBox=True
            saveDir=DROPBOX_PATH
        else:
            self.usingDropBox=False
            saveDir=os.path.dirname(os.path.realpath(__file__))+"/"
            if not os.path.exists(saveDir+"UserList"):
                os.makedirs(saveDir+"UserList")
            if not os.path.exists(saveDir+"Students"):
                os.makedirs(saveDir+"Students")
        
        return saveDir
    
    def loadGameData(self):
        return pickle.load( open(self.userDataLoc, "rb" ) )
    
    def saveGameData(self):
        temp=open(self.userDataLoc,"wb")
        pickle.dump(self.gameData, temp)
        temp.close()
        #give save data some time before doing anything else
        sleep(0.1)        
        #for s in self.stage[0].stats:
        #    print(s)
    
    def prompt(self,message=None,credentials={"foo":"bar"}):
        try:
            self.clearConfig()    
            if message=="failedLogin":
                response=askQuestion("Error, no account found for "+credentials["First Name"]+" "+credentials["Last Name"]+"\n",["Try Again - Login","Create New Account"])
            else:    
                response=askQuestion("Welcome to the Laramie Robotics Club user portal. \nSelect one of the following options.",["Login","Create New Account"])
            
            if response=="Login" or response=="Try Again - Login":       
                userDataLoc=self.login()
            else:   
                userDataLoc=self.createAccount()      
                      
            return userDataLoc
        except:
            print("Error with login.")
            return None
            
    def login(self):   
        credentials=ask(["First Name","Last Name"],"Enter you're credentials.\nNote, case sensitive.")
        username=self.formatUsername(credentials)        
        if os.path.exists(self.userListPath+username+".txt"):
            self.recordLogin(username)
            userDataLoc=self.retrieveAccount(username)
            return userDataLoc
        else:
            self.prompt("failedLogin",credentials)
            return None
        
    def createAccount(self):
        
        credentials= ask(["First Name","Last Name","Gender","Birthday (month/day/year)"],"Please provide the following information\nto create a new account")    
        username=self.formatUsername(credentials)
        
        if os.path.exists(self.userListPath+username+".txt"):
            print("Account for "+username+" already exists.")
            self.recordLogin(username)
            userDataLoc=self.retrieveAccount(username)
            return userDataLoc
        else:
            
            self.config.add_section(username)
            for key,value in credentials.items():
                self.config.set(username,key,value)
            #add today's login
            self.config.set(username,"login",str(datetime.date.today()))
            userFile=open(self.userListPath+username+".txt","w")
            self.config.write(userFile)
            userFile.close()
            
            #effectively resets the parser
            self.clearConfig()
            userDataLoc=self.createUserData(username)
            print("Returning user data location.",userDataLoc)
            return userDataLoc
        
    def formatUsername(self,credentials):
        username=credentials["First Name"]+credentials["Last Name"]
        username=username.replace(" ","") #removes whitespace        
        username=username.lower() #lowercase all letters to make case insensitive        
        return username
    
    def createUserData(self,username):
        if not os.path.isdir(self.userDataPath+username):
            os.makedirs(self.userDataPath+username)
        else:
            print("Attempting to create UserData folder but "+self.userDataPath+username+" already exists")
        
        userDataLoc=self.userDataPath+username+"/"+"UserData.p"
        
        if os.path.exists(userDataLoc):  
            print("Attempting to create UserData but "+userDataLoc+" already exists")
        else:
            #creates a new gameData object
            data=open(userDataLoc,"wb")        
            pickle.dump([GameData(VER)], data)
            data.close()  
            
        return userDataLoc      

    def recordLogin(self,username):
        self.config.read(self.userListPath+username+".txt")
        if self.config.has_option(username,"login"):
            loginRecord=self.config.get(username,"login")
            loginRecord+=":"+str(datetime.date.today())
            self.config.set(username,"login",loginRecord)
            
            userFile=open(self.userListPath+username+".txt","w")    
            self.config.write(userFile)
            userFile.close()
        
        self.clearConfig()
        
    def retrieveAccount(self,username):
        userDataLoc=self.userDataPath+username+"/"+"UserData.p"
        if not os.path.exists(userDataLoc):  
            print("No user data exists for ",username," Creating user data.")
            
            #Does the student folder exist?
            if not os.path.exists(self.userDataPath+username):
                os.mkdir(self.userDataPath+username)
                
            data=open(userDataLoc,"wb")        
            pickle.dump([GameData(VER)], data)
            data.close()    
                    
            return userDataLoc
        else:
            print("Retrieving user data for "+username)   
            return userDataLoc 

    
    ##########################
    #Callback/Event functions#
    ##########################
    def mouseDown(self,obj, event):
        #pass
        print(event.x,event.y)
    
    def keyPressed(self,obj,event):
        if str(event.key)=="q" or str(event.key)=="Q":
            self.quitGame()
    
##     def onMouseMove(self, obj, event):
##         for button in self.activityPanel.button:
##             if button.hit(event.x, event.y):
##                 text = self.descDict[button.tag]
##                 if text is None: return
##                 if text == self.currentDescription: return
##                 if self.currentDescription is not None:
##                     self.currentDescription.visible = False
##                 self.currentDescription = text
##                 self.currentDescription.visible = True
##                 self.mainWindow.win.QueueDraw()
##                 return
##         if self.currentDescription is not None:
##             self.currentDescription.visible = False
##             self.currentDescription = None
##             self.mainWindow.win.QueueDraw()
    
    ####################        
    #General Utilities#
    ###################    
    def _hideWindow(self):
        self.mainWindow.Visible=False
    def _showWindow(self):
        self.update()
        self.mainWindow.Visible=True 
    def quitGame(self):
        self.win.Destroy()
    def clearConfig(self):
        toRemove=[]
        for s in self.config.sections():
            toRemove.append(s)
        for r in toRemove:
            self.config.remove_section(r)
            

    

##     def addDescription(self, tag, text):
##         text=Text((-295,-10), text, color=Color("black"))
##         text.yJustification = "bottom" 
##         text.xJustification = "left"
##         text.fontFace = "Helvetica Neue Light"
##         text.fontSize = 16
##         text.draw(self.descriptionPanel)
##         text.visible = False
##         self.descDict[tag] = text
    ##################
    #Window Functions#
    ##################
    
global portal
portal=None
if __name__ == "__main__":
##     global portal
##     # JH: This allows levels to read all user created functions and variables.
##     __builtins__["userGlobals"] = globals()
    portal=Portal()

'''
def adminPanel():    
    global portal
    bar=ask(base64.b64decode('RW50ZXIgTFJDIFBhc3N3b3Jk'))
    if base64.b64encode(bar)=='MzA3c2NyaWJieQ==':
        a=int(ask(base64.b64decode('RW50ZXIgaGlnaGVzdCBzdGFnZQ==')))
        b=int(ask(base64.b64decode('RW50ZXIgaGlnaGVzdCBsZXZlbA==')))
        portal.gameData[0].highestStage=a
        portal.gameData[0].highetLevel=b
        print(portal.gameData[0].highetLevel)
'''        
##         
## '''
##     def saveDataProxy(self,o,e):
##         self.saveGameData()
##     
##     def createGameData(self):
##         stages=[]
##         #self.stage.append(stageObject("Gauntlet",
##          #   ["gauntlet1","gauntlet1","gauntlet1"],None))
##             #"gauntlet4","gauntlet5","gauntlet6",
##             #"gauntlet7","gauntlet8","gauntlet9"],None))
##             
##         stages.append(stageObject("Gauntlet",
##             ["gauntlet1","gauntlet2","gauntlet3",
##             "gauntlet4","gauntlet5","gauntlet6",
##             "gauntlet7","gauntlet8","gauntlet9","gauntlet10"],1))
##             
##         stages.append(stageObject("Labyrinth",
##             ["labyrinth1","labyrinth2","labyrinth3",
##             "labyrinth4","labyrinth5","labyrinth6",
##             "labyrinth7","labyrinth8","labyrinth9",
##             ],2))
##             
##         stages.append(stageObject("Dungeon",
##             ["dungeon2","dungeon3","dungeon4",
##             "dungeon5","dungeon6","dungeon7",
##             "dungeon8"],3))
##         stages.append(stageObject("CloudCity",
##         ["cloudCity1","cloudCity2","cloudCity3",
##             "cloudCity4","cloudCity5","cloudCity6"],
##             4))       
##                 
##         
##         #unlock the first stage
##         stages[0].unlocked=True
##         
##         return stages
## 
##     
##           
##                     
##     ##########################
##     #Create panels and window#
##     ##########################
##     def testCallback(self):
##         print("test")
##     
##     def createWindow(self,title,x,y,w,h):              
##         
##         win=Window(title,w,h)
##         win.setBackground(Color("white"))
##         #win.addScrollbars(w+100,h+100)
##         
##         win.onMouseDown(self.mouseDown) 
##         win.onKeyPress(self.keyPressed)        
##         
##         return win        
##     
##     def createPanelStages(self,x,y,w,h,vis=True):
##         bW=1 #borderwidth
##         panel=makeSimplePanel(x,y,w,h,bW,Color("blue"))
##         panel.visible=vis
##         self.win.draw(panel)        
##         return panel
##     
##     def createPanelSaveFiles(self,x,y,w,h,vis=True):
##         bW=1 #borderwidth
##         panel=makeSimplePanel(x,y,w,h,bW,Color("red"))
##         panel.visible=vis
##         panel.tag="SavePanel"
##         self.win.draw(panel)
##         
##         buttonWidth=125
##         buttonHeight=20
##         pad=5
##         fS=14
##         
##         bW=50
##         bH=20
##         totalHeight=400
##         fS=14
##         spacer=5
##         
##         createPanelButton(panel,w-3*buttonWidth,h-buttonHeight,buttonWidth,buttonHeight,
##             "Erase","Erase",None,Color("white"),Color("gray"),fS)        
##         createPanelButton(panel,w-2*buttonWidth,h-buttonHeight,buttonWidth,buttonHeight,
##             "Load","Load",None,Color("white"),Color("gray"),fS)
##         createPanelButton(panel,w-buttonWidth,h-buttonHeight,buttonWidth,buttonHeight,
##             "ChangeDirectory","ChangeDirectory",None,Color("white"),Color("black"),fS)
##       
##         panel.connect("click",self.panelSaveClick)
##         return panel
##         
##     
##     def createPanel2(self,x,y,w,h):
##         bW=1 #borderwidth
##         panel2=makeSimplePanel(x,y,w,h,bW,Color("gray"))
##         self.win.draw(panel2)
##         
##         #distance between sides of panel and other objects
##         spacer=10        
##         buttonW = 100
##         buttonH = 20        
##         
##         #Text iterm, name of the window
##         createPanelText(panel2,spacer,2*spacer,"World/Stage Story")
##         
##         #Button item, save
##         createPanelButton(panel2,w-2*buttonW-spacer,h-buttonH-spacer,buttonW,buttonH,"Start","Start")        
##         #Button item, quit
##         createPanelButton(panel2,w-buttonW-spacer,h-buttonH-spacer,buttonW,buttonH,"SaveMenu","SaveMenu")
##         
##         panel2.connect("click",self.panel2Click)
##         return panel2
## 
##     def createPanel3(self,x,y,w,h):
##         bW=1 #borderwidth
##         panel3=makeSimplePanel(x,y,w,h,bW,Color("gray"))
##         self.win.draw(panel3)
##         panel3.connect("click",self.panel3Click)
##                 
##         return panel3
##         
##     
##         
##     ##################################
##     #Background and foreground images#
##     ##################################
##     
##     def createBackground(self,back,fore):
##         win=self.win
##         if back!=None:
##             backPicture=Picture(back)
##             win.draw(backPicture)
##             backPicture.stackOnBottom()
## 
##         if fore!=None:
##             forePicture=Picture(fore)
##             win.draw(forePicture)
##             forePicture.stackOnTop()
##             
##     
##     ###################################################
##     #Functions that update what is displayed on panels#
##     ###################################################
##            
##     def displaySaveFiles(self):
##         panel=self.panelSave
##         maxCol=3
## 
##         #clear all shapes except the load, clear, and switch directory buttons
##         while len(panel.shapes)>3: 
##             panel.shapes.Remove(panel.shapes[3])
##    
##         #get all the .p files which should be the save files    
##         pickleFiles=[]                
##         for f in os.listdir(self.dropboxFolder):
##             if f[-2:]==".p":
##                 pickleFiles.append(f)
##                 
##         #dimension of buttons
##         bWidth=100
##         bHeight=100
##         fS=12
##            
##         numRows=ceil(len(pickleFiles)/maxCol)
##         rowX=panel.height/(numRows+1)
##         colY=panel.width/(maxCol+1)
##         
##         for i in range(len(pickleFiles)):
##             if i !=0 and i%maxCol==0:
##                 rowX+=panel.height/(numRows+1)
##                 colY=panel.width/(maxCol+1)
##             createPanelButton(panel,colY-bWidth/2,rowX-bHeight/2,bWidth,bHeight,
##             pickleFiles[i],pickleFiles[i],None,Color("white"),Color("black"),fS)        
##             
##             colY+=panel.width/(maxCol+1)
##                 
##         #display the current directory
##         createPanelText(panel,5,40,"Current Directory:\n"+self.dropboxFolder,c=Color("black"),fS=14)      
##         
##     
##     def displayStageButtons(self):
##         panel=self.panelStage
##         
##         #clear all shapes
##         while len(panel.shapes)>0: 
##             panel.shapes.Remove(panel.shapes[0])
##             
##         stageDict={}
##         #x,y,w,h,avatar
##         stageDict["Gauntlet"]=[100,500,100,100,None]
##         stageDict["Labyrinth"]=[100,400,100,100,None]
##         stageDict["Dungeon"]=[100,300,100,100,None]
##         stageDict["CloudCity"]=[100,200,100,100,None]
##         
##         for g in self.gameData[0]:
##             print(g)
##             [x,y,w,h,avatar]=stageDict[g.stageName]
##             createPanelButton(panel,x,y,w,h,g.stageName,g.stageName,
##                 avatar,Color("white"),Color("black"),14) 
## 
##     
##         
##     #when a button on the panelSave or panelStage is selected you can
##     #send that tag of that item here and a description will be created
##     #and printed to panel 2
##     def displayDescriptionPanel2(self,selectedTag):
##         if selectedTag==None: #clear the description
##             self.panel2.shapes[0].setText(" ")            
##         elif selectedTag[-2:]==".p": #selected a save file        
##             self.panel2.shapes[0].setText("Selected file is: "+selectedTag)
##             
##     
##     
##     ## JH: START added to fix crash on load bug   ##
##     def makeLoadStageCallback(self, count):
##         return lambda _, __: self.loadStage(count)
##     
##     
##     def loadStage(self, stageIndex):
##         if self.stage:
##             if stageIndex < len(self.stage): 
##                 self.stage[stageIndex].buttonClick(None, None)
##     
##     
##             
##       
##     
##     
## ##         global worldScore
## ##         [self.stage, worldScore] = pickle.load( open( self.saveFile, "rb" ) )
## ##         self.refreshStageButtons()
## ##         
## ##         #just make sure the stageSelected is off
## ##         #and current level is set to the highest level
## ##         for s in self.stage:
## ##             s.stageSelected=False
## ##             s.currentLevelIndex=s.highestLevel
## ##             
## ##         #As far as I can see, this does not do anything
## ##         if currentStage==None:
## ##             global currentStage
## ##             currentStage=None
## ## 
## ##         self.updateWorldStats()
## ## 
## ##         #give load data some time before doing anything else
## ##         sleep(0.1)
## ##   
##           
##    
## '''
##     
## '''
##     def toggleEraseLoadButtons(self,color):
##         #erase button              
##         self.panelSave.shapes[0].shapes[0].setColor(color)
##         #load button
##         self.panelSave.shapes[1].shapes[0].setColor(color)
##                 
##     def clearAnySelectedButtons(self):
##         #if something is already selected, deselect
##         for s2 in self.panelSave.shapes:                        
##             if s2.outline.Equals(Color("yellow")):
##                 s2.outline=Color("black")
##                 s2.setWidth(1)
##         self.displayDescriptionPanel2(None)
##         
##     def panelSaveClick(self,o,e):
##         p=self.panelSave.center
##         cX=p[0]
##         cY=p[1]
##         
##         fileSelected=None
##         if self.panelSave.visible:
##             for s in self.panelSave.shapes:
##                 if s.hit(e.x-cX,e.y-cY):
##                     if s.tag[-2:]==".p":            
##                         self.clearAnySelectedButtons()
##                         s.setWidth(5)
##                         s.outline=Color("yellow")                    
##                         self.toggleEraseLoadButtons(Color("black"))
##                         self.displayDescriptionPanel2(s.tag)
##                         
##                     elif s.tag=="Load" and s.shapes[0].getColor().Equals(Color("black")):                                  
##                         for s2 in self.panelSave.shapes:                        
##                             if s2.outline.Equals(Color("yellow")):
##                                 fileSelected=s2
##                                 print("Loading "+s2.tag+".")
##                                 self.loadData(s2.tag)
##                                 #self.newFile(self.dropboxFolder+"/"+s2.tag) 
##                                 
##                         self.clearAnySelectedButtons() 
##                         self.toggleEraseLoadButtons(Color("gray"))                                  
##                         if fileSelected==None:
##                             print("Error no file selected.")
##                         
##                         #hide the save menu and bring up the stage menu
##                         self.panelSave.visible=False
##                         self.panelStage.visible=True  
##                         #have to update or else panelSave won't show up
##                         self.win.update()  
##                             
##                     elif s.tag=="Erase" and s.shapes[0].getColor().Equals(Color("black")):
##                     
##                         for s2 in self.panelSave.shapes:                        
##                             if s2.outline.Equals(Color("yellow")):
##                                 fileSelected=s2
##                                 print("Erasing "+s2.tag+". Creating new game file.")
##                                 self.newFile(self.dropboxFolder+"/"+s2.tag) 
##                                 
##                         self.clearAnySelectedButtons() 
##                         self.toggleEraseLoadButtons(Color("gray"))                                  
##                         if fileSelected==None:
##                             print("Error no file selected.") 
##                             
##                         fileSelected=None                 
##                     elif s.tag=="ChangeDirectory":
##                         self.dropboxFolder=self.getDropBoxFolder()
##                         self.displaySaveFiles()                    
##                     else:                 
##                         #first clear anything that might already be selected
##                         self.clearAnySelectedButtons()
##                         self.toggleEraseLoadButtons(Color("gray"))
##                                 
##                     return
##             #clicking on panel itself and not one of the buttons
##             #clear anything that might already be selected
##             self.clearAnySelectedButtons()
##             self.toggleEraseLoadButtons(Color("gray"))
##                                 
## 
##     def panel1Click(self,o,e):
##         p=self.panel1.center
##         cX=p[0]
##         cY=p[1]
##         
##         for s in self.panel1.shapes:
##             if s.hit(e.x-cX,e.y-cY):
##                 for stage in self.stage:
##                     if stage.stageName==s.tag:
##                         break
## 
## 
##     def panel2Click(self,o,e):
##         p=self.panel2.center
##         cX=p[0]
##         cY=p[1]
##         for s in self.panel2.shapes:
##             if s.hit(e.x-cX,e.y-cY): 
##                 if s.tag=="Start":
##                     print("Starting level...")
##                 elif s.tag=="SaveMenu":
##                     self.panelSave.visible=True
##                     self.panelStage.visible=False  
##                     #have to update or else panelSave won't show up
##                     self.win.update()
##                 break             
##                 
##                     
##     def panel3Click(self,o,e):
##         p=self.panel3.center
##         cX=p[0]
##         cY=p[1]
##         
##         tag=None        
##         for s in self.panel3.shapes:
##             if s.hit(e.x-cX,e.y-cY):
##                 if s.tag is not None:
##                     tag=s.tag
##                     break
##         
##         if tag!=None:
##             #double click or click the load button
##             if tag==self.fileSelected or tag=="Load":
##                 self.saveFile=self.fileSelected
##                 self.fileSelected=None
##                 self.panel3.shapes[1].shapes[0].fill=Color("gray")
##                 self.loadData()                       
##             elif tag=="Quit":
##                 self.quitGame()
##             elif s.tag=="New":
##                     self.newGame()
##             else: #tag is a directory name        
##                 if not os.path.isfile(tag):
##                     self.clearPanel3()
##                     self.updateLoadWidget(tag) 
##                     self.panel3.shapes[1].shapes[0].fill=Color("gray")
##                     self.fileSelected=None     
##                 else:
##                     self.panel3.shapes[1].shapes[0].fill=Color("black")
##                     self.fileSelected=tag
## 
##     
##     
##     
##     
##     def newGame(self):
##         if len(self.stage)>0:
##             #ask user if they want to save current file
##             #or ask user if they want to sync to cloud
##             pass
##         #init the stages
##         self.setupStages()
##         #init the score
##         global worldScore
##         worldScore=0
##         self.refreshStageButtons()
##         self.win.Visible=False
##         self.saveFile=ask("You will need a save file to play. \nEnter a name to use as the save file. \nFor example 'JakeSaveFile'")
##         if self.saveFile!=None:
##             if self.saveFile[-2:]!=".p":
##                 self.saveFile+=".p"
##         else:
##             print("Error no filename entered.")
##         self.win.Visible=True
##             
##         self.saveData()
##         self.updateLoadWidget()
##     
##     def nextLevel(self,o,e):
##    
##         if currentStage!=None:
##             for s in self.stage:
##                 if s==currentStage:
##                     #at the end of a stage, return to main window
##                     if s.currentLevelIndex+1==len(s.levelList):
##                         getLevelObject().sim.window.Destroy()
##                         self.saveData()
##                         self.win.Visible=True
##                     #below the lenght of levelist and highestleve
##                     elif (s.currentLevelIndex + 1) <= s.highestLevel:
##                         s.currentLevelIndex+=1
##                         success=s.launchLevel(s.currentLevelIndex)
##                     break
##         else:
##             print("No active stage, therefore no next level.")
##             
##         self.saveData()
##         
##     def prevLevel(self,o,e):        
##         if currentStage!=None:
##             for s in self.stage:
##                 if s==currentStage:
##                     if (s.currentLevelIndex-1) >= 0:
##                         s.currentLevelIndex-=1
##                         s.launchLevel(s.currentLevelIndex)
##                     break
##         else:
##             print("No active stage, therefore no prev level.")
## 
##         self.saveData()        
##     
##     def saveData(self):
##         temp=open(self.saveFile,"wb")
##         pickle.dump([self.stage,worldScore], temp)
##         temp.close()
##         #give save data some time before doing anything else
##         sleep(0.1)
##         
##         #for s in self.stage[0].stats:
##         #    print(s)
##     
##     def saveDataProxy(self,o,e):
##         self.saveData()
##         
## 
## 
##         
##     def hideWindow(self,o,e):
##         self.win.Visible=False
##         
##     def showWindow(self,o,e):
##         self.win.Visible=True
##         
##         for s in self.stage:
##             if s==currentStage:
##                 s.stageSelected=False
##                 s.currentLevelIndex=s.highestLevel
##         #when coming back to the main window update the world stats
##         self.updateWorldStats()
##         self.refreshStageButtons()
##     
##     #this message is sent by the level and indicates it have been solved
##     #increament the currentStage's highest level if necessary        
##     def incrementHighestLevel(self,o,e):
##         if currentStage!=None:
##             for i in range(len(self.stage)):
##                 if self.stage[i]==currentStage:
##                     s=self.stage[i]
##                     #solved the last level in this stage
##                     if s.currentLevelIndex+1>len(s.levelList):
##                         print("Error. Somehow current level is greater than length of level list")
##                     elif s.currentLevelIndex+1==len(s.levelList):
##                         if i+1<len(self.stage):
##                             self.stage[i+1].unlocked=True  
##                             #update the stage buttons to show the new stage is unlocked
##                             self.refreshStageButtons()                          
##                     elif s.currentLevelIndex+1<len(s.levelList) and s.currentLevelIndex+1>s.highestLevel:
##                         s.highestLevel=s.currentLevelIndex+1
##                     break
##         else:
##             print("No active stage, therefore can't increment highest level.")
##    
##     
##     #only way to figure out which is the active stage
##     #when user clicks on a stage a signal is sent to set all the
##     #stages active variables to 0
##     #The stage that was clicked on then sets its active variable to 1
##     #the current stage then is the one with active set to 1
##     def setLevelCurrentStage(self,o,e):
##         getLevelObject().levelScore=worldScore
##         for s in self.stage:
##             if s==currentStage:
##                 getLevelObject().currentStage=s
##                 self.updateWorldStats()
##                 break
##     
##     #when a stage is selected (clicked once) this
##     #functions allows that stage to tell the world to
##     #update the world stats
##     def updateWorldStatsProxy(self,o,e):
##         self.updateWorldStats()
##     
##     def updateWorldScore(self,o,e):
##         global worldScore
##         worldScore=getLevelObject().levelScore
##         
##     def setupEventSelection(self):
##         Events.init()
##         Events.subscribe("nextLevel", self.nextLevel)
##         Events.subscribe("prevLevel", self.prevLevel)
##         #Events.subscribe("loadFile", self.getLoadFile)
##         Events.subscribe("saveData", self.saveDataProxy)
##         Events.subscribe("hideWindow", self.hideWindow)
##         Events.subscribe("showWindow", self.showWindow)
##         Events.subscribe("levelSolved",self.incrementHighestLevel)
##         Events.subscribe("setLevelCurrentStage",self.setLevelCurrentStage)
##         Events.subscribe("updateWorldStats",self.updateWorldStatsProxy)
##         Events.subscribe("updateWorldScore",self.updateWorldScore)
##             
##     def getUserName(self):
##         ans=ask("Enter username.")
##         return ans
##     
##     
##     #######################
##     #Game Function Visuals#
##     #######################
##     
##     def refreshStageButtons(self):
##         count=0
##         for s in self.win.canvas.shapes:
##             if s.tag=="Shape":
##                 #s.connect("click",self.stage[count].buttonClick)
##                 #s.connect("click", self.testCallback)
##                 s.setText(self.stage[count].stageName)
##                 if self.stage[count].unlocked:
##                     s.unlockView()
##                 else:
##                     s.lockView()
##                 count+=1
##              
##         
##         self.win.refresh()
##     
##     def updateWorldStats(self):
##         #username        
##         self.panel3.shapes[4].setText(str(self.username))
##         #score
##         self.panel3.shapes[5].setText(str(worldScore))
##         #stages unlocked
##         numU=0
##         for s in self.stage:
##             if s.unlocked:
##                 numU+=1                
##         self.panel3.shapes[6].setText(str(numU)+"/"+str(len(self.stage)))
## 
##         if currentStage != None:
##             for s in self.stage:
##                 if s==currentStage:
##                     self.panel3.shapes[7].setText(str(s.stageName))
##                     self.panel3.shapes[8].setText(str(s.highestLevel+1)+"/"+str(len(currentStage.levelList)))
##                     self.panel3.shapes[9].setText(str(s.currentLevelIndex+1))
##                     break
##             
##     def updateLoadWidget(self,curDir=os.path.dirname(os.path.realpath(__file__))):
## 
##         #start location of the buttons
##         p1X=0
##         p1Y=0
##         
##         #list of directories above the current directory
##         dirList=[]
##         
##         if os.name=="nt":
##             stopLength=3
##         else:
##             stopLength=1
##         
##         while len(curDir)>stopLength:
##             dirList.append(curDir)
##             curDir=os.path.dirname(curDir)        
##         dirList.reverse()
##         
## 
##         
##         #add all the previous directories
##         
##         #width of button
##         bW=75
##         #height of button
##         bH=20
##         #font size
##         fS=12
##         #padding around buttons
##         pad=5
##         
##         numTopDir=len(dirList)
##         dirOff=max(len(dirList)-numTopDir,0)
##         for i in range(min(numTopDir,len(dirList))):          
##                     
##             text=dirList[dirOff+i].split("/")[-1]
##             bW=len(text)*8
##             [lastShape,lastText]=createPanelButton(self.panel3,p1X,p1Y,bW,bH,text,dirList[dirOff+i],None,makeColor(200,200,200,255),Color("black"),12)
##             p1X+=bW
##             
##             if i!=0 and (lastShape.getX()+self.panel3.width/2+bW+pad) > self.panel3.width:
##                     p1Y=p1Y+bH
##                     p1X=0
##                     
##         #longer buttons for the potential files
##         bW=self.panel3.width
##         
##         #reset left x position
##         p1X=-25
##         
##         #positions of list of subdirectories and files
##         lX=p1X+25
##         lY=p1Y+bH        
##         
##         #get the subdirectories and files                
##         [dirPath,dirNames,fileNames]=os.walk(dirList[-1]).next() 
##                 
##         colorCycle=0
##         count=0
##         
##         #first list all directories in current directory
##         for i in range(len(dirNames)):
##             if dirNames[i][0] != ".": #no hidden files
##                 count+=1
##                 if colorCycle%2==0:
##                     buttonColor=makeColor(175,175,175,255)
##                 else:
##                     buttonColor=makeColor(150,150,150,255)
##                 colorCycle+=1
##                 createLoadButton(self.panel3,lX,lY+count*bH,bW,bH,dirNames[i],dirPath+"/"+dirNames[i],None,buttonColor,Color("black"),12,IMAGE_PATH+"folderIcon.png")
##         
##         #sort by creation date
##         #may have to use st_ctime for windows
##         fileNames.sort(key=lambda x: os.stat(os.path.join(dirPath, x)).st_mtime,reverse=True)
##         
##         numFiles=15
##         for i in range(len(fileNames)):
##             if fileNames[i][-2:]==".p":
##                 count+=1
##                 if colorCycle%2==0:
##                     buttonColor=makeColor(175,175,175,255)
##                 else:
##                     buttonColor=makeColor(150,150,150,255)
##                 colorCycle+=1
##                 createLoadButton(self.panel3,lX,lY+count*bH,bW,bH,fileNames[i],dirPath+"/"+fileNames[i],None,buttonColor,Color("black"),12,IMAGE_PATH+"fileIcon.png")
##                 if count>=numFiles:
##                     break
## 
##         
##         #i=len(self.panel1.shapes)
##         #lX=self.panel1.shapes[i-1].getX()+self.panel1.width/2
##         #lY=self.panel1.shapes[i-1].getY()+self.panel1.height/2
##         
##         
##     def clearPanel3(self):
##         
##         while len(self.panel3.shapes)>self.numObjsPanel3: #don't remove the new, load or quit buttons
##             self.panel3.shapes.Remove(self.panel3.shapes[self.numObjsPanel3])
##         self.win.refresh()
##   
##     def viewStageStats(self,index=None):
##         if len(self.stage)>0:
##             if index != None:
##                 print(self.stage[index])
##             else:
##                 for s in self.stage:
##                     print(s)
##         else:
##             print("No stage files loaded. Try creating a new game file or loading a previous game file.")
##     
## '''         
##             



