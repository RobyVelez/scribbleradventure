from Graphics import *
from time import sleep

####################
##### CONSTANTS ####
####################

#width and height of the window and buttons
w=800
h=600
bW=100
bH=50

####################
# GLOBAL VARIABLES #
####################

#use the debug flag during programming to turn
#debugging messages on and off
debug=False

#dictionary that will clicks, primarily
#buttons clicks, to new scenes
clickMap={}

#dictionary to map keys to new scenes
#or other events. not used in this example
#but provided in case ppl want to use it
keyMap={}  

# Handle to the current window
win=None

# Handle to the four buttons
button1=None
button2=None
button3=None
button4=None

# Handle to the prompt
prompt=None

# Handle to the text of the four buttons
bText1=None
bText2=None
bText3=None
bText4=None

####################
##### FUNCTIONS ####
####################

def initGameTemplate():
    global debug
    global clickMap
    global keyMap
    global win
    global button1
    global button2
    global button3
    global button4
    global prompt
    global bText1
    global bText2
    global bText3
    global bText4
    
    # Reset click map and key map
    clickMap={}
    keyMap={}  
    
    #if a window already exists get its position and
    #then close it
    position=None
    try:
        win=getWindow()
        
        if win != None:
            position=win.GetPosition()
            win.Destroy()
    except:
        pass

    ###############################
    ########Initial Setup##########
    ###############################



    #create the window
    win=Window("Your game.",w,h)
    print("Window set", win)
    win.setBackground(Color("gray"))
    #prevents window from bouncing around on restart
    if position != None:
        win.Move(position[0],position[1]+21) 

    #distance, from the bottom of the window,
    #of the panel dividing line and prompt text
    lineDist=100

    #line dividing the scene and menu panels
    line1=Line((0,h-lineDist),(w,h-lineDist))
    line1.draw(win)

    #create clickable buttons
    button1=RoundedRectangle((w-2*bW,h-bW),(w-bW,h-bH),5)
    button2=RoundedRectangle((w-bW,h-2*bH),(w,h-bH),5)
    button3=RoundedRectangle((w-2*bW,h-bH),(w-bW,h),5)
    button4=RoundedRectangle((w-bW,h-bH),(w,h),5)
    #set color of buttons
    button1.fill=Color("blue")
    button2.fill=Color("red")
    button3.fill=Color("green")
    button4.fill=Color("orange")
    #draw buttons to window
    button1.draw(win)
    button2.draw(win)
    button3.draw(win)
    button4.draw(win)

    #assign tags to the buttons
    #used to pair a button to the scene it launches
    button1.tag="button1"
    button2.tag="button2"
    button3.tag="button3"
    button4.tag="button4"

    #create text objects for buttons
    #draws them to the center of the buttons
    bText1=Text((button1.x,button1.y),"Empty text",color=Color("black"),fontSize=12)
    bText2=Text((button2.x,button1.y),"Empty text",color=Color("black"),fontSize=12)
    bText3=Text((button3.x,button3.y),"Empty text",color=Color("black"),fontSize=12)
    bText4=Text((button4.x,button4.y),"Empty text",color=Color("black"),fontSize=12)
    #draw text to window, in center of buttons
    bText1.draw(win)
    bText2.draw(win)
    bText3.draw(win)
    bText4.draw(win)

    #create the main text
    prompt=Text((0,h-lineDist),"Prompt Text",color=Color("black"),fontSize=18)
    #VERY IMPORTANT!!!, shifts the text to the right of origin
    prompt.xJustification="left"
    prompt.yJustification="top"
    #draw main text to window
    prompt.draw(win)
    
    #assign the click (mouseDown) and key
    #press functions to the window       
    win.onMouseDown(mouseDown)
    win.onKeyPress(keyPressed)
    return win


#dummy function that must be reimplemented
#for every game in order for the restart to
#work
#def initialScene():
#    pass

#removes all shapes from the window
#that are lebeled as temp, i.e. pic.tag="temp"
#allows shapes drawn to the window to be erased
#after the scene is done without removing shapes
#like the prompt text or buttons
def refreshScreenPanel():
    win.removeTagged("temp")
    win.update()
    clickMap.clear()
    keyMap.clear()
    

#assumes the first scene in the game is launched
#in the initialScene() function. the initialScene()
#funciton should be reimplemented for every game
## def restartGame():
##     initialScene()
    
def quitGame():
    getWindow().Destroy()
    
              
                

################################
#######Mouse Clicks#############
################################

#1. look at the all the buttons and see if the click
#hit one of them. If the click hit one of the buttons
#get the tag of the button
#2. check if the tag is None. if the tag is None then
#the window was clicked, but not buttons were clicked.
#3. if the tag is not none check to see if it is the
#clickMap. if the tag is in the clickMap get the function
#that was mapped to it
#4. launch the nextScene. as of right now do not refreshScreenPanel()
#during the nextScene() launch. do it in each scene. it is dones
#this way to make the code more transparent and easier to under.

def mouseDown(obj,event):
    tag=None
    if button1.hit(event.x,event.y):
        tag=button1.tag
    elif button2.hit(event.x,event.y):
        tag=button2.tag
    elif button3.hit(event.x,event.y):
        tag=button3.tag
    elif button4.hit(event.x,event.y):
        tag=button4.tag
        
    if tag == None:
        #if none of the buttons were clicked then
        #the tag is still None so do nothing
        pass
    elif tag in clickMap:
        nextScene=clickMap[tag]
        #refreshScreenPanel()
        nextScene()
    
    #the sleep prevents the user from clicking
    #rapidly and skipping between scenes
    sleep(0.2)
    
    if debug:            
        print(event.x,event.y)   

#very simple key press function
#if the pressed key in the keyMap then
#grab the mapped function and launch it
def keyPressed(obj,event):
    
    if event.key in keyMap:
        nextScene=keyMap[event.key]
        nextScene()
    if debug:
        print(event.key)




