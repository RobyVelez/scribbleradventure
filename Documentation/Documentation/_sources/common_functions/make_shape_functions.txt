Make Shape Functions
====================

.. module:: makeShapeFunctions

.. rubric:: Functions

.. autoautosummary:: makeShapeFunctions
    :functions:

.. rubric:: Details

.. mytomodulemembers:: makeShapeFunctions
    :undoc-members:
    :function:
    :show-inheritance:
