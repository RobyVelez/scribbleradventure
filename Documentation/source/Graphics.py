"""
The rest of this page describes the Graphics library using Python syntax.
For example, you can use:

::

    from Myro import *

to include the subset of Graphics functions and classes contained in the Calico Myro module. To directly load all of the defined functions from the Graphics library in Calico, one could:

::

    from Graphics import *

This will make all of the functions and classes available directly:

::

    circle = Circle((100,200), 20)

Or, you could also:

::

    import Graphics

However, then you must always use the "Graphics." prefix on functions and classes:

::

    circle = Graphics.Circle((100,200), 20)

You can also just import the items you want, like so:

::

    from Graphics import Circle

The following code assumes that you have imported all of the functions and classes from Graphics. The term "shape" is used to describe generic graphical objects.
"""

class Color():
    """
    Colors are composed of 4 components: red, green, blue, and alpha. 
    All values are integers between 0 (dark) and 255 (bright). 
    Alpha is the transparency of the color. 
    An alpha of 255 is completely opaque, and an alpha of 0 is completely transparent.

    Constructors:
        * Color(r, g, b)
        * Color(color_name)
        * Color(hexcolor)

    Functions:
        * makeColor(r, g, b)
        * makeColor(color_name)
        * makeColor(hexcolor)

    Properties:
        * red - (read/write) integer value of red component (0 - 255)
        * green - (read/write) integer value of green component (0 - 255)
        * blue - (read/write) integer value of blue component (0 - 255)
        * alpha - (read/write) integer value of alpha component (0 - 255)

    Color is used:
        * for each Pixel of a Picture
        * for the color of a Picture
        * fill of any shape
        * outline of any shape

    See also the function pickAColor() from the Myro library.
    """
    def __init__(self, *args, **kwargs):
        """Creates a color"""
        pass

    def __str__(self):
        return "color"

    def __repr__(self):
        return "color"


def makeColor(r,g,b):
    """
    Helper function for creating colors.

    See Color for details.
    """
    pass


class Shape():
    """
    All shapes have the following properties:
     * border - thickness of border of shape
     * center - center of shape
     * center.x - x coordinate
     * center.y - x coordinate
     * color - shortcut for setting both fill and outline (need to set to a color first)
     * fill - color of area of shape
     * gradient - (as opposed to fill) - a Gradient() of colors (see below)
     * outline - color of border of shape
     * pen - leaves trail if penDown()
     * points - points that make up a shape (if needed)
     * rotation - direction of rotation
     * scaleFactor - amount of scaling
     * tag - string for labeling item
     * window - the (last) window a shape is drawn in

    In addition, a shape can have Physics properties, if it is drawn on a window when the window is in "physics" mode:
     * body - represents the Physical object
     * bodyType - either "static" or "dynamic"
     * bounce - 1.0 is 100% bounce; 0.0 is no bounce
     * density - set this before you draw it
     * friction - amount of friction
     * mass - set this after you draw it (with win.mode set to "physics")
     * wrap - set to True to keep shape on the screen

    Shape Methods
    
    All shapes (which includes anything that you would draw on a window) have the following methods:
     * shape.draw(window) - put shape on window's list of items to draw
     * shape.undraw() - Remove shape from a windows list of items to draw
     * shape1.draw(shape2) - draw shape1 into shape2's reference frame
     * shape.drawAt(window, position) - put shape on window's list of items to draw
     * shape1.drawAt(shape2, position) - draw shape1 into shape2's reference frame
     * shape.forward(distance) - move shape in its zero rotate direction
     * shape.getP1() - returns first point of a shape in screen coordinates
     * shape.getP2() - returns second point of a shape in screen coordinates
     * shape.getScreenPoint(p) - given a point relative to the center of a shape, returns a point in global screen coordinates
     * shape.getX() - get the x component of the center of a shape
     * shape.getY() - get the y component of the center of a shape
     * shape.move(dx, dy) - move by delta dx, delta dy
     * shape.moveTo(x, y) - move to x, y
     * shape.penDown() - put the shapes pen down to allow trace when it moves
     * shape.penUp() - put the pen up and stop trace.
     * line = shape.penUp(True) - put the pen up and stop trace, erase trace, and return line
     * shape.setPenColor(color) - short for:
       line = shape.penUp(True)
       line.color = shape.pen.color
       line.draw(window)
       line.drawAt(window, position)
     * shape.penDown()
     * shape.pen.color = color
     * shape.rotate(degrees) - rotate by degrees (positive is counter-clockwise)
     * shape.rotateTo(degrees) - rotate to degrees (0 is to the right, moving positive clockwise)
     * shape.scale(factor) - scale by a percent (.1 is 10% of original; 1.1 is 10% larger)
     * shape.scaleTo(factor) - set scale to factor (1 is 100% of original)
     * shape.setX(x) - set the x component of the center of a shape
     * shape.setY(y) - set the y component of the center of a shape
     * shape.speak(text) - a speech bubble will appear next to shape
    """
    def __init__(self, *args, **kwargs):
        """No constructor available"""
        pass

class Text(Shape):
    """
    Place text on the window.

     * Text((x, y), text)
     * Text(Point(x, y), text)
     * text.fontFace - typeface of font
     * text.fontWeight - bold?
     * text.fontSlant - italics?
     * text.fontSize - size of font
     * text.xJustification - "center" (or "left" or "right")
     * text.yJustification - "center" (or "top" or "bottom")
     * text.width - (read-only) width of text in pixels
     * text.height - (read-only) height of text in pixels

    ::

        from Graphics import *
        win = Window("Shapes", 200, 200)
        shape = Text((100, 100), "Hello, World!")
        shape.fill = Color("blue")
        shape.rotate(45)
        shape.draw(win)
    
    You can find examples of using justification here:
    http://nbviewer.ipython.org/urls/bitbucket.org/ipre/calico/raw/master/notebooks/Python/Graphics%20Text%20Justification.ipynb
    """
    def __init__(self, *args, **kwargs):
        """Creates text"""
        pass

class Rectangle(Shape):
    """
    Draw a rectangle or square.
     * Rectangle((x1, y1), (x2, y2))
     * Rectangle(Point(x1, y1), Point(x2, y2))

    ::

        from Graphics import *
        win = Window("Shapes", 200, 200)
        shape = Rectangle((10, 10), (190, 190))
        shape.fill = Color("lightblue")
        shape.draw(win)
    """
    def __init__(self, *args, **kwargs):
        """Creates a rectangle"""
        pass

class Circle(Shape):
    """
    Draw a circle.

     * Circle((x, y), radius)
     * Circle(Point(x, y), radius)

    ::
 
        from Graphics import *
        win = Window("Shapes", 200, 200)
        shape = Circle((100, 100), 90)
        shape.fill = Color("lightblue")
        shape.draw(win)
    """
    def __init__(self, *args, **kwargs):
        """Creates a circle"""
        pass

class RoundedRectangle(Shape):
    """
    Draw a rounded rectangle or square. New in Calico 1.0.4.

     * RoundedRectangle((x1, y1), (x2, y2), radius)
     * RoundedRectangle(Point(x1, y1), Point(x2, y2), radius)

    ::

        from Graphics import *
        win = Window("Shapes", 200, 200)
        shape = RoundedRectangle((10, 10), (190, 190), 10)
        shape.fill = Color("lightblue")
        shape.draw(win)
    """
    def __init__(self, *args, **kwargs):
        """Creates a rounded rectangle"""
        pass

class Frame(Shape):
    """
    Construct a frame-of-reference, for different reference for rotate, x,y, etc.

     * Frame(x, y)
     * Frame((x, y))

    You create a frame, and then draw onto it:
     * frame = frame(150, 150)
     * rectangle.draw(frame)

    ::

        from Graphics import *
        win = Window()
        car = Frame(150, 150)
        wheel1 = Rectangle((-10, -10), (10, -5))
        wheel2 = Rectangle((-10, 10), (10, 5))
        wheel1.draw(car)
        wheel2.draw(car)
        car.draw(win)
    
        car.rotate(45)
        car.forward(10)

    
    ::

        from Graphics import *
        import time
        
        win = Window("Clock")
        win.mode = "manual"
        
        face = Circle((150, 150), 125)
        face.fill = None
        face.draw(win)
        
        s = Frame(150, 150)
        line = Line((0, 0), (100, 0))
        line.color = Color("red")
        line.draw(s)
        
        m = Frame(150, 150)
        line = Line((0, 0), (75, 0))
        line.color = Color("blue")
        line.border = 2
        line.draw(m)
    
        h = Frame(150, 150)
        line = Line((0, 0), (50, 0))
        line.color = Color("black")
        line.border = 3
        line.draw(h)
        
        s.draw(win)
        m.draw(win)
        h.draw(win)
        
        def main():
            while True:
                t = time.localtime()
                s.rotateTo(t[5]/60 * 360 - 90)
                m.rotateTo(t[4]/60 * 360 - 90)
                h.rotateTo(t[3]/12 * 360 - 90)
                win.step(1)

        win.run(main)
    """
    def __init__(self, *args, **kwargs):
        """Creates a frame"""
        pass

class Picture(Shape):
    """
    Create a picture.
     * Picture(filename) - filename is a string including path
     * Picture(window) - make a picture of the contents of a window
     * Picture(URL) - get a picture from the web
     * Picture(width, height) - make a blank picture width by height
     * Picture(width, height, color) - make a blank picture width by height of a particular color
     * Picture(picture) - makes a copy

    Additional functions:
     * savePicture(filename, picture) - saves image in GIF, JPG or PNG format
     * savePicture("filename.gif", picture, ...) - saves an animated GIF
     * savePicture("filename.gif", [picture, ...]) - saves an animated GIF
     * savePicture("filename.gif", delay, [picture, ...]) - saves an animated GIF
     * savePicture("filename.gif", delay, loop?, [picture, ...]) - saves an animated GIF
     * setTransparent(picture, color) - sets all pixels that match color to be transparent
     * copyPicture(picture) - returns a copy of a picture

    Can also use makePicture(...) for all versions of Picture(...)
    See also Animated Gif Interface

    Properties:
     * alpha - change the alpha of all pixels
     * width - width of image
     * height - height of image
     * Picture.gif
     * from Graphics import *
     * win = Window("Shapes", 200, 200)
     * shape = Picture(190, 190, Color("lightblue"))
     * shape.draw(win)
    
    ::

        from Graphics import *
        shape = Picture("http://4.bp.blogspot.com/__Y5vVtebEEE/SIeNV78jvAI/AAAAAAAABBY/9FIVWXy_Kho/s400/BarackObamaHead.jpg")
        win = Window("Shapes", shape.width, shape.height)
        shape.draw(win)
    
    Functions:
     * getRegion(center_point, width, height, degrees) - get a region of an image
     * setRegion(center_point, width, height, degrees, color) - set a region of an image to color
     * setAlpha(byte) - set alpha for entire picture
     * flipHorizontal() - flip the image horizontally around a vertical axis
     * flipVertical() - flip the image vertically around a horizontal axis
     * toBitmap([option]) - return a Bitmap
     * toPixbuf() - return a Gdk.Pixbuf

    There is support for moving an Image from Calico Processing to Calico Graphics, and back:

    ::

        from Myro import makePicture
        from Processing import createImage

        p = makePicture(100, 100)
        i = createImage(p.toBitmap())   # turn Graphics.Picture into a Bitmap and load in Processing
        i.loadPixels()                  # load the Pixels
        p2 = makePicture(i.toBitmap())  # turn Processing.Image into a Bitmap and load in Graphics

    Bitmap formats are:
    
    ::

        "Alpha", "Canonical", "DontCare", "Extended", "Format16bppArgb1555", "Format16bppGrayScale",
        "Format16bppRgb555", "Format16bppRgb565", "Format1bppIndexed", "Format24bppRgb",
        "Format32bppArgb", "Format32bppPArgb, "Format32bppRgb", "Format48bppRgb",
        "Format4bppIndexed", "Format64bppArgb", "Format64bppPArgb", "Format8bppIndexed",
        "Gdi", "Indexed", "Max", "PAlpha", "Undefined"
    """
    def __init__(self, *args, **kwargs):
        """Creates a picture"""
        pass
