class Body:
  """
  The Farseer body object is the object returned when calling shape.body after drawing the
  body to a window. As such, all functions and attributes below can be accessed with::

    c=Circle((0,0), 10)
    c.draw(win)
    c.body.AngularDamping

  """
  #: Sets the angular (i.e. rotational) velocity of the object, causing it to spin around
  #: its center of mass.
  #: 
  #: Example::
  #:
  #:   r=Rectangle((0,0), (50,50))
  #:   r.draw(win)
  #:   r.body.AngularVelocity = 1
  AngularVelocity = Vector(0, 0)

  
  def __init__(self): 
      """ 
      Constructs a body.
      """
      pass

  def ApplyAngularImpulse(impulse):
      """
      Applies an angular impulse to the object, causing it to spin around it center of mass.
      In contrast to setting the AngularVelocity attribute, applying an angular impulse will
      add or subtract from the current speed. This means that, if the object is rotating
      clockwise, applying a counter-clockwise impulse may simple slow down the rotation,
      rather than having the object immediately start spinning the other way.
      
      Example::
    
        r=Rectangle((0,0), (50,50))
        r.draw(win)
        r.body.ApplyAngularImpulse (1)
      """
      pass
  
  def ApplyForce(force, point=None):
      """
      Applies a force to the provided point on the object. Normally, the point is provided
      in world coordinates, which may be difficult to work with. As such, it is often more
      useful to first transform the point to object coordinates instead with GetWorldPoint.
      If no point is provided, the force is applied to center-of-mass of the object, meaning
      the force will not cause rotation.

      Example::
        from Graphics import *
        win = Window(500, 500)
        win.mode = "physics"
        win.gravity = Vector(0, 0)

        r=Rectangle((0,0), (50,50))
        r.draw(win)
        point = r.body.GetWorldPoint(Vector(1,0))
        r.body.ApplyForce(Vector(0, 1), point)

        win.run()
      """
      pass

  def ApplyLinearImpulse(force, point=None):
      """
      Similar to ApplyForce, but applies an impulse rather than a force. The main difference
      between a force and an impulse is that the impulse will immediately change the speed
      of the object, while a force will only do so after being summed with all other forces.

      As with ApplyForce, the point is provided in world coordinates, which may be difficult
      to work with. As such, it is often more useful to first transform the point to object
      coordinates instead with GetWorldPoint. If no point is provided, the force is applied
      to center-of-mass of the object, meaning the impulse will not cause rotation.

      Example::
        from Graphics import *
        win = Window(500, 500)
        win.mode = "physics"
        win.gravity = Vector(0, 0)

        r=Rectangle((0,0), (50,50))
        r.draw(win)
        point = r.body.GetWorldPoint(Vector(1,0))
        r.body.ApplyLinearImpulse(Vector(0, 1), point)

        win.run()
      """
      pass
  
  def ApplyTorque(impulse):
      """
      ApplyTorque is to ApplyAngularImpulse what ApplyForce is to ApplyLinearImpulse. That
      is, rather than causing an immediate angular acceleration, all different kinds of 
      torque will be summed before the final speed adjustment is made.
      
      Example::
    
        r=Rectangle((0,0), (50,50))
        r.draw(win)
        r.body.ApplyTorque(1)
      """
      pass
      
  def Clone():
      """
      C# method for creating a copy. Should probably never be called from python.
      """
      pass
  
  def CreateFixture(shape):
      """
      Adds another "fixture" to this body. Will explain more later.
      """
      pass

  def DeepClone():
      """
      C# method for creating a deep copy. Should probably never be called from python.
      """
      pass
  
  def DestroyFixture(fixture):
      """
      Removes and destroys a "fixture" in this body. Will explain more later.
      """
      pass
  
  def Dispose():
      """
      C# destructor. Should probably never be called from python.
      """
      pass
      
  def GetLinearVelocityFromLocalPoint(point):
      """
      Beats me.
      """
      pass

  def GetLocalPoint(point):
      """
      Takes a world point and turns it into a local point.
      """
      pass
      
  def GetLocalVector(point):
      """
      Takes a world vector and turns it into a local vector.
      """
      pass

  def GetTransform():
      """
      I don't have a clue.
      """
      pass
      
  def GetWorldPoint(point):
      """
      Takes a local point and turns it into a world point.
      """
      pass

  def GetLocalVector(point):
      """
      Takes a local vector and turns it into a world vector.
      """
      pass

  def IgnoreCollisionWith(body):
      """
      Presumably allows this body to ignore all collision with the passed body, but I have
      not tested this yet.
      """
      pass

  def ResetDynamics():
      """
      Resets the dynamics of this object. I do not know what that means in practice.
      """
      pass

  def ResetMassData():
      """
      Resets the mass data of this object. Mass data will be recalculated from its fixtures,
      but because most bodies in Calico will only have one fixture, this usually just resets
      the mass to its default (initial) value.
      """
      pass

  def RestoreCollisionWith(body):
      """
      Probably allows you to undo a call to IgnoreCollisionWith.
      """
      pass

  def SetTransform(transform):
      """
      Presumably allows you to change the position and rotation simultaneously.
      """
      pass

  def SetTransformIgnoreContacts(transform):
      """
      I guess similar to SetTransform, but ignores collisions that may occur because of its
      new position or orientation.
      """
      pass
