import traceback

from types import ModuleType
from six import itervalues, text_type, iteritems
from docutils import nodes
from docutils.utils import assemble_option_dict
from docutils.statemachine import ViewList

from sphinx.ext.autosummary import Autosummary, get_import_prefixes_from_env, import_by_name
#import sphinx.ext.autodoc as autodoc
#from sphinx.ext.autodoc import ClassDocumenter
from sphinx.ext.autodoc import AutoDirective, Documenter, ClassDocumenter, bool_option, ALL, Options, AutodocReporter, py_ext_sig_re, ModuleLevelDocumenter, INSTANCEATTR, SUPPRESS
from sphinx.pycode import ModuleAnalyzer, PycodeError
from sphinx.ext.autosummary import mangle_signature, FakeDirective
from sphinx.util import force_decode
from sphinx.util.nodes import nested_parse_with_titles
from docutils.parsers.rst import directives
from sphinx.util.inspect import safe_getattr, safe_getmembers
from sphinx.util.docstrings import prepare_docstring
import re
import sys
import inspect

#help(ClassDocumenter)
#help(autodoc)


def get_documenter(obj, parent, attribute=False):
    """Get an autodoc.Documenter class suitable for documenting the given
    object.

    *obj* is the Python object to be documented, and *parent* is an
    another Python object (e.g. a module or a class) to which *obj*
    belongs to.
    """
    from sphinx.ext.autodoc import AutoDirective, DataDocumenter, ModuleDocumenter

    if inspect.ismodule(obj):
        # ModuleDocumenter.can_document_member always returns False
        return ModuleDocumenter

    # Construct a fake documenter for *parent*
    if parent is not None:
        parent_doc_cls = get_documenter(parent, None, attribute)
    else:
        parent_doc_cls = ModuleDocumenter

    if hasattr(parent, '__name__'):
        parent_doc = parent_doc_cls(FakeDirective(), parent.__name__)
    else:
        parent_doc = parent_doc_cls(FakeDirective(), "")

    # Get the corrent documenter class for *obj*
    classes = [cls for cls in AutoDirective._registry.values()
               if cls.can_document_member(obj, '', attribute, parent_doc)]
    if classes:
        classes.sort(key=lambda cls: cls.priority)
        return classes[-1]
    else:
        return DataDocumenter


class AutoAutoSummary(Autosummary):

    option_spec = {
        'methods': directives.unchanged,
        'attributes': directives.unchanged,
        'instanceattributes': directives.unchanged,
        'functions': directives.unchanged,
        'classes': directives.unchanged,
        'inherited-members': bool_option,
    }

    required_arguments = 1


    def process_instance_attribute(self, name, prefixes):
        try:
            for prefix in prefixes:
                if prefix:
                    prefixed_name = '.'.join([prefix, name])
                else:
                    prefixed_name = name
                name_parts = prefixed_name.split('.')
                # try first interpret `name` as MODNAME.OBJ
                modname = '.'.join(name_parts[:-1])
                if modname:
                    try:
                        __import__(modname)
                        mod = sys.modules[modname]
                        return prefixed_name, INSTANCEATTR, mod, modname
                    except (ImportError, IndexError, AttributeError):
                        pass

                # ... then as MODNAME, MODNAME.OBJ1, MODNAME.OBJ1.OBJ2, ...
                last_j = 0
                modname = None
                for j in reversed(range(1, len(name_parts)+1)):
                    last_j = j
                    modname = '.'.join(name_parts[:j])
                    try:
                        __import__(modname)
                    except ImportError:
                        continue
                    if modname in sys.modules:
                        break

                if last_j < len(name_parts):
                    parent = None
                    obj = sys.modules[modname]
                    for obj_name in name_parts[last_j:]:
                        parent = obj
                        obj = INSTANCEATTR
                    return prefixed_name, obj, parent, modname
                else:
                    return prefixed_name, sys.modules[modname], None, modname
        except (ValueError, ImportError, AttributeError, KeyError) as e:
            raise ImportError(*e.args)


    def get_items(self, names):
        """Try to import the given names, and return a list of
        ``[(name, signature, summary_string, real_name), ...]``.
        """
        # print "Calling my get_items"
        # print "Names:", names
        # print "Instance attributes:", self.instance_attribute_names
        # for name in names:
        #     if name in self.instance_attribute_names:
        #         print "Found instance attribute:", name

        env = self.state.document.settings.env

        prefixes = get_import_prefixes_from_env(env)

        items = []

        max_item_chars = 50


        for name in names:
            display_name = name
            instance_attr = name in self.instance_attribute_names
            if name.startswith('~'):
                name = name[1:]
                display_name = name.split('.')[-1]

            try:
                if instance_attr:
                    real_name, obj, parent, modname = self.process_instance_attribute(name, prefixes=prefixes)
                else:
                    real_name, obj, parent, modname = import_by_name(name, prefixes=prefixes)
            except ImportError:
                self.warn('failed to import %s' % name)
                items.append((name, '', '', name))
                continue

            self.result = ViewList()  # initialize for each documenter
            full_name = real_name
            if not isinstance(obj, ModuleType):
                # give explicitly separated module name, so that members
                # of inner classes can be documented
                full_name = modname + '::' + full_name[len(modname)+1:]
            # NB. using full_name here is important, since Documenters
            #     handle module prefixes slightly differently
            documenter = get_documenter(obj, parent, instance_attr)(self, full_name)
            if not documenter.parse_name():
                self.warn('failed to parse name %s' % real_name)
                items.append((display_name, '', '', real_name))
                continue
            if not documenter.import_object():
                self.warn('failed to import object %s' % real_name)
                items.append((display_name, '', '', real_name))
                continue
            if not documenter.check_module():
                continue

            # try to also get a source code analyzer for attribute docs
            try:
                documenter.analyzer = ModuleAnalyzer.for_module(
                    documenter.get_real_modname())
                # parse right now, to get PycodeErrors on parsing (results will
                # be cached anyway)
                documenter.analyzer.find_attr_docs()
            except PycodeError as err:
                documenter.env.app.debug(
                    '[autodoc] module analyzer failed: %s', err)
                # no source file -- e.g. for builtin and C modules
                documenter.analyzer = None

            # -- Grab the signature

            sig = documenter.format_signature()
            if not sig:
                sig = ''
            else:
                max_chars = max(10, max_item_chars - len(display_name))
                sig = mangle_signature(sig, max_chars=max_chars)
                sig = sig.replace('*', r'\*')

            # -- Grab the summary

            documenter.add_content(None)
            doc = list(documenter.process_doc([self.result.data]))

            while doc and not doc[0].strip():
                doc.pop(0)

            # If there's a blank line, then we can assume the first sentence /
            # paragraph has ended, so anything after shouldn't be part of the
            # summary
            for i, piece in enumerate(doc):
                if not piece.strip():
                    doc = doc[:i]
                    break

            # Try to find the "first sentence", which may span multiple lines
            m = re.search(r"^([A-Z].*?\.)(?:\s|$)", " ".join(doc).strip())
            if m:
                summary = m.group(1).strip()
            elif doc:
                summary = doc[0].strip()
            else:
                summary = ''

            items.append((display_name, sig, summary, real_name))

        return items

    @staticmethod
    def get_members(obj, typ, include_public=[], inherited_members=False):
        items = []

        if inherited_members:
            # safe_getmembers() uses dir() which pulls in members from all
            # base classes
            members = safe_getmembers(obj, attr_getter=Documenter.get_attr)
        else:
            # __dict__ contains only the members directly defined in
            # the class (but get them via getattr anyway, to e.g. get
            # unbound method objects instead of function objects);
            # using keys() because apparently there are objects for which
            # __dict__ changes while getting attributes
            try:
                obj_dict = Documenter.get_attr(obj, '__dict__')
            except AttributeError:
                members = []
            else:
                members = [(mname, Documenter.get_attr(obj, mname, None))
                           for mname in obj_dict.keys()]

        for mname, member in members:
            try:
                documenter = get_documenter(safe_getattr(obj, mname), obj)
            except AttributeError:
                continue
            if documenter.objtype == typ or typ == "all":
                items.append(mname)
        public = [x for x in items if x in include_public or not x.startswith('_')]
        return public, items


    def get_sourcename(self):
        #if self.analyzer:
        #    # prevent encoding errors when the file name is non-ASCII
        #    if not isinstance(self.analyzer.srcname, text_type):
        #        filename = text_type(self.analyzer.srcname,
        #                             sys.getfilesystemencoding(), 'replace')
        #    else:
        #        filename = self.analyzer.srcname
        #    return u'%s:docstring of %s' % (filename, self.fullname)
        return u'docstring of %s' % self.fullname


    def resolve_name(self, modname, parents, path, base):
        if modname is None:
            if path:
                modname = path.rstrip('.')
            else:
                # if documenting a toplevel object without explicit module,
                # it can be contained in another auto directive ...
                modname = self.env.temp_data.get('autodoc:module')
                # ... or in the scope of a module directive
                if not modname:
                    modname = self.env.ref_context.get('py:module')
                # ... else, it stays None, which means invalid
        return modname, parents + [base]


    def parse_name(self):
        """Determine what module to import and what attribute to document.

        Returns True and sets *self.modname*, *self.objpath*, *self.fullname*,
        *self.args* and *self.retann* if parsing and resolving was successful.
        """
        # first, parse the definition -- auto directives for classes and
        # functions can contain a signature which is then used instead of
        # an autogenerated one
        try:
            explicit_modname, path, base, args, retann = \
                py_ext_sig_re.match(self.name).groups()
        except AttributeError:
            self.directive.warn('invalid signature for auto%s (%r)' %
                                (self.objtype, self.name))
            return False

        # support explicit module and class name separation via ::
        if explicit_modname is not None:
            modname = explicit_modname[:-2]
            parents = path and path.rstrip('.').split('.') or []
        else:
            modname = None
            parents = []

        self.modname, self.objpath = \
            self.resolve_name(modname, parents, path, base)

        if not self.modname:
            return False

        self.args = args
        self.retann = retann
        self.fullname = (self.modname or '') + \
                        (self.objpath and '.' + '.'.join(self.objpath) or '')
        return True


    def write_header(self):
        sourcename = self.get_sourcename()
        viewList = ViewList()
        if self.methods:
            viewList.append(u'.. Rubric:: Methods', sourcename)
            viewList.append(u'', sourcename)
        elif self.attributes:
            viewList.append(u'.. Rubric:: Class Attributes', sourcename)
            viewList.append(u'', sourcename)
        elif self.instanceattributes:
            viewList.append(u'.. Rubric:: Instance Attributes', sourcename)
            viewList.append(u'', sourcename)
        node = nodes.section()
        #node.document = self.state.document
        nested_parse_with_titles(self.state, viewList, node)
        return node.children

    def getBaseName(self, name):
        return name.split(".")[-1]

    def run(self):
        # Set local attributes based on provided argument
        self.env = env = self.state.document.settings.env
        self.name = self.arguments[0].strip().split()[0]
        self.parse_name()
        prefixes = get_import_prefixes_from_env(env)

        # Parse options
        self.inherited_members = 'inherited-members' in self.options
        self.methods = 'methods' in self.options
        self.attributes = 'attributes' in self.options
        self.instanceattributes = 'instanceattributes' in self.options
        self.functions = 'functions' in self.options
        self.classes = 'classes' in self.options
        self.instance_attribute_names = []

        # Use analyzer to retrieve runtime attributes
        #print "Name:", self.modname
        self.analyzer = ModuleAnalyzer.for_module(self.modname)
        self.analyzer.find_attr_docs()

        # Import the object and extract the contents to be summarized
        self.content = []
        try:
            real_name, obj, parent, modname = import_by_name(self.name, prefixes=prefixes)
            if self.methods:
                _, methods = self.get_members(obj, 'method', ['__init__'], self.inherited_members)
                self.content = ["~%s.%s" % (self.name, method) for method in methods if not method.startswith('_')]
            if self.attributes:
                _, attribs = self.get_members(obj, 'attribute', [], self.inherited_members)
                self.content = ["~%s.%s" % (self.name, attrib) for attrib in attribs if not attrib.startswith('_')]
            if self.functions:
                _, attribs = self.get_members(obj, 'function', [], self.inherited_members)
                self.content = ["~%s.%s" % (self.name, attrib) for attrib in attribs if not attrib.startswith('_')]
            if self.classes:
                _, attribs = self.get_members(obj, 'class', [], self.inherited_members)
                self.content = ["~%s.%s" % (self.name, attrib) for attrib in attribs if not attrib.startswith('_')]
            if self.instanceattributes:
                _, members = self.get_members(obj, 'all', [], self.inherited_members)
                membernames = set(m[0] for m in members)
                analyzed_member_names = set()
                attr_docs = self.analyzer.find_attr_docs()
                namespace = '.'.join(self.objpath)
                for item in iteritems(attr_docs):
                    if item[0][0] == namespace:
                        analyzed_member_names.add(item[0][1])
                for aname in analyzed_member_names:
                    if aname not in membernames:
                        if not aname.startswith('_'):
                            #print "aname:", aname
                            full_name = "~%s.%s" % (self.name, aname)
                            self.instance_attribute_names.append(full_name)
                            self.content.append(full_name)

        except (Exception) as e:
            errmsg = 'autoautosummary: failed to import %s' % (self.arguments[0])
            errmsg += '; the following exception was raised:\n%s' % traceback.format_exc()
            print errmsg
        finally:
            self.content.sort(key=self.getBaseName)
            nodes = super(AutoAutoSummary, self).run()

            # If there was content for this summary, write the header
            if len(self.content) > 0:
                nodes = self.write_header() + nodes
            return nodes


class CustomClassDocumenter(ClassDocumenter):
    """
    Specialized Documenter subclass for functions.
    """
    objtype = "customclass"
    titles_allowed = True

    option_spec = {
        'function': directives.unchanged,
        'exception': directives.unchanged,
        'module': directives.unchanged,
        'method': directives.unchanged,
        'instanceattribute': directives.unchanged,
        'attribute': directives.unchanged,
        'data': directives.unchanged,
        'class': directives.unchanged,
        'submembers': directives.unchanged,
        'show-inheritance': bool_option,
    }

    def set_module(self):
        self.add_line(u'', self.sourcename)
        self.add_line(u'.. currentmodule:: ' + self.real_modname, self.sourcename)
        self.add_line(u'', self.sourcename)        

    def add_partial_content(self, more_content, content_type='class'):
        cached_docstring = self._new_docstrings
        default_value = self.env.config.autoclass_content
        self.env.config.autoclass_content = content_type
        self._new_docstrings = None
        self.add_content(more_content)
        self.add_line(u'', self.sourcename)
        self.env.config.autoclass_content = default_value
        self._new_docstrings = cached_docstring   

    def add_custom_directive_header(self, sig):
        self.objtype = "class"
        self.add_directive_header(sig)
        self.objtype = "customclass"
        self.add_line(u'', self.sourcename)

    def add_header(self, header):
        #Headers can not have ANY indent
        self.indent = u''
        self.add_line(u'' + header, self.sourcename)
        self.add_line(u'-----------------------------------', self.sourcename)
        self.add_line(u'', self.sourcename)

    def add_summary(self, summary_type, classname):
        if summary_type == 'constructor':
            self.add_line(u'.. rubric:: Constructor', self.sourcename)
            self.add_line(u'', self.sourcename)
            self.add_line(u'.. autosummary:: ' + classname, self.sourcename)
            self.add_line(u'', self.sourcename)
        else:
            self.add_line(u'.. autoautosummary:: ' + classname, self.sourcename)
            self.add_line(u'    :' +summary_type + ':', self.sourcename)
            self.add_line(u'', self.sourcename)

    def add_members(self, member_type, classname):
        self.add_line(u'.. autoclassmembers:: ' + classname, self.sourcename)
        self.add_line(u'    :' + member_type + ':', self.sourcename)
        self.add_line(u'    :submembers:', self.sourcename)
        self.add_line(u'    :undoc-members:', self.sourcename)
        if member_type == "instanceattribute": 
            self.add_line(u'    :annotation:', self.sourcename)
        self.add_line(u'', self.sourcename)

    def add_rubric(self, title):
        self.add_line(u'.. rubric:: ' + title, self.sourcename)
        self.add_line(u'', self.sourcename)

    def generate(self, more_content=None, real_modname=None,
                 check_module=False, all_members=False):
        """Generate reST for the object given by *self.name*, and possibly for
        its members.

        If *more_content* is given, include that content. If *real_modname* is
        given, use that module name to find attribute docs. If *check_module* is
        True, only generate if the object is defined in the module name it is
        imported from. If *all_members* is True, document all members.
        """
        if not self.parse_name():
            # need a module to import
            self.directive.warn(
                'don\'t know which module to import for autodocumenting '
                '%r (try placing a "module" or "currentmodule" directive '
                'in the document, or giving an explicit module name)'
                % self.name)
            return

        # now, import the module and get object to document
        if not self.import_object():
            return

        # If there is no real module defined, figure out which to use.
        # The real module is used in the module analyzer to look up the module
        # where the attribute documentation would actually be found in.
        # This is used for situations where you have a module that collects the
        # functions and classes of internal submodules.
        self.real_modname = real_modname or self.get_real_modname()

        # try to also get a source code analyzer for attribute docs
        try:
            self.analyzer = ModuleAnalyzer.for_module(self.real_modname)
            # parse right now, to get PycodeErrors on parsing (results will
            # be cached anyway)
            self.analyzer.find_attr_docs()
        except PycodeError as err:
            self.env.app.debug('[autodoc] module analyzer failed: %s', err)
            # no source file -- e.g. for builtin and C modules
            self.analyzer = None
            # at least add the module.__file__ as a dependency
            if hasattr(self.module, '__file__') and self.module.__file__:
                self.directive.filename_set.add(self.module.__file__)
        else:
            self.directive.filename_set.add(self.analyzer.srcname)

        # check __module__ of object (for members not given explicitly)
        if check_module:
            if not self.check_module():
                return

        self.sourcename = self.get_sourcename()
        sig = self.format_signature()
                
        # e.g. the module directive doesn't have content
        self.indent = u''

        classname = self.name.split('.')[-1]
        classname = classname.split('::')[-1]

        # introduction
        self.set_module()
        self.add_header(classname)
        self.add_partial_content(more_content, 'class')

        # summary
        self.add_summary('constructor', classname)
        self.add_summary('attributes', classname)
        self.add_summary('instanceattributes', classname)
        self.add_summary('methods', classname)
        
        # details
        self.add_rubric('Details')
        self.add_custom_directive_header(sig)
        self.indent = self.content_indent
        self.add_partial_content(more_content, 'init')
        self.add_members('attribute', classname)
        self.add_members('instanceattribute', classname)
        self.add_members('method', classname)


class MemberDocumenter(Documenter):
    """
    Specialized Documenter subclass for functions.
    """
    titles_allowed = True

    option_spec = {
        'function': directives.unchanged,
        'exception': directives.unchanged,
        'module': directives.unchanged,
        'method': directives.unchanged,
        'instanceattribute': directives.unchanged,
        'attribute': directives.unchanged,
        'data': directives.unchanged,
        'class': directives.unchanged,
        'submembers': directives.unchanged,
        'customclass': directives.unchanged,
        'undoc-members': bool_option,
        'show-inheritance': bool_option,
        'annotation': directives.unchanged
    }

    @classmethod
    def can_document_member(cls, member, membername, isattr, parent):
        # Should never be called to document members, but just in case
        return False


    def document_members(self, all_members=False, specific_members=[]):
        """Generate reST for member documentation.

        If *all_members* is True, do all members, else those given by
        *self.options.members*.
        """
        # set current namespace for finding members
        self.env.temp_data['autodoc:module'] = self.modname
        if self.objpath:
            self.env.temp_data['autodoc:class'] = self.objpath[0]

        want_all = all_members or self.options.inherited_members or \
            self.options.members is ALL
        # find out which members are documentable
        members_check_module, members = self.get_object_members(want_all)

        # remove members given by exclude-members
        if self.options.exclude_members:
            members = [(membername, member) for (membername, member) in members
                       if membername not in self.options.exclude_members]

        # document non-skipped members
        memberdocumenters = []
        for (mname, member, isattr) in self.filter_members(members, want_all):
            #print mname, member, isattr
            if len(specific_members) > 0:
                classes = []
                for specific_member in specific_members:
                    # JH: the attribute documenter thinks it can handle instance attributes
                    # but it can not; explicitly ignore these cases
                    if member is INSTANCEATTR and specific_member == "attribute":
                        continue
                    cls = AutoDirective._registry[specific_member]
                    if cls.can_document_member(member, mname, isattr, self):
                        classes.append(cls)
            else:
                classes = [cls for cls in itervalues(AutoDirective._registry)
                           if cls.can_document_member(member, mname, isattr, self)]
            if not classes:
                # don't know how to document this member
                continue
            # prefer the documenter with the highest priority
            classes.sort(key=lambda cls: cls.priority)
            # give explicitly separated module name, so that members
            # of inner classes can be documented
            full_mname = self.modname + '::' + \
                '.'.join(self.objpath + [mname])
            documenter = classes[-1](self.directive, full_mname, self.indent)
            memberdocumenters.append((documenter, isattr))
        member_order = self.options.member_order or \
            self.env.config.autodoc_member_order
        if member_order == 'groupwise':
            # sort by group; relies on stable sort to keep items in the
            # same group sorted alphabetically
            memberdocumenters.sort(key=lambda e: e[0].member_order)
        elif member_order == 'bysource' and self.analyzer:
            # sort by source order, by virtue of the module analyzer
            tagorder = self.analyzer.tagorder

            def keyfunc(entry):
                fullname = entry[0].name.split('::')[1]
                return tagorder.get(fullname, len(tagorder))
            memberdocumenters.sort(key=keyfunc)

        if len(memberdocumenters) > 0:
            sourcename = self.get_sourcename()
            self.indent = u''
            if "attribute" in specific_members:
                self.add_line(u'', sourcename)
                self.add_line(u'.. rubric:: Class Attributes', sourcename)
                self.add_line(u'', sourcename)
            elif "instanceattribute" in specific_members:
                self.add_line(u'', sourcename)
                self.add_line(u'.. rubric:: Instance Attributes', sourcename)
                self.add_line(u'', sourcename)
            elif "method" in specific_members:
                self.add_line(u'', sourcename)
                self.add_line(u'.. rubric:: Methods', sourcename)
                self.add_line(u'', sourcename)

        for documenter, isattr in memberdocumenters:
            submembers = 'submembers' in self.options
            documenter.options.show_inheritance = 'show-inheritance' in self.options
            if 'annotation' in self.options:
                if self.options['annotation'] == "":
                    documenter.options.annotation = SUPPRESS
                else:
                    documenter.options.annotation = self.options['annotation']
            documenter.indent = u''
            documenter.generate(
                all_members=submembers, real_modname=self.real_modname,
                check_module=members_check_module and not isattr)

        # reset current objects
        self.env.temp_data['autodoc:module'] = None
        self.env.temp_data['autodoc:class'] = None


    def add_directive_header(self, sig):
        """Add the directive header and options to the generated content."""
        domain = getattr(self, 'domain', 'py')
        print(domain)
        directive = getattr(self, 'directivetype', self.objtype)
        name = self.format_name()
        sourcename = self.get_sourcename()
        temp = u'.. %s:%s:: %s%s' % (domain, directive, name, sig)
        print(temp)
        #self.add_line(u'.. %s:%s:: %s%s' % (domain, directive, name, sig), sourcename)
        if self.options.noindex:
            self.add_line(u'   :noindex:', sourcename)
        if self.objpath:
            # Be explicit about the module, this is necessary since .. class::
            # etc. don't support a prepended module name
            #self.add_line(u'   :module: %s' % self.modname, sourcename)
            pass


    def generate(self, more_content=None, real_modname=None,
                 check_module=False, all_members=False):
        """Generate reST for the object given by *self.name*, and possibly for
        its members.

        If *more_content* is given, include that content. If *real_modname* is
        given, use that module name to find attribute docs. If *check_module* is
        True, only generate if the object is defined in the module name it is
        imported from. If *all_members* is True, document all members.
        """
        if not self.parse_name():
            # need a module to import
            self.directive.warn(
                'don\'t know which module to import for autodocumenting '
                '%r (try placing a "module" or "currentmodule" directive '
                'in the document, or giving an explicit module name)'
                % self.name)
            return

        # now, import the module and get object to document
        if not self.import_object():
            return

        # If there is no real module defined, figure out which to use.
        # The real module is used in the module analyzer to look up the module
        # where the attribute documentation would actually be found in.
        # This is used for situations where you have a module that collects the
        # functions and classes of internal submodules.
        self.real_modname = real_modname or self.get_real_modname()

        # try to also get a source code analyzer for attribute docs
        try:
            self.analyzer = ModuleAnalyzer.for_module(self.real_modname)
            # parse right now, to get PycodeErrors on parsing (results will
            # be cached anyway)
            self.analyzer.find_attr_docs()
        except PycodeError as err:
            self.env.app.debug('[autodoc] module analyzer failed: %s', err)
            # no source file -- e.g. for builtin and C modules
            self.analyzer = None
            # at least add the module.__file__ as a dependency
            if hasattr(self.module, '__file__') and self.module.__file__:
                self.directive.filename_set.add(self.module.__file__)
        else:
            self.directive.filename_set.add(self.analyzer.srcname)

        # check __module__ of object (for members not given explicitly)
        if check_module:
            if not self.check_module():
                return

        sourcename = self.get_sourcename()

        # format the object's signature, if any
        sig = self.format_signature()

        # generate the directive header and options, if applicable
        #self.add_directive_header(sig)
        #self.add_line(u'', sourcename)

        # e.g. the module directive doesn't have content
        self.indent = u''

        # make sure that the result starts with an empty line.  This is
        # necessary for some situations where another directive preprocesses
        # reST and no starting newline is present
        self.add_line(u'', sourcename)

        #Gather types for which we have a documenter
        member_types = []
        for key in AutoDirective._registry.keys():
            member_types.append(key)

        # Add specific members if added by option
        specific_members = []
        for key in member_types:
            if key in self.options:
                specific_members.append(key)

        # document members, if possible
        self.document_members(True, specific_members)


class ModuleMemberDocumenter(MemberDocumenter):
    objtype = "modulemembers"

    def get_object_members(self, want_all):
        if want_all:
            if not hasattr(self.object, '__all__'):
                # for implicit module members, check __module__ to avoid
                # documenting imported objects
                return True, safe_getmembers(self.object)
            else:
                memberlist = self.object.__all__
                # Sometimes __all__ is broken...
                if not isinstance(memberlist, (list, tuple)) or not \
                   all(isinstance(entry, string_types) for entry in memberlist):
                    self.directive.warn(
                        '__all__ should be a list of strings, not %r '
                        '(in module %s) -- ignoring __all__' %
                        (memberlist, self.fullname))
                    # fall back to all members
                    return True, safe_getmembers(self.object)
        else:
            memberlist = self.options.members or []
        ret = []
        for mname in memberlist:
            try:
                ret.append((mname, safe_getattr(self.object, mname)))
            except AttributeError:
                self.directive.warn(
                    'missing attribute mentioned in :members: or __all__: '
                    'module %s, attribute %s' % (
                        safe_getattr(self.object, '__name__', '???'), mname))
        return False, ret

    def resolve_name(self, modname, parents, path, base):
        if modname is not None:
            self.directive.warn('"::" in automodule name doesn\'t make sense')
        return (path or '') + base, []


class ClassMemberDocumenter(MemberDocumenter):
    objtype = "classmembers"

    def resolve_name(self, modname, parents, path, base):
        if modname is None:
            if path:
                modname = path.rstrip('.')
            else:
                # if documenting a toplevel object without explicit module,
                # it can be contained in another auto directive ...
                modname = self.env.temp_data.get('autodoc:module')
                # ... or in the scope of a module directive
                if not modname:
                    modname = self.env.ref_context.get('py:module')
                # ... else, it stays None, which means invalid
        return modname, parents + [base]


class ExceptionMemberDocumenter(ClassMemberDocumenter):
    objtype = "exceptionmembers"


class MyAutoDirective(AutoDirective):
    def run(self):
        print "*** HERE ***"
        self.filename_set = set()  # a set of dependent filenames
        self.reporter = self.state.document.reporter
        self.env = self.state.document.settings.env
        self.warnings = []
        self.result = ViewList()

        try:
            source, lineno = self.reporter.get_source_and_line(self.lineno)
        except AttributeError:
            source = lineno = None
        self.env.app.debug('[autodoc] %s:%s: input:\n%s',
                           source, lineno, self.block_text)

        # find out what documenter to call
        objtype = self.name[4:]
        doc_class = self._registry[objtype]
        # add default flags
        for flag in self._default_flags:
            if flag not in doc_class.option_spec:
                continue
            negated = self.options.pop('no-' + flag, 'not given') is None
            if flag in self.env.config.autodoc_default_flags and \
               not negated:
                self.options[flag] = None
        # process the options with the selected documenter's option_spec
        try:
            self.genopt = Options(assemble_option_dict(
                self.options.items(), doc_class.option_spec))
        except (KeyError, ValueError, TypeError) as err:
            # an option is either unknown or has a wrong type
            msg = self.reporter.error('An option to %s is either unknown or '
                                      'has an invalid value: %s' % (self.name, err),
                                      line=self.lineno)
            return [msg]
        # generate the output
        documenter = doc_class(self, self.arguments[0])
        documenter.generate(more_content=self.content)
        if not self.result:
            return self.warnings

        self.env.app.debug2('[autodoc] output:\n%s', '\n'.join(self.result))

        # record all filenames as dependencies -- this will at least
        # partially make automatic invalidation possible
        for fn in self.filename_set:
            self.state.document.settings.record_dependencies.add(fn)

        # use a custom reporter that correctly assigns lines to source
        # filename/description and lineno
        old_reporter = self.state.memo.reporter
        self.state.memo.reporter = AutodocReporter(self.result,
                                                   self.state.memo.reporter)

        print("RESULT:", self.result)
        print("STATE:", self.state)
        print("Before error?")
        if documenter.titles_allowed:
            print "With title"
            node = nodes.section()
            # necessary so that the child nodes get the right source/line set
            node.document = self.state.document
            nested_parse_with_titles(self.state, self.result, node)
        else:
            print "Without title"
            node = nodes.paragraph()
            node.document = self.state.document
            self.state.nested_parse(self.result, 0, node)
        print("WARNINGS:",self.warnings)
        print("CHILDREN:",node.children)
        for child in node.children:
            print ("CHILD:", child.astext())
            #help(child)
        print("After error?")
        self.state.memo.reporter = old_reporter
        return self.warnings + node.children



def setup(app):
    app.add_directive('autoautosummary', AutoAutoSummary)
    #app.add_directive('auto' + ClassMemberDocumenter.objtype, AutoDirective)
    app.add_autodocumenter(ClassMemberDocumenter)
    #app.add_directive('auto' + ModuleMemberDocumenter.objtype, AutoDirective)
    app.add_autodocumenter(ModuleMemberDocumenter)
    #app.add_directive('auto' + ExceptionMemberDocumenter.objtype, AutoDirective)
    app.add_autodocumenter(ExceptionMemberDocumenter)
    #app.add_directive('auto' + CustomClassDocumenter.objtype, AutoDirective)
    app.add_autodocumenter(CustomClassDocumenter)

    return {'version': 1.0, 'parallel_read_safe': True}
