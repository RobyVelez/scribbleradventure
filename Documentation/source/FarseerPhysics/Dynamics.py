from Microsoft.Xna.Framework import Vector2

def Vector(x, y):
  return Vector2(x, y)

class Body:
  """
  The Farseer body object is the object returned when calling shape.body after drawing the
  body to a window. As such, all functions and attributes below can be accessed with::

    c=Circle((0,0), 10)
    c.draw(win)
    c.body.AngularDamping

  """

  #: Applies a constant slow-down on rotations, thus preventing objects from spinning
  #: forever. You should probably add some angular damping to all your objects, unless
  #: you specifically WANT the object to spin forever.
  #: 
  #: Example::
  #:  
  #:   r=Rectangle((0,0), (50,50))
  #:   r.draw(win)
  #:   r.body.AngularDamping = 0.5
  #:   r.body.AngularVelocity = 1
  AngularDamping = 0
  
  #: Sets the angular velocity of the object, causing it to spin around
  #: its center of mass.
  #: 
  #: Example::
  #:
  #:   r=Rectangle((0,0), (50,50))
  #:   r.draw(win)
  #:   r.body.AngularVelocity = 1
  AngularVelocity = Vector(0, 0)

  #: Boolean that determines whether the object is awake or not. By default, if an object
  #: did not move or interact with other objects for a while, the object will be put
  #: "asleep" by the physics simulator, which is a mode in which it object will no longer
  #: be considered when calculating movements and forces. If you notice odd behavior of an
  #: object that hasn't been active for a while, it may be possible that you need to "wake"
  #: the object first.
  Awake = True

  #: Unique identifier of this particular body in the physics simulator. Might be useful
  #: if you want to check the identity of an object your colliding with. Should probably
  #: not be changed, as that could cause issues for the simulator.
  BodyId = 0


  #: The body type of an object is either Static, Dynamic, or Kinematic. Static objects
  #: never move in the physics simulator, though the can be moved manually. Dynamic objects
  #: are the objects will move as a result of forces and collision in the physics 
  #: simulator. I am currently unsure about Kinematic objects.
  BodyType = "dynamic"

  #: The category of objects this object collides with. I will update this entry when I 
  #: learn more.
  CollidesWith = None

  #: Something similar to CollidesWith (maybe these are the categories this object belongs
  #: to). I will update this entry when I learn more.
  CollisionCategories = None


  #: Something to do with collisions, but I don't know how it works.
  CollisionGroup = None

  #: The list this object is currently in (potentially) in contact with. The contact list
  #: is useful for determining things like collisions, but it may be a bit tricky to work
  #: with. Here is an example::
  #: 
  #:   from Graphics import *
  #:   win = Window(500, 500)
  #:   win.mode = "physics"
  #: 
  #:   c = Circle((250,0), 25)
  #:   c.draw(win)
  #: 
  #:   r = Rectangle((0, 400), (500, 500))
  #:   r.bodyType = "static"
  #:   r.draw(win)
  #: 
  #:   while True:
  #:      contactList = c.body.ContactList
  #:      while contactList:
  #:          cont = contactList.Contact
  #:          if cont.IsTouching():
  #:              print("Touching!")
  #:          contactList = contactList.Next
  #:      win.step(0.01)
  ContactList = None 
  
  #: Beats me.
  ControllerFilter = None 
  
  #: Boolean determining whether this body is enabled or not. The exact implications of
  #: this are unclear to me at this point.
  Enabled = True

  #: By settings this to True, the body will no longer rotate as a result of forces applied
  #: to it (you can still rotate it manually). This is useful for objects like the player
  #: character in a platformer.
  FixedRotation = False

  #: The list of fixture part of this body. Will expand on this later.
  FixtureList = None

  #: The friction this body has with other bodies. Friction is only applied when two bodies
  #: are touching or colliding, and thus, this does not simulate air friction.
  Friction = 1

  #: CCD stands for Continuous Collision Detection, and it is form of collision detection
  #: that prevents one object from being able to go through another object if it goes fast 
  #: enough. By default CCD is enabled between dynamic and static objects (but not between
  #: dynamic and dynamic objects). Disabling CCD can increase performance.
  IgnoreCCD = False

  #: As the name implies, allows the body to ignore the gravity of the world.
  IgnoreGravity = False
  
  #: Determines how much torque is required to cause this object to rotate.
  Inertia = 1

  #: If True, enables CCD (Continuous Collision Detection) on a dynamic object. The 
  #: attribute is called IsBullet because this property is most useful on fast moving 
  #: bullets that need to hit other dynamic objects (rather than pass through them).
  IsBullet = False

  #: Whether or not tis object is disposed. I do not understand the implications of this.
  IsDisposed = False
  
  #: If True, this object will function as a sensor. It will not collide with anything
  #: (everything will pass through it), but it will call its collision function if one
  #: is installed. Creating sensor objects is useful is you want to create triggered events
  #: based on an area (if the player moves into this room, the doors close and a boss-fight
  #: starts).
  IsSensor = False
  
  #: Indicates whether this object is static or not. Provides effectively the same 
  #: information as BodyType, because Calico does not usually create kinematic bodies.
  IsStatic = False

  #: The list of joints attached to this body. More on this later.
  JointList = None

  #: By setting linear damping the body will constantly slow-down a little. LinearDamping
  #: is effectively a cheap way to simulate some kind of air resistance, and can be useful
  #: if you want an object to behave a bit like a feather.
  LinearDamping = 0
  
  #: The current speed of the body. You can change this to move your body around.
  LinearVelocity = Vector(0,0)

  #: The center of mass in local (relative to the body) coordinates. Changing the local
  #: center can make an object feel "wobbly" as it turns around a different point.
  LocalCenter = Vector(0,0)

  #: The mass of the object. The mass of an object determines how much force is required
  #: to move the object at a certain speed.
  Mass = 1

  #: The list of collision functions.
  OnCollision = None

  #: The list of separation functions.
  OnSeparation = None

  #: Some sort of filter that I don't know how to use.
  PhysicsLogicFilter = None

  #: The current position of the object is simulator coordinates.
  Position = Vector(0,0)

  #: How much velocity will be retained on collision. Determines the "bounce" of the 
  #: object.
  Restitution= 0.5

  #: How many times did this object rotate around its own axis.
  Revolutions = 0
  
  #: The current rotation of the object.
  Rotation = 0
  
  #: Whether or not this object should be allowed to sleep. Setting this to False makes you
  #: a cruel person.
  SleepingAllowed = True
  
  #: Pointer to anything that you want. In calico, by default, this will point back to the
  #: calico shape associated with this body, which is very useful in collision functions.
  UserData = None
  
  #: The center of mass in world coordinates.
  WorldCenter = Vector(0,0)
     
  def ApplyAngularImpulse(impulse):
      """
      Applies an angular impulse to the object, causing it to spin around it center of mass.
      In contrast to setting the AngularVelocity attribute, applying an angular impulse will
      add or subtract from the current speed. This means that, if the object is rotating
      clockwise, applying a counter-clockwise impulse may simple slow down the rotation,
      rather than having the object immediately start spinning the other way.
      
      Example::
    
        r=Rectangle((0,0), (50,50))
        r.draw(win)
        r.body.ApplyAngularImpulse (1)
      """
      pass
  
  def ApplyForce(force, point=None):
      """
      Applies a force to the provided point on the object. Normally, the point is provided
      in world coordinates, which may be difficult to work with. As such, it is often more
      useful to first transform the point to object coordinates instead with GetWorldPoint.
      If no point is provided, the force is applied to center-of-mass of the object, meaning
      the force will not cause rotation.

      Example::
        from Graphics import *
        win = Window(500, 500)
        win.mode = "physics"
        win.gravity = Vector(0, 0)

        r=Rectangle((0,0), (50,50))
        r.draw(win)
        point = r.body.GetWorldPoint(Vector(1,0))
        r.body.ApplyForce(Vector(0, 1), point)

        win.run()
      """
      pass

  def ApplyLinearImpulse(force, point=None):
      """
      Similar to ApplyForce, but applies an impulse rather than a force. The main difference
      between a force and an impulse is that the impulse will immediately change the speed
      of the object, while a force will only do so after being summed with all other forces.

      As with ApplyForce, the point is provided in world coordinates, which may be difficult
      to work with. As such, it is often more useful to first transform the point to object
      coordinates instead with GetWorldPoint. If no point is provided, the force is applied
      to center-of-mass of the object, meaning the impulse will not cause rotation.

      Example::
        from Graphics import *
        win = Window(500, 500)
        win.mode = "physics"
        win.gravity = Vector(0, 0)

        r=Rectangle((0,0), (50,50))
        r.draw(win)
        point = r.body.GetWorldPoint(Vector(1,0))
        r.body.ApplyLinearImpulse(Vector(0, 1), point)

        win.run()
      """
      pass
  
  def ApplyTorque(impulse):
      """
      ApplyTorque is to ApplyAngularImpulse what ApplyForce is to ApplyLinearImpulse. That
      is, rather than causing an immediate angular acceleration, all different kinds of 
      torque will be summed before the final speed adjustment is made.
      
      Example::
    
        r=Rectangle((0,0), (50,50))
        r.draw(win)
        r.body.ApplyTorque(1)
      """
      pass
      
  def Clone():
      """
      C# method for creating a copy. Should probably never be called from python.
      """
      pass
  
  def CreateFixture(shape):
      """
      Adds another "fixture" to this body. Will explain more later.
      """
      pass

  def DeepClone():
      """
      C# method for creating a deep copy. Should probably never be called from python.
      """
      pass
  
  def DestroyFixture(fixture):
      """
      Removes and destroys a "fixture" in this body. Will explain more later.
      """
      pass
  
  def Dispose():
      """
      C# destructor. Should probably never be called from python.
      """
      pass
      
  def GetLinearVelocityFromLocalPoint(point):
      """
      Beats me.
      """
      pass

  def GetLinearVelocityFromWorldPoint(point):
      """
      Beats me.
      """
      pass

  def GetLocalPoint(point):
      """
      Takes a world point and turns it into a local point.
      """
      pass
      
  def GetLocalVector(point):
      """
      Takes a world vector and turns it into a local vector.
      """
      pass

  def GetTransform():
      """
      I don't have a clue.
      """
      pass
      
  def GetWorldPoint(point):
      """
      Takes a local point and turns it into a world point.
      """
      pass

  def GetWorldVector(point):
      """
      Takes a local vector and turns it into a world vector.
      """
      pass

  def IgnoreCollisionWith(body):
      """
      Presumably allows this body to ignore all collision with the passed body, but I have
      not tested this yet.
      """
      pass

  def ResetDynamics():
      """
      Resets the dynamics of this object. I do not know what that means in practice.
      """
      pass

  def ResetMassData():
      """
      Resets the mass data of this object. Mass data will be recalculated from its fixtures,
      but because most bodies in Calico will only have one fixture, this usually just resets
      the mass to its default (initial) value.
      """
      pass

  def RestoreCollisionWith(body):
      """
      Probably allows you to undo a call to IgnoreCollisionWith.
      """
      pass

  def SetTransform(transform):
      """
      Presumably allows you to change the position and rotation simultaneously.
      """
      pass

  def SetTransformIgnoreContacts(transform):
      """
      I guess similar to SetTransform, but ignores collisions that may occur because of its
      new position or orientation.
      """
      pass
