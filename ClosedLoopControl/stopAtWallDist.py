import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class StopWallDistWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.passEntry=None
        LevelWindow.__init__(self,levelName="Stop At Wall-Distance",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    
    def confirmCompletion(self):
        self.checkPassword(self.passEntry.trueText)    
    
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor & ask for the cardboard bricks (see behind brief screen) & a ",Bold("real")," Scribbler robot."])
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create two  ",self.getI("functions1"),". One that makes the Scribbler robot back up towards the block and stop within 12 inches, and one where it stops within 3 inches."])
        text.append(["Use the   ",self.getI("getDistance()"),"   command to sense distance. See the other Brief Screen for sample code."])
        text.append(["When you're done, confirm with a mentor and they will enter the password to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getDistance()"),self.getR("if"),self.getR("while")])                    
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        
        temp+="def stopAtWallDist1():"        
        temp+="\n    #Thresold that determines at what value and distance"
        temp+="\n    #from wall when robot stops"
        temp+="\n    thres=50"
        temp+="\n    while True:"
        temp+="\n        dist=getDistance()"
        temp+="\n        print('Distance is.',dist)"            
        temp+="\n        if dist<thres:"
        temp+="\n              print('At threshold,probably at wall.Stopping.')"
        temp+="\n        else:"
        temp+="\n              print('Not close enough to wall. Still going.')"
        
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getDistance()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
        
        text=[]
        text.append(["Ask mentor to enter password and fastest time for the challenge (optional) once completed."])
        text.append(["\nEnter time in seconds. (optional)."])
        text.append(["\nEnter password."])
        
        
        b=simpleBrief(text,[],IMAGE_PATH+"blankBrief.png",bulletMarker="")                    
        
        timeEntry=EntryBox((40-b.x,130-b.y),(100-b.x,150-b.y))
        timeEntry.draw(b)
        timeEntry.setup(self.sim.window)
        
        self.passEntry=EntryBox((40-b.x,180-b.y),(200-b.x,200-b.y))
        self.passEntry.draw(b)
        self.passEntry.setup(self.sim.window,True,False)
        self.passEntry.maxChars=26
        self.passEntry.justNumbers=False
        
        self.addEntryObject(timeEntry)
        self.addEntryObject(self.passEntry)
        
        submitButton=RoundedButton((40-b.x,225-b.y),(150-b.x,275-b.y),5)
        submitButton.setup("Enter","PasswordEnterButton")
        submitButton.draw(b)
        submitButton.action=self.confirmCompletion
        self.addMouseDown(submitButton.mouseDownAlt)        
        
        self.addBriefShape(b,"Enter Password")
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Stop set distance from blocks w/ getDistance().")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()"],4)
        
                        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"stopAtWallDist.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        
def startLevel(gameData=None,parent=None):
    level=StopWallDistWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    