import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class StopWallObstacleWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.passEntry=None
        LevelWindow.__init__(self,levelName="Stop At Wall-Obstacle",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    
    def confirmCompletion(self):
        self.checkPassword(self.passEntry.trueText)
    
    def createBriefings(self):
        text=[]
        text.append(["Similiar Challenge as previous level, but here you will use ",self.getI("getObstacle()"),"   and   ",self.getI("setIRPower()"),"  to sense distance ",Bold("infront")," of the robot."])
        text.append(["Once connected, create two  ",self.getI("functions1"),". One that makes the Scribbler robot drive forward towards the block and stop within 12 inches, and one where it stops within 3 inches."])
        text.append(["Use the   ",self.getI("getObstacle()"),"  command to sense distance and ",self.getI("setIRPower()")," to set the range of the distance sensor. See the other Brief Screen for sample code."])
        text.append(["When you're done, confirm with a mentor and they will enter the password to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getObstacle()"),self.getR("setIRPower()")])
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        
        temp+="def stopAtWallObs1():"        
        temp+="\n    #Thresold that determines at what distance to stop"
        temp+="\n    thres=3000"
        temp+="\n    #Allows robot to sense objects about 12 inches ahead"        
        temp+="\n    setIRPower(150)"        
        temp+="\n    while True:"
        temp+="\n        left,center,right=getObstacle()"
        temp+="\n        print('Distances are.',left,center,right)" 
        temp+="\n        if center>thres:"
        temp+="\n              print('At threshold,probably at wall.Stopping.)"
        temp+="\n        else:"
        temp+="\n              print('Not close enough to wall. Still going.')"
        
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getDistance()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
        
        text=[]
        text.append(["Ask mentor to enter password and fastest time for the challenge (optional) once completed."])
        text.append(["\nEnter time in seconds. (optional)."])
        text.append(["\nEnter password."])
        
        
        b=simpleBrief(text,[],IMAGE_PATH+"blankBrief.png",bulletMarker="")                    
        
        timeEntry=EntryBox((40-b.x,130-b.y),(100-b.x,150-b.y))
        timeEntry.draw(b)
        timeEntry.setup(self.sim.window)
        
        self.passEntry=EntryBox((40-b.x,180-b.y),(200-b.x,200-b.y))
        self.passEntry.draw(b)
        self.passEntry.setup(self.sim.window,True,False)
        self.passEntry.maxChars=26
        self.passEntry.justNumbers=False
        
        self.addEntryObject(timeEntry)
        self.addEntryObject(self.passEntry)
        
        submitButton=RoundedButton((40-b.x,225-b.y),(150-b.x,275-b.y),5)
        submitButton.setup("Enter","PasswordEnterButton")
        submitButton.draw(b)
        submitButton.action=self.confirmCompletion
        self.addMouseDown(submitButton.mouseDownAlt)  
        self.addBriefShape(b,"Enter Password")
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Stop set distance from blocks w/ getDistance().")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()"])
        self.helpWidgets[5].setButtonsVisible(["getObstacle()","setIRPower()"],8)
        
                        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"stopAtWallObs.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
def startLevel(gameData=None,parent=None):
    level=StopWallObstacleWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    