import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class ClapAndDanceWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.passEntry=None
        LevelWindow.__init__(self,levelName="Clap and Dance",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    
    
    def confirmCompletion(self):
        self.checkPassword(self.passEntry.trueText)
    def createBriefings(self):
        text=[]
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a ",self.getI("functions1")," to make the Scribbler robot dance and ",self.getI("beep()")," when you clap or tap it's microphone."])
        text.append(["Use    ",self.getI("getMicrophone()"),"   to read the loudness received by Scribbler's built in microphone. See Sample Code for example."])
        text.append(["When your robot can dance and beep in response to sound, confirm with a mentor and they will enter the password to unlock the next level."])        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getMicrophone()"),self.getR("beep()")])                    
        self.addBriefShape(b,"Getting Started")
        
        temp=""
        temp+="def danceAndBeep():"        
        temp+="\n    thres=1200000 #Volume threshold"
        temp+="\n    while True:"
        temp+="\n        vol=getMicrophone()"
        temp+="\n        print('Volume is',vol)"        
        temp+="\n        if vol<thres:"
        temp+="\n              print('It is pretty quiet.')"
        temp+="\n        else:"
        temp+="\n              print('I hear something')"
        temp+="\n              print('I should dance and sing.')"
        text=[]
        text.append([temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getMicrophone()"),self.getR("beep()")])  
        self.addBriefShape(b,"Sample Code")
        
        text=[]
        text.append(["Ask mentor to enter password and fastest time for the challenge (optional) once completed."])
        text.append(["\nEnter time in seconds. (optional)."])
        text.append(["\nEnter password."])
        
        
        b=simpleBrief(text,[],IMAGE_PATH+"blankBrief.png",bulletMarker="")                    
        
        timeEntry=EntryBox((40-b.x,130-b.y),(100-b.x,150-b.y))
        timeEntry.draw(b)
        timeEntry.setup(self.sim.window)
        
        self.passEntry=EntryBox((40-b.x,180-b.y),(200-b.x,200-b.y))
        self.passEntry.draw(b)
        self.passEntry.setup(self.sim.window,True,False)
        self.passEntry.maxChars=26
        self.passEntry.justNumbers=False
        
        self.addEntryObject(timeEntry)
        self.addEntryObject(self.passEntry)
        
        submitButton=RoundedButton((40-b.x,225-b.y),(150-b.x,275-b.y),5)
        submitButton.setup("Enter","PasswordEnterButton")
        submitButton.draw(b)
        submitButton.action=self.confirmCompletion
        self.addMouseDown(submitButton.mouseDownAlt)        
        
        self.addBriefShape(b,"Enter Password")
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()        
        #self.helpWidgets[5].setButtonsInvisible()
        self.createBriefings()
        self.setObjectiveText("-Have Scribbler dance and beep in response to sounds.")       
        self.launchBriefScreen(0)
        self.helpWidgets[5].setButtonsVisible(["getDistance()","getObstacle()","setIRPower()"])
        self.helpWidgets[5].setButtonsVisible(["getMicrophone()","beep()"])
        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"dancingRobot.png")   
        
        #glorified event pads
        self.createEndPad(500,600,locked=True)
        self.createStartPad(300,600)
        
        #create the robot
        self.createRobot(300,600,0)  
        
        
def startLevel(gameData=None,parent=None):
    level=ClapAndDanceWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    