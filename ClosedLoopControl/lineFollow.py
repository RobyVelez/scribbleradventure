import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
initCalico(calico)
from setImports import *

#used mostly to grabbing stage specific images
LEVEL_PATH=os.path.dirname(os.path.realpath(__file__))+"/"
                               
class LineFollowWindow(LevelWindow):    
    def __init__(self,gameData,parent):   
        self.passEntry=None
        LevelWindow.__init__(self,levelName="LineFollow",gameData=gameData,parent=parent,debug=True)                    
        self.setup()
    def confirmCompletion(self):
        self.checkPassword(self.passEntry.trueText)    
    
    def createBriefings(self):
        text=[]
        text.append(["Grab a mentor and ask them for one of the line follow challenges (see behind this brief screen) and a ",Bold("real")," Scribbler robot."])
        text.append(["Connect to the ",Bold("real")," Scribbler robot. You can go back to the main portal page for a link to a Scribbler Connection Guide."])
        text.append(["Once connected, create a ",self.getI("functions1")," to make the Scribbler robot perform line following. The next Brief Screen has sample code to get you started."])
        text.append(["Try your lineFollow() function on all line follow challenges. When you're done, confirm with a mentor and they will enter the password to unlock the next level."])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLine()"),self.getR("if")])                    
        self.addBriefShape(b,"Getting Started")
    
        temp=""
        temp+="def lineFollow():"        
        temp+="\n    while True:"
        temp+="\n        left,right=getLine()"
        temp+="\n        print('Sensor values are.',left,right)"            
        temp+="\n        if left==0 and right==0:"
        temp+="\n              print('See line going forward')"
        temp+="\n        elif left==0 and right==1:"
        temp+="\n              print('Line on the left, going left')"
        temp+="\n        elif left==1 and right==0:"
        temp+="\n              print('Line on the right, going right')"
        temp+="\n        elif left==1 and right==1:"
        temp+="\n              print('Can't see line. Have to find it.')"
        temp+="\n        else:"
        temp+="\n              print('Not sure how I got here.')"
        text=[]
        text.append([SetSpacing(3),temp])
        
        b=simpleBrief(text,[self.getR("functions1"),self.getR("getLine()"),self.getR("if"),self.getR("while")])  
        self.addBriefShape(b,"Sample Code")
    
        text=[]
        text.append(["Ask mentor to enter password and fastest time for the challenge (optional) once completed."])
        text.append(["\nEnter time in seconds. (optional)."])
        text.append(["\nEnter password."])
        
        
        b=simpleBrief(text,[],IMAGE_PATH+"blankBrief.png",bulletMarker="")                    
        
        timeEntry=EntryBox((40-b.x,130-b.y),(100-b.x,150-b.y))
        timeEntry.draw(b)
        timeEntry.setup(self.sim.window)
        
        self.passEntry=EntryBox((40-b.x,180-b.y),(200-b.x,200-b.y))
        self.passEntry.draw(b)
        self.passEntry.setup(self.sim.window,True,False)
        self.passEntry.maxChars=26
        self.passEntry.justNumbers=False
        
        self.addEntryObject(timeEntry)
        self.addEntryObject(self.passEntry)
        
        submitButton=RoundedButton((40-b.x,225-b.y),(150-b.x,275-b.y),5)
        submitButton.setup("Enter","PasswordEnterButton")
        submitButton.draw(b)
        submitButton.action=self.confirmCompletion
        self.addMouseDown(submitButton.mouseDownAlt)        
        
        self.addBriefShape(b,"Enter Password")
        
    def onLevelStart(self):
        self.helpWidgets[4].setButtonsVisible()
        self.createBriefings()
        self.setObjectiveText("-Follow the drawn lines.")       
        self.launchBriefScreen(0)
        
    def createLevel(self):        
        self.setBackground(LEVEL_PATH+"LineFollow.png")   
        
        #glorified event pads
        self.createEndPad(450,350,locked=True)
        self.createStartPad(450,450)
        
        #create the robot
        self.createRobot(450,450,-90)  
        
        
def startLevel(gameData=None,parent=None):
    level=LineFollowWindow(gameData,parent)
    return level
    
if __name__ == "__main__":
    startLevel()
    
    