from Myro import *
from Graphics import *
import os, sys
from functools import partial ### JH: partial added to prevent multi-load crash
import pickle #for saving data
import datetime 
import ConfigParser
import Myro #Adds Myro to global scope
import System #for looking at and setting current directory
import shutil #for copying files


sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/GameObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/WindowObjects")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CommonImages")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Graphics")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Diversions")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/HackableDemos")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/ChooseYourOwnAdventure")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/PlatformerBase")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CommonFunctions") #soon to be depreceated
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Gauntlet")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Labyrinth")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/Dungeon")
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/CloudCity")
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
IMAGE_PATH=os.path.dirname(os.path.realpath(__file__))+"/CommonImages/"
HOME_DIR=os.path.dirname(os.path.realpath(__file__))+"/"

#Experimental. Add directory where PortalWindow.py lives into the path. RV
#sys.path.append(os.path.dirname(os.path.realpath(__file__)))

#On the windows machines in 4045 this is needed because those machines
#lose the calico global variable
import setCalico
setCalico.initCalico(calico)
#make sure the shell doesn't reset
calico.config.SetValue('python-language','reset-shell-on-run',False)
#calico.config.SetValue('python-language','reset-show-on-run',False)

from windowObjects import *
from userFunctions import *

#clears LevelObject b/c reset shell doesn't clear the variable
clearLevelObject()


DEBUG=True
   
#mostly used for debugging purposes
STOP_LOGIN=False

#used mainly in recordClicks
DEBUG_LOGINDATES=False
debugDate="2017-02-21"

#Version number. Relates to save data and how it is structured
from version import VER                                         

#If in ROBOTICS_ROOOM then checks preset locations for club and data path. If they are not found then
#clubPath and dataPath are left as None.Used in the setPaths() function.
ROBOTICS_ROOM=False
#only used if ROBOTICS_ROOM is True. Else club and data folders are put into a local folder
CLUB_LOC="C:\\Users\\lrcguest01\\Dropbox"
DATA_LOC="N:\\LRCData"
                                                                                                                           
class Portal(MenuWindow):
    
    def __init__(self):        
        self.setPaths()        
        #Config object that writes and reads the user data from a text file
        self.config = ConfigParser.SafeConfigParser()        
        success=self.prompt()
        if not success:
            print("Error failure in login process.")
        if self.gameDataLoc==None:
            print("Error no correct path for gameData. Using generic gameData")
            self.gameData={}
        else:
            try:
                self.gameData=self.loadGameData()                    
            except:
                print("\Failure to load game data. Using generic.\n")
                self.gameData={}    
        if self.workspaceLoc==None:
            print("Error no location for user workspace.")
        
                
        ##############################################################
        ###########Everything Else                        ############
        ##############################################################
        
        #if there is a previous window shut it down 
        try:
            existingWin=getWindow()
            existingWin.Destroy()
            if LevelWindow!=None:
                LevelWindow=None
        except:
            pass
        
        #create the main window
        MenuWindow.__init__(self, None, "LRC_Portal", "Activities Portal")
        
        #items on left side/panel of window, includes Activities label and activity buttons
        #text=Text((-275,-70), "Activities", color=Color("black"))
        #text.yJustification = "bottom" 
        #text.xJustification = "left"
        #text.fontFace = "Helvetica Neue Light"
        #text.fontSize = 30
        #text.draw(self.mainPanel)
        self.mainPanel.addText((25,225),"Activities",fontColor=Color("black"),xJustification="left",yJustification="bottom",fontSize=30,fontType="Helvetica Neue Light")
        
        #avatar for scrigAdv button
        scribAdAvatar = Picture(IMAGE_PATH + "/ScribLogo.png")
        scribAdAvatar.moveTo(0,0)
        scribAdAvatar.border = 0
        scribAdAvatar.scale(0.2)
        
        self.scribAdBut = self.mainPanel.addButton(25,250,250,40,self.launchActivity,"Scribbler Adventure", avatar=scribAdAvatar)
        self.scribAdBut.text.visible=False
        self.mainPanel.addButton(25,300,250,40,self.launchActivity,"Graphics")
        self.mainPanel.addButton(25,350,250,40,self.launchActivity,"Platformer")
        self.mainPanel.addButton(25,400,250,40,self.launchActivity,"Choose Your Own Adventure")
        self.mainPanel.addButton(25,450,250,40,self.launchActivity,"Hackable Demos")
        self.mainPanel.addButton(25,500,250,40,self.launchActivity,"Diversions")

        #Items on right side/panel of window, includes Documentation label and documentation buttons for links        
        #text=Text((25,-70), "Documentation", color=Color("black"))
        #text.yJustification = "bottom" 
        #text.xJustification = "left"
        #text.fontFace = "Helvetica Neue Light"
        #text.fontSize = 30
        #text.draw(self.mainPanel)        
        self.mainPanel.addText((325,225),"Documentation",fontColor=Color("black"),xJustification="left",yJustification="bottom",fontSize=30,fontType="Helvetica Neue Light")
        
        self.mainPanel.addButton(325,250,250,40,self.launchActivity,"LRC Wiki")
        self.mainPanel.addButton(325,300,250,40,self.launchActivity,"Connect Scribbler Robot")
        

        ##################################
        ####Configure Calcio For User#####        
        ##################################
        
        #set current directory to be user's workspace folder
        #note in launchActivity the Current Directory has to be set back to the
        #location of PortalWindow.py for things to work and be found properly
        if self.workspaceLoc!=None:
            print("Workspace is ",self.workspaceLoc)
            System.IO.Directory.SetCurrentDirectory(self.workspaceLoc)
            print("Current directory is ",System.IO.Directory.GetCurrentDirectory())
            
        #initializes the gameData
        #self.mainWindow.hideWindow()
        
        
    ##########################
    ####Button Callbacks######
    ##########################

    def openPDF(self, filename):
        if sys.platform == 'linux2':
            subprocess.call(["xdg-open", "Graphics01.pdf"])
        else:
            os.startfile(filename)
            
            
    def openWebPage(self, filename):
        os.startfile(filename)
                                        
            
    def launchActivity(self,buttonName=None):
        #set current directoy back to the location of PortalWindow.py so all the
        #stuff that has to be loaded below can be found
        #System.IO.Directory.SetCurrentDirectory(os.path.dirname(os.path.realpath(__file__)))
        
        if buttonName=="Scribbler Adventure":
            #inits ScribblerAdventure data
            if "ScribblerAdventure" not in self.gameData:
                self.gameData["ScribblerAdventure"]={}
                self.gameData["ScribblerAdventure"]["highestStage"]=0
                self.gameData["ScribblerAdventure"]["highestLevel"]=0        
                #mostly holds login/clicks for stages
                self.gameData["ScribblerAdventure"]["stageData"]={}
                #holds clicks for levels as well as game info like [score,wins,bestTime,loses,restarts]
                self.gameData["ScribblerAdventure"]["levelData"]={}            
            
            self.recordClicks(self.gameData["ScribblerAdventure"])                    
            self._openWindow("ScribblerAdventureWindow")    
##             position=self.mainWindow.GetPosition()
##             self.mainWindow.hideWindow()            
##             launcher=__import__("ScribblerAdventureWindow", globals(), locals(), ['*'])
##             self.mainWindow.child=launcher.start(self.gameData,self)
##             self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)
        elif buttonName=="Graphics":            
            #inits Graphics data            
            if "Graphics" not in self.gameData:
                self.gameData["Graphics"]={}
            self.recordClicks(self.gameData["Graphics"])
            self._openWindow("graphicsWindow")
##             position=self.mainWindow.GetPosition()
##             self.mainWindow.hideWindow()            
##             launcher=__import__("graphicsWindow", globals(), locals(), ['*'])
##             self.mainWindow.child=launcher.start(self.gameData,self)
##             self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)        
            #self.openPDF(HOME_DIR+"/Graphics/Graphics01.pdf")
        elif buttonName=="Platformer":        
            #inits Platformer data
            if "Platformer" not in self.gameData:
                self.gameData["Platformer"]={}
            self.recordClicks(self.gameData["Platformer"])     
            self._openWindow("Platformer.platformerWindow")                
##             self.openPDF(HOME_DIR+"/Platformer/Platformer_level_1.pdf")                    
        
        elif buttonName=="Choose Your Own Adventure":            
            #inits CYOA data
            if "CYOA" not in self.gameData:
                self.gameData["CYOA"]={}
            self.recordClicks(self.gameData["CYOA"])
            self.openPDF(HOME_DIR+"/ChooseYourOwnAdventure/ChooseYourOwnAdventure.pdf")
            
            #copy CYOA directory to user's workspace
            if self.workspaceLoc!=None and os.path.exists(self.workspaceLoc):
                if not os.path.exists(self.workspaceLoc+"/ChooseYourOwnAdventure/"):
                    shutil.copytree(os.path.dirname(os.path.realpath(__file__))+"/ChooseYourOwnAdventure/",self.workspaceLoc+"/ChooseYourOwnAdventure/")
                    #only open up myGame.py the first time. subsequent times the user may have changed the filename
                
                #This might be worriesome if the user changes the name of the file they are using
                if os.path.exists(self.workspaceLoc+"/ChooseYourOwnAdventure/myGame.py"):
                    calico.Open(self.workspaceLoc+"/ChooseYourOwnAdventure/myGame.py")
            
                System.IO.Directory.SetCurrentDirectory(self.workspaceLoc+"/ChooseYourOwnAdventure/")
                
        elif buttonName=="Hackable Demos":            
            #inits Graphics data            
            if "HackableDemos" not in self.gameData:
                self.gameData["HackableDemos"]={}
            self.recordClicks(self.gameData["HackableDemos"])
            self._openWindow("hackableDemosWindow")
##             position=self.mainWindow.GetPosition()
##             self.mainWindow.hideWindow()            
##             launcher=__import__("hackableDemosWindow", globals(), locals(), ['*'])
##             self.mainWindow.child=launcher.start(self.gameData,self)
##             self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)        
      
        
        elif buttonName=="Diversions":            
            #inits Graphics data            
            if "Diversions" not in self.gameData:
                self.gameData["Diversions"]={}
            self.recordClicks(self.gameData["Diversions"])
            
            self._openWindow("diversionsWindow")
##             position=self.mainWindow.GetPosition()
##             self.mainWindow.hideWindow()            
##             launcher=__import__("diversionsWindow", globals(), locals(), ['*'])
##             self.mainWindow.child=launcher.start(self.gameData,self)
##             self.mainWindow.child.mainWindow.Move(position[0],position[1]+21)        
        
        
        elif buttonName=="Connect Scribbler Robot":        
            #inits documentation use data
            if "ConnectScribDoc" not in self.gameData:
                self.gameData["ConnectScribDoc"]={}
            self.recordClicks(self.gameData["ConnectScribDoc"])                
            self.openPDF("http://cobi.cs.uwyo.edu/lrc/index.php/Connect_to_a_Scribbler_robot")
        elif buttonName=="LRC Wiki":        
            #inits wiki use data
            if "wikiDoc" not in self.gameData:
                self.gameData["wikiDoc"]={}
            self.recordClicks(self.gameData["wikiDoc"])
            self.openPDF("http://www.evolvingai.org/lrc/index.php/Main_Page")
            
    def _openWindow(self, windowName):
        position=self.mainWindow.GetPosition()
        self.mainWindow.hideWindow()            
        launcher=__import__(windowName, globals(), locals(), ['*'])
        self.mainWindow.child=launcher.start(self.gameData,self)
        self.mainWindow.child.mainWindow.Move(position[0],position[1]+21) 
            
    ###########################################
    ####Checking save paths and directories####
    ###########################################
    
    #Checks to see if the dirName exists simpliy return True
    #If dirName doesn't exists than if the MAKE_DIRECTORY flag is true make the directory and return False
    #If the MAKE_DIRECTORY flag is false then don't make the directory and return False
    def checkDirectory(self, dirName,makeDir=False):
        if os.path.exists(dirName):    
            return True
        else:
            print("No directory found for ",dirName)
            if makeDir:
                print("Making directory.")            
                os.makedirs(dirName)
                return True
            else:
                print("Stopping.")                            
                return False                
    
    #Figuring out save paths and directories is always the most fun... RV
    def setPaths(self):
        #self.clubPath - Root directory for the Apps and Students folders
        #    -self.appsPath
        #    -self.studentsPath 
        #self.dataPath - Points a folder called LRCData. Root directory for the LoginRecord and GameData folders. 
        #    -self.loginRecPath - Point to LoginRecord folder
        #    -self.gameDataPath - Point to GameData folder
        if not ROBOTICS_ROOM:
            self.checkDirectory(HOME_DIR+"DropLocal/",makeDir=True)
            
            #creates the two main root directories
            self.clubPath=HOME_DIR+"DropLocal/ClubFiles"
            self.checkDirectory(self.clubPath,makeDir=True)

            self.dataPath=HOME_DIR+"DropLocal/LRCData"
            self.checkDirectory(self.dataPath,makeDir=True)

        else:
			#assumes the club and data paths are created beforehand
			self.clubPath=CLUB_LOC
			self.dataPath=DATA_LOC

			if not os.path.exists(self.clubPath):
				self.clubPath=None
			if not os.path.exists(self.dataPath):
				self.dataPath=None

		#makes sure the subdirectories exist				
        if self.dataPath != None:

            self.loginRecPath=self.dataPath+"/LoginRecord/"
            self.gameDataPath=self.dataPath+"/GameData/"

            self.checkDirectory(self.loginRecPath,makeDir=True)
            self.checkDirectory(self.gameDataPath,makeDir=True)

        if self.clubPath != None:
            self.appsPath=self.clubPath+"/Apps/"
            self.studentsPath=self.clubPath+"/Students/"

            self.checkDirectory(self.appsPath,makeDir=True)
            self.checkDirectory(self.studentsPath,makeDir=True)
                    
    ########################################
    ####Login and User Data Funcitons#######
    ########################################
   
    #TODO JH: May be used to backpropgate this information at some poin in the future
    def update(self):
        pass
##         self.scribAdBut.metaText.text = str(0) + "/" + str(len(ScribblerAdventure.stages))
    
    def updateLoginRec(self,username):
        self.config.read(self.loginRecLoc)
        
        if self.config.has_option(username,"login"):
            loginRecord=self.config.get(username,"login")
            #used mainly in recordClicks
            if DEBUG_LOGINDATES:
                loginRecord+=":"+debugDate
            else:
                loginRecord+=":"+str(datetime.date.today())
            self.config.set(username,"login",loginRecord)
        else:
            print("Error. Login file does not have a sect for login "+username+".Adding one.")
            self.config.set(username,"login",str(datetime.date.today()))
            print("Login file: "+loginFile)
    def createLoginRec(self,username,credentials):
        genderBDay=ask(["Gender","Birthday (month/day/year)"],"You do not have an account. Please provide the following information to create one.")    

        self.config.add_section(username)
        for key,value in credentials.items():
            self.config.set(username,key,value)
        for key,value in genderBDay.items():
            self.config.set(username,key,value)
        #add today's login
        self.config.set(username,"login",str(datetime.date.today()))


    def prompt(self,message=None,credentials={"foo":"bar"}):
        #try:
        self.clearConfig()    
        credentials=ask(["First Name","Last Name"],"Welcome to the Laramie Robotics Club user portal. Please enter your First and Last name to start.")
        username=self.formatUsername(credentials)        

        #LRC Data
        if self.dataPath != None:
            #login record
            self.loginRecLoc=self.loginRecPath+username+".txt"
            if not os.path.exists(self.loginRecLoc):
                self.createLoginRec(username,credentials)
            else:
                self.updateLoginRec(username)
            tempFile=open(self.loginRecLoc,"w")
            self.config.write(tempFile)
            tempFile.close()
            self.clearConfig()

            #checks for game data		
            self.gameDataLoc=self.gameDataPath+username+".p"
            if not os.path.exists(self.gameDataLoc): 
                self.createGameData()
        else:
			self.loginRecLoc=None
			self.gameDataLoc=None

		#workspace
        if self.clubPath != None:
            self.workspaceLoc=self.studentsPath+username
            if not os.path.exists(self.workspaceLoc):
                os.makedirs(self.workspaceLoc)
        else:
            self.workspaceLoc=None
        return True
##         except:
##             #if something about the login fails then return False and handle it correctly
##             print("Error with login.")
##             self.loginRecLoc=None
##             self.gameDataLoc=None
##             self.workspaceLoc=None
##             return False
    

    def formatUsername(self,credentials):
        username=credentials["First Name"]+credentials["Last Name"]
        username=username.replace(" ","") #removes whitespace        
        username=username.lower() #lowercase all letters to make case insensitive        
        return username


    		

    def createGameData(self):
        #creates a new gameData object which is just a simple dictionary
		if self.gameDataLoc!=None:
			data=open(self.gameDataLoc,"wb")        
			pickle.dump({}, data)
			data.close()     
    
    
    def loadGameData(self):
        if self.gameDataLoc!=None:
            return pickle.load(open(self.gameDataLoc, "rb" ) )
    
    def saveGameData(self):
        if self.gameDataLoc!=None:
            temp=open(self.gameDataLoc,"wb")
            pickle.dump(self.gameData, temp)
            temp.close()
            sleep(0.1) #give save data some time before doing anything else               
 
    def recordClicks(self, dataLoc):
        if "logins" not in dataLoc:
                dataLoc["logins"]=[]                
        
        if DEBUG_LOGINDATES:
            dataLoc["logins"].append(debugDate)         
        else:            
            dataLoc["logins"].append(str(datetime.date.today())) 
        self.saveGameData()               

    
    ##########################
    #Callback/Event functions#
    ##########################
    def mouseDown(self,obj, event):
        #pass
        print(event.x,event.y)
    
    def keyPressed(self,obj,event):
        if str(event.key)=="q" or str(event.key)=="Q":
            self.quitGame()
    
    
    ####################        
    #General Utilities#
    ###################    
    def _hideWindow(self):
        self.mainWindow.Visible=False
    def _showWindow(self):
        self.update()
        self.mainWindow.Visible=True 
    def quitGame(self):
        self.win.Destroy()
    def clearConfig(self):
        toRemove=[]
        for s in self.config.sections():
            toRemove.append(s)
        for r in toRemove:
            self.config.remove_section(r)
    ##################
    #Window Functions#
    ##################
    
global portal
portal=None
if __name__ == "__main__":
    portal=Portal()
