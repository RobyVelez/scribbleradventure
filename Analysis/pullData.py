import shutil, errno,distutils,time
import os

if os.name=="nt":
    DROPBOX_PATH="C:\\Users\\lrcguest01\\Dropbox\\"
else:
    DROPBOX_PATH="/Users/robyvelez/Dropbox"


def copyAnything(src, dst):
    try:
        #distutils.dir_util.copy_tree(src,dst)
		shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise


def main():
	if not os.path.exists("Archive"):
		os.makedirs("Archive")

	if os.path.exists("UserList"):
		if not os.path.exists("Archive/"+"UserList_"+time.strftime("%m_%d_%Y")):
			os.rename("UserList","Archive/"+"UserList_"+time.strftime("%m_%d_%Y"))
			copyAnything(DROPBOX_PATH+"/UserList","UserList")
	else:
		copyAnything(DROPBOX_PATH+"/Students","Students")
	if os.path.exists("Students"):
		if not os.path.exists("Archive/"+"Students_"+time.strftime("%m_%d_%Y")):
			os.rename("Students","Archive/"+"Students_"+time.strftime("%m_%d_%Y"))
			copyAnything(DROPBOX_PATH+"/Students","Students")
	else:
		copyAnything(DROPBOX_PATH+"/Students","Students")

	

if __name__ == "__main__":
    main()
