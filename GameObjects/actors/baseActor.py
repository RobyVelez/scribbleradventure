import sys
import os
import warnings
import types
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *

class BaseActor(object):
    """
    The base class for Actors.
    
    Actors are objects that: can be used, can be interacted with, can be talked to,
    or can be picked up by the user. The BaseActor does not have any shape and, as a result,
    it can not be drawn into the world. The BaseActor is intended to serve as a parent
    class to children that can be drawn to the world. See ShapeActor as an example.
    
    Note: if your object can not be used, can not be interacted with, can not be talked to,
    and can not be picked up by the player, there is no reason to extend BaseActor.
    
    When extending this object, the following functions may be interesting to overwrite:
     * onUse: Called when this object is used. Overwritting 'onUse' sets 'canUse' to True.
     * onInteract: Called when this object is interacted with. Overwriting 'onInteract' sets
       'canInteract' to True.
     * onPickup: Called when this object is picked up. Overwriting 'onPickup' does NOT set
       'canPickup' to True, see 'pickup'.
    """

    def __init__(self, interact=None, use=None, pickup=False, returned=None, onPickup=None, description=None, tag = "actor"):
        """
        Constructs a BaseActor.
        
        Args:
            interact: Function of the form func(self, items), called when
                this object is interacted with.
            use: Function of the form func(self) called when this object
                is used.
            pickup: Anything, generally will be the object that is returned to the 
                player upon pickup. If set to True, will return the Actor object
                itself instead. If set to True, and a value for returned is povided,
                will return whatever value returned was set to.
                (deprecated) If pickup is the string "self", a reference to
                this instance will be returned instead, just as if pickup was set
                to True.
            returned: Anything, will be the object that is returned to the player
                if pickup is set to True.
            onPickup: Function of the form func(self) called when this object
                is picked up by the player. Note: passing a value for onPickup
                is NOT enough to enable the player to pickup this object.
            description: A description of this object, used when this object is printed.
                If left blank, the class and memory address of this object is printed.
            tag: The tag of the actor, usually a string helpful for identifying what 
                kind of actor you are dealing with.
        """
        #: Indicates whether this actor can be used
        #: Can be set to False anywhere to limit use.
        self.canUse = None

        #: Boolean indicating whether this object can be picked up by the player.
        #: Can be set to false at any time to prevent the player from picking it up.        
        self.canPickup = None

        #: Anything, will be the object that is returned to the player upon pickup.
        self.returned = None

        #: Boolean indicating whether this object can be interacted with.
        #: Can be set to false at any time to prevent the player from interacting with it.
        self.canInteract = False

        #: A dictionary to which you can add arbitrary data if you want to.
        self.userData = {}

        #: A tag, generally a string, usefull for classifying this actor as something.
        self.tag = tag

        #: The description of the actor, printed when the actor is printed.
        self.description

        #: An optional portrait of the actor used in conversations.
        self.portrait = None
        
        self.canUse = (use is not None) or self.isOverwritten("onUse")
        self.bind("onUse", use)
        
        #Pickup attributes
        if isinstance(pickup, bool):
            self.canPickup = pickup
        else:
            self.canPickup = pickup is not None
        
        if returned is None:
            if isinstance(pickup, bool):
                if pickup:
                    self.returned = self
                else:
                    self.returned = None
            elif pickup == "self":
                self.returned = self
            else:
                self.returned = pickup
        else:
            self.returned =  returned
        self.bind("onPickup", onPickup)
        
        #Interact attributes
        self.canInteract = (interact is not None) or self.isOverwritten("onInteract")
        self.bind("onInteract", interact)
        
        #Description
        self.description = description
        if self.description is None: 
            self.description = '<%s.%s object at %s>' % (self.__class__.__module__, self.__class__.__name__, hex(id(self)))
  
        

    def onUse(self, items=[]):
        """
        Called when this object is used. To be overwritten.
        """
        pass
        

    def onInteract(self, items):
        """
        Called when this object is interacted with. To be overwritten.
        
        Args:
            items: A list of things passed by the user, where the things
                can be anything.
        """
        pass
        

    def onPickup(self):
        """
        Called when this object is picked up. To be overwritten.
        """
        pass
 
        
    def draw(self, levelWorld):
        """
        Adds this object to the level world.
        
        This function should be overwritten, such that it adds all shapes associated
        with this object to the window of the level world (levelWorld.win.draw(shape)).
        In addition, this function should also register any callback functions, the
        common ones being levelWindow.interactCollide and levelWindow.pickupCollide.
        Lastly, this function should set any other attributes that require the bodies
        of the shapes to be initialized.
        
        Important: first draw all relevant shapes to the window before settings body
        properties, object bodies do not exist before they have been added to the 
        simulator.
        
        Args:
            levelWorld: an instance of LevelWorld.
        """
        pass
        

    def isOverwritten(self, function):
        """
        Returns whether the indicated function is overwritten.
        
        Args:
            function: A string indicating the function to be queried.
            
        Returns:
            True if the function is overwritten, False otherwise.
        """
        return BaseActor.__dict__[function] != getattr(self, function).im_func


    def installCollision(self, collision, shape):
        """
        Adds a collision event to the shape, and sets its userData to this instance.
        
        Does not complain when either the collision function or the shape is None,
        but it does usher a warning when the shape exists, but does not have a body.
        
        Args:
            collision: A function of the form: func(myfixture, otherfixture, contact).
            shape: An instance of a Calico Shape object.
        """
        if not collision: return
        if not shape: return
        if shape.body:
            shape.body.OnCollision += collision
            shape.body.UserData = self
        else:
            message = "Collision shape does not have a body: collision could not be installed.\n"
            message += " - Collision shape: " + str(shape)
            message += " - Tag: " + str(shape.tag)
            message += " - Collision event: " + str(collision)
            warnings.warn(message)


    def installSeparation(self, separation, shape):
        """
        Adds a separation event to the shape, and sets its userData to this instance.
        
        Does not complain when either the separation function or the shape is None,
        but it does usher a warning when the shape exists, but does not have a body.
        
        Args:
            separation: A function of the form: func(myfixture, otherfixture).
            shape: An instance of a Calico Shape object.
        """
        if not separation: return
        if not shape: return
        if shape.body:
            shape.body.OnSeparation += separation
            shape.body.UserData = self
        else:
            message = "Collision shape does not have a body: separation could not be installed.\n"
            message += " - Collision shape: " + str(shape)
            message += " - Tag: " + str(shape.tag)
            message += " - Separation event: " + str(separation)
            warnings.warn(message)


    def bind(self, name, function):
        """
        Binds a function to this instance.
        
        Usefull adding functions to an instance of a class after it was created.
        
        Example:
        ::
        
            def func(self):
                print(self)
                
            instance = LRC_Object()
            instance.bind("printMe", func)
            instance.printMe()
            
        Args:
            name: A string containing the name this function will bind to.
            function: A function of the form: func(self, ...)
        """
        if function: setattr(self, name, types.MethodType(function, self))

        
    def __str__(self):
        """
        Returns the string representation of this object.
        """
        return str(self.description)

        
    def __repr__(self):
        """
        Returns the raw string representation of this object.
        """
        return "%r" % self.description
        
