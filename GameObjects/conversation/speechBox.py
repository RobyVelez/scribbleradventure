from Graphics import *
from fancyTextFunctions import *
from utilityFunctions import *
#from actors import *

class SpeechBox(object):
    """
    The speech box shown to the player that displays the text of a conversation.
    """
    def __init__(self, levelWindow, x=50, y=50, w=600, h=100, text=""):
        """
        Constructs a SpeechBox object.

        Args:
            levelWindow: The LevelWindow associated with this SpeechBox.
            x: The x coordinate of the SpeechBox.
            y: The y coordinate of the SpeechBox.
            w: The width of the SpeechBox.
            x: The height of the SpeechBox.
            text: The text to be shown in the SpeechBox.
        """
        self.levelWindow = levelWindow
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.left_border = -(w/2)
        self.frame = Frame(0, 0)
        self.text = makeList(text)
        #self.box = None
        self.box = Rectangle((self.x, self.y), (self.x+self.w, self.y+self.h))
        self.box.fill = Color("white")
        self.box.border = 1
        self.box.outline = Color("black")
        self.frame.visible = False
        self.portrait = None
        self.portraitSize = 100
        self.portraitOffset = 50
        self.textOffset = 10
        self.nextButtonWidth=70
        self.nextButtonHeigth=30
        self.nextButtonOffsetY=6
        self.nextButtonOffsetX=-3
        self.currentButton = None
        self.box.draw(self.frame)
        self.buttons = []
        
        
    #Not thread safe, but pretending to be anyway.
    #def nextCount(self):
    #    self.count += 1
    #    return self.count
        
        
    #def setText(self, text=""):
    #    self.text = makeList(text)
    #    #writeFancyText(self.box, [text], x_pos_start=left_border+100, wrap=w-110, min_hspace=6)
    
    
    #def setPortrait(self, portrait=None):
    #    self.portrait = portrait
       
    def buildAndShow(self, hasNext=False):
        """
        Actually builds and shows the SpeechBox.

        Args:
            hasNext: If True, the SpeechBox will have a 'more dialog is
                available' marker.
        """
        self.build(hasNext)
        self.show()
                

    def build(self, hasNext=False):
        """
        Builds the SpeechBox.

        Args:
            hasNext: If True, the SpeechBox will have a 'more dialog is
                available' marker.
        """       
        #if self.box:
        #    self.box.undraw()        
        #self.box = Rectangle((self.x, self.y), (self.x+self.w, self.y+self.h), color=Color("white"))
        self.buttons = []
        toUndraw = []
        for s in self.box.shapes:
            toUndraw.append(s)
        for s in toUndraw:
            s.undraw()
            
        self.box.set_points(Point(self.x, self.y), Point(self.x+self.w, self.y), Point(self.x+self.w, self.y+self.h), Point(self.x, self.y+self.h))
        if self.portrait:
            w = self.portrait.width*self.portrait.scaleFactor
            h = self.portrait.height*self.portrait.scaleFactor
            if w > self.portraitSize or h > self.portraitSize:
                self.portrait.scaleTo(float(self.portraitSize)/float(max(self.portrait.width, self.portrait.height)))
                #print(float(100)/float(max(self.portrait.width, self.portrait.height)))
                #self.portrait.scaleTo(0.2)
            self.portraitFrame = Frame(self.left_border + self.portraitOffset,0)
            self.portrait.draw(self.portraitFrame)
            self.portraitFrame.draw(self.box)
            #self.portrait.moveTo(self.left_border+50,0)
        x_pos = self.left_border + self.portraitSize
        x_wrap = self.w - self.portraitSize - self.textOffset
        writeText(self.box, [Wrap(x_wrap, True)] + self.text, x_pos_start=x_pos)
        self.connectButtons(self.text)
        x_but_pos = self.w - self.nextButtonWidth +  self.nextButtonOffsetX
        y_but_pos = self.h - self.nextButtonHeigth + self.nextButtonOffsetY
        if hasNext:
            #print("Show NEXT!")
            self.currentButton = createNextButton(self.box, x_but_pos, y_but_pos, self.nextButtonWidth, self.nextButtonHeigth, "talk()", "next", Color(255, 235, 100), fS=16, shape="triangle")
        else:
            self.currentButton = createNextButton(self.box, x_but_pos, y_but_pos, self.nextButtonWidth, self.nextButtonHeigth, "done", "done", Color(80, 166, 65), fS=16, shape="rectangle")
        #self.box.draw(self.frame)
    
    
    def connectButtons(self, obj):
        """
        Connects buttons within the SpeechBox.

        Args:
            obj: The button to connect.
        """
        if isinstance(obj, LRCButton): self.buttons.append(obj)
        if hasattr(obj, '__iter__'):
            for newObj in obj:
                self.connectButtons(newObj)
        
    def draw(self, levelWindow):
        """
        Adds this object to the level.
        
        Important: requires a LevelWindow object, not a Calico Window object.
        
        Args:
            levelWindow: A LevelWindow object.
        """
        self.frame.draw(levelWindow.uiFrame)
        self.box.window = self.frame.window
        self.box.connect("click", self.onClick)


    def show(self):
        """
        Makes the SpeechBox visible.
        """
        self.frame.visible = True

        
    def hide(self):
        """
        Makes the SpeechBox invisible.
        """
        self.frame.visible = False
        
    def visible(self):
        """
        Indicates whether the SpeechBox is currently visible.

        Return:
            True if the SpeechBox is visible, False otherwise.
        """
        return self.frame.visible

    def onClick(self,o,e):
        """
        Callback function for if someone clicks on the SpeechBox.

        Args:
            o: Reference to the current window.
            e: Reference to the click event.
        """
        for shape in self.buttons:
            if shape.hit(e.x,e.y):
                if shape.action is not None: shape.action()
                #print("You have clicked on button:", shape.button)
##         if self.currentButton and self.frame.visible:
##             if self.currentButton.hit(e.x,e.y):
##                 if self.currentButton.tag=="next":
##                     pass
##                 elif self.currentButton.tag=="done":
##                     self.hide()

    #def undraw(self):
    #    self.hide()
        
         
    #def softHide(self, count):
    #    if count == self.count:
    #        self.hide()
        

    #def silence(self):
    #    """
    #    Synonym for hide.
    #    """
    #    self.hide()
