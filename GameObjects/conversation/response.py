import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
from utilityFunctions import *

class Response(object):
    """
    Response holds a single response for a :class:`.Conversation`.
    
    Responses can also include an action, which should be something callable.
    """
    
    def __init__(self, text, action=None, portrait=None, sticky=False):
        """
        Constructs a response.
        
        Args:
            text: String indicating the response.
            action: A function that will be executed with this response.
            portrait: The portrait to be shown with this response.
            sticky: If True, this response will not be removed from the response
                list, meaning it will be repeated over and over untill some
                other event removes it.
        """
        self.text = text
        self.action = action
        self.returned = self.text
        self.portrait = portrait
        self.sticky = sticky
        self.executed = False
        
    def __str__(self):
        """
        Converts this object to its string representation.
        
        Returns:
            A string containing the text associated with this response.
        """
        return str(self.text)
        
    def __repr__(self):
        """
        The representation of this object, always printed as the empty string.
        
        Note: in general this is exactly *NOT* how __repr__ should be used,
        __repr__ is intended to be an unambigious representation.
        """
        #return '\"' + str(self.text[0:10]) + '\"'
        return ""
        
        
    def launchActions(self):
        """
        Launches all actions associated with this response.
        """
        if self.executed: return
        self.executed = True
        if isinstance(self.action, list):
            for act in self.action:
                act()
        elif self.action: self.action()
