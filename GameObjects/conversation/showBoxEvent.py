import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../CommonFunctions")
from setCalico import *
from utilityFunctions import *
from response import Response

class ShowBoxEvent(object):
    """
    Event to be scheduled to the levelThread which will show the speech box.

    This event exists because showing new lines in a conversation requires 
    drawing new objects to the Calico window. We can only draw objects safely if
    if we synchronize the drawing with main simulator thread.
    """
    def __init__(self, levelWindow, y=50, text="", portrait=None):
        """
        Creates a new ShowBoxEvent. 

        Args:
            levelWindow: The level window associated with the event.
            y: The horizontal position of the SpeechBox. Especially usefull if
                the box would otherwise obscure an important part of the screen.
            text: The text to be shown in the SpeechBox.
            portrait: The portrait to be shown with the SpeechBox.
        """
        self.levelWindow = levelWindow
        self.y = y
        self.text = text
        self.portrait = portrait
        self.hasNext = False


    def __call__(self):
        """
        Actually builds and shows the SpeechBox.

        Return:
            Always returns False, so it will never be excecuted more than once
            by the levelThread.
        """
        self.levelWindow.speechBox.y = self.y
        self.levelWindow.speechBox.text = makeList(self.text)
        self.levelWindow.speechBox.portrait = self.portrait
        self.levelWindow.speechBox.buildAndShow(self.hasNext)
        return False
