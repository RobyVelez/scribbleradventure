from time import time

class RepeatedActionEvent:
    """
    Event for executing an action repeatedly for a number of times.
    """
    def __init__(self, interval, numActions, action,fireOnStart=False):
        """
        Constructs a RepeatedActionEvent object.

        Args:
            interval: The number of simulator ticks between triggers.
            numActions: The total number of time the action will trigger.
            action: The function to call when the action is triggered.
            fireOnStart: If True, the action will trigger as soon as this event
                is scheduled. Otherwise, the action won't trigger until the
                first interval has passed.
        """
        self.interval=interval
        self.numActions = numActions
        self.action = action
        self.fireOnStart=fireOnStart #whether an action is run on start
        
        
        self.started=False
        self.startTime=None
        
        self.eventSchedule=[]
        for i in range(numActions):
            self.eventSchedule.append((i+1)*interval)
            
            
    def __call__(self):
        """
        Trigger the action if all conditions are met.

        Return: False if all actions have been triggered, True otherwise.
        """
        if not self.started:
            self.started=True
            self.startTime=time()
            if self.fireOnStart:
                self.action()
                    
        toRemove=None        
        elapse=time()-self.startTime
        
        for i in range(len(self.eventSchedule)):
            if elapse>self.eventSchedule[i]:
                self.action()
                toRemove=i
                break
        if toRemove != None:
            del self.eventSchedule[toRemove]
                                
        if len(self.eventSchedule)==0:
            return False
        else:
            return True
