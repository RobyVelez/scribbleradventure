class StaticEvent:
    """
    Event that will be executed unless explicitly removed. 

    Can be added to the levelThread without an associated action, and it will
    passively wait untill a new action is assigned to it. Always assign a new
    action to the newAction attribute, rather than the action attribute, to 
    prevent odd race conditions.
    """
    def __init__(self, action=None, remove=False):
        """
        Constructs a StaticEvent object.

        Args:
            action: The function to call at every time step.
            remove: If set to True, this event will be deleted next time it is
                called.
        """
        #: Set to True to have this event removed the next time it is executed.
        self.remove = remove
        #: The current action is only executed if active is set to True.
        #: Assigning a newAction automatically sets active to True.
        self.active = bool(action)
        #: The current action that will be executed if this event is active.
        #: Do not change this attribute after the event has been scheduled, but
        #: use the newAction attribute instead.
        self.action = action
        #: A new action that will replace the current action at the next time
        #: step.
        self.newAction = None


    def __call__(self):
        """
        Executed the current action if active.

        Returns: False if the event is to be removed, True otherwise.
        """
        if self.newAction:
            self.action = self.newAction
            self.newAction = None
            self.active = True
        if self.active:
            self.active = self.action()
        if self.remove:
            return False
        return True
        
        
