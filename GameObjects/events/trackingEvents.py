from Myro import *

class TrackRobotEvent(object):
    """
    Event for having the worldFrame follow the robot as it moves.
    """
    def __init__(self, levelWindow):
        """
        Constructs a TrackRobotEvent object.

        Args:
            levelWindow: The :class:`.LevelWindow` for the current level. 
        """
        self.levelWindow = levelWindow
        self.anchorX = 350
        self.anchorY = 350
        self.minX = 0
        self.maxX = 0
        self.minY = 0
        self.maxY = 0

    def __call__(self):
        """
        Keeps the robot within a certain margin of the center of the frame.
        
        Return: Always returns True.
        """
        r = getRobot()
        if r:
            x = max(min(self.anchorX - r.frame.x, self.maxX), self.minX)
            y = max(min(self.anchorY - r.frame.y, self.maxY), self.minY)
            self.levelWindow.worldFrame.moveTo(x, y)
        return True
        
       
class TrackResizeEvent(object):
    """
    Event for responding to window resizes (deprecated).

    Should not be used, use the on resize triggers of Calico instead.
    """
    def __init__(self, levelWindow):
        """
        Constructs a TrackResizeEvent object.

        Args:
            levelWindow: The :class:`.LevelWindow` for the current level. 
        """
        self.levelWindow = levelWindow
        self.prevDx=0
        self.prevDy=0


    def __call__(self):
        """
        Keeps the masterFrame centerd when the window is resized.
        
        Return: Always returns True.
        """
        iw = self.levelWindow.viewW# + self.levelWindow.panel3W
        ih = self.levelWindow.viewH# + self.levelWindow.panel2H
        w = self.levelWindow.sim.window.width
        h = self.levelWindow.sim.window.height
        dx = 0
        dy = 0
        if w > iw:
            dw = w - iw
            dx = dw/2
        if h > ih:
            dh = h - ih
            dy = dh/2
        if self.prevDx != dx or self.prevDy != dy:
            self.levelWindow.masterFrame.moveTo(dx, dy)
            self.prevDx = dx
            self.prevDy = dy
        return True
