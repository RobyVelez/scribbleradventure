class EventQueue:
    """
    A queue of events that should be executed one after the other.
    """
    def __init__(self):
        """
        Create an EventQueue object.
        """
        self.events = []
        self.sticky = False


    def add(self, event):
        """
        Add a new event to the queue.

        Args:
            event: The event to add to the queue.
        """
        self.events.append(event)


    def __call__(self):
        """
        Executes the next event.

        Returns: True if there events remaining, False otherwise.
        """
        if len(self.events) > 0:
            if not self.events[0]():
                del self.events[0]
            return True
        elif self.sticky:
            return True
        else:
            return False
