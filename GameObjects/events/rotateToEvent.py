class RotateToEvent(object):
    """
    Event that rotates a :class:`.Shape` or :class:`.SpriteActor` to a certain 
    angle.
    """
    def __init__(self, obj, angle, speed=1, nextAction=None):
        """
        Constructs a RotateToEvent object.

        Args:
            obj: The object to be rotated.
            angle: The angle to which to rotate the object.
            speed: The speed with which to rotate the object.
            nextAction: A function called when the rotation is complete.
        """
        self.obj = obj
        self.angle = angle
        self.speed = speed
        self.nextAction = nextAction
        self.hasRotation = False
        if hasattr(self.obj, "rotation"):
            self.hasRotation = True
        #if hasattr(self.obj, "getRotation"):
        #    self.hasGetRotation = True
        
    def __call__(self):
        """
        Performs one iteration of the rotation.

        Return: False if the rotation is completed, True otherwise.
        """
        if self.hasRotation:
            rotation = self.obj.rotation
        else:
            rotation = self.obj.getRotation()
        #print("Current rotation:", rotation)
        #print("Target rotation:", self.angle)
        if rotation < self.angle:
            rotation += self.speed
            if rotation > self.angle:
                self.obj.rotate(self.angle - (rotation - self.speed))
                if self.nextAction: self.nextAction()
                return False
            self.obj.rotate(self.speed)
        elif rotation > self.angle:
            rotation -= self.speed
            if rotation < self.angle:
                #-12 - (-20 + 10) = -2
                self.obj.rotate(self.angle - (rotation + self.speed))
                if self.nextAction: self.nextAction()
                return False
            self.obj.rotate(-self.speed)
        else:
            return False
        return True
