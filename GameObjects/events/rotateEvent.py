from rotateToEvent import RotateToEvent

class RotateEvent(RotateToEvent):
    """
    Event that rotates a :class:`.Shape` or :class:`.SpriteActor` by a certain 
    angle.
    """
    def __init__(self, obj, angle, speed=1, nextAction=None):
        """
        Constructs a RotateEvent object.

        Args:
            obj: The object to be rotated.
            angle: The angle by which to rotate the object.
            speed: The speed with which to rotate the object.
            nextAction: A function called when the rotation is complete.
        """
        if hasattr(obj, "rotation"):
            targetAngle = obj.rotation + angle
        else:
            targetAngle = obj.getRotation() + angle
        super(RotateEvent, self).__init__(obj, targetAngle, speed, nextAction)
