class TimedEvent:
    """
    Event that will trigger after a number of simulator ticks has passed.
    """
    def __init__(self, ticks, action):
        """
        Constructs a TimedEvent object.

        Args:
            ticks: The number of ticks untill this event triggers.
            action: The function that will be called when this event triggers.
        """
        self.ticks = ticks
        self.action = action
        
    def __call__(self):
        """
        Executed this event when all conditions are met.

        Return: False if the action triggered, True otherwise.
        """
        self.ticks -= 1
        if self.ticks < 1:
            self.action()
            return False
        else:
            return True
        
        
