from utilityFunctions import clipInformed

class MoveToEvent(object):
    """
    MoveTo event for a :class:`.Shape` or :class:`.SpriteActor`.
    """
    def __init__(self, obj, x, y, speed=1, nextAction=None):
        """
        Constructs a MoveEvent.

        Args:
            ojb: The object to move.
            x: The horizontal coordinate of the target position.
            y: The vertical coordinate of the target position.
            speed: The speed with which to move.
            nextAction: A function called after the move is completed.
        """
        self.obj = obj
        self.x = x
        self.y = y
        self.speed = speed
        self.nextAction = nextAction
        self.pause = False
        self.remove = False
        
    def __call__(self):
        """
        Excecutes one iteration of the move action.

        Return: False if the move is completed, and True otherwise.
        """
        #print("Move:", self.obj.x, "to", self.x, "and", self.obj.y, "to", self.y)
        #print(abs(self.obj.x - self.x), "<=", self.dx)
        #print(abs(self.obj.y - self.y), "<=", self.dy)
        if self.pause: return True
        if self.remove: return False
        self.dx, xClipped = clipInformed(self.x - self.obj.getX(), -self.speed, self.speed)
        self.dy, yClipped = clipInformed(self.y - self.obj.getY(), -self.speed, self.speed)
##         self.steps = max(abs(dx), abs(dy))/self.speed 
##         self.dx = dx/self.steps
##         self.dy = dy/self.steps
        if (not xClipped) and (not yClipped):
            self.obj.moveTo(self.x, self.y)
            if self.nextAction: self.nextAction()
            return False
        else:
            self.obj.move(self.dx, self.dy)
            return True
