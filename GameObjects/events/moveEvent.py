from moveToEvent import MoveToEvent

class MoveEvent(MoveToEvent):
    """
    Move event for a :class:`.Shape` or :class:`.SpriteActor`.
    """
    def __init__(self, obj, dx, dy, speed=1, nextAction=None):
        """
        Constructs a MoveEvent.

        Args:
            ojb: The object to move.
            dx: The distance to move in the horizontal direction.
            dy: The distance to move in the vertical direction.
            speed: The speed with which to move.
            nextAction: A function called after the move is completed.
        """
        x = obj.getX() + dx
        y = obj.getY() + dy
        super(MoveEvent, self).__init__(obj, x, y, speed, nextAction)
