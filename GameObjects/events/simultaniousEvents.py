class SimultaniousEvents:
    """
    Set of events that will be executed simultaniously.
    """
    def __init__(self):
        """
        Constructs a SimultaniousEvents object.
        """
        self.events = []


    def add(self, event):
        """
        Adds an event to the set of events to be executed simultaniously.
        """
        self.events.append(event)


    def __call__(self):
        """
        Calls all events added to this event.

        Return: False if all events are complete, True otherwise.
        """
        if len(self.events) > 0:
            self.events = [event for event in self.events if event()]
            return len(self.events) > 0
        else:
            return False
