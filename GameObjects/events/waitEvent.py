class WaitEvent:
    """
    I see absolutely no use for this event given its current implementation.
    """
    def __init__(self):
        self.finished = False
        
    def __call__(self):
        self.finished = True
