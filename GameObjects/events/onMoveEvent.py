from Myro import getRobot

class OnMoveEvent:
    """
    Event that triggers if the robot moves.
    """
    def __init__(self, action=None, d=5, t=0):
        """
        Constructs an OnMoveEvent.

        Args:
            action: The function that will be called when the robot moves.
            d: The minimum distance that the robot has to move before this event
                triggers.
            t: Time to wait before this event becomes active. Usefull if the
                event is trigger by movement, and you wish to give the robot
                some time to come to a halt before triggering the event.
        """
        robot = getRobot()
        self.x = robot.frame.x
        self.y = robot.frame.y
        self.d = d
        self.action = action
        self.timer = 0 # Time to wait untill this event activates
        
    def __call__(self):
        """
        Trigger the event if all conditions are met.

        Return: False if all conditions were met and the event was triggered,
            or if there was no robot initialized and the event was discarded.
            True otherwise.
        """
        if self.timer > 0:
            self.timer -= 1
            return True
        if self.timer == 0:
            self.timer -= 1
            robot = getRobot()
            self.x = robot.frame.x
            self.y = robot.frame.y
            return True
        robot = getRobot()
        if robot:
            if (abs(self.x - robot.frame.x) > self.d) or (abs(self.y - robot.frame.y) > self.d):
                self.action()
                return False
            else:
                return True
        else:
            return False
        
