from Graphics import Picture

class SpriteSheet(object):
    def __init__(self, filename, w, h, xSpace, ySpace):
        self.sheet = Picture(filename)
        self.w = w
        self.h = h
        self.xSpace = xSpace
        self.ySpace = ySpace
        
    def get(self, x, y, flipH=False, flipV=False, noBorder=True, center=True):
        r = self.sheet.getRegion([self.xSpace*x, self.ySpace*y], self.w, self.h)
        if flipH: r.flipHorizontal()
        if flipV: r.flipVertical()
        if noBorder: r.border = 0
        if center: r.moveTo(0,0)
        return r

    def getAll(self, xMax, yMax):
        allSprites=[]
        for y in range(yMax):
            for x in range(xMax):
                allSprites.append(self.get(x,y))
        return allSprites
