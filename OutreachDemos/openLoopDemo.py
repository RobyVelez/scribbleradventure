from Graphics import *
from Myro import *
from os.path import dirname, realpath
import sys
import copy
import threading
import traceback
import Myro

sys.path.append(dirname(realpath(__file__))+"/../WindowObjects")
from windowObjects import DragAndDropButton, WindowBase, TextP, ButtonP, EntryBox


##############Special Classess############

## class myRectangle(Rectangle):
##     def __init__(self,p1,p2):
##         self.data={}
##         self.p1=p1
##         self.p2=p2
##         
##         fontsize=24        
##         cX=self.center[0]
##         cY=self.center[1]
##         
##         self.label=Text((p1[0]-cX,p1[1]-cY+self.height/2+fontsize/2)," ",xJustification="left",yJustification="bottom",color=Color("black"),fontSize=fontsize)
##         self.label.draw(self)        
##         
##     def setup(self,dictionaryData):
##         self.data=copy.copy(dictionaryData)
##         self.label.setText(self.data["command"])
##     
##     def __copy__(self):
##         ret=myRectangle(self.p1,self.p2)
##         ret.setup(self.data)
##         ret.tag=self.tag
##         return ret

def indent(column):
    return " " * column

class DragAndDropControl(WindowBase):
    # Static variables
    DESC_FONTSIZE = 18

    def __init__(self):
        ################Main Window and Frames################
        WindowBase.__init__(self, "Control Script",800,800)
##         self.mainWindow=Window("Control Script",800,800)
##         self.mainWindow.onMouseDown(self.mouseDown)
##         self.mainWindow.onMouseUp(self.mouseUp)
##         self.mainWindow.onMouseMovement(self.mouseMove)
        self.optionsPanel=Frame(0,0)
        self.scriptPanel=Frame(400,0)
        self.scriptPanelHitBox=Rectangle((400,0),(800,800))
        self.scrollPanel=Frame(780,0)
        self.descriptionPanel=Frame(0,700)


        self.optionsPanel.draw(self.mainWindow)
        self.scriptPanelHitBox.fill.alpha=0
        self.scriptPanelHitBox.draw(self.mainWindow)
        self.scriptPanel.draw(self.mainWindow)
        self.descriptionPanel.draw(self.mainWindow)

        ##########Robot Connection Objects############
        self.connected=False
        
        self.comPortEntry=EntryBox((25,25),(400,50))
        self.comPortEntry.maxChars=99
        self.comPortEntry.justNumbers=False
        self.comPortEntry.setup(self.mainWindow)
        self.comPortEntry.draw(self.mainWindow)
        
        self.initLabel=TextP((0,40),"init")
        self.initLabel.draw(self.mainWindow)
        
        self.status=TextP((25,20),"Connected To:")
        self.statusValue=TextP((150,20)," ")
        self.status.draw(self.mainWindow)
        self.statusValue.draw(self.mainWindow)
        
        
        self.connectButton=ButtonP((25,50),(125,75),5)
        self.connectButton.init()
        self.connectButton.text.setText("Connect")
        self.connectButton.draw(self.mainWindow)
        self.connectButton.fill=Color("green")
        
        self.disconnectButton=ButtonP((125,50),(250,75),5)
        self.disconnectButton.init()
        self.disconnectButton.text.setText("Disconnect")
        self.disconnectButton.draw(self.mainWindow)
        self.disconnectButton.fill=Color("red")
        
        ##########Scroll bar###########
        self.scrollPanel.draw(self.mainWindow)
        self.scrollButton=Rectangle((0,0),(20,25))
        self.scrollButton.draw(self.scrollPanel)
        self.scrollButton.tag="up"
        self.dragOffsetY=0

        #########Scripting area############
        self.placeH=50
        self.placeW=250
        self.placeholder=DragAndDropButton((0,0),(self.placeW,self.placeH))
        self.placeholder.draw(self.scriptPanel)            
        self.scriptList=[]
        self.scriptList.append(self.placeholder)
        self.deltaX = 0
        self.deltaY = self.placeH

        ######### Run button #########
        self.runButton = ButtonP((100,625),(300,675), 5)
        self.runButton.xMargin = 100
        self.runButton.yMargin = 10
        self.runButton.defaultColor = Color("lightgreen")
        self.runButton.init()
        self.runButton.text.xJustification="center"
        self.runButton.text.setFontSize(28)
        self.runButton.text.text="RUN!"
        self.runButton.draw(self.mainWindow)
        self.runButton.connect("click",self.run)
        
        ######### Stop button #########
        self.stopButton = ButtonP((100,550),(300,600), 5)
        self.stopButton.xMargin = 100
        self.stopButton.yMargin = 10
        self.stopButton.defaultColor = Color(255, 100, 100)
        self.stopButton.init()
        self.stopButton.text.xJustification="center"
        self.stopButton.text.setFontSize(28)
        self.stopButton.text.text="STOP"
        self.stopButton.disable()
        self.stopButton.draw(self.mainWindow)
        self.stopButton.connect("click",self.stop)

        #########Control Variables#########
        self.heldObject=None
        self.currentThread=None
        
        ######### Description panel #########
        descriptionBox = Rectangle((0,0),(400,100))
        descriptionBox.fill = Color("lightblue")
        descriptionBox.draw(self.descriptionPanel)
        self.descriptionText = TextP((5, 5), " ")
        self.descriptionText.configText(" ", fontSize=self.DESC_FONTSIZE)
        self.descriptionText.yJustification="top"
        self.descriptionText.draw(self.descriptionPanel)
        

        
                                                                            
        #########Options on lefthand side############
        self.optionButtons=[]

        self.startX=0
        self.startY=200
        self.commandNames=["forward","backward","turnLeft","turnRight", "turnBy"]
        self.numParams=[2,2,2,2,1]
        self.defaultParams=[[0.5,1],[0.5,1],[0.5,1],[0.5,1],[90]]
        self.descriptions=["Moves the robot forward. The first argument is\nthe speed of the robot, the second argument is\nthe duration of the movement.",
            "Moves the robot backward. The first argument is\nthe speed of the robot, the second argument is\nthe duration of the movement.",
            "Turns the robot left. The first argument is\nthe speed of the turn, the second argument is\nthe duration of the turn.",
            "Turns the robot right. The first argument is\nthe speed of the turn, the second argument is\nthe duration of the turn.",
            "Turns the robot by degrees."]
        for i in range(len(self.commandNames)):
            oX=self.startX +i*self.deltaX
            oY=self.startY +i*self.deltaY
            self.optionButtons.append(DragAndDropButton((oX,oY),(oX+self.placeW,oY+self.placeH)))
            #have to draw the button before do setup, b/c the entry text need a handle to the window
            self.optionButtons[i].mouseOverText = self.descriptions[i]
            self.optionButtons[i].disabledTextColor = Color("black")
            self.optionButtons[i].defaultColor = Color(255, 80, 80)
            self.optionButtons[i].applyColor()
            self.optionButtons[i].draw(self.optionsPanel)            
            self.optionButtons[i].setup({"command":self.commandNames[i],"numParams":self.numParams[i],"editable":False,"defaultParams":self.defaultParams[i]})
            
            self.optionButtons[i].tag="option"
            self.connectMouseOverEnter(self.optionButtons[i], self.optionButtons[i].onMouseEnter)
            self.connectMouseOverLeave(self.optionButtons[i], self.optionButtons[i].onMouseLeave)
            self.connectMouseOverEnter(self.optionButtons[i], self.showDescription)
            self.connectMouseOverLeave(self.optionButtons[i], self.hideDescription)
            self.optionButtons[i].draw(self.optionsPanel)
        self.addIdleEvent(self.updateConnectionStatus)
        self.setup()    

    def showDescription(self, button, event):
        self.descriptionText.text=button.mouseOverText
        
    def hideDescription(self, button, event):
        self.descriptionText.text=" "
        
        ##############Helper functions#############
        #tries to removes a shape from mainWindow
        #then from scriptPanel
    def removeShape(self, s):
        if s in self.mainWindow.canvas.shapes:
            self.mainWindow.canvas.shapes.RemoveAt(self.mainWindow.canvas.shapes.IndexOf(s))
        if s in self.scriptPanel.shapes:
            self.scriptPanel.shapes.RemoveAt(self.scriptPanel.shapes.IndexOf(s))
                                                                       
        #redraws the scriptList
    def updateScriptList(self):
        #grabs x coordinate of center of placeholder
        cX=self.scriptList[-1].center[0]
        #need a way to specify cY rather than just assume placeH/2
        for i in range(len(self.scriptList)):
            self.scriptList[i].moveTo(cX,self.placeH/2+self.placeH*i)

    #removes the toStackObject from scriptPanel.shapes and 
    #adds it back at the end. Ensures the toStackObject appears ontop
    def scriptPanelStackOnTop(self, toStackObject):
        if toStackObject in self.scriptPanel.shapes:
            self.scriptPanel.shapes.Remove(toStackObject)
            self.scriptPanel.shapes.Add(toStackObject)
            self.mainWindow.update()        
        
        
    def updateConnectionStatus(self):
        if getRobot() != None:
            if getRobot().isConnected():
                self.connected=True
                name=getName()
                self.statusValue.setText(name)            
            else:
                self.connected=False
        else:
            self.connected=False
            
        if not self.connected:
            self.statusValue.setText("Not Connected")
        return self.mainWindow.IsRealized
    
    def mouseDown(self, obj,event):
        #global dragOffsetY,heldObject    
        #print("click at ",event.x,event.y)    
        
        if self.scrollButton.hit(event.x,event.y): #handle scrolling    
            self.scrollButton.tag="down"
            self.dragOffsetY=0#scriptPanel.y-event.y    
            
        elif self.connectButton.hit(event.x,event.y):
                t=threading.Thread(target=lambda:init(self.comPortEntry.trueText))
                t.start()                
        elif self.disconnectButton.hit(event.x,event.y):
            uninit() 
            Myro.robot=None   
                    
        elif self.scriptPanelHitBox.hit(event.x,event.y): #clicking on one of the placed options
            if calico.isRunning: return
            for c in self.scriptList:
                if c.hit(event.x,event.y):
                    if c == self.placeholder or c.hitEntry(event.x,event.y):
                        continue
                    self.heldObject=c
                    #objects in scriptList are draw to scriptPanel 
                    #so have to offset the center of scriptPanel
                    #to move them to the correct location
                    self.heldObject.moveTo(event.x-self.scriptPanel.x,event.y-self.scriptPanel.y) 
                    #makes sure it is visible and on top
                    self.scriptPanelStackOnTop(self.heldObject)
                    
                    break                                   
        else:            
            for c in self.optionButtons:
                if c.hit(event.x,event.y):
                    if c.hitEntry(event.x,event.y):
                        continue
                    self.heldObject=copy.copy(c)
                    self.connectMouseOverEnter(self.heldObject, self.heldObject.onMouseEnter)
                    self.connectMouseOverLeave(self.heldObject, self.heldObject.onMouseLeave)
                    self.heldObject.draw(self.mainWindow)
                    self.heldObject.moveTo(event.x,event.y)
                    
                    break
                    
    def mouseUp(self, obj,event):
        #global heldObject
        
        #reset the scroll button
        self.scrollButton.tag="up"
        
        if self.heldObject!=None:        
            if self.heldObject in self.scriptList:
                #objects in scriptList drawn to scriptPanel so have to
                #offset center of scriptPanel to get the proper
                #screen coordinates
                screenX=self.heldObject.center[0]+self.scriptPanel.x
                screenY=self.heldObject.center[1]+self.scriptPanel.y
            else:        
                screenX=self.heldObject.center[0]
                screenY=self.heldObject.center[1]
            
            #if release was in the scriptPanel area
            if self.scriptPanelHitBox.hit(screenX,screenY):
                if calico.isRunning: return
                if self.heldObject in self.scriptList:
                    #remove from the list so that it can be reinserted with same code 
                    #used to insert it if dragged from options                      
                    self.scriptList.remove(self.heldObject)
                else: #assumes heldObject was dragged from the options panel
                    self.removeShape(self.heldObject) 
                    self.heldObject.makeEditable()
                    self.heldObject.draw(self.scriptPanel) #redraw to scriptPanel
                    
                    #init default parameters when dragged from options panel to scriptpanel. RV
                    self.heldObject.setDefaultValue()
                    
                    
                #by default object will be inserted into the second to last position
                insertIndex=len(self.scriptList)-1
                #see if the object was dropped on any of the commands already in the scripting area
                for i in range(len(self.scriptList)):
                    if self.scriptList[i].hit(screenX,screenY):
                        insertIndex=i  
                    
                #heldObject.moveTo(commandList[insertIndex].center[0],commandList[insertIndex].center[1])
                self.scriptList.insert(insertIndex,self.heldObject)
                self.updateScriptList()
       
            else:
                #acts like a delete. simply drag a command out of the scripting area
                if self.heldObject in self.scriptList:
                    self.disconnect(self.heldObject)
                    self.scriptList.remove(self.heldObject)   
                    self.updateScriptList()
                         
                self.removeShape(self.heldObject)
                self.mainWindow.update()           
        
        self.heldObject=None
        
        # Update highlights
        WindowBase.mouseMove(self, obj, event)
        self.placeholder.unhighlight()
        self.updateConnectionStatus()
        
    def mouseMove(self, obj,event):
        WindowBase.mouseMove(self, obj, event)
        #handles scrolling
        if self.scrollButton.tag=="down":
            if self.dragOffsetY+event.y>=0 and self.dragOffsetY+event.y <= self.mainWindow.height-100:
                self.scriptPanel.moveTo(self.scriptPanel.x,-1*(self.dragOffsetY+event.y))
                self.scrollPanel.moveTo(self.scrollPanel.x,self.dragOffsetY+event.y)            
            elif self.dragOffsetY+event.y>self.mainWindow.height-50:
                self.scriptPanel.moveTo(self.scriptPanel.x,-1*(self.mainWindow.height-50))
                self.scrollPanel.moveTo(self.scrollPanel.x,self.mainWindow.height-50)
            elif self.dragOffsetY+event.y<0:
                self.scriptPanel.moveTo(self.scriptPanel.x,0)
                self.scrollPanel.moveTo(self.scrollPanel.x,0)
        elif self.heldObject!=None:
                #items in commandList are drawn to scriptPanel so we have to offset their movement.
                if self.heldObject in self.scriptList:
                    self.heldObject.moveTo(event.x-self.scriptPanel.x,event.y-self.scriptPanel.y)        
                else:
                    self.heldObject.moveTo(event.x,event.y)
                if self.scriptPanelHitBox.hit(event.x, event.y) and (self.mouseOverShapes == [self.heldObject] or len(self.mouseOverShapes)==0):
                    if calico.isRunning: return
                    self.placeholder.highlight()
                else:
                    self.placeholder.unhighlight()
        else:
            self.placeholder.unhighlight()
            
    def printScriptList(self):
        for i in range(len(self.scriptList)-1):
            print("Command: "+str(i)+" "+self.scriptList[i].data["command"]+" "+str(self.scriptList[i].getEntryValues()))
         

    def stop(self, obj=None, event=None):
        #if self.currentThread is None: return
        if not self.stopButton.enabled: return
        calico.AbortThread()
        stop()
        self.onFinished()

        
    def onFinished(self):
        self.stopButton.disable()
        self.runButton.enable()
        for button in self.scriptList:
            #button.unhighlight()
            button.enable()
        #self.currentThread=None
    
    def traceButton(self, i):
        self.scriptList[i].fill = Color("yellow")
        
    def untraceButton(self, i):
        self.scriptList[i].fill = self.scriptList[i].disabledColor
                   
    
            
    def run(self, obj=None, event=None):
        #if self.currentThread is not None: return
        if not self.runButton.enabled: return
        self.stopButton.enable()
        self.runButton.disable()
        script = "try:\n"
        currentIndent = 1
        script += indent(currentIndent) + "pass\n"
        for j in xrange(len(self.scriptList)):
            button = self.scriptList[j]
            button.disable()
            if button == self.placeholder: continue
            # Trace
            script += indent(currentIndent) + "dragAndDropWindow.traceButton(" + str(j) + ")\n"
            
            script += indent(currentIndent) + button.data["command"]
            script += "("
            for i in xrange(button.data["numParams"]):                
                
                #manually fixing turnBy command so that it is more intuitive. 
                #multiplying any value sent to turnBy by -1. RV
                if button.data["command"]=="turnBy":
                    if self.canCastToNumber(button.getEntryValues()[i]):
                        v=self.castToNumber(button.getEntryValues()[i])
                        script+=str(-1*v)
                    else:   
                        script += str(button.getEntryValues()[i])
                else:
                    script += str(button.getEntryValues()[i])
                    
                
                if i < button.data["numParams"] - 1:
                    script += ","
            script += ")\n"
            script += indent(currentIndent) + "dragAndDropWindow.untraceButton(" + str(j) + ")\n"
        script += "except KeyboardInterrupt:\n"
        script += indent(4) + "pass\n"
        script += "except:\n"
        script += indent(4) + "print(traceback.format_exc())\n"
        script += "dragAndDropWindow.onFinished()\n"
        calico.ExecuteInBackground(script)
        #self.currentThread = threading.Thread(target=lambda: self.executeScript(script))
        #self.currentThread.start()
        
        #print(script)
            
    def executeScript(self, script):
        calico.manager["python"].engine.Execute(script)
          
     
                                  
if __name__=="__main__":
    global dragAndDropWindow
    dragAndDropWindow = DragAndDropControl()                    



'''
class draggableButton():
    def __init__(self,canvas,p1,p2,text,numParams):
        self.shape=Rectangle(p1,p2)
        
        cX=self.shape.center[0]
        cY=self.shape.center[1]
        
        fontsize=24
        
        self.label=Text((p1[0]-cX,p1[1]-cY+self.shape.height/2+fontsize/2),text,xJustification="left",yJustification="bottom",color=Color("black"),fontSize=fontsize)
        
        self.shape.draw(canvas)        
        self.label.draw(self.shape)
        
        self.paramIndications=[]
        #for i in range(numParams):
'''
'''    
    for i in range(insertIndex+1,len(commandList)):
        commandList[i].move(0,commandSlotH)

#picked up one of the options. tag should be 'options'

    
    heldObject.moveTo(commandList[insertIndex].center[0],commandList[insertIndex].center[1])
    commandList.insert(insertIndex,heldObject)
    for i in range(insertIndex+1,len(commandList)):
        commandList[i].move(0,commandSlotH)
    
heldObject=None
    
    
    
    
    for i in range(len(commandList)):
        if commandList[i].hit(heldObject.center[0],heldObject.center[1]):
            insertIndex=i      

          
    
    
    
    
    #by default object will be inserted into the second to last position
    insertIndex=len(commandList)-1
    #see if the object was dropped on any of the commands already in the scripting area
    for i in range(len(commandList)):
        if commandList[i].hit(heldObject.center[0],heldObject.center[1]):
            insertIndex=i        
    
    removeShape(heldObject)
    heldObject.draw(scriptPanel)
    heldObject.tag="placedOption"
    heldObject.moveTo(commandList[insertIndex].center[0],commandList[insertIndex].center[1])
    
    commandList.insert(insertIndex,heldObject)
    for i in range(insertIndex+1,len(commandList)):
        commandList[i].move(0,commandSlotH)
    
    heldObject=None
    
    for i in range(len(commandList)):
        if commandList[i].hit(heldObject.center[0],heldObject.center[1]):
            insertIndex=i        
    
''' 
