#!/usr/local/bin/python

#check to see of a PortalWindow.zip file exists
#if it does rename it with the data
#make a new PortalWindow folder
#move all the include files to the folder

import os
import shutil #coping directories
import zipfile
import time
import subprocess

#Current Directory
SRCDIR=os.path.dirname(os.path.realpath(__file__))+"/"
#One Directory up
DESTDIR=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Transfer/"

#print(SRCDIR)
#print(DESTDIR)

includeDirectories=["Intro","Gauntlet","OpenLoopControl","Labyrinth","Chamber","ClosedLoopControl","CommonFunctions","GameObjects","WindowObjects",
                            "CommonImages","ChooseYourOwnAdventure","PlatformerBase","Graphics","OutreachDemos","Diversions","HackableDemos"]
    

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))
            #ziph.write(file)

def main():

    if os.path.exists(DESTDIR):

        if os.path.isfile(DESTDIR+"PortalWindow.zip"):
            os.rename(DESTDIR+"PortalWindow.zip",DESTDIR+"PortalWindow_"+time.strftime("%m_%d_%Y")+".zip")

        if os.path.isdir(DESTDIR+"PortalWindow"):
            shutil.rmtree(DESTDIR+'PortalWindow')        
        os.makedirs(DESTDIR+"PortalWindow")

    
        for d in includeDirectories:
            if os.path.isdir(SRCDIR+d):
                shutil.copytree(SRCDIR+d,DESTDIR+"PortalWindow/"+d)
    

        includeFiles=["PortalWindow.py"]

        for i in includeFiles:        
            if os.path.isfile(SRCDIR+i):
                shutil.copyfile(SRCDIR+i,DESTDIR+"PortalWindow/"+i)
    


		#go to the destination directory
		os.chdir(DESTDIR)
				
		subprocess.call("zip -r PortalWindow.zip PortalWindow",shell=True)

		#go back to current directory
		os.chdir(SRCDIR)

        #zipf=zipfile.ZipFile(DESTDIR+"PortalWindow.zip","w")
        #zipdir(DESTDIR+"PortalWindow",zipf)
        #zipf.close()
    
        #print("Enter username:")
        #username=raw_input()

        #os.system("rsync -pavz --progress "+"PortalWindow.zip "+username+"@cobi.cs.uwyo.edu:/var/www/EvolvingAILab/sites/fish34.cs.uwyo.edu.lab/files/share/LRC/")


    else:
        print("Warning, not destination folder ",DESTDIR," does not exist. Please create one and try again.")
    

main()



'''
    if os.path.isdir("Gauntlet"):
        shutil.copytree("Gauntlet","PortalWindow/Gauntlet")
    if os.path.isdir("Labyrinth"):
        shutil.copytree("Labyrinth","PortalWindow/Labyrinth")
    if os.path.isdir("Dungeon"):
        shutil.copytree("Dungeon","PortalWindow/Dungeon")
    if os.path.isdir("CloudCity"):
        shutil.copytree("CloudCity","PortalWindow/CloudCity")
    if os.path.isdir("CommonImages"):
        shutil.copytree("CommonImages","PortalWindow/CommonImages")
    if os.path.isdir("CommonFunctions"):
        shutil.copytree("CommonFunctions","PortalWindow/CommonFunctions")
    if os.path.isdir("GameObjects"):
        shutil.copytree("GameObjects","PortalWindow/GameObjects")
'''    
