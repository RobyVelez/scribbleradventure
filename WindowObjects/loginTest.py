from Myro import *
import ConfigParser
import os
from random import *

USERLIST="TestDropbox/UserData/UserList.txt"
USERDATA="TestDropbox/Students/"


#writes and reads the user data from a text file
config = ConfigParser.SafeConfigParser()

def clearConfig():
    global config
    toRemove=[]
    for s in config.sections():
        toRemove.append(s)
    for r in toRemove:
        config.remove_section(r)

def userExists(username):
    config.read(USERLIST1)        
    exists=config.has_section(username)
    clearConfig()        
    return exists

def createUserData(username):

    if not os.path.isdir(USERDATA+username):
        os.makedirs(USERDATA+username)
    else:
        print("Note, "+USERDATA+username+" already exists")
    
    dataLoc=USERDATA+username+"/"+"userData.p"
    data=open(dataLoc,"w")
    for i in range(100):
        data.write(str(random()))
    data.close()  
    
    return dataLoc      

def retrieveAccount(credentials):

    username=credentials["First Name"]+credentials["Last Name"]
    userFile=open(USERLIST1,"r")
    
    if userExists(username):
        dataLoc=USERDATA+username+"/"+"userData.p"
        if not os.path.exists(dataLoc):            
            print("Error, no user data found at "+dataLoc)
        else:
            print("Retrieving user data")   
            print(dataLoc) 
    else:
        prompt("failedLogin",credentials)
        
       
def createAccount():

    credentials= ask(["First Name","Last Name","Gender","Birthday (month/day/year)"],
            "Please provide the following information\nto create a new account")    
            
    username=credentials["First Name"]+credentials["Last Name"]

    if not userExists(username):
        userFile=open(USERLIST1,"a")
        
        config.add_section(username)
    
        for key,value in credentials.items():
            config.set(username,key,value)
    
        config.write(userFile)
        userFile.close()
    
        #effectively resets the parser
        clearConfig()
     
        dataLoc=createUserData(username)
        
        print("Returning user data just created")
        print(dataLoc)
    else:
        print("Account already existed. Returning data") 

def login():    
    credentials=ask(["First Name","Last Name"],"Enter you're credentials.\nNote, case sensitive.")
    username=credentials["First Name"]+credentials["Last Name"]          
    
    if userExists(username):
        retrieveAccount(credentials)
    else:
        prompt("failedLogin",credentials)
        

def prompt(message=None,credentials={"foo":"bar"}):
    if message=="failedLogin":
        response=askQuestion("Error, no account found for "+credentials["First Name"]+" "+credentials["Last Name"]+"\n",["Try Again - Login","Create New Account"])
    else:    
        #only checking the existence of UserList.txt once
        if not os.path.exists(USERLIST1):
            userFile=open(USERLIST1,"wb")        
            userFile.close()                
        response=askQuestion("Welcome to the Laramie Robotics Club user portal. \nSelect one of the following options.",["Login","Create New Account"])
    
    if response=="Login" or response=="Try Again - Login":       
        login()
    else:   
        createAccount()
        
try:           
    prompt()
except:
    print("Warning, sudden exit from login.")
    
    
    
    
    