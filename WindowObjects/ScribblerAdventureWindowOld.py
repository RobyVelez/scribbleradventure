from Myro import *
from Graphics import *
import os 
import sys
from functools import partial ### JH: partial added to prevent multi-load crash
import pickle #for saving data

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/GameObjects")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/WindowObjects")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonImages")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions") #soon to be depreceated
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Gauntlet")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Labyrinth")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Dungeon")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CloudCity")
IMAGE_PATH=os.path.dirname(os.path.realpath(__file__))+"/CommonImages/"   

if __name__=="__main__":
    #make sure the shell doesn't reset
    calico.config.SetValue('python-language','reset-shell-on-run',False)
    #calico.config.SetValue('python-language','reset-show-on-run',False)
    #On the windows machines in 4045 this is needed because those machines
    #lose the calico global variable
    import setCalico
    setCalico.initCalico(calico)
    
from windowObjects import *
from userFunctions import *

class ScribblerAdventure():
    def __init__(self,gameData=None,parent=None):    
        
        if gameData==None:
            #make a new default GameData object
            self.gameData=GameData()
        else:
            self.gameData=gameData  
        self.parent=parent
              
        self.currentStage=None
        self.currentLevel=None
        
        self.stages=["Gauntlet","Labyrinth","Dungeon","CloudCity"]
        #self.stages=["Intro,Gauntlet","Labyrinth","Chamber"]
        
        self.levels=[["gauntlet1","gauntlet2","gauntlet3","gauntlet4","gauntlet5","gauntlet6","gauntlet7","gauntlet8","gauntlet9","gauntlet10"],
            ["labyrinth1","labyrinth2","labyrinth3","labyrinth4","labyrinth5","labyrinth6", "labyrinth7"],
            ["dungeon1","dungeon2","dungeon3","dungeon4","dungeon5","dungeon6","dungeon7","dungeon8"],
            ["cloudCity1","cloudCity2","cloudCity3","cloudCity4","cloudCity5","cloudCity6"]]
            
        #self.levels=[["gauntlet1","gauntlet2","gauntlet3","gauntlet4","gauntlet5","gauntlet6","gauntlet7","gauntlet8","gauntlet9","gauntlet10"],
        #    ["labyrinth1","labyrinth2"],
        #    ["dungeon1","dungeon2","dungeon3","dungeon4","dungeon5","dungeon6","dungeon7","dungeon8"],
        #    ["cloudCity1","cloudCity2","cloudCity3","cloudCity4","cloudCity5","cloudCity6"]]
        
        
        #create windows and panels
        self.mainWindow=WindowLRC("ScribblerAdventure",800,700)
        self.mainWindow.parent=parent
        self.optionPanel=self.mainWindow.addPanel("Options",600,0,200,800,Color("gray"))
        self.descriptionPanel=self.mainWindow.addPanel("Description:",0,600,600,100,Color("blue"))
        self.mainPanel=self.mainWindow.addPanel("Activities Portal",0,0,600,600,Color("white"),) 
        if self.mainWindow.parent != None:
            self.mainPanel.addButton(0,550,125,50,self.returnToPortalAction,"Back To Portal")        

        tempX=400
        tempY=500
        for s in self.stages:
            self.mainPanel.addButton(tempX,tempY,200,50,self.launchButtonAction,s)
            tempY=tempY-75
     
    def setParent(self,parent):
        self.parent=parent
                        
    def returnToPortalAction(self,buttonName=None):  
        self.mainWindow.quit()
         
    def launchButtonAction(self,buttonName=None):
        if self.gameData.highestStage>=self.stages.index(buttonName): 
            self.hideWindow()
            self.currentStage=self.stages.index(buttonName)          
            if self.gameData.highestStage==self.stages.index(buttonName):                
                self.currentLevel=self.gameData.highestLevel
            else:
                self.currentLevel=0
            self.launchLevel()        
    
    def launchLevel(self):
        if self.currentLevel != None and self.currentStage != None:
            launcher=__import__(self.levels[self.currentStage][self.currentLevel], globals(), locals(), ['*'])
            self.child=launcher.startLevel(self.gameData,self)
            
    def launchNextLevel(self):
        nextLevel=self.nextLevelExists()
        if nextLevel=="level exists": #self.nextLevelExists():
            self.currentLevel += 1
            self.launchLevel()
        elif nextLevel=="end of stage":
            print("End of this stage")
            self.child.quitGame()
        elif nextLevel=="level locked":
            print("Next level locked.")
              
    def launchPrevLevel(self):
        if self.prevLevelExists():
            self.currentLevel -= 1
            self.launchLevel()    
        else:
            print("Error. No previous level!")

    def prevLevelExists(self):
        if self.currentLevel==0:
            return False
        else:
            return True
    def nextLevelExists(self):
        if self.currentStage < self.gameData.highestStage:
            if self.currentLevel < len(self.levels[self.currentStage])-1:
                return "level exists" #1 #next level exists 
            else:
                return "end of stage" #0 #no more levels in this stage
        else:
            if self.currentLevel < self.gameData.highestLevel:
                return "level exists" #1 #next level exists
            else:
                return "level locked" #-1 #next level not unlocked
    
    #function called by the level after gameover.
    def levelSolved(self):
        if self.currentStage < self.gameData.highestStage:
            #current stage has already been beaten. nothing to unlock
            pass
        elif self.currentLevel < self.gameData.highestLevel:
            #didn't beat the furthest level. nothing to unlock
            pass
        elif self.currentLevel == self.gameData.highestLevel:
            if self.gameData.highestLevel == len(self.levels[self.currentStage])-1:
                self.gameData.highestStage=min(self.currentStage+1,len(self.stages)-1)
                self.gameData.highestLevel=0
            else:
                self.gameData.highestLevel=self.currentLevel+1
        else:
            print("Error is LevelSolved funtion. Should not reach here!")    
                            
         
    def saveGameData(self):
        if self.parent != None:
            self.parent.saveGameData()
        else:
            pass
    ##################    
    #Wrapper Functions#
    ##################        
    def showWindow(self):
        self.mainWindow.showWindow()
    def hideWindow(self):
        self.mainWindow.hideWindow()

scribAdj=None        
def start(gameData=None,parent=None):    
    global scribAdj
    scribAdj=ScribblerAdventure(gameData,parent)
    return scribAdj
if __name__ == "__main__":
    start()
       