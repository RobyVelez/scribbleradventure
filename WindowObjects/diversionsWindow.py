from os.path import realpath, dirname
import sys
sys.path.append(dirname(realpath(__file__)) + "/..")
from PortalWindow import *

#Normally you have to run the PortalWindow.py to get access to the Hall Of Fame
#Make RUN_INDEPENDENT=True in order to run diversionsWindow.py as a standalone
#that can interface with the Hall Of Fame. 
RUN_INDEPENDENT=False

class DiversionsWindow(MenuWindow):
    def __init__(self, gameData=None,parent=None):
        MenuWindow.__init__(self, parent, "Diversions")
        self.parent=parent
        
        if self.parent!=None:
            self.hallDir=self.parent.hallDir
        else:
            if RUN_INDEPENDENT:
                self.hallDir=self.getHallOfFamePath() 
                if isinstance(self.hallDir,bool):
                    self.hallDir=None
            else:
                self.hallDir=None   
                            
        if gameData==None:
            self.gameData={}
            self.gameData["Diversions"]={}
        else:
            self.gameData=gameData
            
        self.hallMax=5
        
        # Add buttons
        self.addHeader("Simple Games")
        self.addButton("Increment Score", "Simple game where the High Score is whatever you want.")
        self.addButton("Your Game", "A placeholder for if you want to add your own game.")
                      
    
    #Figuring out save paths and directories is always the most fun... RV
    def getHallOfFamePath(self):
        
        #Both DROPBOX_PATH and OFFLINE can be found in PortalWindow.py
        
        #sets or creates the top main folder that holds everything
        if os.path.exists(DROPBOX_PATH) and not OFFLINE:
            self.usingDropBox=True            
            self.mainDir=DROPBOX_PATH    
        else:
            self.usingDropBox=False
            tempDir=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/"            
            if self.checkDirectory(tempDir+"DropLocal/"):
                self.mainDir=tempDir+"DropLocal/"
            else:
                return False #the directory doesn't exist
        
        #Sets or creates a the LRCData, directory    
        if self.checkDirectory(self.mainDir+"LRCData/"):
            self.dataDir=self.mainDir+"LRCData/"
        else:
            return False

        #Sets or creates a the HallOfFame, directory   
        if self.checkDirectory(self.dataDir+"HallOfFame/"):
            self.hallDir=self.dataDir+"HallOfFame/"
            return self.hallDir
        else:
            return False       
       
    def checkDirectory(self, dirName):
        if os.path.exists(dirName):    
            return True
        else:
            #MAKE_DIRECTORY is found in PortalWindow.py
            print("No directory found for ",dirName)
            if MAKE_DIRECTORY:
                print("Making directory.")            
                os.makedirs(dirName)
                return True
            else:
                print("Stopping.")                            
                return False                
      
    
    def launchActivity(self, buttonName=None):
        position=self.mainWindow.win.GetPosition()               
                
        if buttonName == "Increment Score":
            
            #Stuff that has to do with gameData and recoring information
            if "IncrementScore" not in self.gameData["Diversions"]:
                self.gameData["Diversions"]["IncrementScore"]={}    
                self.gameData["Diversions"]["IncrementScore"]["score"]=[]
                 
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["Diversions"]["IncrementScore"])                       
            
            ###########If you want to add your own games.#############
            #First add a button above in the init, but after you have to configure the launcher
            #This is where the external games are launched from
            #'incrementScore' is the name of a file ('incrementScore.py') in the /Diversions folder
            #it's assumed that the 'incrementScore.py' has a 'start()' function that accepts two
            #variables: gameData and self (which is just a point back to diversionsWindow.py)
            launcher=__import__("incrementScore", globals(), locals(), ['*'])
            self.mainWindow.child=launcher.start(self.gameData,self)
            
            #moves the newly created window so it's in the same location as the diversionsWindow.py window
            if isinstance(self.mainWindow.child,WindowClass):
                position=self.mainWindow.GetPosition()
                self.mainWindow.child.Move(position[0],position[1])#self.mainWindow.GetPosition()[0],self.mainWindow.GetPosition()[1])
            
            
            #for some reason the hide has to happen after you move the window or else the move won't work
            self.hideWindow() #hide this window    
            
            
        elif buttonName == "Your Game":
            #TODO: Fill this out and make an empty game for people to add to. RV  
            pass  
    
    def hallDisplayAndUpdate(self,newScore,existingScores,newHighScore):
        prompt="Your score: "
        prompt+=str(newScore)+"\n\n"
        prompt+="Top "+str(self.hallMax)+" Scores:\n\n"
        for s in existingScores:
            prompt+="%-5g"%(s[0])
            prompt+=" - "+s[1]+"\n"
        prompt+="\n"        
        if newHighScore:
            prompt+="Congratulations! Your score has placed within the top "+str(self.hallMax)+".\n"
            prompt+="If you want to record your score please enter a name.\n"
            prompt+="and then hit 'Ok'. Leave blank and hit 'Cancel' otherwise.\n"
            entry=ask("Name",prompt)
            return entry
        else:
            prompt+="Hit 'Ok' to return back to previous window.\n"
            askQuestion(prompt,["Ok"])
            return ""
            
            
    def updateHallOfFame(self,activityName,newScore):
        if activityName in self.gameData["Diversions"]:
            if "score" in self.gameData["Diversions"][activityName] and isinstance(self.gameData["Diversions"][activityName]["score"],list):
                self.gameData["Diversions"][activityName]["score"].append(newScore) 
                
    
        if self.hallDir!=None: #parent should be the portal
            if os.path.exists(self.hallDir):
                newHighScore=False
                scores=[]
                if not os.path.exists(self.hallDir+activityName+".txt"):
                    #no other current entry so the newScore is a high score
                    newHighScore=True
                    #ret=self.hallDisplayAndUpdate(newScore,scores,newHighScore)
                else: #simply going to read then overwrite the score file.                     
                    #read file
                    f=open(self.hallDir+activityName+".txt","r")
                    lines=[]
                    for line in f:
                        if line.rstrip("\n")!="":
                            lines.append(line.rstrip("\n"))
                    f.close()
                    for l in lines:
                        scores.append([float(l.split(":")[0]),l.split(":")[1]])
                    
                    #sorts based on the score which is the first index
                    scores=sorted(scores,key=lambda x:x[0],reverse=True)
                    
                    #possible if someone deletes all the scores from a file, but leaves the file
                    if len(scores)==0 or len(scores)<self.hallMax: #possible if someone deletes all the scores from a file, but leaves the file
                        newHighScore=True
                    #if there are less than 10 scores than your score is within the top ten
                    elif len(scores)<self.hallMax: 
                        newHighScore=True
                    else:
                        #grabs lowest score within the top ten
                        lowestScore=scores[ min( max(len(scores)-1,0) ,self.hallMax-1) ][0]
                        if lowestScore<=newScore:
                            newHighScore=True
                    
                ret=self.hallDisplayAndUpdate(newScore,scores,newHighScore)
                if newHighScore and ret!="" and ret!=None:
                    scores.append([newScore,ret])
                    #resorting the list is wasteful and I htink the code below the sort
                    #which is commented out is correct, but the sort method is just bullet proof
                    scores=sorted(scores,key=lambda x:x[0],reverse=True)
                    
                    #if len(scores)==0:
                    #    scores.append([newScore,ret])
                    #else:
                    #    #grabs lowest score within the top ten
                    #    lowestScore=scores[ min( max(len(scores)-1,0) ,9) ][0]
                    #    if lowestScore<=newScore:
                    #        scores.insert(min( max(len(scores)-1,0) ,9),newScore)
                    #    else: 
                    #        #insert new entry into the scores list
                    #        for i in range(len(scores)):
                    #            if scores[i][0]<=newScore:
                    #                scores.insert(i,[newScore,ret])
                    #                continue
                                                       
                    #write back to file
                    f=open(self.hallDir+activityName+".txt","w")
                    #print("got here",scores)
                    for i in range( min(len(scores),self.hallMax) ):
                        f.write(str(scores[i][0])+":"+str(scores[i][1])+"\n")
                    f.close()
                        

diversionsWindow=None        
def start(gameData=None,parent=None):    
    global diversionsWindow
    diversionsWindow=DiversionsWindow(gameData,parent)
    return diversionsWindow
        
if __name__ == "__main__":
    start()
    

       