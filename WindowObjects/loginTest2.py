from Myro import *
import ConfigParser
import os
from random import *
import datetime

USERLIST="TestDropbox/UserList/"
USERDATA="TestDropbox/Students/"


#writes and reads the user data from a text file
config = ConfigParser.SafeConfigParser()

def clearConfig():
    global config
    toRemove=[]
    for s in config.sections():
        toRemove.append(s)
    for r in toRemove:
        config.remove_section(r)

def createUserData(username):

    if not os.path.isdir(USERDATA+username):
        os.makedirs(USERDATA+username)
    else:
        print("Attempting to create UserData folder but "+USERDATA+username+" already exists")
    
    dataLoc=USERDATA+username+"/"+"userData.p"
    
    if os.path.exists(dataLoc):  
        print("Attempting to create UserData but "+dataLoc+" already exists")
    else:
        data=open(dataLoc,"w")
        for i in range(100):
            data.write(str(random()))
        data.close()  
        
    return dataLoc      

def recordLogin(username):
    config.read(USERLIST+username+".txt")
    if config.has_option(username,"login"):
        loginRecord=config.get(username,"login")
        loginRecord+=":"+str(datetime.date.today())
        config.set(username,"login",loginRecord)
        
        userFile=open(USERLIST+username+".txt","w")    
        config.write(userFile)
        userFile.close()
    
    clearConfig()
    
def retrieveAccount(username):
    dataLoc=USERDATA+username+"/"+"userData.p"
    if not os.path.exists(dataLoc):  
        print("No user data exists for ",username)          
        return None
    else:
        print("Retrieving user data for "+username)   
        return dataLoc 
    
       
def formatUsername(credentials):
    username=credentials["First Name"]+credentials["Last Name"]
    #removes whitespace
    username=username.replace(" ","")
    #lowercase all letters to make case insensitive
    username=username.lower()
    return username

def createAccount():
    credentials= ask(["First Name","Last Name","Gender","Birthday (month/day/year)"],
            "Please provide the following information\nto create a new account")    
    username=formatUsername(credentials)
    
    if os.path.exists(USERLIST+username+".txt"):
        
        print("Account for "+username+" already exists.")
        recordLogin(username)
        dataLoc=retrieveAccount(username)
        return dataLoc
    else:
        config.add_section(username)
        for key,value in credentials.items():
            config.set(username,key,value)
        #add today's login
        config.set(username,"login",str(datetime.date.today()))
        
        userFile=open(USERLIST+username+".txt","w")
        config.write(userFile)
        userFile.close()
        
        #effectively resets the parser
        clearConfig()
        
        dataLoc=createUserData(username)
            
        print("Returning user data")
        return dataLoc
    
def login():    
    credentials=ask(["First Name","Last Name"],"Enter you're credentials.\nNote, case sensitive.")
    username=formatUsername(credentials)
    
    if os.path.exists(USERLIST+username+".txt"):
        recordLogin(username)
        userData=retrieveAccount(username)
        return userData
    else:
        prompt("failedLogin",credentials)
    

def prompt(message=None,credentials={"foo":"bar"}):
    if message=="failedLogin":
        response=askQuestion("Error, no account found for "+credentials["First Name"]+" "+credentials["Last Name"]+"\n",["Try Again - Login","Create New Account"])
    else:    
        #only checking the existence of UserList.txt directory once
        if not os.path.exists(USERLIST):
            print("Error. No "+USERLIST+" directory detected.")
        response=askQuestion("Welcome to the Laramie Robotics Club user portal. \nSelect one of the following options.",["Login","Create New Account"])
    
    if response=="Login" or response=="Try Again - Login":       
        userData=login()
    else:   
        userData=createAccount()
    
    return userData
        

try:           
    clearConfig()
    userDataPath=prompt()
    print(userData)
except:
    print("Warning, sudden exit from login.")
    
    
    
    
    