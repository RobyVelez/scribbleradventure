from Graphics import *
import Cairo


class MasterFrame(Frame):
    def __init__(self, x, y):
        Frame.__init__(x, y)
        self.levelWindow = None
        self.dirty = True
        
    def updateFromPhysics(self):
        for shape in self.shapes:
            shape.updateFromPhysics()
            
    def render(self, context):
        try:
            context.NewPath()
            context.MoveTo(self.x, self.y)
            context.LineTo(self.x, self.y + self.levelWindow.viewH + self.levelWindow.panel2H)
            context.LineTo(self.x + self.levelWindow.viewW + self.levelWindow.panel3W, self.y + self.levelWindow.viewH + self.levelWindow.panel2H)
            context.LineTo(self.x + self.levelWindow.viewW + self.levelWindow.panel3W, self.y)
            context.Clip()
            Frame.render(self, context)
        except:
            print("Error with render in Master Frame")

class WorldFrame(Frame):
    def __init__(self, x, y):
        Frame.__init__(x, y)
        self.levelWindow = None
        
    def updateFromPhysics(self):
        for shape in self.shapes:
            shape.updateFromPhysics()
            
            
class LRC_Background(Shape):
    def __init__(self, filename):
        self.background = Cairo.ImageSurface(filename)
    def render(self, context):
        context.SetSourceSurface(self.background, 0, 0)
        context.Paint()
           
