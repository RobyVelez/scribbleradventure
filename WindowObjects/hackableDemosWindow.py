from os.path import realpath, dirname
import sys
sys.path.append(dirname(realpath(__file__)) + "/..")
from PortalWindow import *

PORTAL_PATH=dirname(dirname(realpath(__file__)))

class HackableDemosWindow(MenuWindow):
    def __init__(self, gameData=None,parent=None):
        MenuWindow.__init__(self, parent, "HackableDemos")
        self.parent=parent
        if gameData==None:
            self.gameData={}
            self.gameData["HackableDemos"]={}
        else:
            self.gameData=gameData
            
        self.hallMax=5
        
        self.mainPanel.addText((25,50),"Demos files will open in a new script.\nHit the green circle to run them.",fontSize=30,fontColor=Color("black"),xJustification="left")
        
        # Add buttons
        self.addHeader("Simple Demos")
        self.addButton("Bouncing Ball Animation", "Simple examples of how to create an animation.")
        self.addButton("Click Box Color Change", "Simple example of how to use mouse actions.")
        self.addButton("Key Move Shape", "Simple example of how to use key presses to move an object.")
        self.addButton("Top Down Tank", "Use keys to move and object, shoot bullets, and destroy other objects.")
        self.addButton("Endless Platformer", "Jump endlessly to survive!")
        
                                    
    def launchActivity(self, buttonName=None):
        position=self.mainWindow.win.GetPosition()               
                
        if buttonName == "Bouncing Ball Animation":
            
            #Stuff that has to do with gameData and recoring information
            if "BouncingBallAnimation" not in self.gameData["HackableDemos"]:
                self.gameData["HackableDemos"]["BouncingBallAnimation"]={}     
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["HackableDemos"]["BouncingBallAnimation"])   
                
            self.moveFileAndSetCurrent("simpleAnimations.py")            
            calico.Open("simpleAnimations.py")
            #launcher=__import__("simpleAnimations", globals(), locals(), ['*'])
        
        elif buttonName == "Click Box Color Change":
                    
            #Stuff that has to do with gameData and recoring information
            if "ClickColorChange" not in self.gameData["HackableDemos"]:
                self.gameData["HackableDemos"]["ClickColorChange"]={}     
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["HackableDemos"]["ClickColorChange"])   
                
            self.moveFileAndSetCurrent("clickDemo1.py")            
            calico.Open("clickDemo1.py")            

        elif buttonName == "Key Move Shape":
                    
            #Stuff that has to do with gameData and recoring information
            if "KeyMoveShape" not in self.gameData["HackableDemos"]:
                self.gameData["HackableDemos"]["KeyMoveShape"]={}     
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["HackableDemos"]["KeyMoveShape"])   
                
            self.moveFileAndSetCurrent("keyDemo1.py")            
            calico.Open("keyDemo1.py")

        elif buttonName == "Top Down Tank":
                    
            #Stuff that has to do with gameData and recoring information
            if "TopDownTank" not in self.gameData["HackableDemos"]:
                self.gameData["HackableDemos"]["TopDownTank"]={}     
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["HackableDemos"]["TopDownTank"])   
                
            self.moveFileAndSetCurrent("tank.py")            
            calico.Open("tank.py")    

        elif buttonName == "Endless Platformer":
                    
            #Stuff that has to do with gameData and recoring information
            if "EndlessPlatformer" not in self.gameData["HackableDemos"]:
                self.gameData["HackableDemos"]["EndlessPlatformer"]={}     
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["HackableDemos"]["EndlessPlatformer"])   
                
            self.moveFileAndSetCurrent("endlessPlatformer.py")            
            calico.Open("endlessPlatformer.py")  

    def moveFileAndSetCurrent(self,filename):
        #copy file to user's workspace
        if self.parent!=None:
            if self.parent.workspaceLoc!=None and os.path.exists(self.parent.workspaceLoc):
                if not os.path.exists(self.parent.workspaceLoc+"/"+filename):
                    shutil.copy(PORTAL_PATH+"/HackableDemos/"+filename,self.parent.workspaceLoc+"/"+filename)
                System.IO.Directory.SetCurrentDirectory(self.parent.workspaceLoc)
                #print(System.IO.Directory.GetCurrentDirectory())
            else:
                print("No user workspace. Using local copy ofr file.")
                System.IO.Directory.SetCurrentDirectory(PORTAL_PATH+"/HackableDemos/")
        else:
            System.IO.Directory.SetCurrentDirectory(PORTAL_PATH+"/HackableDemos/")
        
                           

hackableDemosWindow=None        
def start(gameData=None,parent=None):    
    global hackableDemosWindow
    hackableDemosWindow=HackableDemosWindow(gameData,parent)
    return hackableDemosWindow
        
if __name__ == "__main__":
    start()
    

       
