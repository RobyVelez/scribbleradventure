from os.path import realpath, dirname
import sys
sys.path.append(dirname(realpath(__file__)) + "/..")
from PortalWindow import *

class GraphicsWindow(MenuWindow):
    def __init__(self, gameData=None,parent=None):
        MenuWindow.__init__(self, parent, "Graphics")
        self.parent=parent
        if gameData==None:
            self.gameData={}
            self.gameData["Graphics"]={}
        else:
            self.gameData=gameData
        # Add buttons
        self.addHeader("Winter scene")
        self.addButton("Winter scene", "Create a winter scene through programming!")
        
        self.addHeader("Space Ship Simulator")
        self.addButton("Space Ship Simulator (part 1)", "Program your own space ship simulator!")
        self.addButton("Space Ship Simulator (part 2)", "Program your own space ship simulator!")
        #self.addButton("Space Ship Simulator (part 3)", "Program your own space ship simulator!")   


    def launchActivity(self, buttonName=None):
        print(buttonName)
        if buttonName == "Space Ship Simulator (part 1)":
            if "SpaceShipSimulator1" not in self.gameData["Graphics"]:
                self.gameData["Graphics"]["SpaceShipSimulator1"]={}
            if self.parent!=None:
                self.parent.recordClicks(self.gameData["Graphics"]["SpaceShipSimulator1"])
            self.openPDF(HOME_DIR+"/Graphics/SpaceAdventure1.pdf")
            
        elif buttonName == "Space Ship Simulator (part 2)":
            if "SpaceShipSimulator2" not in self.gameData["Graphics"]:
                self.gameData["Graphics"]["SpaceShipSimulator2"]={}
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["Graphics"]["SpaceShipSimulator2"])            
            self.openPDF(HOME_DIR+"/Graphics/SpaceAdventure2.pdf")
        #elif buttonName == "Space Ship Simulator (part 3)":
        #    self.openPDF(HOME_DIR+"/Graphics/Graphics01.pdf")
        
        elif buttonName == "Winter scene":
            if "WinterScene" not in self.gameData["Graphics"]:
                self.gameData["Graphics"]["WinterScene"]={}
            if self.parent!=None:            
                self.parent.recordClicks(self.gameData["Graphics"]["WinterScene"])            
            
            self.openPDF(HOME_DIR+"/Graphics/Graphics01.pdf")

graphicsWindow=None        
def start(gameData=None,parent=None):    
    global graphicsWindow
    graphicsWindow=GraphicsWindow(gameData,parent)
    return graphicsWindow
        
if __name__ == "__main__":
    start()
       