from Myro import *
from Graphics import *
import os 
import sys
from functools import partial ### JH: partial added to prevent multi-load crash
import pickle #for saving data
import importlib
import Gtk # JH: Provides access to the Gtk event handler (since there is no level thread here)
import GLib # JH: Apparently Gtk is deprecated
import Myro #Adds Myro to global scope
import time as t

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/GameObjects")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/WindowObjects")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonImages")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonFunctions") #soon to be depreceated
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Intro")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Gauntlet")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/OpenLoopControl")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/ClosedLoopControl")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Labyrinth")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/Chamber")
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

IMAGE_PATH=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))+"/CommonImages/"   

if __name__=="__main__":
    # JH: This allows levels to read all user created functions and variables.
    __builtins__["userGlobals"] = globals()
    #make sure the shell doesn't reset
    calico.config.SetValue('python-language','reset-shell-on-run',False)
    #calico.config.SetValue('python-language','reset-show-on-run',False)
    #On the windows machines in 4045 this is needed because those machines
    #lose the calico global variable
    import setCalico
    setCalico.initCalico(calico)
    from version import VER
    #clears LevelObject b/c reset shell doesn't clear the variable
    clearLevelObject()

    
                                        
from windowObjects import *
from userFunctions import *
from makeShapeFunctions import *

BUTTON_SPACE = 50
SPECIAL_CODE = "You didn't think it would that easy, did you?"

class ScribblerAdventure(MenuWindow):
    stages=["Intro","Gauntlet","Open_Loop_Control","Labyrinth","Chamber"]

    def __init__(self,gameData=None,parent=None):    
        MenuWindow.__init__(self, parent, "Scribbler Adventure", "Scribbler Adventure")
        
        
        
        if gameData==None:
            #make a new default GameData object
            #self.gameData=GameData(VER)
            
            #potentially we could get rid of gameData if the ScribblerAdventure window is launched standalone
            #one reason to not get rid of it is so that much of code acts the same as if run through portal.RV
            self.gameData={}
            self.gameData["ScribblerAdventure"]={}
            self.gameData["ScribblerAdventure"]["logins"]=[]            
            self.gameData["ScribblerAdventure"]["highestStage"]=0
            self.gameData["ScribblerAdventure"]["highestLevel"]=0      
            #mostly holds login/clicks for stages
            self.gameData["ScribblerAdventure"]["stageData"]={}
            #holds clicks for levels as well as game info like [score,wins,bestTime,loses,restarts]
            self.gameData["ScribblerAdventure"]["levelData"]={}
                  
        else:
            self.gameData=gameData  
        
        self.parent=parent
              
        self.currentStage=self.gameData["ScribblerAdventure"]["highestStage"]
        self.currentLevel=self.gameData["ScribblerAdventure"]["highestLevel"]
        
        self.stages=["Intro","Gauntlet","Open_Loop_Control","Labyrinth","Chamber","Closed_Loop_Control"]
        
        self.levels=[
            ["intro1","intro2","intro3","intro4","intro5","intro6"],
            ["gauntlet1","gauntlet2","gauntlet3","gauntlet4","gauntlet5","gauntlet6","gauntlet7","gauntlet8","gauntlet9","gauntlet10","gauntlet11","gauntlet12"],
            ["loopofdoom","minosmaze"],
            ["labyrinth1","labyrinth2","labyrinth3","labyrinth4","labyrinth5","labyrinth6", "labyrinth7", "labyrinth8", "labyrinth9", "labyrinth10", "labyrinth11"],
            ["chamber1","chamber2","chamber3","chamber4","chamber5","chamber6","chamber7","chamber8","chamber9","chamber10","chamber11","chamber12"],
            ["lineFollow","navigateMaze","stopAtWallDist","stopAtWallObs","wallFollow1","wallFollow2","clapAndDance","shyPartyRobot"] ]
                                
        
        
        #create windows and panels
##         self.mainWindow=WindowLRC("ScribblerAdventure",800,700)
##         self.mainWindow.parent=parent
##         self.optionPanel=self.mainWindow.addPanel("Options",600,0,200,800,Color("gray"))
##         self.descriptionPanel=self.mainWindow.addPanel("Description:",0,600,600,100,Color("lightblue"))
##         self.mainPanel=self.mainWindow.addPanel(" ",0,0,600,600,Color("white"),) 
##         if self.mainWindow.parent != None:
##             self.mainPanel.addButton(0,550,125,50,self.returnToPortalAction,"Back To Portal")        

        scribAdAvatar = Picture(IMAGE_PATH + "/ScribLogo.png")
        scribAdAvatar.moveTo(0,-240)
        scribAdAvatar.border = 0
        scribAdAvatar.scale(0.4)
        scribAdAvatar.draw(self.mainPanel)

        # Add mouse-over description to buttons
##         self.buttonDict = {}
##         self.currentDescription = None
##         self.mainWindow.win.onMouseMovement(self.onMouseMove)
        self.leftColumnY = 150
        self.columsX = [350, 325]
        
        # Stage cleared stuff
        self.shouldPlayVictory=False
        self.playGameVictory=False
        self.timeoutTimout = 120
        self.textIndex = 0
        self.victoryObjects = []
        self.textPool = []
        self.victoryText=makeText(" ", 36, xjust="center", pos=(0,-200))
        self.victoryText.draw(self.mainPanel)
##         
##         self.addVictoryText(" ")
        self.scribby=makePic(IMAGE_PATH + "fakeScribbler.png")
##         self.scribby.border=0
        self.positionScribby()
        self.scribby.draw(self.mainPanel)
        self.checkmarks = []
        self.levelButtons = []
        
        for s in self.stages:
            self.addButton(s,s)
        self.update()
        
        self.addKeyPress(self.keyPress)
          
##     def setParent(self,parent):
##         self.parent=parent
##                         
##     def returnToPortalAction(self,buttonName=None):  
##         self.mainWindow.quit()
         
      
    #########################
    # Level control methods #
    ######################### 
    def launchLevel(self,position=None):
        """
        Launches the current level in the current stage.
        
        Args:
            position: Tuple indicating the position of the level window.
        """
        stageName=self.stages[self.currentStage] 
        levelName=self.levels[self.currentStage][self.currentLevel]
        
        #Record stage engagement
        if stageName not in self.gameData["ScribblerAdventure"]["stageData"]:
           self.gameData["ScribblerAdventure"]["stageData"][stageName]={}
           self.gameData["ScribblerAdventure"]["stageData"][stageName]["logins"]=[]
           if self.parent!=None:
                self.parent.recordClicks(self.gameData["ScribblerAdventure"]["stageData"][stageName])
            
        #Record level engagement
        if levelName not in self.gameData["ScribblerAdventure"]["levelData"]:
           self.gameData["ScribblerAdventure"]["levelData"][levelName]={}
           self.gameData["ScribblerAdventure"]["levelData"][levelName]["logins"]=[]
           self.gameData["ScribblerAdventure"]["levelData"][levelName]["stats"]=[]            
           if self.parent!=None:
                self.parent.recordClicks(self.gameData["ScribblerAdventure"]["levelData"][levelName])           
             
        
        self.hideWindow()            
        self.launcher=__import__(self.levels[self.currentStage][self.currentLevel], globals(), locals(), ['*'])

        
        
        ## JH: THIS IS A TEST
        ##Leave for now, Spring 2017. RV
        #launcher=__import__(self.levels[self.currentStage][self.currentLevel], globals(), locals(), [''])
        #launcher = importlib.import_module(self.levels[self.currentStage][self.currentLevel])
        #globals().update(launcher.__dict__)
        #self.child=startLevel(self.gameData,self)
        self.child=self.launcher.startLevel(self.gameData,self)
        
        if position!=None:
            self.child.sim.window.Move(position[0],position[1]+20)
        #self.child.mainWindow.Move(position[0],position[1]+21
        
        
    def launchNextLevel(self,position=None):
        """
        Launches the next level, if it exists.
        
        Will close the current level window if the end of the stage,
        or the end of the game is reached.
        
        Args:
            position: Tuple indicating the position of the level window.
        """
        nextLevel=self.nextLevelExists()
        if nextLevel=="level exists": #self.nextLevelExists():
            self.currentLevel += 1
            self.launchLevel()
        elif nextLevel=="end of stage":
            print("End of this stage")
            self.child.quitGame()
        elif nextLevel=="game complete":
            print("Game completed.")
            self.child.quitGame()
        elif nextLevel=="level locked":
            print("Next level locked.")
              
              
    def launchPrevLevel(self,position=None):
        """
        Launches the previous level, if it exists.
        
        Args:
            position: Tuple indicating the position of the level window.
        """
        if self.prevLevelExists():
            self.currentLevel -= 1
            self.launchLevel(position)    
        else:
            print("Error. No previous level!")


    def prevLevelExists(self):
        """
        Check whether a previous level exists.
        
        Return:
            True if a previous level exists, false otherwise.
        """
        if self.currentLevel==0:
            return False
        else:
            return True
            
            
    def nextLevelExists(self):
        """
        Checks whether a next level exists.
        
        Return:
            Returns a string indicating whether the next level exists
            or not. The possible strings are:
            - "level exists": A next level exists and is in the same stage.
            - "end of stage": A next level exists, but it is in the next stage.
            - "level locked": A next level exists, but the player has not unlocked it yet.
            - "game complete": No next level exists.
        """
        if self.currentStage < self.gameData["ScribblerAdventure"]["highestStage"]:#.highestStage:
            if self.currentLevel < len(self.levels[self.currentStage])-1:
                return "level exists" #1 #next level exists 
            else:
                return "end of stage" #0 #no more levels in this stage
        else:
            if self.currentLevel < self.gameData["ScribblerAdventure"]["highestLevel"]:#.highestLevel:
                return "level exists" #1 #next level exists
            elif (self.currentLevel == len(self.stages[-1])-1) and (self.currentStage == len(self.stages)-1):
                return "game complete"
            else:
                return "level locked" #-1 #next level not unlocked
    

    def levelSolved(self):
        """
        Function called by the levelWindow whenever a level is solved.
        """
        if self.currentStage < self.gameData["ScribblerAdventure"]["highestStage"]:#.highestStage:
            #current stage has already been beaten. nothing to unlock
            pass
        elif self.currentLevel < self.gameData["ScribblerAdventure"]["highestLevel"]:#.highestLevel:
            #didn't beat the furthest level. nothing to unlock
            pass
        elif self.currentLevel == self.gameData["ScribblerAdventure"]["highestLevel"]:#.highestLevel:
            if self.gameData["ScribblerAdventure"]["highestLevel"] == len(self.levels[self.currentStage])-1:
                if self.gameData["ScribblerAdventure"]["highestStage"] == len(self.stages) - 1:
                    # You have beaten the entire game!
                    self.playGameVictory = True
                    self.gameData["ScribblerAdventure"]["highestStage"]=min(self.currentStage+1,len(self.stages))
                else:
                    # You have beaten the current stage
                    self.gameData["ScribblerAdventure"]["highestStage"]=min(self.currentStage+1,len(self.stages)-1)
                    self.gameData["ScribblerAdventure"]["highestLevel"]=0
                    self.shouldPlayVictory=True
            else:
                # You have beaten the current level
                self.gameData["ScribblerAdventure"]["highestLevel"]=self.currentLevel+1
        else:
            print("Error is LevelSolved funtion. Should not reach here!")    
                            
         
    def saveGameData(self,stats):
        """
        Saves the current state of the game.
        """
        levelName=self.levels[self.currentStage][self.currentLevel]
        try:
            self.gameData["ScribblerAdventure"]["levelData"][levelName]["stats"].append(stats)
            
            if self.parent != None:
                self.parent.saveGameData()
            else:
                pass
        except:
            print("ERROR!!! Trying to save stats for level ",levelName)
            print("Unable to access data for ",levelName," in gameData")
                      
              
    def setHighestStage(self,v):
        """
        Sets the highest stage achieved by the current user.
        
        Args:
            v: Integer indicating the highest stage.
        """
        if v<0:
            self.gameData["ScribblerAdventure"]["highestStage"]=0
        elif v<len(self.stages):
            self.gameData["ScribblerAdventure"]["highestStage"]=v
        
        self.gameData["ScribblerAdventure"]["highestLevel"]=0                
        
    
    def setHighestLevel(self,v):
        """
        Sets the highest level achieved by the current user.
        
        Args:
            v: Integer indicating the highest level.
        """
        if v<0:
            self.gameData["ScribblerAdventure"]["highestLevel"]=0    
        elif v<len(self.levels[self.currentStage]):
            self.gameData["ScribblerAdventure"]["highestLevel"]=v
        
##     def addVictoryText(self, text):
##         text=Text((0,-200), text, color=Color("black"))
##         text.yJustification = "bottom" 
##         text.xJustification = "center"
##         text.fontFace = "Helvetica Neue Light"
##         text.fontSize = 36
##         text.draw(self.mainPanel)
##         text.visible = True
##         return text
        
    
    def confirmDebugCommand(self,success,message):
        if success:
            print("Success at "+message)
        else:
            print("Failure at "+message)

            
    def debugConsole(self):
        """
        Launches and an input dialog box where you can enter commands that can be used
        to debug and quickly move around the levels
        """
        cmd=ask("Enter debug command.")
        if cmd=="incrementHighestStage":
            self.setHighestStage(self.gameData["ScribblerAdventure"]["highestStage"]+1)
            self.confirmDebugCommand(True,"Incrementing highest stage.")            
        elif cmd=="decrementHighestStage":
            self.setHighestStage(self.gameData["ScribblerAdventure"]["highestStage"]-1)   
            self.confirmDebugCommand(True,"Decrementing highest stage.")                                 
        elif cmd=="incrementHighestLevel":
            self.setHighestLevel(self.gameData["ScribblerAdventure"]["highestLevel"]+1)
            self.confirmDebugCommand(True,"Incrementing highest level.")            
        elif cmd=="decrementHighestLevel":
            self.setHighestLevel(self.gameData["ScribblerAdventure"]["highestLevel"]-1)        
            self.confirmDebugCommand(True,"Decrement highest level.")
        elif cmd.startswith("setHighestStage"):
            try:
                highestStage = int(cmd.split()[1])
                self.setHighestStage(highestStage)        
                self.confirmDebugCommand(True,"Set highest stage to: " + str(highestStage))
                self.update()
            except:
                self.confirmDebugCommand(False,"Setting highest stage")
        elif cmd.startswith("setHighestLevel"):
            try:
                highestLevel = int(cmd.split()[1])
                self.setHighestLevel(highestLevel)        
                self.confirmDebugCommand(True,"Set highest level to: " + str(highestLevel))
                self.update()
            except:
                self.confirmDebugCommand(False,"Setting highest stage")
            
    
        #TODO: Hey Joost, you'll have to check to see if I did all this correct or if there
        #are any other commands we want to add. The purpose of these debug commands should be
        #to test the end stage or end game conditions b/c we don't see them all that often
        #and they can be hard to trigger since right now you have to actually beat the whole
        #game to trigger them
        if cmd=="playStageVictory":
            self.playVictory()
            self.confirmDebugCommand(True,"Playing stage victory.")            
        elif cmd=="playGameVictory":
            self._playGameVictory()
            self.confirmDebugCommand(True,"Playing game victory.")            
        elif cmd=="playAfterShow":
            self.afterShow()
            self.confirmDebugCommand(True,"Playing aftershow.")            
        elif cmd=="updateWindow":
            self.update()
            self.confirmDebugCommand(True,"Updating window.")            
                
                
    #####################
    # Animation methods #
    #####################   
    def positionScribby(self):
        """
        Positions the Scriby avatar next to the highest accessible stage.
        """
        self.scribby.moveTo(self.columsX[0] - 330, -130 + self.gameData["ScribblerAdventure"]["highestStage"]*BUTTON_SPACE)
        

    def playVictory(self):
        """
        Plays the "STAGED CLEARED" animation.
        """
        self.shouldPlayVictory=False
        self.victoryText.text=" "
        self.victoryText.visible=True
        final="STAGE CLEARED!"
        current=""
        dy = BUTTON_SPACE/len(final)
        for c in final:
            current+=c
            self.victoryText.text=current
            self.scribby.move(0, dy)
            #wait(0.1)
            sleep(0.04)
            #self.mainWindow.win.GdkWindow.ProcessUpdates(True)
            self.mainWindow.win.refresh()
            self.mainWindow.win.updateNow()
            
        self.positionScribby()
        
        
    def _updateVictory(self):
        """
        Callback that executes one step of playing the credits.
        """
        finished=True
        for text in self.textPool:
            text.move(0,-2)
            if text.getY() < 0:
                if self.textIndex >= len(self.text):
                    continue
                text.move(0,len(self.textPool)*38)
                text.text = self.text[self.textIndex]
                self.textIndex += 1
            else:
                finished = False
        self.mainWindow.win.refresh()
        if finished:
            for shape in self.victoryObjects:
                shape.undraw()
            return False
        else:
            return True

        
    def _playGameVictory(self):
        """
        Plays the credits.
        """
        self.playGameVictory=False
        self.initQueue()
        self.r=Rectangle((0,0), (1024, 1024), color=Color("white"))
        self.mainWindow.win.draw(self.r)
        self.victoryObjects.append(self.r)
        self.text = ["Thank you for playing:"]
        self.text += ["Scribbler Adventure\n"]
        self.text += [" "]
        self.text += [" "]
        self.text += ["By:"]
        self.text += ["The Laramie Robotics Club"]
        self.text += [" "]
        self.text += [" "]
        self.text += ["Lead programmer:"]
        self.text += ["Roby Velez"]
        self.text += [" "]
        self.text += ["Other programmer:"]
        self.text += ["Joost Huizinga"]
        self.text += [" "]
        self.text += ["Advisor (is that even a title in game development?):"]
        self.text += ["Jeff Clune"]
        self.text += [" "]
        self.text += ["Art direction:"]
        self.text += ["We're curious as well"]
        self.text += [" "]
        self.text += ["Story:"]
        self.text += ["Roby Velez"]
        self.text += ["Joost Huizinga"]
        self.text += [" "]
        self.text += ["That code you are looking for:"]
        self.text += [SPECIAL_CODE]

        self.textPool = []
        self.textIndex = 0
        for i in xrange(20):
            _text = Text((400,700 + 38*i), self.text[self.textIndex], color=Color("black"))
            _text.yJustification = "bottom" 
            _text.xJustification = "center"
            _text.fontFace = "Helvetica Neue Light"
            _text.fontSize = 36
            self.textPool.append(_text)
            self.mainWindow.win.draw(_text)
            self.victoryObjects.append(_text)
            self.textIndex += 1
        GLib.Timeout.Add(33, self._updateVictory)
        
        
    def initQueue(self):
        """
        Initializes some data structures for showing the credits.
        """
        eventIds=[162, 159, 148, 146, 152, 144, 155, 174, 146, 158, 147, 148]
        eventQueue=""
        for event in eventIds: eventQueue += chr(event - (self.gameData["ScribblerAdventure"]["highestStage"]*12 + self.gameData["ScribblerAdventure"]["highestLevel"]))
        creditIds=[145, 187, 176, 178, 186, 111, 156, 180, 194, 176]
        creditQueue=""
        for event in creditIds: creditQueue += chr(event - (self.gameData["ScribblerAdventure"]["highestStage"]*12 + self.gameData["ScribblerAdventure"]["highestLevel"]))
        globals()[eventQueue] = creditQueue
        
        
    ####################
    # Callback methods #
    #################### 
    
    def keyPress(self,o,e):
        if str(e.key)=="F1":
            self.debugConsole()    
              
                          
    def afterShow(self):
        """
        On return to this window set gameLevel (getLevelObject()) to None,
        and check if any victory animations should be played.
        """
        Myro.gameLevel=None
        if self.shouldPlayVictory:
            self.playVictory()
        elif self.playGameVictory:
            self._playGameVictory()
        else:
            self.victoryText.visible=False
            self.positionScribby()


    def launchActivity(self,buttonName=None):
        """
        Launches a stage based on the provided button name.
        
        Called whenever one of the stage buttons is clicked.
        
        Args:
            buttonName: The name of the button being clicked.
        """
        
        if self.gameData["ScribblerAdventure"]["highestStage"]>=self.stages.index(buttonName): 
            self.currentStage=self.stages.index(buttonName)          
            if self.gameData["ScribblerAdventure"]["highestStage"]==self.stages.index(buttonName):                
                self.currentLevel=self.gameData["ScribblerAdventure"]["highestLevel"]
            else:
                self.currentLevel=0
            
            self.launchLevel(self.mainWindow.GetPosition())  


    def update(self):
        """
        Enables or disables buttons based on the current state
        of the level window.
        """
        i=0
        for button in self.levelButtons:
            #print("index:", i, "stage:", self.gameData.highestStage)
            if i < self.gameData["ScribblerAdventure"]["highestStage"]:
                self.checkmarks[i].visible=True
                button.metaText.visible=False
            else:
                self.checkmarks[i].visible=False
                button.metaText.visible=True
            if i == self.gameData["ScribblerAdventure"]["highestStage"]:
                button.metaText.text = str(self.gameData["ScribblerAdventure"]["highestLevel"]) + "/" + str(len(self.levels[i]))
            if i > self.gameData["ScribblerAdventure"]["highestStage"]:
                #print("Button disabled")
                button.disable()
                button.metaText.text = "0/" + str(len(self.levels[i]))
            else:
                #print("Button enabled")
                button.enable()
            i+=1


    ########################
    # Construction methods #
    ########################
    def addButton(self, text, desc, column=0):
        """
        Adds a button to the window.
        
        Overwrites MenuWindow.addButton so it can add a checkmark and
        meta text (current level progress) to each button.
        
        Args:
            text: String indicating the text to be drawn to the button.
            desc: String indicating the mouse-over text to be shown.
            column: In which column to place the button.
        """
        button = self.mainPanel.addButton(self.columsX[column],self.leftColumnY,250,40,self.launchActivity,text)
        button.metaText.text = "4/6"
        button.mouseOverText = desc
        self.levelButtons.append(button)
##         desc = self.addDescription(text, desc)
##         self.buttonDict[text] = ButtonData(button, desc)
        mark=Picture(IMAGE_PATH + "checkmark_small.png")
        mark.border=0
        mark.visible=False
        mark.moveTo(self.columsX[column]-250,-2)
        mark.draw(button)
        self.checkmarks.append(mark)
        
        self.leftColumnY += BUTTON_SPACE
    
##     def addHeader(self, text, column=0):
##         #add buttons to activity panel
##             
##         createPanelHeader(self.mainPanel, self.columsX[column], self.leftColumnY+40, text)
##         self.leftColumnY += 50  
        
    
##     def addDescription(self, tag, text):
##         text=Text((-295,-10), text, color=Color("black"))
##         text.yJustification = "bottom" 
##         text.xJustification = "left"
##         text.fontFace = "Helvetica Neue Light"
##         text.fontSize = 16
##         text.draw(self.descriptionPanel)
##         text.visible = False
##         return text


##     def onMouseMove(self, o, e):
##         for button in self.mainPanel.button:
##             if button.hit(e.x, e.y):
##                 if button.tag not in self.buttonDict: return
##                 buttonData = self.buttonDict[button.tag]
##                 if buttonData is None: return
##                 if buttonData.desc is None: return
##                 if buttonData.ptr is None: return
##                 if buttonData == self.currentDescription: return
##                 if self.currentDescription is not None:
##                     self.currentDescription.desc.visible = False
##                     self.currentDescription.ptr.unhighlight()
##                 self.currentDescription = buttonData
##                 self.currentDescription.ptr.highlight()
##                 self.currentDescription.desc.visible = True
##                 self.mainWindow.win.QueueDraw()
##                 return
##         if self.currentDescription is not None:
##             self.currentDescription.desc.visible = False
##             self.currentDescription.ptr.unhighlight()
##             self.currentDescription = None
##             self.mainWindow.win.QueueDraw()



                      
scribAdj=None        
def start(gameData=None,parent=None):    
    global scribAdj
    scribAdj=ScribblerAdventure(gameData,parent)
    return scribAdj

if __name__ == "__main__":
    start()
    
 
       